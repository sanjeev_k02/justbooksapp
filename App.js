/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  TextInput,
  Linking, 
  Platform,
  Text
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import Icon from 'react-native-vector-icons/FontAwesome5';
import VirtualIcon from 'react-native-vector-icons/Ionicons';
import Dialog from 'react-native-dialog';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Menu from "./app/components/Menu/Menu";
import Login from "./app/components/Login/Login";
import ChatBot from "./app/components/ChatBot/ChatBot";
import VirtualView from "./app/components/VirtualView/VirtualView";
import Widget from "./app/components/Widget/Widget";
import Search from "./app/components/Search/Search";
import SearchBar from "./app/components/SearchBar/SearchBar";
import ChatBotBar from "./app/components/ChatBotBar/ChatBotBar";
import HeaderBar from "./app/components/HeaderBar/HeaderBar";

import SplashScreen from './app/screens/Splash/Splash';
import SplashScreen1 from './app/screens/Splash/Splash1';
import SplashScreen2 from './app/screens/Splash/Splash2';
import IntroSliderScreen from './app/screens/Splash/IntroSlider';
import HomeScreen from './app/screens/Home/Home';
import CategoryFilterScreen from './app/screens/CategoryFilter/CategoryFilter';
import CategoryResultScreen from './app/screens/CategoryResult/CategoryResult';
import ViewVideoReviewScreen from './app/screens/ViewVideoReview/ViewVideoReview';
import VirtualBranchSearch from './app/screens/VirtualLibrary/BranchSearch';
import BranchResults from './app/screens/VirtualLibrary/BranchResults';
import VirtualLibraryResults from './app/screens/VirtualLibrary/VirtualLibraryResults';
import BookDetailsScreen from './app/screens/BookDetails/BookDetails';
import AuthorDetailsScreen from './app/screens/AuthorDetails/AuthorDetails';
import AllBooksScreen from './app/screens/AllBooks/AllBooks';
import AllAuthorsScreen from './app/screens/AllAuthors/AllAuthors';
import NotificationScreen from './app/screens/Notification/Notification';
import WishlistScreen from './app/screens/Wishlist/Wishlist';
import RecommendationsScreen from './app/screens/Recommendations/Recommendations';
import SearchScreen from './app/screens/Search/Search';
import ChatBotScreen from './app/screens/ChatBot/ChatBot';
import SearchResultsScreen from './app/screens/SearchResults/SearchResults';
import VoiceSearchScreen from './app/screens/VoiceSearch/VoiceSearch';
import ProfileScreen from './app/screens/Profile/Profile';
import ShelfScreen from './app/screens/Shelf/Shelf';
import ProfileInfoScreen from './app/screens/ProfileInfo/ProfileInfo';
import SubscriptionScreen from './app/screens/Subscription/Subscription';
import TakeSubscriptionScreen from './app/screens/TakeSubscription/TakeSubscription';
import MemberScreen from './app/screens/Member/Member';
import AddMemberScreen from './app/screens/AddMember/AddMember';
import EditMemberScreen from './app/screens/EditMember/EditMember';
import VideoReviewScreen from './app/screens/VideoReview/VideoReview';
import AddVideoReviewScreen from './app/screens/AddVideoReview/AddVideoReview';
import EditVideoReviewScreen from './app/screens/EditVideoReview/EditVideoReview';
import FAQsScreen from './app/screens/FAQs/FAQs';
import TermsConditionScreen from './app/screens/TermsCondition/TermsCondition';
import SignInScreen from './app/screens/SignIn/SignIn';
import OtpSignInScreen from './app/screens/OtpSignIn/OtpSignIn';
import ForgotPasswordScreen from './app/screens/ForgotPassword/ForgotPassword';
import GetOtpSignInScreen from './app/screens/GetOtpSignIn/GetOtpSignIn';
import SignUpScreen from './app/screens/SignUp/SignUp';
import SignUpInfoScreen from './app/screens/SignUpInfo/SignUpInfo';
import InternetConnectivityScreen from './app/screens/InternetConnectivity/InternetConnectivity';
import ShowWidgetScreen from './app/screens/Widget/ShowWidget/ShowWidget';
import AddWidgetScreen from './app/screens/Widget/AddWidget/AddWidget';
import EditWidgetScreen from './app/screens/Widget/EditWidget/EditWidget';
import FlashMessage from "react-native-flash-message";
const Stack = createStackNavigator();

function App() {

  const [showDialog, setShowDialog] = useState(false);
  const [isConnected,setIsConnected] = useState(false);

  useEffect(()=>{
    NetInfo.addEventListener(handleConnectivityChange);
    NetInfo.fetch().then(state => {
        setIsConnected(state.isConnected)
      });

    return () => {
        NetInfo.addEventListener(state => {
            setIsConnected(state.isConnected)
         });
    }
  },[NetInfo])


  function handleConnectivityChange (state)  {
    if (state.isConnected) {
      setIsConnected(true)
    } else {
      setIsConnected(false)
    }
  }

  if(!isConnected) {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="InternetConnectivity">
          <Stack.Screen options={({navigation}) => ({
            headerShown: false,
            cardStyle: { backgroundColor: '#FFFFFF' },
          })} name="InternetConnectivity" component={InternetConnectivityScreen} />
          </Stack.Navigator>
      </NavigationContainer>)
  } else {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen options={({navigation}) => ({
            headerShown: false,
            cardStyle: { backgroundColor: '#132351' },
          })} name="Splash" component={SplashScreen} />

          <Stack.Screen options={({navigation}) => ({
            headerShown: false,
            cardStyle: { backgroundColor: '#132351' },
          })} name="Splash1" component={SplashScreen1} />

          <Stack.Screen options={({navigation}) => ({
            headerShown: false,
            cardStyle: { backgroundColor: '#132351' },
          })} name="Splash2" component={SplashScreen2} />

          <Stack.Screen options={({navigation}) => ({
            headerShown: false,
            cardStyle: { backgroundColor: '#132351' },
          })} name="IntroSlider" component={IntroSliderScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Home'}  />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Widget navigation={navigation} />
                <VirtualView navigation={navigation} />
                <ChatBot navigation={navigation} />
                <Login navigation={navigation} />
                {/* <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} /> */}
              </View>
            ),
          })} name="Home" component={HomeScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title : "Choose Your Category",
            headerTintColor: '#2E2E2E',
            headerShown: false,
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="CategoryFilter" component={CategoryFilterScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title : "Category Search",
            headerTintColor: '#2E2E2E',
            headerShown: false,
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                {/* <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} /> */}
              </View>
            ),
          })} name="CategoryResults" component={CategoryResultScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Video Reviews'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="ViewVideoReview" component={ViewVideoReviewScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title : "Choose Your Branch",
            headerTintColor: '#2E2E2E',
            headerShown: false,
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="VirtualBranchSearch" component={VirtualBranchSearch} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: false,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Virtual Library'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="BranchResults" component={BranchResults} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Virtual Library'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="VirtualLibraryResults" component={VirtualLibraryResults} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Book Details'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="BookDetails" component={BookDetailsScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            // title: "Author Details",
            // headerTintColor: '#2E2E2E',
            // headerShown: true,
            // headerStyle: {
            //   height: 50,
            //   elevation: 0,
            //   backgroundColor: '#FFFFFF',
            // },
            // cardStyle: { backgroundColor: '#FFFFFF' },
            // headerRight: () => (
            //   <View style={{ display: 'flex', flexDirection: 'row' }}>
            //     <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
            //   </View>
            // ),
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Author Details'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="AuthorDetails" component={AuthorDetailsScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Books'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="AllBooks" component={AllBooksScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Search Results'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="SearchResults" component={SearchResultsScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Authors'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="AllAuthors" component={AllAuthorsScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'My Notification'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="Notification" component={NotificationScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'My Wishlist'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="Wishlist" component={WishlistScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'My Recommendations'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="Recommendations" component={RecommendationsScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            // title: "Video Review",
            // headerTintColor: '#2E2E2E',
            // headerShown: true,
            // headerStyle: {
            //   height: 50,
            //   elevation: 0,
            //   backgroundColor: '#FFFFFF',
            // },
            // cardStyle: { backgroundColor: '#FFFFFF' },
            // headerRight: () => (
            //   <View style={{ display: 'flex', flexDirection: 'row' }}>
            //     <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
            //   </View>
            // ),
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Video Review'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="VideoReview" component={VideoReviewScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Add Video Review'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="AddVideoReview" component={AddVideoReviewScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Edit Video Review'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="EditVideoReview" component={EditVideoReviewScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerTintColor: '#FFFFFF',
            headerShown: false,
            headerStyle: {
              height: 200,
              backgroundColor: '#FFFFFF',
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
              elevation: 0, // remove shadow on Android
              shadowOpacity: 0, // remove shadow on iOS
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <SearchBar  navigation={navigation} />
            ),
          })} name="Search" component={SearchScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 80,
              backgroundColor: '#0A6752',
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
              elevation: 0, // remove shadow on Android
              shadowOpacity: 0, // remove shadow on iOS
            },
            cardStyle: { backgroundColor: '#e6ddd5' },
            headerLeft: () => (
              <ChatBotBar  navigation={navigation} />
            ),
          })} name="ChatBot" component={ChatBotScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "Voice Recognition Search",
            headerTintColor: '#2E2E2E',
            headerShown: true,
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="VoiceSearch" component={VoiceSearchScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'My Profile'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="Profile" component={ProfileScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'My Shelf'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="Shelf" component={ShelfScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            // title: "Profile Info",
            // headerTintColor: '#2E2E2E',
            // headerShown: true,
            // headerStyle: {
            //   height: 50,
            //   elevation: 0,
            //   backgroundColor: '#FFFFFF',
            // },
            // cardStyle: { backgroundColor: '#FFFFFF' },
            // headerRight: () => (
            //   <View style={{ display: 'flex', flexDirection: 'row' }}>
            //     <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
            //   </View>
            // ),
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Profile Info'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="ProfileInfo" component={ProfileInfoScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Subscription'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="Subscription" component={SubscriptionScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Subscription'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="TakeSubscription" component={TakeSubscriptionScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Member Account'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="Member" component={MemberScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Add Member'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="AddMember" component={AddMemberScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Edit Member'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="EditMember" component={EditMemberScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Custom Widget'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="ShowWidget" component={ShowWidgetScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Add Widget'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="AddWidget" component={AddWidgetScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Edit Widget'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="EditWidget" component={EditWidgetScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'FAQs'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="FAQs" component={FAQsScreen} />
  
          <Stack.Screen options={({navigation}) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 50,
              elevation: 0,
              backgroundColor: '#FFFFFF',
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <Search navigation={navigation} headerName={'Terms & Condition'} />
            ),
            headerRight: () => (
              <View style={{ display: 'flex', flexDirection: 'row' }}>
                <Menu navigation={navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} />
              </View>
            ),
          })} name="TermsCondition" component={TermsConditionScreen} />
  
          <Stack.Screen options={(navigation) => ({
            title: "",
            headerShown: false,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 100,
              backgroundColor: '#FFFFFF',
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
              elevation: 0, // remove shadow on Android
              shadowOpacity: 0, // remove shadow on iOS
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <HeaderBar navigation={navigation} />
            ),
          })} name="SignIn" component={SignInScreen} />
  
          <Stack.Screen options={(navigation) => ({
            title: "",
            headerShown: false,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 100,
              backgroundColor: '#FFFFFF',
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
              elevation: 0, // remove shadow on Android
              shadowOpacity: 0, // remove shadow on iOS
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <HeaderBar navigation={navigation} />
            ),
          })} name="OtpSignIn" component={OtpSignInScreen} />
  
          <Stack.Screen options={(navigation) => ({
            title: "",
            headerShown: false,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 100,
              backgroundColor: '#FFFFFF',
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
              elevation: 0, // remove shadow on Android
              shadowOpacity: 0, // remove shadow on iOS
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <HeaderBar navigation={navigation} />
            ),
          })} name="ForgotPassword" component={ForgotPasswordScreen} />
  
          <Stack.Screen options={(navigation) => ({
            title: "",
            headerShown: false,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 100,
              backgroundColor: '#FFFFFF',
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
              elevation: 0, // remove shadow on Android
              shadowOpacity: 0, // remove shadow on iOS
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <HeaderBar navigation={navigation} />
            ),
          })} name="GetOtpSignIn" component={GetOtpSignInScreen} />
  
          <Stack.Screen options={(navigation) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 100,
              backgroundColor: '#FFFFFF',
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
              elevation: 0, // remove shadow on Android
              shadowOpacity: 0, // remove shadow on iOS
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <HeaderBar navigation={navigation} />
            ),
          })} name="SignUp" component={SignUpScreen} />
  
          <Stack.Screen options={(navigation) => ({
            title: "",
            headerShown: true,
            headerTintColor: '#FFFFFF',
            headerStyle: {
              height: 100,
              backgroundColor: '#FFFFFF',
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
              elevation: 0, // remove shadow on Android
              shadowOpacity: 0, // remove shadow on iOS
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerLeft: () => (
              <HeaderBar navigation={navigation} />
            ),
          })} name="SignUpInfo" component={SignUpInfoScreen} />
        </Stack.Navigator>
        <Dialog.Container visible={showDialog}>
          <Dialog.Title>Follow Us!</Dialog.Title>
          <View style={{ display: "flex", flexDirection: "row", justifyContent: "space-evenly" }}>
            <Icon name="instagram" size={60} color="#29465e" onPress={() => Linking.openURL('https://www.instagram.com/justbooksclc/')} />
            <Icon name="facebook" size={60} color="#29465e" onPress={() => Linking.openURL('https://www.facebook.com/JustBooksCLC/')} />
            <Icon name="twitter" size={60} color="#29465e" onPress={() => Linking.openURL('https://twitter.com/justbooksclc')} />
          </View>
          <Dialog.Button label="Close" onPress={() => { setShowDialog(false) }} />
        </Dialog.Container>
        <FlashMessage position="bottom" />
      </NavigationContainer>
    );
  }
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#FFFFFF',
  },
});

export default App;
