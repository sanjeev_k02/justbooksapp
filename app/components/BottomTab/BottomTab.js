import React from 'react';
import { View, Text, TouchableOpacity,  Platform, Keyboard } from 'react-native';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';

import AsyncStorage from '@react-native-community/async-storage';

import styles from './styles'

class BottomTab extends React.Component {
  constructor(props){
    super(props);
    // console.log("routeName",this.props.routeName);
    this.state = {
      subscription: true,
      visible: true
    }
  }

  componentWillUnmount() {
    this.keyboardEventListeners && this.keyboardEventListeners.forEach((eventListener) => eventListener.remove());
  }

  visible = visible => () => this.setState({visible});

  componentDidMount = async() => {
    if (Platform.OS === 'android') {
      this.keyboardEventListeners = [
        Keyboard.addListener('keyboardDidShow', this.visible(false)),
        Keyboard.addListener('keyboardDidHide', this.visible(true))
      ];
    }

    try {
        const user = await AsyncStorage.getItem('userData');
        if (user !== null) {
            const userData = JSON.parse(user)
            this.setState({
              subscription : true
            })
        } else {
          this.setState({
            subscription : false
          })
        }
    } catch (error) {
        console.log("error", error)
    }
  };

  _screenNavigate = async (key) => {
    switch(key) {
      case 'home':
        this.props.navigation.push('Home');
        break;
      case 'shelf':
        this.props.navigation.push('Shelf');
        break;
      case 'wishlist':
        this.props.navigation.push('Wishlist');
        break;
      case 'profile':
        this.props.navigation.push('Profile');
        break;
      case 'notification':
        this.props.navigation.push('Notification');
        break;
      case 'subscription_now':
        this.props.navigation.push('SignUp');
        break;
      default :
      this.props.navigation.push('Home');
    }
  }

  render () {
    if (!this.state.visible) {
      return null;
    } else {
      return (<View style={styles.footer}>
        {this.state.subscription ?
        <View style={styles.tabView}>
          <TouchableOpacity style={styles.bottomButtons} onPress={()=>{this._screenNavigate('home')}} >
              <Icons name="home" size={this.props.routeName == 'home' ? 28 : 24} color={this.props.routeName == 'home' ? '#000000' : '#8F8E94'} />
              <Text style={[
                  styles.footerText,
                  this.props.routeName == 'home' ? { color: "#000000" } : { color: "#8F8E94" }
              ]}>Home</Text>
          </TouchableOpacity>
          
          <TouchableOpacity style={styles.bottomButtons}  onPress={()=>{this._screenNavigate('shelf')}}>
              <Icons name="bookshelf" size={this.props.routeName == 'shelf' ? 28 : 24} color={this.props.routeName == 'shelf' ? '#000000' : '#8F8E94'} />
              <Text style={[
                  styles.footerText,
                  this.props.routeName == 'shelf' ? { color: "#000000" } : { color: "#8F8E94" }
              ]}>Shelf</Text>
          </TouchableOpacity>

          {/* <TouchableOpacity style={styles.bottomButtons}  onPress={()=>{this._screenNavigate('subscription')}}>
              <Icons name="currency-inr" size={this.props.routeName == 'subscription' ? 28 : 24} color={this.props.routeName == 'subscription' ? '#000000' : '#8F8E94'} />
              <Text style={[
                  styles.footerText,
                  this.props.routeName == 'subscription' ? { color: "#000000" } : { color: "#8F8E94" }
              ]}>Subscription</Text>
          </TouchableOpacity> */}

          <TouchableOpacity style={styles.bottomButtons}  onPress={()=>{this._screenNavigate('wishlist')}}>
              <Icons name="heart" size={this.props.routeName == 'wishlist' ? 28 : 24} color={this.props.routeName == 'wishlist' ? '#000000' : '#8F8E94'} />
              <Text style={[
                  styles.footerText,
                  this.props.routeName == 'wishlist' ? { color: "#000000" } : { color: "#8F8E94" }
              ]}>Wishlist</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.bottomButtons}  onPress={()=>{this._screenNavigate('profile')}}>
              <Icons name="face-profile" size={this.props.routeName == 'profile' ? 28 : 24} color={this.props.routeName == 'profile' ? '#000000' : '#8F8E94'} />
              <Text style={[
                  styles.footerText,
                  this.props.routeName == 'profile' ? { color: "#000000" } : { color: "#8F8E94" }
              ]}>Profile</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.bottomButtons}  onPress={()=>{this._screenNavigate('notification')}}>
              <Icons name="bell-ring-outline" size={this.props.routeName == 'notification' ? 28 : 24} color={this.props.routeName == 'notification' ? '#000000' : '#8F8E94'} />
              <Text style={[
                  styles.footerText,
                  this.props.routeName == 'notification' ? { color: "#000000" } : { color: "#8F8E94" }
              ]}>Notification</Text>
          </TouchableOpacity>
        </View> :
        <View style={styles.subscriptionView}>
          <TouchableOpacity style={styles.subscriptionBottomButtons} onPress={()=>{this._screenNavigate('subscription_now')}} >
              <Text style={[
                  styles.subscriptionFooterText,
                  { color: "#273C96" }
              ]}><Icons name="currency-inr" size={30} color={'#273C96'} /> Signup Now</Text>
          </TouchableOpacity>
        </View>}
    </View>)
    }
  }
}

export default BottomTab;