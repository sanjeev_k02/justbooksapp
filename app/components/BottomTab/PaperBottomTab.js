import * as React from 'react';
import { View, Text, StatusBar, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import BookShelf from 'react-native-vector-icons/MaterialCommunityIcons';
import BottomNavigation, {
    FullTab
  } from 'react-native-material-bottom-navigation'
// import { TouchableOpacity } from 'react-native-gesture-handler';
   
export default class PaperBottomTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab : 'Home',
      tabs: [
        {
          key: 'Home',
          icon: 'home',
          label: 'Home',
          barColor: '#2b2a2a',
          activeColor: '#273C96',
          pressColor: 'rgba(255, 255, 100, 0.16)'
        },
        {
          key: 'Shelf',
          icon: 'bookshelf',
          label: 'Shelf',
          barColor: '#2b2a2a',
          activeColor: '#273C96',
          pressColor: 'rgba(255, 255, 255, 0.16)'
        },
        {
          key: 'Subscription',
          icon: 'currency-inr',
          label: 'Subscription',
          barColor: '#2b2a2a',
          activeColor: '#273C96',
          pressColor: 'rgba(255, 255, 255, 0.16)'
        },
        {
          key: 'Profile',
          icon: 'face-profile',
          label: 'Profile',
          barColor: '#2b2a2a',
          activeColor: '#273C96',
          pressColor: 'rgba(255, 255, 255, 0.16)'
        },
        {
          key: 'Notification',
          icon: 'bell-ring-outline',
          label: 'Notification',
          barColor: '#2b2a2a',
          activeColor: '#273C96',
          pressColor: 'rgba(255, 255, 255, 0.16)'
        }
      ]
    }
  }
  
  renderIcon = (icon, key, activeColor) => ({ isActive }) => (
    <TouchableOpacity onPress={()=>{this.props.navigation.navigate(key)}}>
      <BookShelf size={isActive ? 28 : 24} color={isActive ? activeColor : '#fff'} name={icon} />
    </TouchableOpacity>
  )
  
  renderTab = ({ tab, isActive }) => (
    <FullTab
      isActive={isActive}
      key={tab.key}
      label={tab.label}
      style={{margin: -2}}
      renderIcon={this.renderIcon(tab.icon, tab.key, tab.activeColor)}
    />
  )

  render() {
    return (
      <BottomNavigation
        activeTab={this.state.activeTab}
        onTabPress={newTab => this.setState({ activeTab: newTab.key })}
        renderTab={this.renderTab}
        tabs={this.state.tabs}
      />
    )
  }
}