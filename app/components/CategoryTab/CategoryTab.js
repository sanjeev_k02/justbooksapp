import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { Chip, Title } from 'react-native-paper';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { ScrollView } from 'react-native-gesture-handler';
import styles from './styles'

function CategoryTab(props) {
  
  function categoryAction(mainCategoryId) {
    console.log("Category",mainCategoryId);
    props.navigation.push('CategoryResults',{
      categories: mainCategoryId,
      isMainCategory: true
    });
  }

  const MoreCategoryAttributes = {
    onPress() {
      console.log("More Category");
      props.navigation.navigate("CategoryFilter");
    }
  }

  return (<View style={styles.category}>
      <ScrollView 
      horizontal={true}>
        <Chip avatar={<Image source={{ uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/main_category_icons/nf.png' }} />} style={styles.bottomButtons}  onPress={()=>categoryAction(2)}>
        <Title style={styles.footerText}>Non-Ficition</Title>
        </Chip>
        
        <Chip avatar={<Image source={{ uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/main_category_icons/Fiction.png' }} />} style={styles.bottomButtons}  onPress={()=>categoryAction(1)}>
          <Title style={styles.footerText}>Fiction</Title>
        </Chip>

        <Chip avatar={<Image source={{ uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/main_category_icons/Young+readers.png' }} />} style={styles.bottomButtons} onPress={()=>categoryAction(4)}>
          <Title style={styles.footerText}>Young Readers</Title>
        </Chip>
        
        <Chip avatar={<Image source={{ uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/main_category_icons/regional.png' }} />} style={styles.bottomButtons}  onPress={()=>categoryAction(3)}>
          <Title style={styles.footerText}>Regional Languages</Title>
        </Chip>
        
        <Chip avatar={<Image source={{ uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/main_category_icons/Indian+fiction.png' }} />} style={styles.bottomButtons}  onPress={()=>categoryAction(5)}>
          <Title style={styles.footerText}>Indian Writing</Title>
        </Chip>
        
        <Chip avatar={<Image source={{ uri: 'https://cdn3.iconfinder.com/data/icons/eightyshades/512/44_Grid_alt-512.png' }} />} style={styles.bottomButtons} {...MoreCategoryAttributes}>
          <Title style={styles.footerText}>More</Title>
        </Chip>

      </ScrollView>
  </View>)
}

export default CategoryTab;