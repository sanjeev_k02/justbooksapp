import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    category: {
        position: 'absolute',
        flex:0.1,
        left: 0,
        right: 0,
        top: 5,
        height:50,
        alignItems:'center',
        elevation:25,
        display: "flex", 
        flexDirection: "row", 
        flexWrap: "wrap", 
        alignContent: "center", 
        alignItems: "center", 
        justifyContent: "center",
      },
      tabSection: {
        width: 120,
        height: 220,
        margin: 10,
        // backgroundColor: '#101211'
      },
      bottomButtons: {
        width: '100%',
        margin: 10,
        alignItems:'center',
        justifyContent: 'space-around',
        flex:1,
        marginTop:9,
        backgroundColor: '#F1F1F1'
      },
      footerText: {
        color:'#101211',
        fontWeight:'600',
        alignItems:'center',
        fontSize:18,
        lineHeight:20,
        margin: 10
      }
});
