import React from 'react';
import { View, Text, Alert} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icons from 'react-native-vector-icons/Ionicons';
import { showMessage } from "react-native-flash-message";
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles';

class ChatBot extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subscription: true,
            subscription_id: 0,
            memberCard: 0,
        }
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard,
                    subscription : true
                })
            } else {
                this.setState({
                    subscription : false
                })
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }
    };

    _chatBotNavigateAfterLogin = async() => {
        this.props.navigation.push('ChatBot');
    }

    _chatBotNavigateBeforeLogin = async() => {
        showMessage({
            message: "Chat with us!",
            description: 'Please login to chat with us...',
            icon: "danger",
            type: "danger",
        });
    }

    render() {
        return (
            <View style={styles.loginSection}>
                {this.state.subscription ?
                    <TouchableOpacity  onPress={()=>{this._chatBotNavigateAfterLogin()}}>
                        <View style={styles.login}>
                            <Icons name="chatbubble-ellipses" size={18} color="#2E2E2E" style={{marginRight: 25}} />
                            <Text style={styles.footerText}>Chat</Text>
                        </View>
                    </TouchableOpacity>
                    : 
                    <TouchableOpacity  onPress={()=>{this._chatBotNavigateBeforeLogin()}}>
                        <View style={styles.login}>
                            <Icons name="chatbubble-ellipses" size={18} color="#2E2E2E" style={{marginRight: 25}} />
                            <Text style={styles.footerText}>Chat</Text>
                        </View>
                    </TouchableOpacity>
                }
            </View>
        );
    }

}

export default ChatBot;