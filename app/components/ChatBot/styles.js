import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    footerText: {
      color:'#2E2E2E',
      fontWeight:'600',
      fontSize:11,
      lineHeight:20,
      marginTop: -2, 
    //   marginLeft: -5
    },
    // loginSection : {
    //     marginTop: 10
    // }
});
