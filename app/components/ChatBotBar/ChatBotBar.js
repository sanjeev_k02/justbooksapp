import React from 'react';
import { View, Text, Image} from 'react-native';
import { Headline } from 'react-native-paper';
import Icons from 'react-native-vector-icons/Fontisto';
import styles from './styles';

class ChatBotBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
        <View style={styles.contain}>
            <View style={styles.textStyle}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'space-between', position: 'absolute', bottom: 0}}>
                    <View style={{flex: 1}}>
                        <Image source={{ uri: "https://justbooks.in/images/noimage.jpg" }} style={styles.imageCard}>
                        </Image>
                    </View>
                        <View style={{flex: 1}}>
                            <Text style={styles.titleDesciption}>
                                <Headline style={styles.color}>Justbooks Ask</Headline>{'\n'}
                                <Text style={{color: 'green', fontSize: 16 }}>
                                    <Headline style={styles.colorParagraph}>online</Headline>
                                </Text>
                            </Text>
                        </View>
                    </View>
            </View>
      </View>
        );
    }

}

export default ChatBotBar;