import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: { 
    flex: 1,
    display: 'flex', 
    alignContent: 'center', 
    justifyContent: 'center' ,
    margin: 20,
    backgroundColor: '#0A6752',
  },
  textStyle: {
    backgroundColor: '#0A6752',
  },
  SectionStyle: {
    flex: 1,
    flexDirection: 'row-reverse',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#2E2E2E',
    height: 60,
    borderRadius: 4,
    // padding: 20,
    paddingRight: 20,
    shadowColor: 'grey',
    shadowOffset: {
        width: 0,
        height: 1
    },
    elevation: 0,
    marginBottom: 10,
    shadowOpacity: 0.5,
    shadowRadius: 1,
    backgroundColor: '#0A6752',
  },
  titleDesciption: {
    marginTop: 0,
    marginRight: 30,
    marginLeft: 20
  },
  color: {
    color: '#FFFFFF',
    fontSize: 22,
    fontWeight: '600',
    fontFamily: 'Montserrat-Bold'
  },
  colorParagraph: {
    color: '#FFFFFF',
    fontSize: 20,
    lineHeight: 20
  },
  imageCard: {
      width: 50, 
      height: 50,
      borderRadius: 50
  },
});
