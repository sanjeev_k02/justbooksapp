import React from 'react';
import styles from './styles';
import { FAB } from 'react-native-paper';

function ChatBotIcon(props) {

  const _chatBotNavigate = {
    onPress() {
      console.log("chatBot");
      props.navigation.navigate('ChatBot');
    }
  }
  return (<FAB
      style={styles.fab}
      small={false}
      sizeLabel={40}
      icon="chat-processing"
      size={40}
      color={'#2E2E2E'}
      {..._chatBotNavigate}
  />)
}

export default ChatBotIcon;