import React from "react";
import Menu, {MenuItem} from "react-native-material-menu";
import {TouchableOpacity, View, Text, FlatList} from "react-native";
import Icons from 'react-native-vector-icons/Ionicons';
import FilterIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Divider } from "react-native-paper";
import styles from './styles';

export default class Filter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {basicInfo: null};
    }

    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };

    render() {
        return (
            <Menu
                style={styles.menu}
                ref={(ref) => this._menu = ref}
                button={<TouchableOpacity onPress={() => this._menu.show()} style={styles.menuButton}>
                    <Text title="filter" titleStyle={styles.titleStyle} style={styles.SeeAllHeading}>
                        <Icons name="filter" size={18} color="#8F8E94" style={{ marginLeft: 10 }} /> filter
                    </Text>
                </TouchableOpacity>}>
                    <View>
                        <MenuItem 
                            onPress={() => {this._menu.hide()}}
                            textStyle={{color: '#2E2E2E', fontSize: 16}} 
                            style={styles.faq}
                        ><FilterIcon name="currency-inr" size={18} color="#8F8E94" style={{ marginLeft: 10 }} /> By Price</MenuItem>
                          <View style={styles.container}>
                                <FlatList
                                    data={[
                                        {key: 'under rs. 500'},
                                        {key: 'under rs. 1000'}
                                    ]}
                                    renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
                                />
                            </View>
                        <Divider style={styles.divider} />

                        <MenuItem 
                            onPress={() => {this._menu.hide()}}
                            textStyle={{color: '#2E2E2E', fontSize: 16}} 
                            style={styles.discount}
                        ><FilterIcon name="offer" size={18} color="#8F8E94" style={{ marginLeft: 10 }} /> By Discount</MenuItem>
                          <View style={styles.container}>
                                <FlatList
                                    data={[
                                        {key: '10% Off or more'},
                                        {key: '25% Off or more'},
                                        {key: '40% Off or more'},
                                        {key: '50% Off or more'}
                                    ]}
                                    renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
                                />
                            </View>
                        <Divider style={styles.divider} />

                        <MenuItem 
                            onPress={() => {this._menu.hide()}}
                            textStyle={{color: '#2E2E2E', fontSize: 16}} 
                            style={styles.discount}
                        ><FilterIcon name="book-alphabet" size={18} color="#8F8E94" style={{ marginLeft: 10 }} />  New Arrivals</MenuItem>
                        <View style={styles.container}>
                                <FlatList
                                    data={[
                                        {key: 'Last 7 days'},
                                        {key: 'Last 15 days'},
                                        {key: 'Last 30 days'}
                                    ]}
                                    renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
                                />
                            </View>
                        <Divider style={styles.arrivalsDivider} />

                    </View>
            </Menu>
        )
    }

    
}