import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    menu : {
        marginTop: 25,
        marginLeft: 8, 
        backgroundColor: '#FFFFFF', 
        color: '#fff',
        fontSize: 16,
        width: 200,
        fontWeight: '600',
    },
    menuButton : {
        paddingRight:0,
        paddingLeft:10,  
        alignItems:'center', 
        justifyContent: 'center',
        marginRight: 0, 
        borderRadius: 4
    },
    faq: {
        marginTop: 20,
        height: 10,
        color: '#2E2E2E'
    },
    discount: {
        marginTop: 0,
        height: 10
    },
    arrivals: {
        marginTop: 0,
        height: 10,
        paddingBottom: 10
    },
    arrivalsDivider: {
        marginTop: 10,
        marginBottom: 10,
    },
    divider: {
        marginTop: 20,
        marginBottom: 20,
        color:'#fff',
        backgroundColor: '#fff'
    },
    tc: {
        color: '#fff', 
        fontSize: 16
    },
    logout: { 
        marginBottom: 25, 
        height: 10
    },
    SeeAllHeading: {
        color: '#2E2E2E',
        fontSize: 16,
        fontWeight: '600',
        textAlign: 'right',
        alignContent: 'flex-end',
        alignItems: 'flex-end'
    },
    titleStyle : {
        fontSize: 14, 
        textAlign: 'right',
        alignContent: 'flex-end',
        alignItems: 'flex-end'
    },
    container: {
        flex: 1,
        marginTop: 10
    },
    item: {
        paddingLeft: 40,
        paddingTop: 6,
         fontSize: 14,
         color: '#2E2E2E'
    },
});
