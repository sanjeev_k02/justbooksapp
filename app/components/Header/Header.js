import React, { useState, useEffect} from 'react';
import { View, Text, StatusBar, StyleSheet , TextInput} from 'react-native';
import { Appbar } from 'react-native-paper';
import Icons from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import Menu from "../Menu/Menu";

function Header(props) {
    const [showDialog, setShowDialog] = useState(false);

  const _goBack = () => props.navigation.goBack();

    return (
        <View style={styles.contain}>
            <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0}}>
                <Appbar.BackAction onPress={_goBack} />
                <Appbar.Content title={props.title} />
                {/* <Menu navigation={props.navigation} dialogVisibilityHandler={setShowDialog} dialogVisibility={showDialog} /> */}
            </Appbar.Header>
      </View>
    );

}

export default Header;