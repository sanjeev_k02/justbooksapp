import React from 'react';
import { View, Text, StatusBar, StyleSheet , TextInput} from 'react-native';
import { Avatar, Appbar, Divider, Button, Card, Title, Paragraph, Headline, Subheading, IconButton } from 'react-native-paper';
import Icons from 'react-native-vector-icons/AntDesign';
import styles from './styles';

class HeaderBar extends React.Component {
    constructor(props) {
        super(props);
        console.log("a",this.props.navigation.route.name);
    }
    render() {
        return (
        <View style={styles.contain}>
            <View style={styles.textStyle}>
                <Text style={styles.titleDesciption}>
                    {this.props.navigation.route.name == 'SignUp' &&
                    (<Headline style={styles.color}>
                        SignUp With
                    </Headline>)
                    }
                    {this.props.navigation.route.name == 'SignUpInfo' &&
                    (<Headline style={styles.color}>
                        SignUp With
                    </Headline>)
                    }
                    {this.props.navigation.route.name == 'OtpSignIn' &&
                    (<Headline style={styles.color}>
                        Login With
                    </Headline>)
                    }
                    {this.props.navigation.route.name == 'SignIn' &&
                    (<Headline style={styles.color}>
                        Login With
                    </Headline>)
                    }
                    {this.props.navigation.route.name == 'GetOtpSignIn' &&
                    (<Headline style={styles.color}>
                        OTP Login With
                    </Headline>)
                    }
                </Text>
            </View>
            <View>
                <Text style={styles.titleDesciption}>
                    <Text style={{color: 'green', fontSize: 16 }}>
                        <Headline style={styles.colorParagraph}>JustBooks</Headline>{'\n'}
                    </Text>{'\n'}
                </Text>
            </View>
      </View>
        );
    }

}

export default HeaderBar;