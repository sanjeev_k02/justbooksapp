import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: { 
    flex: 1,
    display: 'flex', 
    alignContent: 'center', 
    justifyContent: 'center' ,
    marginTop: 25,
    marginLeft: 20,
  },
  titleDesciption: {
    marginTop: 2,
    marginRight: 30,
    width: '100%',
    textAlign: 'left',
  },
  color: {
    color: '#2E2E2E',
    fontSize: 20
  },
  colorParagraph: {
    color: '#2E2E2E',
    fontSize: 30
  },
});
