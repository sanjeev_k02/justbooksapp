import React from 'react';
import { View, Text, Alert} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icons from 'react-native-vector-icons/Entypo';
import AwesomeAlert from 'react-native-awesome-alerts';
import styles from './styles';

import AsyncStorage from '@react-native-community/async-storage';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subscription: true,
            loginAlert: false,
        }
    }

    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user)
                this.setState({
                    subscription : true
                })
            } else {
                this.setState({
                    subscription : false
                })
            }
        } catch (error) {
            console.log("error", error)
        }
    };

    _loginNavigate = async() => {
        this.props.navigation.push('SignIn');
    }

    _logoutNavigate = async() => {
        this.setState({ 
            loginAlert: true,
        });
    }
     
    hideLoginAlert = () => {
        this.setState({
            loginAlert: false 
        });
    };

    _logoutActionPreform = async () => {
        this.setState({ loginAlert: false });
        AsyncStorage.clear().then(() => {
            this.setState({ loginAlert: false });
            this.props.navigation.push('Home');
        })
    }

    

    render() {
        return (
            <View style={styles.loginSection}>
                {/* wishlist */}
                <AwesomeAlert
                    show={this.state.loginAlert}
                    showProgress={false}
                    icon={'alert'}
                    iconColor={'#AB3247'}
                    title="JustBooks"
                    message="Are you want to log out?"
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText="No, cancel"
                    confirmText="Yeah, logout"
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this.hideLoginAlert();
                    }}
                    onConfirmPressed={() => {
                        this._logoutActionPreform();
                    }}
                />
                {this.state.subscription ?
                    <TouchableOpacity style={{ alignContent: 'center', alignItems: 'center', textAlign: 'center'}}  onPress={()=>{this._logoutNavigate()}}>
                        <View style={styles.logout}>
                            <Icons name="log-out" size={18} color="#2E2E2E" style={{ marginRight: 15 }} />
                            <Text style={styles.footerText}>Logout</Text>
                        </View>
                    </TouchableOpacity>
                    : 
                    <TouchableOpacity style={{ alignContent: 'center', alignItems: 'center', textAlign: 'center'}} onPress={()=>{this._loginNavigate()}}>
                        <View style={styles.login}>
                            <Icons name="login" size={18} color="#2E2E2E" style={{ marginRight: 15 }} />
                            <Text style={styles.footerText}>Login</Text>
                        </View>
                    </TouchableOpacity>
                }
            </View>
        );
    }

}

export default Login;