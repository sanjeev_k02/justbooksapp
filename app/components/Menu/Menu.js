import React from "react";
import Menu, {MenuItem} from "react-native-material-menu";
import {TouchableOpacity, View} from "react-native";
import Icons from 'react-native-vector-icons/Entypo';
import { Divider } from "react-native-paper";
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles';

export default class MenuComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {basicInfo: null};
    }

    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };

    renderLogOutMenu() {
        return <MenuItem onPress={() => {
            AsyncStorage.clear().then(() => {
                this.props.navigation.push('Home');
            })
            this._menu.hide()}}
        style={styles.logout}>Logout</MenuItem>;
    }

    render() {
        return (
            <Menu
                style={styles.menu}
                ref={(ref) => this._menu = ref}
                button={<TouchableOpacity onPress={() => this._menu.show()} style={styles.menuButton}>
                    <Icons name="dots-three-vertical" size={20} color="#2E2E2E" style={{ marginRight: 0 }} />
                </TouchableOpacity>}>
                    <View>
                        <MenuItem onPress={() => {this.props.navigation.navigate('FAQs');this._menu.hide()}} style={styles.faq}>FAQ's</MenuItem>
                        <Divider style={styles.divider} />
                        <MenuItem onPress={() => {this.props.navigation.navigate('TermsCondition');this._menu.hide()}} textStyle={styles.tc} style={{ height: 10}}>Terms & Condition</MenuItem>
                        <Divider style={styles.divider} />
                        <MenuItem onPress={() => {this.props.dialogVisibilityHandler(!this.props.dialogVisibility);this._menu.hide()}} textStyle={styles.tc} style={{ height: 10}}>Follow us</MenuItem>
                        <Divider style={styles.divider} />
                        {this.renderLogOutMenu()}
                    </View>
            </Menu>
        )
    }

    
}