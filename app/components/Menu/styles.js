import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    menu : {
        marginTop: 30,
        marginLeft: 8, 
        backgroundColor: '#F1F1F1', 
        color: '#2E2E2E'
    },
    menuButton : {
        paddingRight:15,
        paddingLeft:10,  
        alignItems:'center', 
        justifyContent: 'center',
        marginRight: 0, 
        borderRadius: 4
    },
    faq: {
        marginTop: 20,
        height: 10
    },
    divider: {
        marginTop: 20,
        marginBottom: 20,
        color:'#2E2E2E',
        backgroundColor: '#2E2E2E'
    },
    tc: {
        color: '#2E2E2E', 
        fontSize: 16
    },
    logout: { 
        marginBottom: 25, 
        height: 10
    }
});
