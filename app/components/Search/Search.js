import React from 'react';
import { View, Text, StatusBar, StyleSheet , TextInput} from 'react-native';
import Icons from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import { Headline  } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';

class Search extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
        <TouchableOpacity onPress={() => {console.log("voice Search")}}>
            <View style={styles.topStyle}>
                <View style={styles.SectionStyle}>
                    {/* <TextInput 
                    label= "searchText"
                    style={{flex: 1, width: 160, color:'#2E2E2E' }}
                    underlineColor= "#2E2E2E"
                    placeholderTextColor = "#2E2E2E"
                    placeholder= "Search"
                    mode= "outlined" />
                    <Icons name="search1" size={18} color="#2E2E2E" style={{ marginRight: 10 }}  onPress={() => {this.props.navigation.navigate('Search');}} /> */}
                    <Headline style={styles.heading}>{this.props.headerName}</Headline>
                </View>
            </View>
        </TouchableOpacity>
        );
    }

}

export default Search;