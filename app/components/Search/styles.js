import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    SectionStyle: {
      flexDirection: 'row-reverse',
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1,
      borderColor: '#FFFFFF',
      height: 36,
      borderRadius: 4,
      // padding: 20,
      paddingRight: 20,
      // backgroundColor: '#F1F1F1',
      // shadowColor: 'grey',
      // shadowOffset: {
      //     width: 0,
      //     height: 1
      // },
      marginTop: 16,
      marginBottom:10,
      marginLeft: 10,
      marginRight: 10,
      shadowOpacity: 0.5,
      shadowRadius: 1,
      // elevation: 1,
    },
    heading : {
      fontSize: 25,
      fontWeight: '900',
      fontFamily: 'Montserrat-ExtraBold',
      color: '#000000'
    },
    topStyle : { 
      display: 'flex', 
      flexDirection: 'row', 
      alignContent: 'center', 
      justifyContent: 'center' 
    }
});
