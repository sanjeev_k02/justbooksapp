import React from 'react';
import { View, Text, StatusBar, StyleSheet , TextInput, ToastAndroid} from 'react-native';
import { Avatar, Appbar, Divider, Button, Card, Title, Paragraph, Headline, Subheading, IconButton } from 'react-native-paper';
import Icons from 'react-native-vector-icons/AntDesign';
import styles from './styles';

class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: ''
        }
    }

    searchBooks = () => {
        if (this.state.searchText === '') {
        ToastAndroid.show("Search query cannot be empty", ToastAndroid.LONG)
          return;
        }
    
        this.props.navigation.navigate('SearchResults', {
          searchQuery: this.state.searchText,
        });
    };

    render() {
        return (
        <View style={styles.contain}>
            <View style={styles.textStyle}>
                <Text style={styles.titleDesciption}>
                    <Headline style={styles.color}>Find For</Headline>{'\n'}
                    <Text style={{color: 'green', fontSize: 16 }}>
                        <Headline style={styles.colorParagraph}>Books and Authors</Headline>{'\n'}
                    </Text>{'\n'}
                </Text>
            </View>
            <View style={styles.SectionStyle}>
                <TextInput 
                label= "searchText"
                returnKeyType= "next"
                style={styles.searchTextInput}
                underlineColor= "#2E2E2E"
                placeholderTextColor = "#2E2E2E"
                placeholder= "What book are looking for..."
                mode= "outlined" 
                onChangeText={changedText =>
                    this.setState({ searchText: changedText })
                }
                onBlur={this.searchBooks} />
                <Icons name="search1" size={24} color="#2E2E2E" style={{ marginRight: 20 }} />
            </View>
      </View>
        );
    }

}

export default SearchBar;