import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: { 
    flex: 1,
    display: 'flex', 
    alignContent: 'center', 
    justifyContent: 'center' ,
    margin: 20
  },
  SectionStyle: {
    flex: 1,
    flexDirection: 'row-reverse',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#2E2E2E',
    height: 50,
    borderRadius: 4,
    // padding: 20,
    paddingRight: 20,
    shadowColor: 'grey',
    shadowOffset: {
        width: 0,
        height: 1
    },
    elevation: 0,
    marginBottom: 10,
    shadowOpacity: 0.5,
    shadowRadius: 1,
    backgroundColor: '#F1F1F1',
  },
  searchTextInput: {
    width: 250, 
    color: '#2E2E2E',
    backgroundColor: '#F1F1F1'
  },
  titleDesciption: {
    marginTop: 5,
    marginRight: 30
  },
  color: {
    color: '#2E2E2E',
  },
  colorParagraph: {
    color: '#2E2E2E',
  },
});
