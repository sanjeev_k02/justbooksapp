import React from 'react';
import styles from './styles';
import { FAB } from 'react-native-paper';

function SearchTab(props) {

  const _searchNavigate = {
    onPress() {
      console.log("Search");
      props.navigation.navigate('Search');
    }
  }
  return (<FAB
      style={styles.fab}
      small={false}
      sizeLabel={30}
      icon="book-search"
      size={30}
      color={'#2E2E2E'}
      {..._searchNavigate}
  />)
}

export default SearchTab;