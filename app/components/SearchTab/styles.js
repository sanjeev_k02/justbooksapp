import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    fab: {
      position: 'absolute',
      margin: 20,
      right: 0,
      bottom: 50,
      color: '#2E2E2E',
      backgroundColor: '#FFFFFF'
    },
});
