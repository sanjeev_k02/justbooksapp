import React from 'react';
import { StatusBar } from 'react-native';
import styles from './styles';

class StatusTopBar extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (<StatusBar backgroundColor = "#000000" />);
    }

}

export default StatusTopBar;