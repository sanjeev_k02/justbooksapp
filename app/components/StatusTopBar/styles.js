import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    }
});
