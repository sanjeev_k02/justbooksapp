import * as React from 'react';
import { View, StyleSheet, Dimensions, Text } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';

import { ScrollView } from 'react-native-gesture-handler';
import CurrentlyReading from '../../screens/Shelf/CurrentlyReading';
import BookToBeDelivered from '../../screens/Shelf/BookToBeDelivered';
import BookToBePicked from '../../screens/Shelf/BookToBePicked';

const CurrentlyReadingRoute = () => {
  return (<CurrentlyReading />);
};

const BookToBeDeliveredRoute = () => {
  return (<BookToBeDelivered />);
};

const BookToBePickedRoute = () => {
  return (<BookToBePicked />);
};

const initialLayout = { width: Dimensions.get('window').width };

export default function PaperTopTab() {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'first', title: 'Currently Reading' },
    { key: 'second', title: 'Book To Be Deliverd' },
    { key: 'third', title: 'Book To Be Picked' },
  ]);

  const renderScene = SceneMap({
    first: CurrentlyReadingRoute,
    second: BookToBeDeliveredRoute,
    third: BookToBePickedRoute,
  });

  return (
          <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            style={{backgroundColor: '#101211'}}
          />
  );
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});