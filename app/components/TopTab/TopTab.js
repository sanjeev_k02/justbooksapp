import * as React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import Animated from 'react-native-reanimated';

import { ScrollView } from 'react-native-gesture-handler';
import CurrentlyReading from '../../screens/Shelf/CurrentlyReading';
import BookToBeDelivered from '../../screens/Shelf/BookToBeDelivered';
import BookToBePicked from '../../screens/Shelf/BookToBePicked';
import Wishlist from '../../screens/Shelf/Wishlist';
import PastReads from '../../screens/Shelf/PastReads';
import MyRecommendations from '../../screens/Shelf/MyRecommendations';
import PreOrderedBooks from '../../screens/Shelf/PreOrderedBooks';

var propsSend;

const CurrentlyReadingRoute = () => {
  return (<CurrentlyReading navigation = {propsSend} />);
};

const BookToBeDeliveredRoute = () => {
  return (<BookToBeDelivered navigation = {propsSend} />);
};

const BookToBePickedRoute = () => {
  return (<BookToBePicked navigation = {propsSend} />);
};

const WishlistRoute = () => {
  return (<Wishlist navigation = {propsSend} />);
};

const PastReadsRoute = () => {
  return (<PastReads navigation = {propsSend} />);
};

const MyRecommendationsRoute = () => {
  return (<MyRecommendations navigation = {propsSend} />);
};

const PreOrderedBooksRoute = () => {
  return (<PreOrderedBooks navigation = {propsSend} />);
};

export default class TopTab extends React.Component {
  constructor(props) {
    super(props);
    propsSend = this.props;
  }
  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'Currently Reading' },
      { key: 'second', title: 'Book To Be Deliverd' },
      { key: 'third', title: 'Book To Be Picked' },
      { key: 'fourth', title: 'Wishlist' },
      { key: 'fifth', title: 'Past Reads' },
      { key: 'sixth', title: 'My Recommendations' },
      { key: 'seven', title: 'Pre Ordered Books' },
    ],
  };

  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);

    return (
      <View style={styles.tabBar}>
        <ScrollView 
        horizontal={true}>
          {props.navigationState.routes.map((route, i) => {
            const color = Animated.color(
              Animated.round(
                Animated.interpolate(props.position, {
                  inputRange,
                  outputRange: inputRange.map(inputIndex =>
                    inputIndex === i ? 0 : 90
                  ),
                })
              ),
              Animated.round(
                Animated.interpolate(props.position, {
                  inputRange,
                  outputRange: inputRange.map(inputIndex =>
                    inputIndex === i ? 0 : 90
                  ),
                })
              ),
              Animated.round(
                Animated.interpolate(props.position, {
                  inputRange,
                  outputRange: inputRange.map(inputIndex =>
                    inputIndex === i ? 0 : 90
                  ),
                })
              ),
            );

            return (
              <TouchableOpacity
                key={i}
                style={styles.tabItem}
                onPress={() => this.setState({ index: i })}>
                <Animated.Text style={{ color: color, fontSize: 18 }}>{route.title}</Animated.Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    );
  };

  _renderScene = SceneMap({
    first: CurrentlyReadingRoute,
    second: BookToBeDeliveredRoute,
    third: BookToBePickedRoute,
    fourth: WishlistRoute,
    fifth: PastReadsRoute,
    sixth: MyRecommendationsRoute,
    seven: PreOrderedBooksRoute,
  });

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        onIndexChange={this._handleIndexChange}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabBar: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
  },
  tabItem: {
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15,
    width: 200,
    // borderColor: '#fff',
    // borderRightWidth: 2
  },
});
