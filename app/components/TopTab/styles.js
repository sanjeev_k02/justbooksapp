import { StyleSheet } from "react-native";

export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    footer: {
        position: 'absolute',
        flex:0.1,
        left: 0,
        right: 0,
        top: 0,
        backgroundColor:'#2b2a2a',
        height:50,
        alignItems:'center',
        elevation:25,
        display: "flex", 
        flexDirection: "row", 
        flexWrap: "wrap", 
        alignContent: "center", 
        alignItems: "center", 
        justifyContent: "center",
      },
      tabSection: {
        width: 120,
        height: 220,
        margin: 10,
        // backgroundColor: '#101211'
      },
      bottomButtons: {
        width: '100%',
        margin: 10,
        alignItems:'center',
        justifyContent: 'space-around',
        flex:1,
        marginTop:9,
        borderRightColor:'#fff',
        borderLeftColor:'#2b2a2a',
        borderBottomColor:'#2b2a2a',
        borderTopColor:'#2b2a2a',
        borderWidth: 2
      },
      footerText: {
        color:'#fff',
        fontWeight:'600',
        alignItems:'center',
        fontSize:18,
        lineHeight:20,
        margin: 10
      },
      cardView: { 
          display: "flex", 
          flexDirection: "row", 
          flexWrap: "wrap", 
          alignContent: "center", 
          alignItems: "center", 
          justifyContent: "center",
          marginLeft: 10
      },
      cardSection: {
          width: 120,
          height: 220,
          margin: 10,
          // backgroundColor: '#101211'
      },
});
