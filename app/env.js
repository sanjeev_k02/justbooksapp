const API_ESAPI_POINT = 'http://esapi.justbooks.in'; // Live server
// const API_JUSTBOOK_ENDPOINT = 'https://api.justbooks.in/v1/api'; // Live server
const API_JUSTBOOK_ENDPOINT = 'http://52.66.5.24:8080/v1/api'; // Staging server
const API_AFTERLOGIN_POINT = 'https://justbooks.in'; // Live server

// RAZORPAY_ENVIRONMENT TEST
const RZP_API_KEY = 'rzp_test_A9kyfNmjRMRX6E'; // Test Credentials
const RZP_SECRET_KEY = '2IBE8JCWRVdo1nnW3EjtIHtA'; // Test Credentials

// RAZORPAY_ENVIRONMENT PROD
// const RZP_API_KEY = 'rzp_live_rtD9f5lNZHVgce'; // Live Credentials
// const RZP_SECRET_KEY = 'd9z1VRdplqAzUjWcydDKhHNu'; // Live Credentials


const API = {
    API_ESAPI_POINT: API_ESAPI_POINT,
    API_JUSTBOOK_ENDPOINT: API_JUSTBOOK_ENDPOINT,
    API_AFTERLOGIN_POINT: API_AFTERLOGIN_POINT,
    RZP_API_KEY: RZP_API_KEY,
    RZP_SECRET_KEY: RZP_SECRET_KEY
}

export default API;