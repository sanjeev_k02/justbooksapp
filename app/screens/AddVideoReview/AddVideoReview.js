import React from 'react';
import { View, Text, StatusBar, Image, PermissionsAndroid, Alert } from 'react-native';
import styles from './styles';
import { TextInput, Appbar, Divider, RadioButton, Button, Card, Title, Paragraph, Headline, Subheading, IconButton } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';
import { WebView } from 'react-native-webview';

import { showMessage, hideMessage } from "react-native-flash-message";
import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import API from '../../env';
import * as axios from 'axios';

class AddVideoReview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subscription_id: 0,
            memberCard: 0,
            branchId: 0,
            cityId: 0,
            primusMappedBranches: 0,
            primusMember: false,
            page: 1,
            video: '',
            uploadVideoUrl: '',
            title_id: this.props.route.params.title_id,
            bookTitle: this.props.route.params.bookTitle,
            filePath: {},
            videoError:  false,
            errorMsg: '',
            successError: false,
        }
    }

    componentDidMount = async () => {
        try {
          const user = await AsyncStorage.getItem('userData');
          // console.log("user",user);
          if (user !== null) {
              const userData = JSON.parse(user);
              this.setState({
                  subscription_id: userData.login.subscriptionId,
                  memberCard: userData.login.memberCard,
                  branchId: userData.login.branchId,
                  primusMappedBranches: userData.login.primusMappedBranches,
                  cityId: userData.login.cityId,
                  primusMember: userData.login.primus
              })
          } else {
              console.log("subscription Not Found");
          }
      } catch (error) {
          console.log("error", error)
      }

      try {
        let granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: "Justbooks needs Gallery Permission",
            message:
              "Justbook access to your Gallery " +
              "so you can take awesome Video Upload.",
            buttonNeutral: "Ask Me Later",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the Gallery");
        } else {
          console.log("Gallery permission denied");
        }
      } catch (err) {
        console.warn(err);
      }
    };


    chooseFile = () => {
      var options = {
        title: 'Select Video',
        mediaType: 'video',
        path:'video',
        storageOptions: {
          skipBackup: true,
          path: 'video',
        },
      };
      ImagePicker.showImagePicker(options, response => {
  
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          let source = response;
          // You can also display the image using data:
          // let source = { uri: 'data:image/jpeg;base64,' + response.data };
          console.log("video source",source);
          this.setState({
            filePath: source,
          });
        }
      });
    };

    _uploadVideo = async() => {
      console.log("video source",this.state.filePath);
        if (Object.keys(this.state.filePath).length === 0) {
            this.setState({ 
                videoError : true,
                errorMsg: 'Video is Required field!'
            })
            return;
        }
    
        // this._uploadVideoRequest();
        this._videoUploadAPI(this.state.filePath);
    }

    
    // video upload function
    async _videoUploadAPI(videoFile) {
        var videoName = 'Video_Upload'+this.state.title_id;
        const video = {
          uri: videoFile.uri,
          type: 'video/mp4',
          name: videoName
        };
        const form = new FormData();
        form.append("video", video);
        const uploadRequest =  API.API_JUSTBOOK_ENDPOINT +"/titleReview/uploadVideoReviewVideo";
        await fetch(uploadRequest).then((response) => {
          console.log("response",response);
          if (response.ok) {
            return response.json();
          } else {
            showMessage({
                message: "Video Review",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
            return;
          }
        })
        .then((responseData) => {
          // Do something with the response
          console.log("response responseData", responseData);
          if(responseData.status) {
            this.setState({ 
                refreshing: false,
                successError : true,
                uploadVideoUrl: responseData.successObject
            });
            this._uploadVideUrlToServerMethod(responseData.successObject);
          } else {
            showMessage({
              message: "Video Review",
                description: responseData.errorDescription,
                icon: "danger",
                type: "danger",
            });
          } 
        })
        .catch((error) => {
          console.log(error);
          showMessage({
              message: "Video Review",
              description: 'Something went wrong, Please try again later!',
              icon: "danger",
              type: "danger",
          });
          return;
        });
    }

    _uploadVideoRequest() {

        const uploadRequest = API.API_JUSTBOOK_ENDPOINT + "/titleReview/uploadVideoReviewVideo";
        let formData = new FormData();
        formData.append('name', 'video Upload');
        formData.append("video", this.state.filePath);
        axios.post(uploadRequest ,{
            "video" : formData
            }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response insert Register", response.data);
            if (response.data.status) {
                this.setState({ 
                    refreshing: false,
                    successError : true,
                    uploadVideoUrl: response.data.successObject
                });
                this._uploadVideUrlToServerMethod(response.data.successObject);
            } else {
                showMessage({
                  message: "Video Review",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Video Review",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _uploadVideUrlToServerMethod = (videoURL) => {

        const uploadVideoRequest = API.API_JUSTBOOK_ENDPOINT + "/titleReview/insertTitleVideoReview";
        axios.post(uploadVideoRequest ,{
          "videoUrl": videoURL,
          "subscriptionId":this.state.subscription_id,
          "titleId": this.state.title_id,
          "createdAt":810,
          "createdBy":1000
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response uploadVideoRequest", response.data);
            if (response.data.status) {
                  showMessage({
                      message: "Video Review",
                      description: response.data.successObject,
                      icon: "success",
                      type: "success",
                  });
                  setTimeout(() => {
                    this.props.navigation.push("ViewVideoReview", {
                        title_id: this.state.title_id,
                        bookTitle : this.state.bookTitle
                    });
                  }, 2000);
            } else {
                showMessage({
                  message: "Video Review",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
              message: "Video Review",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <ScrollView>
                    <View style={{ marginBottom: 5 ,marginTop: 10}}>
                        <View style={styles.cardHomeView}>
                            <Text style={styles.titleDesciption}>
                                <Headline style={styles.titleStyle}>Review for "<Text style={{color: '#273C96', fontWeight: 'bold'}}>{this.state.bookTitle}</Text>"</Headline>{'\n'}
                                <Text style={{color: 'green', fontSize: 16 }}>
                                    <Paragraph style={styles.subtitleStyle}>Size must be below 10MB.</Paragraph>
                                </Text>{'\n'}
                            </Text>
                            <View style={styles.form}>
                                {/* <TextInput
                                    label="choose Video File"
                                    value={this.state.video}
                                    style={styles.textInput}
                                    onChangeText={text => this.setState({video : text})}
                                /> */}

                                <Button icon="camera" mode="outline" labelStyle={styles.chooseStyle} style={styles.choose_btn} onPress={this.chooseFile.bind(this)} >
                                    Choose File
                                </Button>
                                {this.state.videoError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                
                                <Button icon="account-plus" mode="contained" labelStyle={styles.labelStyle} style={styles.btn} onPress={() => this._uploadVideo()}>
                                    Upload Now
                                </Button>
                            </View>

                            <View style={styles.wrapper}>
                              {/* <View style={styles.ProfileCard}>
                                    <Image
                                        source={{ uri: this.state.filePath.uri }}
                                        style={{ width: 250, height: 250 }}
                                    />
                              </View> */}
                            </View>

                        </View>
                    </View>
                </ScrollView>
                <View style={{flex: 10, marginBottom:40, alignItems: 'center'}}>
                  {/* <Text>{this.state.filePath.path}</Text> */}
                  {this.state.filePath.uri && <WebView
                        style={{ flex: 1, width: 250, }} 
                        scalesPageToFit={true}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        source={{uri: this.state.uploadVideoUrl }}
                        // source={{ html: `<html>
                        //     <body>
                        //       <video width="100%" height="600" controls>
                        //         <source src="https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/video_reviews/title_review_194_1599041865883.mp4" type="video/mp4">
                        //         Your browser does not support the video 
                        //       </video>
                        //     </body>
                        //   </html>`
                        // }}
                    />
                  }
                </View>
                <BottomTab navigation={this.props.navigation} />
            </View>
        );
    }

}

export default AddVideoReview;