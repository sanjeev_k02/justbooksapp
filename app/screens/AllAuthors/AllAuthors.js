import React from 'react';
import { View, Text, StatusBar, StyleSheet, Image, FlatList } from 'react-native';
import styles from './styles';
import { Avatar, Appbar, Divider, Button, Card, Title, Paragraph, Headline, ActivityIndicator, Colors } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';
import API from '../../env';
import * as axios from 'axios';

class AllAuthors extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          refreshing: true,
          authorFlag: false,
          authorData: {}
        };
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
  
    componentDidMount = async() => {
      this.getAuthorData();
      setTimeout(
        () => {
          this.setState({ refreshing: false });
        },
        2000
      );
    };

    getAuthorData = () => {
      const getAuthor = API.API_ESAPI_POINT+"/getPopularAuthors";
      console.log(getAuthor);
      axios.get(getAuthor)
        .then((response) => {
        //   console.log(response.data.successObject.content);
            if (response.data.status && response.data.successObject.content.length > 0) {
                this.setState({
                    authorData : response.data.successObject.content,
                    authorFlag: true
                })
            } else {
                this.setState({
                    authorFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _authorDetailsNavigate = async(author_id) => {
        this.props.navigation.navigate("AuthorDetails", { author_id });
    }
    
    render() {
        var _renderRideItem =(item,index) => {
            return (
                <View key={index} style={styles.renderItemStyle}>
                    <TouchableOpacity  onPress={()=>{this._authorDetailsNavigate(item.author_id)}}>
                        <View style={styles.card} >
                            <Image 
                                source={{ uri: item.image_url }} style={styles.imageCard}></Image>
                            <View style={styles.cardDescription}>
                                <Text style={styles.titleDesciption}>
                                    <Headline style={styles.color}>{this._toTitleCase(item.author_name)}</Headline>{'\n'}
                                    <Text style={{color: 'green', fontSize: 16 }}>By : 
                                        <Paragraph style={styles.colorParagraph}> Writer</Paragraph>
                                    </Text>{'\n'}
                                    <Rating
                                        type='custom'
                                        ratingColor='#FFD54B'
                                        ratingBackgroundColor='#F1F1F1'
                                        ratingCount={5}
                                        imageSize={20}
                                        onFinishRating={this.ratingCompleted}
                                        style={{ paddingVertical: 10 }}
                                    />
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <Divider style={styles.divider} />
                </View>
            )
        }
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                <View style={styles.activityClass}>
                    {/* <ScrollView> */}
                        <View style={{ marginBottom: 5 ,marginTop: 15}}>
                            <View style={styles.cardHomeView}>
                                {(Object.keys(this.state.authorData).length > 0 ) && (
                                    <View>
                                        <FlatList 
                                            data={this.state.authorData}
                                            ref={"flatlist"}
                                            removeClippedSubviews={true}
                                            initialNumToRender={10}
                                            bounces={false}
                                            renderItem={({ item, index }) => _renderRideItem(item, index)}
                                            keyExtractor={(item, index) => item.id}
                                            bounces={false}
                                            onEndReachedThreshold={0.5}
                                        />
                                    </View>
                                )}
                            </View>
                        </View>
                    {/* </ScrollView> */}
                    <SearchTab navigation={this.props.navigation} />
                    <BottomTab navigation={this.props.navigation} />
                </View>
                }
            </View>
        );
    }

}

export default AllAuthors;