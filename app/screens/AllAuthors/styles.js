import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    activityClass: {
        paddingBottom: 40
    },
    headingView: {
        marginTop: 0,
        marginBottom: 0,
        marginRight: 10,
        marginLeft: 10
    },
    heading: {
        color: '#fff',
        fontSize: 24,
        fontWeight: '600'
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 15, 
        marginTop: 0, 
        paddingLeft: 15,
        paddingTop: 15,
    },
    cardView: {
        backgroundColor: '#FFFFFF', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 0, 
        marginTop: 0,
    },
    cardSection: {
        width: 120,
        height: 225,
        margin: 10,
        // backgroundColor: '#FFFFFF'
    },
    coverImage: {
        height: 160,
    },
    card: {
        width: '90%',
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    imageCard: {
        width: 90, 
        height: 125,
        borderRadius: 4
    },
    cardDescription : {
        width: '80%',
        flexDirection: 'column',
        marginLeft: '6%', 
        marginTop: -5, 
        justifyContent: 'space-between'
    },
    titleDesciption: {
        marginTop: 5,
        marginRight: 30
    },
    color: {
        color: '#2E2E2E',
    },
    colorParagraph: {
        color: 'pink',
    },
    title: {
        width: '100%',
        color: '#2E2E2E',
        fontSize: 16,
        lineHeight: 15,
        marginTop: 8,
        marginBottom: 0,
        fontWeight: '600'
    },
    Paragraph: {
        marginTop: 0,
        width: '100%',
        fontSize: 12,
        color: '#2E2E2E'
    },
    divider : {
        backgroundColor: '#8F8E94', 
        marginTop: 15, 
        marginBottom: 15
    }

});
