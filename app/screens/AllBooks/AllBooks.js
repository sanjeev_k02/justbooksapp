import React from 'react';
import { View, Text, RefreshControl, Image } from 'react-native';
import styles from './styles';
import { Divider, Title, Paragraph, ActivityIndicator, Colors } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';
import API from '../../env';
import * as axios from 'axios';

class AllBooks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          subscription_id: 0,
          memberCard: 0,
          branchId: 0,
          cityId: 0,
          primusMappedBranches: 0,
          primusMember: false,
          page: 1,
          userDetails: {},
          refreshing: true,
          authorFlag: false,
          authorData: {},
          newArrivlesFlag: false,
          newArrivlesData: {},
          shelfRecommendedBooksFlag: false,
          shelfRecommendedBooks: {},
          comingSoonBooksFlag: false,
          comingSoonBooks: {},
          topMagazineBooksFlag: false,
          topMagazineBooks: {},
          topTitleFlag: false,
          topTitleData: {},
          action:  this.props.route.params.action
        };
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }

    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard,
                    branchId: userData.login.branchId,
                    primusMappedBranches: userData.login.primusMappedBranches,
                    cityId: userData.login.cityId,
                    primusMember: userData.login.primus
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }
        
        await this._allActionBookDisplay(this.state.action);
        setTimeout(() => {
            this.setState({ refreshing: false });
        }, 2000);
    };

    _allActionBookDisplay = async (getAction) => {
        switch(getAction) {
            case 'newArrivlesData':
                if(this.state.subscription_id > 0) {
                    await this.getAllNewArrivlesAfterLogin(this.state.subscription_id);
                } else {
                    await this.getAllNewArrivles();
                }
                break;
            case 'topTitleData':
                await this.getAllTopTitle(this.state.subscription_id);
                break;
            case 'comingSoonBooks':
                if(this.state.subscription_id > 0) {
                    await this.getComingSoonAfterLogin();
                }
                break;
            case 'topMagazineBooks':
                if(this.state.subscription_id > 0) {
                    await this.getAllTopMagazineBooksAfterLogin();
                }
                break;
            case 'shelfRecommendedBook':
                if(this.state.subscription_id > 0) {
                    await this.getAllShelfRecommendedBooksAfterLogin();
                }
                break;
            default:
                console.log("defalut book found");
        }

    }

    getAllNewArrivlesAfterLogin = (subscriptionId) => {
        let getNewArrivles;
        if(this.state.primusMember) {
            getNewArrivles = API.API_ESAPI_POINT+"/getNewArrivalsForPrimus?subscriptionId="+subscriptionId+"&branchIds="+this.state.primusMappedBranches+"&cityId="+this.state.cityId;
        } else { 
            getNewArrivles = API.API_ESAPI_POINT+"/getNewArrivalsMobile?subscriptionId="+subscriptionId+"&branchId="+this.state.branchId;
        } 
      console.log(getNewArrivles);
      axios.get(getNewArrivles)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.content.length > 0) {
                this.setState({
                    newArrivlesData : response.data.successObject.content,
                    newArrivlesFlag: true
                })
            } else {
                this.setState({
                    newArrivlesFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllShelfRecommendedBooksAfterLogin = () => {
      const getShelfRecommendedBooks = API.API_ESAPI_POINT+"/getRecommendedTitles?membershipNo="+this.state.memberCard;
      console.log(getShelfRecommendedBooks);
      axios.get(getShelfRecommendedBooks)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    shelfRecommendedBooks : response.data.successObject,
                    shelfRecommendedBooksFlag: true
                })
            } else {
                this.setState({
                    shelfRecommendedBooksFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllTopMagazineBooksAfterLogin = () => {
      const getTopMagazineBooks = API.API_ESAPI_POINT+"/getTopMagazinesAvailableForBranch?branchId="+this.state.branchId+"&page="+this.state.page;
      console.log(getTopMagazineBooks);
      axios.get(getTopMagazineBooks)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    topMagazineBooks : response.data.successObject,
                    topMagazineBooksFlag: true
                })
            } else {
                this.setState({
                    topMagazinesBooksFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getComingSoonAfterLogin = () => {
      const getComingSoonBooks = API.API_ESAPI_POINT+"/getComingSoonBooks?page="+this.state.page;
      console.log(getComingSoonBooks);
      axios.get(getComingSoonBooks)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    comingSoonBooks : response.data.successObject,
                    comingSoonBooksFlag: true
                })
            } else {
                this.setState({
                    comingSoonBooksFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllNewArrivles = () => {
      const getNewArrivles = API.API_ESAPI_POINT+"/getNewArrivalsMobile";
      console.log(getNewArrivles);
      axios.get(getNewArrivles)
        .then((response) => {
        //   console.log(response.data.successObject.content);
            if (response.data.status && response.data.successObject.content.length > 0) {
                this.setState({
                    newArrivlesData : response.data.successObject.content,
                    newArrivlesFlag: true
                })
            } else {
                this.setState({
                    newArrivlesFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllAuthorData = () => {
      const getAuthor = API.API_ESAPI_POINT+"/getPopularAuthors";
      console.log(getAuthor);
      axios.get(getAuthor)
        .then((response) => {
        //   console.log(response.data.successObject.content);
            if (response.data.status && response.data.successObject.content.length > 0) {
                this.setState({
                    authorData : response.data.successObject.content,
                    authorFlag: true
                })
            } else {
                this.setState({
                    authorFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllTopTitle = (subscriptionId) => {
      let getTopTitle = API.API_ESAPI_POINT+"/getTopTitles";
      if(subscriptionId > 0 && this.state.primusMember) {
        getTopTitle = API.API_ESAPI_POINT+"/getTopTitlesForPrimus?subscriptionId="+subscriptionId+"&page="+this.state.page+"&branchIds="+this.state.primusMappedBranches+"&cityId="+this.state.cityId;
      } else {
        getTopTitle = API.API_ESAPI_POINT+"/getTopTitles?subscriptionId="+subscriptionId+"&page="+this.state.page;
      }
      console.log(getTopTitle);
      axios.get(getTopTitle)
        .then((response) => {
        //   console.log(response.data.successObject.content);
            if (response.data.status && response.data.successObject.content.length > 0) {
                this.setState({
                    topTitleData : response.data.successObject.content,
                    topTitleFlag: true
                })
            } else {
                this.setState({
                    topTitleFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _onRefresh = async () => {
        this.setState({refreshing: true});

        await this._allActionBookDisplay(this.state.action);
        
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
    }

    _bookDetailsNavigate = async(title_id) => {
        this.props.navigation.push("BookDetails", { title_id });
    }

    _avgRating = (rating) => {
        var avg_reading_time = rating ? rating : 0;
        while(avg_reading_time > 5){
            avg_reading_time = avg_reading_time / 5;
        }
        return avg_reading_time;
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { (this.state.refreshing) ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView 
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            <View style={{ marginBottom: 5 ,marginTop: 15}}>
                                <View style={styles.cardHomeView}>

                                    {this.state.shelfRecommendedBooksFlag && <View style={styles.newArrivles}>
                                        {this.state.shelfRecommendedBooks.map((item)=> {
                                            return (
                                                <View key={item.title_id} style={styles.bindView}>
                                                    <View style={styles.card} >
                                                       <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.title_id)}}>
                                                            {(item.image_url != 'null' && item.image_url != null && item.image_url != '') ? <Image 
                                                                source={{ uri: item.image_url }} style={styles.imageCard}>
                                                            </Image>
                                                            : <Image 
                                                                source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} style={styles.imageCard}>
                                                            </Image>}
                                                            {this.state.primusMember && <View style={styles.primusTag}>
                                                                <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                            </View>}
                                                        </TouchableOpacity>
                                                            <View style={styles.cardDescription}>
                                                                <TouchableOpacity key={item.title_id} onPress={()=>{this._bookDetailsNavigate(item.title_id)}}>
                                                                    <Text style={styles.titleDesciption}>
                                                                        <Title style={styles.color}>{this._toTitleCase(item.title)}</Title>{'\n'}

                                                                        {(item.author != "" && item.author != null) && <Text style={{color: 'green', fontSize: 16 }}
                                                                            >By : 
                                                                            <Paragraph style={styles.colorParagraph}> {this._toTitleCase(item.author)}</Paragraph>{'\n'}
                                                                        </Text>}

                                                                        {(item.rented != "" && item.rented != null) && <Text style={{color: 'green', fontSize: 16 }}>
                                                                            <Paragraph style={styles.colorParagraph}>No of times read : {item.rented}</Paragraph>{'\n'}
                                                                        </Text>}
                                                                    </Text>
                                                                </TouchableOpacity>
                                                                <Text style={styles.titleDesciption}>
                                                                    <Rating
                                                                        startingValue={ item.rented ? this._avgRating(item.rented): 0 }
                                                                        ratingCount={5}
                                                                        imageSize={20}
                                                                        ratingBackgroundColor='#FFD54B'
                                                                        onFinishRating={this.ratingCompleted}
                                                                        style={{ paddingVertical: 10 }}
                                                                    />
                                                                </Text>

                                                                {/* <View style={styles.actionStyle}>
                                                                    <View style={styles.footer}>
                                                                        <TouchableOpacity style={styles.bottomButtons} onPress={()=>{this._rentNowAction('rent',this.state.title_id)}} >
                                                                            <Icons name="book-outline" size={26} color={'#000000'} />
                                                                            <Text style={styles.footerText}>Rent</Text>
                                                                        </TouchableOpacity>

                                                                        {this.state.wishlistAvailable ? 
                                                                            (<TouchableOpacity style={styles.bottomButtons} onPress={()=>this._wishlistAction('remove',this.state.title_id)} >
                                                                                <Icons name="heart" size={26} color={'red'} />
                                                                                <Text style={styles.footerText}>Wishlist</Text>
                                                                            </TouchableOpacity> )
                                                                            : 
                                                                            (<TouchableOpacity style={styles.bottomButtons} onPress={()=>this._wishlistAction('add',this.state.title_id)} >
                                                                            <Icons name="heart-outline" size={26} color={'#000000'} />
                                                                            <Text style={styles.footerText}>Wishlist</Text>
                                                                        </TouchableOpacity>)
                                                                        }
                                                                        
                            
                                                                        <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(this._toTitleCase(this.state.titleDetails.jb_info.title))} >
                                                                            <Icons name="share-variant" size={26} color={'#000000'} />
                                                                            <Text style={styles.footerText}>Share</Text>
                                                                        </TouchableOpacity>
                                                                    </View>
                                                                </View> */}
                                                            </View>
                                                        </View>
                                                    <Divider style={styles.divider} /> 
                                                </View>              
                                            )
                                        })}
                                    </View>}

                                    {this.state.topMagazineBooksFlag && <View style={styles.newArrivles}>
                                        {this.state.topMagazineBooks.map((item)=> {
                                            return (
                                                <View key={item.titleId} style={styles.bindView}>
                                                    <View style={styles.card} >
                                                        <TouchableOpacity onPress={()=>console.log("magnize Clicked")}>
                                                            {(item.imageUrl != 'null' && item.imageUrl != null && item.imageUrl != '') ? <Image 
                                                                source={{ uri: item.imageUrl }} style={styles.imageCard}>
                                                            </Image>
                                                            : <Image 
                                                                source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} style={styles.imageCard}>
                                                            </Image>}
                                                            {this.state.primusMember && <View style={styles.primusTag}>
                                                                <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                            </View>}
                                                            </TouchableOpacity>
                                                            <View style={styles.cardDescription}>
                                                                <TouchableOpacity key={item.titleId} onPress={()=>console.log("magnize Clicked")}>
                                                                    <Text style={styles.titleDesciption}>
                                                                        <Title style={styles.color}>{this._toTitleCase(item.title)}</Title>{'\n'}

                                                                        {(item.totalRents != "" && item.totalRents != null) && <Text style={{color: 'green', fontSize: 16 }}>
                                                                            <Paragraph style={styles.colorParagraph}>No of times read : {item.totalRents}</Paragraph>{'\n'}
                                                                        </Text>}
                                                                    </Text>
                                                                </TouchableOpacity>
                                                                <Text style={styles.titleDesciption}>
                                                                    <Rating
                                                                        startingValue={ item.totalRents ? this._avgRating(item.totalRents): 0 }
                                                                        ratingCount={5}
                                                                        imageSize={20}
                                                                        ratingBackgroundColor='#FFD54B'
                                                                        onFinishRating={this.ratingCompleted}
                                                                        style={{ paddingVertical: 10 }}
                                                                    />
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    <Divider style={styles.divider} /> 
                                                </View>
                                            )
                                        })}
                                    </View>}

                                    {this.state.comingSoonBooksFlag && <View style={styles.newArrivles}>
                                        {this.state.comingSoonBooks.map((item)=> {
                                            return (
                                                <View key={item.titleId} style={styles.bindView}>
                                                    <View style={styles.card} >
                                                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}}>
                                                            {(item.imageUrl != 'null' && item.imageUrl != null && item.imageUrl != '') ? <Image 
                                                                source={{ uri: item.imageUrl }} style={styles.imageCard}>
                                                            </Image>
                                                            : <Image 
                                                                source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} style={styles.imageCard}>
                                                            </Image>}
                                                            {this.state.primusMember && <View style={styles.primusTag}>
                                                                <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                            </View>}
                                                            </TouchableOpacity>
                                                            <View style={styles.cardDescription}>
                                                                <Text style={styles.titleDesciption}>
                                                                    <Title style={styles.color}>{this._toTitleCase(item.title)}</Title>{'\n'}

                                                                    {(item.author != "" && item.author != null) && <Text style={{color: 'green', fontSize: 16 }}
                                                                        >By : 
                                                                        <Paragraph style={styles.colorParagraph}> {this._toTitleCase(item.author)}</Paragraph>{'\n'}
                                                                    </Text>}
                                                                </Text>
                                                                <Text style={styles.titleDesciption}>
                                                                    <Rating
                                                                        startingValue={ item.rented ? this._avgRating(item.rented): 0 }
                                                                        ratingCount={5}
                                                                        imageSize={20}
                                                                        ratingBackgroundColor='#FFD54B'
                                                                        onFinishRating={this.ratingCompleted}
                                                                        style={{ paddingVertical: 10 }}
                                                                    />
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    <Divider style={styles.divider} /> 
                                                </View>
                                            )
                                        })}
                                    </View>}

                                    {this.state.newArrivlesFlag && <View style={styles.newArrivles}>
                                        {this.state.newArrivlesData.map((item)=> {
                                            return (
                                                <View key={item.title_id} style={styles.bindView}>
                                                    <View style={styles.card} >
                                                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.title_id)}}>
                                                            {(item.image_url != 'null' && item.image_url != null) ? <Image 
                                                                source={{ uri: item.image_url }} style={styles.imageCard}>
                                                            </Image>
                                                            : <Image 
                                                                source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} style={styles.imageCard}>
                                                            </Image>}
                                                            {this.state.primusMember && <View style={styles.primusTag}>
                                                                <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                            </View>}
                                                        </TouchableOpacity>
                                                            <View style={styles.cardDescription}>
                                                                <TouchableOpacity key={item.title_id} onPress={()=>{this._bookDetailsNavigate(item.title_id)}}>
                                                                    <Text style={styles.titleDesciption}>
                                                                        <Title style={styles.color}>{this._toTitleCase(item.title)}</Title>{'\n'}

                                                                        {(item.author != "" && item.author != null) && <Text style={{color: 'green', fontSize: 16 }}
                                                                            >By : 
                                                                            <Paragraph style={styles.colorParagraph}> {this._toTitleCase(item.author)}</Paragraph>{'\n'}
                                                                        </Text>}

                                                                        {(item.rented != "" && item.rented != null) && <Text style={{color: 'green', fontSize: 16 }}>
                                                                            <Paragraph style={styles.colorParagraph}>No of times read : {item.rented}</Paragraph>{'\n'}
                                                                        </Text>}
                                                                    </Text>
                                                                </TouchableOpacity>
                                                                <Text style={styles.titleDesciption}>
                                                                    <Rating
                                                                        startingValue={ item.rented ? this._avgRating(item.rented): 0 }
                                                                        ratingCount={5}
                                                                        imageSize={20}
                                                                        ratingBackgroundColor='#FFD54B'
                                                                        onFinishRating={this.ratingCompleted}
                                                                        style={{ paddingVertical: 10 }}
                                                                    />
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    <Divider style={styles.divider} /> 
                                                </View>
                                            )
                                        })}
                                        <Divider style={styles.divider} />
                                    </View>}
                                    
                                    {this.state.topTitleFlag && <View style={styles.bindDiv}>
                                        {this.state.topTitleData.map((item)=> {
                                            return (
                                                <View key={item.id} style={styles.bindView}>
                                                    <View style={styles.card} >
                                                        <TouchableOpacity key={item.id} onPress={()=>{this._bookDetailsNavigate(item.title_id)}}>
                                                            {(item.image_url != null && item.image_url != "null" && item.image_url != '') ? <Image 
                                                                source={{ uri: item.image_url }} style={styles.imageCard}>
                                                            </Image>
                                                            : <Image 
                                                                source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} style={styles.imageCard}>
                                                            </Image>}
                                                            {this.state.primusMember && <View style={styles.primusTag}>
                                                                <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                            </View>}
                                                        </TouchableOpacity>
                                                            <View style={styles.cardDescription}>
                                                                <TouchableOpacity key={item.id} onPress={()=>{this._bookDetailsNavigate(item.title_id)}}>
                                                                    <Text style={styles.titleDesciption}>
                                                                        <Title style={styles.color}>{this._toTitleCase(item.title)}</Title>{'\n'}

                                                                        {(item.author != "" && item.author != null) && <Text style={{color: 'green', fontSize: 16 }}
                                                                            >By : 
                                                                            <Paragraph style={styles.colorParagraph}> {this._toTitleCase(item.author)}</Paragraph>{'\n'}
                                                                        </Text>}

                                                                        {(item.rented != "" && item.rented != null) && <Text style={{color: 'green', fontSize: 16 }}>
                                                                            <Paragraph style={styles.colorParagraph}>No of times read : {item.rented}</Paragraph>{'\n'}
                                                                        </Text>}
                                                                    </Text>
                                                                </TouchableOpacity>
                                                                <Text style={styles.titleDesciption}>
                                                                    <Rating
                                                                        startingValue={ item.rented ? this._avgRating(item.rented): 0 }
                                                                        type='custom'
                                                                        ratingColor='#FFD54B'
                                                                        ratingBackgroundColor='#F1F1F1'
                                                                        ratingCount={5}
                                                                        imageSize={20}
                                                                        onFinishRating={this.ratingCompleted}
                                                                        style={{ paddingVertical: 10 }}
                                                                    />
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    <Divider style={styles.divider} /> 
                                                </View>
                                            )
                                        })}
                                    </View>}
                                    
                                </View>
                            </View>
                        </ScrollView>
                        <SearchTab navigation={this.props.navigation} />
                        <BottomTab navigation={this.props.navigation} />
                     </View>
                }
            </View>
        );
    }

}

export default AllBooks;