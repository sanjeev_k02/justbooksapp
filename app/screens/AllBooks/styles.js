import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    headingView: {
        marginTop: 0,
        marginBottom: 0,
        marginRight: 10,
        marginLeft: 10
    },
    heading: {
        color: '#fff',
        fontSize: 24,
        fontWeight: '600'
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },
    cardView: {
        backgroundColor: '#FFFFFF', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 0, 
        marginTop: 0,
    },
    cardSection: {
        width: 120,
        height: 225,
        margin: 10,
        // backgroundColor: '#FFFFFF'
    },
    coverImage: {
        height: 160,
    },
    card: {
        width: '90%',
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    imageCard: {
        width: 90, 
        height: 140,
        borderRadius: 4
    },
    cardDescription : {
        width: '80%',
        flexDirection: 'column',
        marginLeft: '6%', 
        marginTop: -5, 
        justifyContent: 'space-between'
    },
    titleDesciption: {
        marginTop: 5,
        marginRight: 30
    },
    color: {
        color: '#2E2E2E',
    },
    colorParagraph: {
        color: 'pink',
    },
    title: {
        width: '100%',
        color: '#273C96',
        fontSize: 16,
        lineHeight: 15,
        marginTop: 8,
        marginBottom: 0,
        fontWeight: '600'
    },
    Paragraph: {
        marginTop: 0,
        width: '100%',
        fontSize: 12,
        color: '#273C96'
    },
    divider : {
        backgroundColor: '#8F8E94', 
        marginTop: 15, 
        marginBottom: 15
    },
    primusImage: {
      width: 80, 
      height: 35,
   },
   primusTag: {
      width: 80, 
      height: 35, 
      position: 'absolute', 
      bottom: 5,
      left: 0
   },
   footer: {
       position: 'absolute',
       flex:1,
       left: 0,
       right: 0,
       bottom: -10,
       flexDirection:'row',
       height:60,
       alignItems:'center',
       elevation:25,
    },
    bottomButtons: {
      alignItems:'center',
      justifyContent: 'space-around',
      flex:1,
      marginTop:9,
      marginRight: 15
    },
    footerText: {
      color:'#2E2E2E',
      fontWeight:'600',
      alignItems:'center',
      fontSize:11,
      lineHeight:20,
      marginBottom:12
    },

});
