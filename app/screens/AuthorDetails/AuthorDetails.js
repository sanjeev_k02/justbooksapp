import React from 'react';
import { View, Text, StatusBar, StyleSheet, Image } from 'react-native';
import styles from './styles';
import { Avatar, Appbar, Divider, Card, Title, Paragraph, Headline, ActivityIndicator, Colors } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { ScrollView } from 'react-native-gesture-handler';
import ReadMore from 'react-native-read-more-text';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';
import API from '../../env';
import * as axios from 'axios';

class AuthorDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          refreshing: true,
          author_id: this.props.route.params.author_id,
          authorFlag: false,
          authorDetails: {},
          allAuthorData: {}
        };
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }

    _toRelatedTitleCase(str) { 
        if(str.length > 18) {
            var res = str.substring(0, 18);
            return res.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            )+"...";
        } else {
            return str.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            );
        }
    }
  
    componentDidMount = async() => {
      this.getAuthorDetails(this.state.author_id);
      this.getRelatedAuthorData(this.state.author_id);
      setTimeout(
        () => {
          this.setState({ refreshing: false });
        },
        2000
      );
    };

    getAuthorDetails = (authorId) => {
      const getAuthor = API.API_ESAPI_POINT+"/getAuthorDetails?authorId="+authorId;
      console.log(getAuthor);
      axios.get(getAuthor)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status) {
                this.setState({
                    authorDetails : response.data.successObject,
                    refreshing: false
                })
            } else {
                this.setState({
                    refreshing: true
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getRelatedAuthorData = (authorId) => {
      const getRelatedAuthor = API.API_ESAPI_POINT+"/getRelatedAuthors?authorId="+authorId;
      axios.get(getRelatedAuthor)
        .then((response) => {
        //   console.log(response.data.successObject.content);
            if (response.data.status && response.data.successObject.content.length > 0) {
                this.setState({
                    allAuthorData : response.data.successObject.content,
                    authorFlag: true
                })
            } else {
                this.setState({
                    authorFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _authorDetailsNavigate = async(author_id) => {
        // this.setState({ refreshing: true });
        this.props.navigation.push("AuthorDetails",{author_id})
        // this.getAuthorDetails(author_id);
    }

    _allAuthorNavigate = async() => {
        this.props.navigation.navigate("AllAuthors");
    }

    _renderTruncatedFooter = (handlePress) => {
        return (
            <Paragraph style={{color: '#273C96', marginTop: 2, textDecorationLine: 'underline'}} onPress={handlePress}>Read more</Paragraph>
        );
    }
    
    _renderRevealedFooter = (handlePress) => {
        return (
            <Paragraph style={{color: '#273C96', marginTop: 2, textDecorationLine: 'underline'}} onPress={handlePress}>Show less</Paragraph>
        );
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    :
                    <View style={styles.activityClass}>
                        <ScrollView>
                            <View style={{ marginBottom: 15 ,marginTop: 25}}>
                                <View style={styles.cardHomeView}>
                                    <View style={styles.card}>
                                        {(this.state.authorDetails.image_url != null && this.state.authorDetails.image_url != 'null' && this.state.authorDetails.image_url != '') ? (<Image source={{ uri: this.state.authorDetails.image_url }} style={styles.imageCard}></Image>)
                                        : <Image 
                                            source={{ uri: "https://justbooks.in/assets/img/user.png" }} style={styles.imageCard}>
                                        </Image>}
                                        <View style={styles.cardDescription}>
                                            <Text style={styles.titleDesciption}>
                                                <Headline style={styles.color}>{this._toTitleCase(this.state.authorDetails.author_name)}</Headline>{'\n'}
                                                <Text style={{color: 'green', fontSize: 16 }}>By : 
                                                    <Paragraph style={styles.colorParagraph}> Writer</Paragraph>
                                                </Text>{'\n'}
                                                <Rating
                                                    type='custom'
                                                    ratingColor='#FFD54B'
                                                    ratingBackgroundColor='#F1F1F1'
                                                    ratingCount={5}
                                                    imageSize={20}
                                                    onFinishRating={this.ratingCompleted}
                                                    style={{ paddingVertical: 10 }}
                                                />
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{marginTop: 30}}>
                                        {/* <Paragraph style={styles.color}>{this.state.authorDetails.description}</Paragraph> */}
                                        <ReadMore
                                            numberOfLines={3}
                                            renderTruncatedFooter={this._renderTruncatedFooter}
                                            renderRevealedFooter={this._renderRevealedFooter}>
                                                <Paragraph style={styles.color}>{this.state.authorDetails.description}</Paragraph>
                                        </ReadMore>
                                    </View>

                                    <Divider style={{backgroundColor: '#6f6c6c', marginTop: 20}} />
                                    {this.state.authorFlag && <View style={styles.book}>
                                        <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation: 0, marginLeft: -15}}>
                                            <Appbar.Content title="Others Authors" titleStyle={styles.headingStyle} style={styles.heading} />
                                            <Appbar.Content title="More" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._allAuthorNavigate()}} />
                                        </Appbar.Header>
                                        <View style={styles.cardView}>
                                            <ScrollView horizontal={true}>
                                                {this.state.allAuthorData.map((item)=> {
                                                    return (
                                                        <Card key={item.id} style={styles.cardSection} onPress={()=>{this._authorDetailsNavigate(item.author_id)}}>
                                                            {(item.image_url != '' && item.image_url != null && item.image_url != "null") ? <Card.Cover style={styles.coverImage} source={{ uri: item.image_url }} />
                                                            : <Card.Cover style={styles.coverImage} source={{ uri: "https://justbooks.in/assets/img/user.png" }} />}
                                                            <Card.Content>
                                                                <Title style={styles.title}>{this._toRelatedTitleCase(item.author_name)}</Title>
                                                                {/* <Paragraph style={styles.Paragraph}> Writer</Paragraph> */}
                                                            </Card.Content>
                                                        </Card>
                                                    )
                                                })}
                                            </ScrollView>
                                        </View>
                                    </View>}
                                </View>
                            </View>
                        </ScrollView>
                        <SearchTab navigation={this.props.navigation} />
                        <BottomTab navigation={this.props.navigation} />
                    </View>
                    }
            </View>
        );
    }

}

export default AuthorDetails;