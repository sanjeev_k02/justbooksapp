import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    headingView: {
        marginTop: 0,
        marginBottom: 0,
        marginRight: 10,
        marginLeft: 10
    },
    heading: {
        color: '#fff',
        fontSize: 24,
        fontWeight: '600'
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },
    cardView: {
        backgroundColor: '#FFFFFF', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 20, 
        marginTop: 0,
    },
    cardSection: {
        width: 120,
        height: 210,
        margin: 10,
        backgroundColor: '#FFFFFF'
    },
    coverImage: {
        height: 160,
    },
    card: {
        width: '90%',
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    imageCard: {
        width: 120, 
        height: 160,
        resizeMode: 'stretch',
        borderRadius: 2
    },
    cardDescription : {
        width: '80%',
        flexDirection: 'column',
        marginLeft: '6%', 
        marginTop: -5, 
        justifyContent: 'space-between'
    },
    titleDesciption: {
        marginTop: 5,
        marginRight: 30
    },
    color: {
        color: '#2E2E2E',
    },
    colorParagraph: {
        color: 'pink',
    },
    title: {
        width: '100%',
        color: '#273C96',
        fontSize: 14,
        lineHeight: 15,
        paddingTop: 10,
        marginBottom: 0,
        fontWeight: '500'
    },
    Paragraph: {
        marginTop: 0,
        width: '100%',
        fontSize: 12,
        color: '#273C96'
    },
    author: {
        marginTop: 20,
    },
    book: {
        marginTop: 12,
    },
    heading: {
        color: '#2E2E2E',
        fontSize: 24,
        fontWeight: '600'
    },
    SeeAllHeading: {
        color: '#2E2E2E',
        fontSize: 14,
        fontWeight: '600',
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end'
    },
    titleStyle : {
        fontSize: 16, 
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end'
    },
    headingStyle : {
        fontSize: 22, 
        fontWeight: '600'
    },

});
