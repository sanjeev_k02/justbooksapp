import React from "react";
import {
  View,
  Text,
  Alert,
  Image,
  TouchableHighlight,
  Share,
} from "react-native";
import styles from "./styles";
import {
  Avatar,
  Appbar,
  Divider,
  TouchableRipple,
  Card,
  Title,
  Paragraph,
  Headline,
  ActivityIndicator,
  Colors,
} from "react-native-paper";
import { Rating, AirbnbRating } from "react-native-ratings";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import AsyncStorage from "@react-native-community/async-storage";
import Icons from "react-native-vector-icons/MaterialCommunityIcons";
import Modal from "react-native-modal";
import { WebView } from "react-native-webview";
import ReadMore from "react-native-read-more-text";
import { showMessage, hideMessage } from "react-native-flash-message";
import AwesomeAlert from "react-native-awesome-alerts";

import StatusTopBar from "../../components/StatusTopBar/StatusTopBar";
import Header from "../../components/Header/Header";
import BottomTab from "../../components/BottomTab/BottomTab";
import SearchTab from "../../components/SearchTab/SearchTab";
import API from "../../env";
import * as axios from "axios";

class BookDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      subscription_id: 0,
      memberCard: 0,
      branchId: 0,
      primusMappedBranches: 0,
      cityId: 0,
      primusMember: false,
      webUser: false,
      page: 1,
      isModalVisible: false,
      successError: false,
      bookNotFoundError: false,
      wishlistAvailable: false,
      wishlistId: 0,
      refreshing: true,
      title_id: this.props.route.params.title_id,
      imageLoading : true,
      relatedTitleFlag: false,
      titleDetails: {},
      average_rating: 0,
      allRelatedTitleData: {},
      authorFlag: false,
      authorData: {},
      isWebUserAlert: false,
      rentNowAlert: false,
      webUserAlert: false,
      rentNowTitleId: 0,
    };
  }

  _toTitleCase(str) {
    if (str.length > 45) {
      var res = str.substring(0, 45);
      return (
        res.replace(/\w\S*/g, function (txt) {
          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) + "..."
      );
    } else {
      return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    }
  }

  _toRelatedTitleCase(str) {
    if (str.length > 16) {
      var res = str.substring(0, 16);
      return (
        res.replace(/\w\S*/g, function (txt) {
          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        }) + ""
      );
    } else {
      return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    }
  }

  componentDidMount = async () => {
    try {
      const user = await AsyncStorage.getItem("userData");
      if (user !== null) {
        const userData = JSON.parse(user);
        // console.log("us",userData);
        this.setState({
          subscription_id: userData.login.subscriptionId,
          memberCard: userData.login.memberCard,
          branchId: userData.login.branchId,
          primusMappedBranches: userData.login.primusMappedBranches,
          cityId: userData.login.cityId,
          primusMember: userData.login.primus,
          webUser: userData.webUser,
        });
      } else {
        console.log("subscription Not Found");
      }
    } catch (error) {
      console.log("error", error);
    }
    // console.log(this.state);
    this.getTitleDetails(this.state.title_id, this.state.subscription_id);
    this.getReleatedTitleData(
      this.state.title_id,
      this.state.subscription_id,
      this.state.primusMember,
      this.state.cityId
    );
    this.getAllAuthorData();
    // setTimeout(() => {
    //   this.setState({ refreshing: false });
    // }, 2000);
  };

  getTitleDetails = (titleId, subscriptionId) => {
    let getTitle;
    if (subscriptionId > 0) {
      getTitle =
        API.API_ESAPI_POINT +
        "/getTitleDetails?titleId=" +
        titleId +
        "&subscriptionId=" +
        subscriptionId;
    } else {
      getTitle = API.API_ESAPI_POINT + "/getTitleDetails?titleId=" + titleId;
    }
    console.log(getTitle);
    axios
      .get(getTitle)
      .then((response) => {
        //   console.log(response.data.successObject);
        if (response.data.status) {
          var avg_reading_time = response.data.successObject.jb_info
            .avg_reading_time
            ? response.data.successObject.jb_info.avg_reading_time
            : 0;
          while (avg_reading_time > 5) {
            avg_reading_time = avg_reading_time / 5;
          }
          this.setState({
            titleDetails: response.data.successObject,
            wishlistAvailable: response.data.successObject.wishlistAvailable,
            refreshing: false,
            average_rating: (Math.round(avg_reading_time * 100) / 100).toFixed(
              2
            ),
          });
        } else {
          this.setState({
            refreshing: false,
            bookNotFoundError: true,
            errorMsg: response.data.errorDescription,
          });
          setTimeout(() => {
            this.props.navigation.navigate("Home");
          }, 1000);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  getReleatedTitleData = (titleId, subscriptionId, primusMember, cityId) => {
    let getReleatedTitle;
    if (subscriptionId > 0 && primusMember) {
      getReleatedTitle =
        API.API_ESAPI_POINT +
        "/getRelatedTitlesForPrimus?titleId=" +
        titleId +
        "&subscriptionId=" +
        subscriptionId +
        "&branchIds=" +
        this.state.primusMappedBranches +
        "&cityId=" +
        cityId;
    } else if (subscriptionId > 0) {
      getReleatedTitle =
        API.API_ESAPI_POINT +
        "/getRelatedTitles?titleId=" +
        titleId +
        "&subscriptionId=" +
        subscriptionId;
    } else {
      getReleatedTitle =
        API.API_ESAPI_POINT + "/getRelatedTitles?titleId=" + titleId;
    }
    console.log(getReleatedTitle);
    axios
      .get(getReleatedTitle)
      .then((response) => {
        //   console.log(response.data.successObject);
        if (response.data.status && response.data.successObject.length > 0) {
          this.setState({
            allRelatedTitleData: response.data.successObject,
            relatedTitleFlag: true,
          });
        } else {
          this.setState({
            relatedTitleFlag: false,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  _videoReview = (title_id, bookTitle) => {
    if (this.state.subscription_id == 0) {
      showMessage({
        message: "Video Review",
        description: "Become a member to review books!!",
        icon: "danger",
        type: "danger",
      });
    } else {
      this.props.navigation.navigate("ViewVideoReview", {
        title_id: title_id,
        bookTitle: bookTitle,
      });
    }
  };

  handleSeeVideo = () => {
    this.setState({ isModalVisible: false });
    this.props.navigation.navigate("VideoReview");
  };

  handleUploadVideo = () => {
    this.setState({ isModalVisible: false });
    console.log("uploadNew Video");
    this.props.navigation.navigate("AddVideoReview", {
      bookTitle: "Harry Portter",
    });
  };

  _authorDetailsNavigate = async (author_id) => {
    this.props.navigation.push("AuthorDetails", { author_id });
  };

  _allAuthorNavigate = async () => {
    this.props.navigation.push("AllAuthors");
  };

  _bookDetailsNavigate = async (title_id) => {
    // this.getTitleDetails(title_id, this.state.subscription_id);
    this.props.navigation.push("BookDetails", { title_id });
  };

  _shareBookDetails = async (title) => {
    Share.share({
      message:
        "I found A " +
        title +
        " on JustBooks and thought you might find it interesting! Sign up today to read ",
    })
      //after successful share return result
      .then((result) => console.log(result))
      //If any thing goes wrong it comes here
      .catch((errorMsg) => console.log(errorMsg));
  };

  _rentNowAction = async (titleId) => {
    console.log("rent");
    if (this.state.subscription_id == 0) {
      showMessage({
        message: "Rent Now",
        description: "Become a member to wishlist books!!",
        icon: "danger",
        type: "danger",
      });
    } else {
      if (this.state.webUser) {
        if (
          this.state.titleDetails.primus ||
          this.state.titleDetails.primus == "true"
        ) {
          this.setState({
            rentNowAlert: true,
            webUserAlert: false,
            rentNowTitleId: titleId,
          });
        } else {
          this._rentActionPerform(
            "DOOR_DELIVERY",
            false,
            this.state.rentNowTitleId
          );
        }
      } else {
        if (
          this.state.titleDetails.primus ||
          this.state.titleDetails.primus == "true"
        ) {
          this.setState({
            isWebUserAlert: true,
            rentNowAlert: true,
            webUserAlert: false,
            rentNowTitleId: titleId,
          });
        } else {
          this.setState({
            webUserAlert: true,
            rentNowAlert: false,
            rentNowTitleId: titleId,
          });
        }
      }
    }
  };

  _rentActionPerform = (orderType, isPrimusOrder, titleId) => {
    this.setState({ rentNowAlert: false, webUserAlert: false });
    let rentNow = API.API_JUSTBOOK_ENDPOINT + "/insertOrUpdateBookRequest";
    axios
      .post(
        rentNow,
        {
          createdAt: 810,
          createdBy: 1000,
          subscriptionId: this.state.subscription_id,
          titleId: titleId,
          bookReqType: "DELIVERY",
          reqSource: "WEBSITE",
          status: "NEW",
          deliveryType: orderType,
          requestedBranchId: this.state.branchId ? this.state.branchId : 1008,
          titleIsMagazine: false,
          primusOrder: isPrimusOrder,
        },
        { headers: { "X-SECRET-TOKEN": "qwerty" } }
      )
      .then((response) => {
        console.log("rent now", response.data);
        if (response.data.status) {
          this.setState({
            // refreshing: true,
            errorMsg: response.data.successObject,
          });
          showMessage({
            message: "Rent",
            description: response.data.successObject,
            icon: "success",
            type: "success",
          });
        } else {
          showMessage({
            message: "Rent",
            description: response.data.errorDescription,
            icon: "danger",
            type: "danger",
          });
        }
      })
      .catch(function (error) {
        console.log(error);
        showMessage({
          message: "Rent",
          description: "Something went wrong, Please try again later!",
          icon: "danger",
          type: "danger",
        });
      });
  };

  _wishlistAction = async (action, titleId) => {
    console.log("wishlist");
    if (this.state.subscription_id == 0) {
      console.log("Become a member to wishlist books!!");
      showMessage({
        message: "Wishlist",
        description: "Become a member to wishlist books!!",
        icon: "danger",
        type: "danger",
      });
    } else {
      let wishlistAddRemove;
      if (action == "add") {
        wishlistAddRemove = API.API_JUSTBOOK_ENDPOINT + "/addToWishlist";
      } else {
        wishlistAddRemove = API.API_JUSTBOOK_ENDPOINT + "/removeFromWishlist";
      }
      axios
        .post(
          wishlistAddRemove,
          {
            titleId: titleId,
            createdAt: 810,
            createdBy: 1000,
            subscriptionId: this.state.subscription_id,
          },
          { headers: { "X-SECRET-TOKEN": "qwerty" } }
        )
        .then((response) => {
          console.log("response wishlist", response.data);
          if (response.data.status) {
            this.setState({
              wishlistId:
                action == "add" ? response.data.successObject.wishlistId : 0,
              errorMsg:
                action == "add"
                  ? response.data.successObject.message
                  : response.data.successObject,
            });
            showMessage({
              message: "Wishlist",
              description:
                action == "add"
                  ? response.data.successObject.message
                  : response.data.successObject,
              icon: "success",
              type: "success",
            });
            this.getTitleDetails(titleId, this.state.subscription_id);
          } else {
            showMessage({
              message: "Wishlist",
              description: response.data.errorDescription,
              type: "danger",
              icon: "danger",
            });
          }
        })
        .catch(function (error) {
          console.log(error);
          showMessage({
            message: "Wishlist",
            description: "Something went wrong, Please try again later!",
            icon: "danger",
            type: "danger",
          });
        });
    }
  };

  _renderTruncatedFooter = (handlePress) => {
    return (
      <Paragraph
        style={{
          color: "#273C96",
          marginTop: 2,
          textDecorationLine: "underline",
        }}
        onPress={handlePress}
      >
        Read more
      </Paragraph>
    );
  };

  _renderRevealedFooter = (handlePress) => {
    return (
      <Paragraph
        style={{
          color: "#273C96",
          marginTop: 2,
          textDecorationLine: "underline",
        }}
        onPress={handlePress}
      >
        Show less
      </Paragraph>
    );
  };

  getAllAuthorData = () => {
    const getAuthor = API.API_ESAPI_POINT + "/getPopularAuthors";
    console.log(getAuthor);
    axios
      .get(getAuthor)
      .then((response) => {
        //   console.log(response.data.successObject.content);
        if (
          response.data.status &&
          response.data.successObject.content.length > 0
        ) {
          this.setState({
            authorData: response.data.successObject.content,
            authorFlag: true,
          });
        } else {
          this.setState({
            authorFlag: false,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  _allAuthorsNavigate = async () => {
    this.props.navigation.push("AllAuthors");
  };

  _imageLoading_Error(){
    this.setState({ imageLoading: false });
  }

  render() {
    if (this.state.bookNotFoundError) {
      return (
        <View style={{ display: "flex", flex: 1 }}>
          <View
            style={{
              width: "100%",
              height: "100%",
              backgroundColor: "#FFFFFF",
            }}
          >
            <View style={styles.containerImage}>
              <Image
                style={styles.logoImage}
                source={require("../../../assets/images/logo.jpg")}
              />
            </View>
          </View>
          <View style={styles.footerNotFound}>
            <View style={styles.subscriptionView}>
              <TouchableOpacity
                style={styles.subscriptionBottomButtons}
                onPress={() => {
                  console.log("error Message");
                }}
              >
                <Text
                  style={[styles.subscriptionFooterText, { color: "#273C96" }]}
                >
                  {" "}
                  {this.state.errorMsg}{" "}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      );
    }
    return (
      <View style={styles.contain}>
        {/* rent Now */}
        <AwesomeAlert
          show={this.state.rentNowAlert}
          showProgress={false}
          icon={"cart-arrow-right"}
          iconColor={"#AB3247"}
          title="JustBooks"
          message="Hey, Please select Order Type!"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelButtonColor="green"
          cancelText="Normal Order"
          confirmText="Primus Order"
          confirmButtonColor="#DD6B55"
          onCancelPressed={() => {
            if (this.state.isWebUserAlert) {
              this.setState({
                rentNowAlert: false,
                webUserAlert: true,
              });
            } else {
              this._rentActionPerform(
                "DOOR_DELIVERY",
                false,
                this.state.rentNowTitleId
              );
            }
          }}
          onConfirmPressed={() => {
            this._rentActionPerform(
              "DOOR_DELIVERY",
              true,
              this.state.rentNowTitleId
            );
          }}
        />

        <AwesomeAlert
          show={this.state.webUserAlert}
          showProgress={false}
          icon={"cart-arrow-right"}
          iconColor={"#AB3247"}
          title="JustBooks"
          message="Pickup from Branch or Home Deliver?!"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          cancelButtonColor="green"
          cancelText="Home Delivery"
          confirmText="Store Delivery"
          confirmButtonColor="#DD6B55"
          onCancelPressed={() => {
            this._rentActionPerform(
              "DOOR_DELIVERY",
              false,
              this.state.rentNowTitleId
            );
          }}
          onConfirmPressed={() => {
            this._rentActionPerform(
              "BRANCH_DELIVERY",
              false,
              this.state.rentNowTitleId
            );
          }}
        />
        <StatusTopBar />
        <View>
          <Modal isVisible={this.state.isModalVisible}>
            <View style={{ flex: 1 }}>
              <WebView
                style={{ flex: 1 }}
                scalesPageToFit={true}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{
                  uri:
                    "https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/video_reviews/title_review_194_1599041865883.mp4",
                }}
              />
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "space-between",
                  position: "absolute",
                  bottom: 0,
                }}
              >
                <TouchableHighlight
                  onPress={this.toggleModal}
                  style={{ flex: 1, backgroundColor: "#F1F1F1" }}
                >
                  <Text style={styles.actionClose}>Close</Text>
                </TouchableHighlight>
                <TouchableHighlight
                  onPress={this.handleUploadVideo}
                  style={{ flex: 1, backgroundColor: "#273C96" }}
                >
                  <Text style={styles.actionUpload}>Upload Video</Text>
                </TouchableHighlight>
              </View>
            </View>
          </Modal>
        </View>
        {this.state.refreshing ? (
          <ActivityIndicator
            style={{ flex: 1 }}
            size={25}
            animating={true}
            color={Colors.blue700}
          />
        ) : (
          <View style={styles.activityClass}>
            <ScrollView>
              {/* <Header title="Book Details" navigation={this.props.navigation} /> */}
              <View style={{ marginBottom: 15, marginTop: 5 }}>
                <View style={styles.cardHomeView}>
                  <View style={styles.card}>
                    {this.state.titleDetails.image_url != null &&
                    this.state.titleDetails.image_url != "null" &&
                    this.state.titleDetails.image_url != "" ? (
                      <Image
                        // source={{ uri: this.state.titleDetails.image_url }}
                        onError={this._imageLoading_Error.bind(this)}
                        source = { this.state.imageLoading 
                            ? 
                            { uri: this.state.titleDetails.image_url } 
                            : 
                            { uri: 'https://justbooks.in/images/ThumbnailNoCover.png' } }
                        style={styles.imageCard}
                      ></Image>
                    ) : (
                      <Image
                        source={{
                          uri:
                            "https://justbooks.in/images/ThumbnailNoCover.png",
                        }}
                        style={styles.imageCard}
                      ></Image>
                    )}
                    {this.state.primusMember && (
                      <View style={styles.primusTag}>
                        <Image
                          source={{
                            uri:
                              "https://justbooks.in/assets/img/primus_tag.png",
                          }}
                          style={styles.primusImage}
                        ></Image>
                      </View>
                    )}
                    <View style={styles.cardDescription}>
                      <Text style={styles.titleDesciption}>
                        <Headline style={styles.color}>
                          {this._toTitleCase(
                            this.state.titleDetails.jb_info.title
                          )}
                        </Headline>
                        {"\n"}
                        <Text style={{ color: "green", fontSize: 16 }}>
                          By :
                          <Paragraph style={styles.colorParagraph}>
                            {" "}
                            {this._toTitleCase(
                              this.state.titleDetails.jb_info.author.name
                            )}
                          </Paragraph>
                        </Text>
                        {"\n"}
                        {/* <TouchableOpacity style={styles.authorCardSection} onPress={()=>{this._authorDetailsNavigate(this.state.titleDetails.jb_info.author.id)}} >
                                                    <Image  style={styles.authorNonImageCoverImage} source={{ uri: "https://justbooks.in/assets/img/user.png" }} ></Image>
                                                    <View style={styles.NonImageCardText}>
                                                        <Paragraph style={styles.AuthorParagraph}>{this._toTitleCase(this.state.titleDetails.jb_info.author.name)}</Paragraph>
                                                    </View>
                                                </TouchableOpacity>{'\n'} */}
                        <Rating
                          startingValue={
                            this.state.average_rating
                              ? this.state.average_rating
                              : 0
                          }
                          type="custom"
                          ratingColor="#FFD54B"
                          ratingBackgroundColor="#F1F1F1"
                          ratingCount={5}
                          imageSize={20}
                          onFinishRating={this.ratingCompleted}
                          style={{ paddingVertical: 10 }}
                        />
                        {"\n"}
                        <Text style={{ color: "green", fontSize: 16 }}>
                          <Paragraph style={styles.variableParagraph}>
                            {this.state.titleDetails.jb_info.times_rented}
                          </Paragraph>
                          {"\n"}
                          <Paragraph style={styles.colorParagraph}>
                            times read
                          </Paragraph>
                        </Text>
                        {"\n"}
                      </Text>
                      <View style={styles.actionStyle}>
                        <View style={styles.footer}>
                          <TouchableOpacity
                            style={styles.bottomButtons}
                            onPress={() => {
                              this._rentNowAction("rent", this.state.title_id);
                            }}
                          >
                            <Icons
                              name="book-outline"
                              size={28}
                              color={"#000000"}
                            />
                            <Text style={styles.footerText}>Rent</Text>
                          </TouchableOpacity>

                          {/* <TouchableOpacity style={styles.bottomButtons} onPress={()=>console.log("buy Now")} >
                                                        <Icons name="cash-multiple" size={28} color={'#000000'} />
                                                        <Text style={styles.footerText}>Buy</Text>
                                                    </TouchableOpacity> */}

                          {this.state.wishlistAvailable ? (
                            <TouchableOpacity
                              style={styles.bottomButtons}
                              onPress={() =>
                                this._wishlistAction(
                                  "remove",
                                  this.state.title_id
                                )
                              }
                            >
                              <Icons name="heart" size={28} color={"red"} />
                              <Text style={styles.footerText}>Wishlist</Text>
                            </TouchableOpacity>
                          ) : (
                            <TouchableOpacity
                              style={styles.bottomButtons}
                              onPress={() =>
                                this._wishlistAction("add", this.state.title_id)
                              }
                            >
                              <Icons
                                name="heart-outline"
                                size={28}
                                color={"#000000"}
                              />
                              <Text style={styles.footerText}>Wishlist</Text>
                            </TouchableOpacity>
                          )}

                          <TouchableOpacity
                            style={styles.bottomButtons}
                            onPress={() =>
                              this._shareBookDetails(
                                this._toTitleCase(
                                  this.state.titleDetails.jb_info.title
                                )
                              )
                            }
                          >
                            <Icons
                              name="share-variant"
                              size={28}
                              color={"#000000"}
                            />
                            <Text style={styles.footerText}>Share</Text>
                          </TouchableOpacity>

                          <TouchableOpacity
                            style={styles.bottomButtons}
                            onPress={() =>
                              this._videoReview(
                                this.state.title_id,
                                this._toTitleCase(
                                  this.state.titleDetails.jb_info.title
                                )
                              )
                            }
                          >
                            <Icons
                              name="book-play-outline"
                              size={24}
                              color={"#000000"}
                            />
                            <Text style={styles.footerText}>Review</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </View>
                  {/* <Paragraph style={styles.color}>{this.state.titleDetails.description}</Paragraph> */}
                  {this.state.titleDetails.description != "" &&
                    this.state.titleDetails.description != null &&
                    this.state.titleDetails.description != "null" && (
                      <View style={{ marginTop: 30 }}>
                        <Headline style={styles.color}>
                          Quick look at the book
                        </Headline>
                        <ReadMore
                          numberOfLines={3}
                          renderTruncatedFooter={this._renderTruncatedFooter}
                          renderRevealedFooter={this._renderRevealedFooter}
                        >
                          <Paragraph style={styles.color}>
                            {this.state.titleDetails.description}
                          </Paragraph>
                        </ReadMore>
                      </View>
                    )}
                  <View style={styles.author}>
                    {/* <Divider style={{backgroundColor: '#6f6c6c'}} /> */}
                    <Appbar.Header
                      style={{
                        backgroundColor: "#FFFFFF",
                        elevation: 0,
                        marginLeft: -15,
                        height: 40,
                      }}
                    >
                      <Appbar.Content
                        title="Author Details"
                        titleStyle={styles.headingStyle}
                        style={styles.heading}
                      />
                      <Appbar.Content
                        title="More"
                        titleStyle={styles.titleStyle}
                        style={styles.SeeAllHeading}
                        onPress={() => {
                          this.props.navigation.navigate("AllAuthors");
                        }}
                      />
                    </Appbar.Header>
                    <TouchableRipple
                      onPress={() => {
                        this._authorDetailsNavigate(
                          this.state.titleDetails.jb_info.author.id
                        );
                      }}
                      rippleColor="rgba(255, 255, 0, 1.32)"
                    >
                      <Appbar.Header style={styles.authorDetailsStyle}>
                        <Avatar.Text
                          size={40}
                          label={this._toTitleCase(
                            this.state.titleDetails.jb_info.author.name
                          )
                            .substring(0, 2)
                            .toUpperCase()}
                          style={styles.textIconColor}
                        />
                        <Appbar.Content
                          title={this._toTitleCase(
                            this.state.titleDetails.jb_info.author.name
                          )}
                          subtitle={"Writer"}
                          style={styles.heading}
                        />
                      </Appbar.Header>
                    </TouchableRipple>
                    <Divider style={{ backgroundColor: "#6f6c6c" }} />
                  </View>

                  {this.state.relatedTitleFlag && (
                    <View style={styles.book}>
                      <Appbar.Header
                        style={{
                          backgroundColor: "#FFFFFF",
                          elevation: 0,
                          marginLeft: -15,
                          height: 36,
                        }}
                      >
                        <Appbar.Content
                          title="Related Books"
                          titleStyle={styles.headingStyle}
                          style={styles.heading}
                        />
                        <Appbar.Content
                          title="More"
                          titleStyle={styles.titleStyle}
                          style={styles.SeeAllHeading}
                          onPress={() => {
                            this.props.navigation.push("AllBooks", {
                              action: "topTitleData",
                            });
                          }}
                        />
                      </Appbar.Header>
                      <View style={styles.cardView}>
                        <ScrollView horizontal={true}>
                          {this.state.allRelatedTitleData.map((item) => {
                            return (
                              <Card
                                key={item.title_id}
                                style={styles.cardSection}
                                onPress={() => {
                                  this._bookDetailsNavigate(item.title_id);
                                }}
                              >
                                {item.image_url != null &&
                                item.image_url != "" ? (
                                  <Card.Cover
                                    style={styles.coverImage}
                                    source={{ uri: item.image_url }}
                                  />
                                ) : (
                                  <View style={styles.NonImage}>
                                    <Card.Cover
                                      style={styles.NonImageCoverImage}
                                      source={{
                                        uri:
                                          "https://justbooks.in/images/ThumbnailNoCover.png",
                                      }}
                                    />
                                    <Card.Content
                                      style={styles.NonImageCardText}
                                    >
                                      <Title style={styles.NonImageTitle}>
                                        {this._toRelatedTitleCase(item.title)}
                                      </Title>
                                      {!this.state.primusMember &&
                                      this.state.subscription_id > 1 ? (
                                        <Paragraph
                                          style={styles.NonImageParagraph}
                                        >
                                          {item.author
                                            ? this._toRelatedTitleCase(
                                                item.author
                                              )
                                            : ""}
                                        </Paragraph>
                                      ) : (
                                        <Paragraph
                                          style={styles.NonImageParagraph}
                                        >
                                          {item.author
                                            ? this._toRelatedTitleCase(
                                                item.author
                                              )
                                            : ""}
                                        </Paragraph>
                                      )}
                                    </Card.Content>
                                  </View>
                                )}
                                {this.state.primusMember && (
                                  <View style={styles.primusTag}>
                                    <Image
                                      source={{
                                        uri:
                                          "https://justbooks.in/assets/img/primus_tag.png",
                                      }}
                                      style={styles.primusImage}
                                    ></Image>
                                  </View>
                                )}
                              </Card>
                            );
                          })}
                        </ScrollView>
                      </View>
                    </View>
                  )}

                  {this.state.authorFlag && (
                    <View style={styles.authorSection}>
                      <Appbar.Header
                        style={{
                          backgroundColor: "#FFFFFF",
                          elevation: 0,
                          marginLeft: -15,
                          height: 36,
                        }}
                      >
                        <Appbar.Content
                          title="Related Authors"
                          titleStyle={styles.headingStyle}
                          style={styles.heading}
                        />
                        <Appbar.Content
                          title="More"
                          titleStyle={styles.titleStyle}
                          style={styles.SeeAllHeading}
                          onPress={() => {
                            this._allAuthorsNavigate();
                          }}
                        />
                      </Appbar.Header>
                      <View style={styles.cardView}>
                        <ScrollView horizontal={true}>
                          {this.state.authorData.map((item) => {
                            return (
                              <TouchableOpacity
                                key={item.author_id}
                                style={styles.authorCardSection}
                                onPress={() => {
                                  this._authorDetailsNavigate(item.author_id);
                                }}
                              >
                                {item.image_url != null &&
                                item.image_url != "" ? (
                                  <Image
                                    source={{ uri: item.image_url }}
                                    style={styles.authorCoverImage}
                                  ></Image>
                                ) : (
                                  <View style={styles.authorNonImage}>
                                    <Image
                                      style={styles.authorNonImageCoverImage}
                                      source={{
                                        uri:
                                          "https://justbooks.in/assets/img/user.png",
                                      }}
                                    ></Image>
                                  </View>
                                )}
                                <View style={styles.NonImageCardText}>
                                  <Paragraph style={styles.AuthorParagraph}>
                                    {this._toTitleCase(item.author_name)}
                                  </Paragraph>
                                </View>
                              </TouchableOpacity>
                            );
                          })}
                        </ScrollView>
                      </View>
                    </View>
                  )}
                </View>
              </View>
            </ScrollView>
            <SearchTab navigation={this.props.navigation} />
            <BottomTab navigation={this.props.navigation} />
            {this.state.successError && (
              <View style={styles.footerMessage}>
                <View style={styles.subscriptionViewMessage}>
                  <TouchableOpacity
                    style={styles.subscriptionBottomButtons}
                    onPress={() => {
                      console.log("error Message");
                    }}
                  >
                    <Text
                      style={[
                        styles.subscriptionFooterText,
                        { color: "#ffffff" },
                      ]}
                    >
                      {" "}
                      {this.state.errorMsg}{" "}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}

export default BookDetails;
