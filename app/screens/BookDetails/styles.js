import { StyleSheet ,Dimensions, Platform } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    headingView: {
        marginTop: 0,
        marginBottom: 0,
        marginRight: 10,
        marginLeft: 10
    },
    heading: {
        color: '#2E2E2E',
        fontSize: 24,
        fontWeight: '600'
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF',  
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },
    cardView: {
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 15, 
        marginBottom: 10, 
        marginTop: 0,
    },
    cardSection: {
        width: 120,
        height: 180,
        margin: 10,
        // backgroundColor: '#FFFFFF'
    },
    coverImage: {
        height: 180,
    },
    card: {
        width: '90%',
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    imageCard: {
        width: 140, 
        height: undefined,
        resizeMode: 'stretch',
        borderRadius: 2
    },
    cardDescription : {
        width: '80%',
        flexDirection: 'column',
        marginLeft: '6%', 
        paddingRight: 20, 
        marginTop: -5, 
        justifyContent: 'space-between'
    },
    titleDesciption: {
        marginTop: 5,
        marginRight: 30
    },
    titleStyle : {
        fontSize: 16, 
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end'
    },
    headingStyle : {
        fontSize: 22, 
        fontWeight: '600'
    },
    color: {
        color: '#2E2E2E',
    },
    colorParagraph: {
        color: 'pink',
    },
    variableParagraph: {
        color: '#821E1E',
        fontSize: 24,
        lineHeight: 24
    },
    title: {
        width: '100%',
        color: '#273C96',
        fontSize: 16,
        lineHeight: 15,
        marginTop: 8,
        marginBottom: 0,
        fontWeight: '600'
    },
    Paragraph: {
        marginTop: 0,
        width: '100%',
        fontSize: 12,
        color: '#273C96'
    },
    author: {
        marginTop: 20,
    },
    book: {
        marginTop: 12,
    },
    textIconColor: {
        backgroundColor: '#273C96'
    },
    authorDetailsStyle : {
        backgroundColor: '#FFFFFF', 
        marginLeft: 0, 
        marginBottom: 20,
        elevation: 0
    },
    actionStyle: {
        marginTop: 20,
    },
    footer: {
        // position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        bottom: 0,
        flexDirection:'row',
        height:60,
        alignItems:'center',
        elevation:25,
      },
    footerNotFound:  {
        position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        bottom: 0,
        flexDirection:'row',
        height:60,
        alignItems:'center',
        elevation:25,
      },
    subscriptionView: {
        position: "absolute",
        flex: 1,
        // top: (Dimensions.get('screen').height - 130),
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: "#F1F1F1",
        height: 60,
        alignItems: "center",
        elevation: 25,
    },
    footerMessage: {
        position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        bottom: 0,
        flexDirection:'row',
        height:80,
        alignItems:'center',
        elevation:25,
      },
    subscriptionViewMessage: {
        position: "absolute",
        flex: 1,
        // top: (Dimensions.get('screen').height - 130),
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: "green",
        height: 80,
        alignItems: "center",
        elevation: 25,
    },
    subscriptionBottomButtons: {
        alignItems: "center",
        justifyContent: "space-around",
        flex: 1,
        width: "100%",
        marginTop: 9,
    },
    subscriptionFooterText: {
      // color:'#8F8E94',
      fontWeight: "600",
      alignItems: "center",
      textAlign: 'center',
      fontSize: 18,
      marginBottom: 12,
      marginLeft: 10,
      marginRight: 10,
      fontWeight: "600",
    },
      bottomButtons: {
        alignItems:'center',
        justifyContent: 'space-around',
        flex:1,
        marginTop:9,
        marginRight: 15
      },
      footerText: {
        color:'#2E2E2E',
        fontWeight:'600',
        alignItems:'center',
        fontSize:11,
        lineHeight:20,
        marginBottom:12
      },
      WebViewContainer: {
          marginTop: (Platform.OS == 'ios') ? 20 : 0,
      },
      actionClose : {
        color:'#2E2E2E',
        margin: 15,
        textAlign: 'center'
      },
      actionUpload : {
        color:'#FFFFFF',
        margin: 15,
        textAlign: 'center'
      },

    NonImage: {
        position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        top: 0,
        alignItems:'center',
    },
    NonImageCoverImage: {
        position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        top: 0,
        height: 180,
    },
    NonImageCardText: {
        textAlign: 'center',
        alignItems: 'center'
    },
    NonImageTitle: {
        width: 100,
        fontSize: 17,
        lineHeight: 17,
        marginTop: 15,
        fontWeight: '600',
        color: '#CB8223',
        textAlign: 'center'
    },
    NonImageParagraph: {
        marginTop: 2,
        width: 120,
        fontSize: 14,
        color: '#CB8223',
        textAlign: 'center'
    },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  logoImage: {
    width: 250,
    height: 250,
    marginTop: 0
  },
  primusImage: {
    width: 100, 
    height: 35,
 },
 primusTag: {
    width: 100, 
    height: 35, 
    position: 'absolute', 
    bottom: 10,
    left: 0
 },
 authorSection: {
     marginBottom: 20,
 },
 authorCardSection: {
     width: 75,
     height: 100,
     margin: 10,
     borderRadius: 100
     // backgroundColor: '#FFFFFF'
 },
 authorCoverImage: {
     height: 75,
     borderRadius: 100
 },
 authorNonImage: {
     alignItems:'center',
 },
 authorNonImageCoverImage: {
     height: 75,
 },
 authorNonImageCardText: {
     textAlign: 'center',
     alignItems: 'center'
 },
 authorNonImageTitle: {
     width: 100,
     fontSize: 17,
     lineHeight: 17,
     marginTop: 15,
     fontWeight: '600',
     color: '#273C96',
     textAlign: 'center'
 },
 AuthorParagraph: {
     marginTop: 5,
     width: '100%',
     fontSize: 12,
     lineHeight: 12,
     color: '#000000',
     textAlign: 'center'
 },

});
