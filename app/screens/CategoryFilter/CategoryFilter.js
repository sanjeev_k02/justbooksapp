import React from 'react';
import { View, Text, TextInput, FlatList } from 'react-native';

import Icons from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Appbar, ActivityIndicator, Colors, Checkbox  } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import { showMessage, hideMessage } from "react-native-flash-message";
import BottomTab from '../../components/BottomTab/BottomTab';
import API from '../../env';
import * as axios from 'axios';

class CategoryFilter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
            searchText: '',
            successError: false,
            errorMsg: '',
            category:{},
            checkedId: [],
            checked:[],
        };
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
  
    componentDidMount = async() => {
        await this.getAllCategoryList();
        setTimeout(
            () => {
            this.setState({ refreshing: false });
            },
            2000
        );
    };

    getAllCategoryList = () => {
      const getGeners = API.API_JUSTBOOK_ENDPOINT+"/getAllGenres";
      console.log(getGeners);
      axios.get(getGeners)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                var successData = response.data.successObject.map(function(el) {
                    var o = Object.assign({}, el);
                    o.checked = false;
                    return o;
                  })
                // console.log(successData);
                this.setState({
                    category : successData,
                    refreshing: false
                })
            } else {
                this.setState({
                    refreshing: true
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _checkedVal = (id) => {
        const index = this.state.checkedId.indexOf(id);
        if (index > -1) {
            this.state.checkedId.splice(index, 1);
        } else {
            this.state.checkedId.push(id);
        }

        let category=this.state.category
        category[id].checked=!category[id].checked
        this.setState({category:category})
        console.log("checkedId",this.state.checkedId)
    }

    _renderItem = ({ item, index }) => (
        <View style={styles.item} key={item.id}>
            <TouchableOpacity>
                <View style={styles.catContainer}>
                    <View style={styles.catLeftItem}>
                        <Text style={styles.title}>{this._toTitleCase(item.name)}</Text>
                    </View>
                    <View style={styles.catRightItem}>
                        <Checkbox
                            status={this.state.checkedId.includes(item.id) ? 'checked' : 'unchecked'}
                            uncheckedColor={'#273C96'}
                            color={'#273C96'}
                            onPress={() => {this._checkedVal(item.id)}}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );

    _navigateCategoryFilter = () => {
        console.log(this.state.checkedId.length)
        if(this.state.checkedId.length == 0) {
            showMessage({
                message: "Category",
                description: 'Please Select Category Item to better search!',
                icon: "danger",
                type: "danger",
            });
        } else {
            this.props.navigation.push('CategoryResults',{
                categories: this.state.checkedId+"",
                isMainCategory: false
            });
        }
    };
      
    render() {
        // console.log(this.state.category);
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        {/* <ScrollView> */}
                            <View style={{ marginBottom: 5 ,marginTop: 10}}>
                                <View style={styles.cardHomeView}>
                                    <View style={styles.headingSearchView}>
                                        <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                            <Appbar.Content title="Select Your Category" titleStyle={styles.headingSearchStyle} style={styles.searchHeading} />
                                            <Appbar.Content title="OK" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._navigateCategoryFilter()}} />
                                        </Appbar.Header>
                                    </View>
                                    <View style={styles.branchView}>
                                        <FlatList
                                            data={this.state.category}
                                            renderItem={this._renderItem}
                                            keyExtractor={item => item.id}
                                        />
                                    </View>
                                </View>
                            </View>
                        {/* </ScrollView> */}
                        {/* <BottomTab navigation={this.props.navigation} /> */}
                        {this.state.successError && <View style={styles.footer}>
                            <View style={styles.categoryView}>
                                <TouchableOpacity style={styles.categoryBottomButtons} onPress={()=>{console.log("error Message")}} >
                                    <Text style={[
                                        styles.categoryFooterText,
                                        { color: "#273C96" }
                                    ]}> {this.state.errorMsg} </Text>
                                </TouchableOpacity>
                            </View>
                        </View>}
                </View>}
            </View>
        );
    }

}

export default CategoryFilter;