import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 30, 
        marginTop: 0,
    },

    SectionStyle: {
      flex: 1,
      flexDirection: 'row-reverse',
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1,
      borderColor: '#565555',
      height: 50,
      borderRadius: 100,
      // padding: 20,
      // paddingRight: 10,
      shadowColor: 'grey',
      shadowOffset: {
          width: 0,
          height: 1
      },
      marginBottom: 10,
      shadowOpacity: 0.5,
      shadowRadius: 1,
      backgroundColor: '#FFFFFF',
    },
    searchTextInput: {
      width: 250, 
      color: '#2E2E2E',
      backgroundColor: '#FFFFFF'
    },
    divider : {
        margin: 10,
    },
    cardStyle : {
        backgroundColor: '#39403c', 
        borderRadius: 6
    },
    titleStyle: {
        color : '#CB8223'
    },
    subtitleStyle: {
        color : '#fff'
    },
    rightStyle : {
        backgroundColor: '#39403c', 
        borderBottomRightRadius: 6, 
        borderTopRightRadius: 6, 
        height: 70, 
        paddingTop: 8
    },
    iconColor : {
        backgroundColor: '#273C96'
    }
    ,button: {
        width: 50,
        height: 50,
      },
      container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        marginTop:25
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      action: {
        width: '100%',
        textAlign: 'center',
        color: 'white',
        paddingVertical: 8,
        marginVertical: 5,
        fontWeight: '600',
      },
      instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
      },
      stat: {
        textAlign: 'center',
        color: '#B0171F',
        marginBottom: 1,
        marginTop: 30,
      },
      action : {
        color:'#2E2E2E',
        margin: 10,
        textAlign: 'center'
      },
    headingSearchView: {
      marginTop: 0,
      marginBottom: 0,
      marginRight: 10,
      marginLeft: -10 ,
      padding: 15,
    },
    branchView: {
      marginTop: 10,
      marginBottom: 0,
    //   marginRight: 10,
      marginLeft: 15
    },
    searchHeading: {
        color: '#fff',
        fontSize: 20,
        width: 250,
        fontWeight: '600',
        fontFamily: 'Montserrat-Bold'
    },
    headingSearchStyle : {
        fontSize: 20, 
        width: 250,
        fontWeight: '600',
        fontFamily: 'Montserrat-Bold'
    },
    item: {
      paddingBottom: 5,
      marginVertical: 2,
      marginHorizontal: 6,
    },
    title: {
      fontSize: 20,
      fontWeight: '600',
      fontFamily: 'Montserrat-Regular'
    },
    contain: {
      display: 'flex',
      flex: 1,
      marginTop: 0
  },
  activityClass: {
      paddingBottom: 0
  },
  headingView: {
      marginTop: 0,
      marginBottom: 0,
      marginRight: 10,
      marginLeft: 10
  },
  heading: {
      color: '#fff',
      fontSize: 14,
      fontWeight: '600'
  },
  SeeAllHeading: {
      color: '#CB8223',
      fontSize: 14,
      fontWeight: '600',
      textAlign: 'right',
      alignContent: 'flex-end',
      alignContent: 'flex-end'
  },
  titleStyle : {
      fontSize: 16, 
      textAlign: 'right',
      alignContent: 'flex-end',
      alignContent: 'flex-end',
      color : '#CB8223'
  },
  headingStyle : {
      fontSize: 16, 
      fontWeight: '600'
  },
  cardSliderView: { 
      display: "flex", 
      flexDirection: "row", 
      flexWrap: "wrap", 
      alignContent: "center", 
      alignItems: "center", 
      justifyContent: "center",
      marginLeft: 10,
      marginTop: 20
  },
  cardSliderSection: {
      width: 250,
      height: 140,
      margin: 10,
      // backgroundColor: '#101211'
  },
  coverSliderImage: {
      height: 140,
  },
  cardView: { 
      display: "flex", 
      flexDirection: "row", 
      flexWrap: "wrap", 
      alignContent: "center", 
      alignItems: "center", 
      justifyContent: "center",
      // marginLeft: 15
  },
  cardSection: {
      width: 90,
      height: 150,
      marginRight: 5,
      marginLeft: 5,
      // backgroundColor: '#101211'
  },
  sliderView: {
      marginTop: 35
  },
  coverImage: {
      height: 150,
  },
  // title: {
  //     width: '100%',
  //     fontSize: 16,
  //     lineHeight: 15,
  //     paddingTop: 5,
  //     marginBottom: 0,
  //     fontWeight: '600',
  //     color: '#273C96'
  // },
  Paragraph: {
      marginTop: 0,
      width: '100%',
      fontSize: 12,
      color: '#273C96'
  },
  newArrivles: {
      marginBottom: 15,
      backgroundColor: '#E8E8E8',
      margin: 10
  },
  latestBook: {
      marginBottom: 15,
  },
  catContainer: {
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginRight: 20,
      alignItems: 'flex-start' // if you want to fill rows left to right
  },
  catLeftItem: {
      width: '90%' // is 50% of container width
  },
  catRightItem: {
      width: '10%', // is 50% of container width
  },

  footer: {
    position: 'absolute',
    flex:1,
    left: 0,
    right: 0,
    bottom: 0,
    flexDirection:'row',
    height:60,
    alignItems:'center',
    elevation:25,
  },
categoryView: {
    position: "absolute",
    flex: 1,
    // top: (Dimensions.get('screen').height - 130),
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "#F1F1F1",
    height: 60,
    alignItems: "center",
    elevation: 25,
},
categoryBottomButtons: {
    alignItems: "center",
    justifyContent: "space-around",
    flex: 1,
    width: "100%",
    marginTop: 9,
},
categoryFooterText: {
  // color:'#8F8E94',
  fontWeight: "600",
  alignItems: "center",
  textAlign: 'center',
  fontSize: 18,
  marginBottom: 12,
  fontWeight: "600",
},

});
