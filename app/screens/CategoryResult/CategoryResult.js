import React from 'react';
import { View, RefreshControl, FlatList, Image } from 'react-native';
import styles from './styles';
import { Divider, Text, Card, Title, Paragraph, ActivityIndicator, Colors  } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { Appbar } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import CategoryTab from '../../components/CategoryTab/CategoryTab';
import API from '../../env';
import * as axios from 'axios';

class CategoryResult extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          subscription_id: 0,
          memberCard: 0,
          branchId: 0,
          cityId: 0,
          primusMappedBranches: 0,
          primusMember: false,
          page: 1,
          userDetails: {},
          refreshing: true,
          categoryDataFlag: false,
          categoryData: {},
          categories : this.props.route.params.categories,
          isMainCategory : this.props.route.params.isMainCategory,
        };
        console.log(this.props.route.params)
    }

    _toTitleCase(str) { 
        if(str.length > 30) {
            var res = str.substring(0, 30);
            return res.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            )+"...";
        } else {
            return str.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            );
        }
    }

    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            // console.log("user",user);
            if (user !== null) {
                const userData = JSON.parse(user);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard,
                    branchId: userData.login.branchId,
                    primusMappedBranches: userData.login.primusMappedBranches,
                    cityId: userData.login.cityId,
                    primusMember: userData.login.primus
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        if(this.state.subscription_id > 0) {
            await this.getAllCategoryWiseAfterLogin(this.state.subscription_id, this.state.categories);
        } else {
            await this.getAllCategoryWise( this.state.categories);
        }
        setTimeout(() => {
            this.setState({ refreshing: false });
        }, 1000);
    };

    getAllCategoryWiseAfterLogin = (subscriptionId, categoryIds) => {
        let getCategory;
        if(this.state.isMainCategory){
            getCategory = API.API_ESAPI_POINT+"/getMainCategorywise?mainCategories="+categoryIds+"&branchId="+this.state.branchId+"&page="+this.state.page+"&subscriptionId="+subscriptionId+"&cityId="+this.state.cityId+"&searchInBranch=false";
        } else {
            if(this.state.primusMember) {
                getCategory = API.API_ESAPI_POINT+"/getCategorywiseForPrimus?categories="+categoryIds+"&branchId="+this.state.branchId+"&page="+this.state.page+"&subscriptionId="+subscriptionId+"&cityId="+this.state.cityId+"&branchIds="+this.state.primusMappedBranches+"&onlyPrimus=false";
            } else { 
                getCategory = API.API_ESAPI_POINT+"/getCategorywise?categories="+categoryIds+"&branchId="+this.state.branchId+"&page="+this.state.page+"&subscriptionId="+subscriptionId+"&cityId="+this.state.cityId+"&searchInBranch=false";
            } 
        }
      console.log(getCategory);
      axios.get(getCategory)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    categoryData : response.data.successObject,
                    categoryDataFlag: true
                })
            } else {
                this.setState({
                    categoryDataFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllCategoryWise = (categoryIds) => {
      let getCategory;
      if(this.state.isMainCategory){
        getCategory = API.API_ESAPI_POINT+"/getMainCategorywise?mainCategories="+categoryIds+"&branchId="+this.state.branchId+"&page="+this.state.page+"&cityId="+this.state.cityId+"&searchInBranch=false";
      } else {
        getCategory = API.API_ESAPI_POINT+"/getCategorywise?categories="+categoryIds+"&branchId="+this.state.branchId+"&page="+this.state.page+"&cityId="+this.state.cityId+"&searchInBranch=false";
      }
      console.log(getCategory);
      axios.get(getCategory)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    categoryData : response.data.successObject,
                    categoryDataFlag: true
                })
            } else {
                this.setState({
                    categoryDataFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _onRefresh = async () => {
        this.setState({refreshing: true});

        if(this.state.subscription_id > 0) {
            await this.getAllCategoryWiseAfterLogin(this.state.subscription_id);
        } else {
            await this.getAllCategoryWise();
        }
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
    }

    _bookDetailsNavigate = async(titleid) => {
        console.log(titleid);
        this.props.navigation.push("BookDetails",{ title_id: titleid });
    }

    _backNavigation = () => {
        if(this.state.isMainCategory) {
            this.props.navigation.navigate("Home");
        } else {
            this.props.navigation.navigate("CategoryFilter");
        }
    }

    _avgRating = (rating) => {
        var avg_reading_time = rating ? rating : 0;
        while(avg_reading_time > 5){
            avg_reading_time = avg_reading_time / 5;
        }
        return avg_reading_time;
    }

    render() {
        var _renderRideItem =(item,index) => {
            return (
                <View key={index}>
                    <View style={styles.card} >
                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.jb_info.titleid)}}>
                            {(item.image_url != 'null' && item.image_url != null && item.image_url != '') ? <Image 
                                source={{ uri: item.image_url }} style={styles.imageCard}>
                            </Image>
                            : <Image 
                                source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} style={styles.imageCard}>
                            </Image>}
                            {this.state.primusMember && <View style={styles.primusTag}>
                                <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                            </View>}
                        </TouchableOpacity>
                        <View style={styles.cardDescription}>
                            <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.jb_info.titleid)}}>
                                <Text style={styles.titleDesciption}>
                                    <Title style={styles.color}>{this._toTitleCase(item.jb_info.title)}</Title>{'\n'}
                                    {(item.jb_info.author != null && item.jb_info.author.name != "" && item.jb_info.author.name != null) && <Text style={{color: 'green', fontSize: 16 }}>By : 
                                        <Paragraph style={styles.colorParagraph}> {this._toTitleCase(item.jb_info.author.name)}</Paragraph>
                                    </Text>}{'\n'}
                                    {(item.jb_info.times_rented != "" && item.jb_info.times_rented != null) && <Text style={{color: 'green', fontSize: 16 }}>
                                        <Paragraph style={styles.colorParagraph}>No of times read : {item.jb_info.times_rented}</Paragraph>{'\n'}
                                    </Text>}
                                </Text>
                            </TouchableOpacity>
                            <Text style={styles.titleDesciption}>
                                <Rating
                                    startingValue={ item.jb_info.times_rented ? this._avgRating(item.jb_info.times_rented): 0 }
                                    type='custom'
                                    ratingColor='#FFD54B'
                                    ratingBackgroundColor='#F1F1F1'
                                    ratingCount={5}
                                    imageSize={20}
                                    onFinishRating={this.ratingCompleted}
                                    style={{ paddingVertical: 10 }}
                                />
                            </Text>
                        </View>
                    </View>
                    <Divider style={styles.divider} />
                </View>
            )
        }
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { (this.state.refreshing) ?
                <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        {this.state.categoryDataFlag && <View style={styles.newArrivles}>
                            <View style={styles.headingSearchView}>
                                <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                    <Appbar.Content title="Search By Category" titleStyle={styles.headingSearchStyle} style={styles.searchHeading} />
                                    <Appbar.Content title="Back" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._backNavigation()}} />
                                </Appbar.Header>
                            </View>
                            <View style={styles.cardView}>
                                <View style={{ marginBottom: 5 ,marginTop: 15}}>
                                    <View style={styles.cardHomeView}>
                                        {(Object.keys(this.state.categoryData).length > 0 ) && (
                                            <View>
                                                <FlatList 
                                                    data={this.state.categoryData}
                                                    ref={"flatlist"}
                                                    removeClippedSubviews={true}
                                                    initialNumToRender={10}
                                                    bounces={false}
                                                    renderItem={({ item, index }) => _renderRideItem(item, index)}
                                                    keyExtractor={(item, index) => item.jb_info.titleid}
                                                    bounces={false}
                                                    onEndReachedThreshold={0.5}
                                                />
                                            </View>
                                        )}
                                        
                                        {((Object.keys(this.state.categoryData).length == 0 ) && this.state.refreshing) && (
                                        <View class={styles.shareContainer}>
                                            <View style={{ alignSelf: "center" }}>
                                                <View style={{
                                                    marginTop: 60,
                                                    width: 220,
                                                    height: 220,
                                                    borderRadius: 100,
                                                    overflow: "hidden"}}>
                                                <Image source={require("../../assets/no-data.png")} style={{
                                                    flex: 1,
                                                    height: 250,
                                                    width: undefined}} ></Image>
                                                </View>
                                            </View>
                                        </View>
                                        )}
                                    </View>
                                </View>
                            </View>
                            <Divider style={styles.divider} />
                        </View>}
                        {/* <ChatBotIcon navigation={this.props.navigation} /> */}
                        <BottomTab navigation={this.props.navigation} routeName="home"/>
                    </View>
                }
            </View>
        );
    }

}

export default CategoryResult;