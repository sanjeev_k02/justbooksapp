import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import styles from './styles';
import { Avatar, Divider, Card, FAB, IconButton, TextInput } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import { GiftedChat, Bubble, InputToolbar, Send } from 'react-native-gifted-chat'; // 0.3.0

class ChatBot extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          subscription_id: 0,
          memberCard: 0,
          messages: [
            // {
            //   _id: 2,
            //   text: <Text><Text onPress={() => { this.props.navigation.navigate("Profile")}} style={{ color:'#fff', textDecorationLine: 'underline',}} >Justbooks!</Text><Text> we are here to help you?{'\n'}please chat with us to grow your search result</Text></Text>,
            //   createdAt: new Date(Date.UTC(2016, 5, 11, 17, 20, 0)),
            //   user: {
            //     _id: 1,
            //     name: 'React Native',
            //     avatar: 'https://justbooks.in/images/ThumbnailNoCover.png',
            //   },
            //   sent: true,
            //   received: true,
            //   pending: true,
            // }
          ]
        }
    }

    componentDidMount = async() => {
      try {
        const user = await AsyncStorage.getItem('userData');
        // console.log("user",user);
        if (user !== null) {
            const userData = JSON.parse(user);
            this.setState({
                subscription_id: userData.login.subscriptionId,
                memberCard: userData.login.memberCard
            })
        } else {
            console.log("subscription Not Found");
        }
      } catch (error) {
          console.log("error", error)
      }

      let fetchServices = await fetch("http://esapi.justbooks.in/talkToJBBot");
      let response = await fetchServices.json();
      // console.log("response", response);
      let emptyMessage = [];
      if(response.status) {
        emptyMessage.push({
          "_id": Date.now() + Math.floor(Math.random(1,999)*1000)+"jb", 
          "text": response.successObject.text,
          "createdAt": new Date(),
           "user": {
              "_id": 1,
              "name": 'Justbooks BOT',
              "avatar": 'https://justbooks.in/images/noimage.jpg',
            },
            "sent": true,
            "received": true,
        });
      }
      this.setState({
        messages: emptyMessage
      });
    }

    async onSend(messages = []) {
      console.log("message",messages);
      var membership = this.state.memberCard ? this.state.memberCard : "M1197731";
      var gettingMessage =  messages[0].text;
      var masterId, mappingId;
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, messages),
      }))
      let fetchServices;
      if(gettingMessage.length == 1) {
        masterId = messages[0].text;
        fetchServices = await fetch(`http://esapi.justbooks.in/talkToJBBot?masterId=${masterId}&membership=${membership}`);
      } else if(gettingMessage.length >= 6) {
        masterId = 1;
        mappingId = messages[0].text;
        fetchServices = await fetch(`http://esapi.justbooks.in/talkToJBBot?masterId=${masterId}&membership=${membership}&mappingId=${mappingId}`);
      }else {
        masterId = messages[0].text;
        fetchServices = await fetch(`http://esapi.justbooks.in/talkToJBBot?masterId=${masterId}&membership=${membership}`);
      }

      let response = await fetchServices.json();
      // console.log("response", response);
      let emptyMessage = [];
      if(response.status) {
        emptyMessage.push({
          "_id": Date.now() + Math.floor(Math.random(1,999)*1000)+"jb", 
          "text": response.successObject.text,
          "createdAt": new Date(),
           "user": {
              "_id": 1,
              "name": 'Justbooks BOT',
              "avatar": 'https://justbooks.in/images/noimage.jpg',
            },
            "sent": true,
            "received": true,
        });
      }
      console.log("this.state.messages",this.state.messages);

      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, emptyMessage),
      }))
    }
    
    renderInputToolbar (props) {
        //Add the extra styles via containerStyle
       return <TextInput {...props} containerStyle={{borderTopWidth: 1.5, borderTopColor: '#000', paddingTop: 10}} />
    }

    renderBubble(props) {
      return (
        <Bubble
          {...props}
          textStyle={{
            right: { color: '#000000' },
            left: { color: '#000000' },
          }}
          wrapperStyle={{
            right: { backgroundColor: '#FFFFFF', color: '#FFFFFF' },
            left: { backgroundColor: '#FFFFFF', color: '#000000' }
          }}
        />
      );
    }
    
    renderSend(props) {
      return (
          <Send
              {...props}
              containerStyle={{
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                marginRight: 5,}}>
              <View style={{marginRight: 2, marginBottom: 5, borderWidth: 1, borderColor: '#F1F1F1', padding: 5, borderRadius: 4}}>
                <Text style={styles.footerText}><Icons name="send" size={20} color={'#000000'} /> Send</Text>
              </View>
          </Send>
      );
    }
    
    render() {
        return (
          <ImageBackground 
          source={{uri: 'https://wallpapercave.com/wp/wp4410779.png'}}
          style={styles.image}>
            <View style={styles.contain}>
                <StatusTopBar />
                  <View style={{flex: 1}}>
                      <GiftedChat
                          textInputProps={{autoFocus: true}}
                          messages={this.state.messages}
                          renderBubble={this.renderBubble}
                          // minInputToolbarHeight={60}
                          // multiline={true}
                          showUserAvatar={true}
                          // renderInputToolbar={this.renderInputToolbar}
                          renderSend={this.renderSend}
                          // textInputStyle= {{height: 60, backgroundColor: 'blue'}}
                          // onSend={Fire.shared.send}
                          onSend={messages => this.onSend(messages)}
                          user={{
                              _id: 2,
                              name: 'Member',
                              avatar: 'https://www.timeforamassage.co.uk/wp-content/uploads/2020/09/girl-place.jpg',
                          }}
                      />
                  </View>
            </View>
          </ImageBackground>
        );
    }

}

export default ChatBot;