import React from 'react';
import { View, Text, StatusBar } from 'react-native';
import styles from './styles';
import { TextInput, Appbar, Divider, RadioButton, Button, Card, Title, Paragraph, Headline, Subheading, IconButton } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import { showMessage } from "react-native-flash-message";
import AsyncStorage from '@react-native-community/async-storage';
import API from '../../env';
import * as axios from 'axios';

class EditMember extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subscription_id: 0,
            memberCard: 0,
            name: this.props.route.params.name,
            mobile: this.props.route.params.mobile,
            dob: this.props.route.params.dateOfBirth,
            familyMemberId: this.props.route.params.familyMemberId,
            nameError: false,
            mobileError: false,
            dobError: false,
        }
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }
    };

    _editMember = async() => {
        if (this.state.name === '') {
            this.setState({ 
                nameError : true,
                errorMsg: 'Name is required Field!'
            })
            return;
        }

        if (this.state.mobile === '') {
            this.setState({ 
                mobileError : true,
                errorMsg: 'Mobile Required field!'
            })
            return;
        }

        if (this.state.dob === '') {
            this.setState({ 
                dobError : true,
                errorMsg: 'Date of birth Required field!'
            })
            return;
        }
    
        this._editMemberRequest();
    }

    _editMemberRequest() {

        const editMember = API.API_JUSTBOOK_ENDPOINT + "/updateFamilyMember";
        axios.post(editMember ,{
            "familyMemberId" : this.state.familyMemberId,
            "createdAt":810,
            "createdBy":1000,
            "name" : this.state.name,
            "mobile" : this.state.mobile,
            "dateOfBirth": this.state.dob
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response update Member", response.data);
            if (response.data.status) {
                showMessage({
                    message: "Update Member",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                setTimeout(() => {
                    this.props.navigation.push("Member");
                },2000);
            } else {
                showMessage({
                    message: "Update Member",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Update Member",
                description: "Something went wrong, Please try again later!",
                icon: "danger",
                type: "danger",
            });
        });
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <ScrollView>
                    <View style={{ marginBottom: 5 ,marginTop: 10}}>
                        <View style={styles.cardHomeView}>
                            <Text style={styles.titleDesciption}>
                                <Headline style={styles.titleStyle}>Member Details</Headline>{'\n'}
                                <Text style={{color: 'green', fontSize: 16 }}>
                                    <Paragraph style={styles.subtitleStyle}>Enter your Member details to ensure  Member account</Paragraph>
                                </Text>{'\n'}
                            </Text>
                            <View style={styles.form}>
                                {/* <Paragraph style={styles.label}> Enter Name </Paragraph> */}
                                <TextInput
                                    placeholder="Enter Name"
                                    value={this.state.name}
                                    style={styles.textInput}
                                    underlineColor={'#BEBEBE'}
                                    selectionColor={'#BEBEBE'}
                                    labelStyle={{color: '#BEBEBE'}}
                                    placeholderTextColor={'#BEBEBE'}
                                    theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                    onChangeText={text => this.setState({name : text, nameError : false})}
                                />
                                {this.state.nameError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}

                                <TextInput
                                    placeholder="Enter Mobile"
                                    value={this.state.mobile}
                                    style={styles.textInput}
                                    underlineColor={'#BEBEBE'}
                                    selectionColor={'#BEBEBE'}
                                    labelStyle={{color: '#BEBEBE'}}
                                    placeholderTextColor={'#BEBEBE'}
                                    theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                    onChangeText={text => this.setState({mobile : text, mobileError : false})}
                                />
                                {this.state.mobileError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}

                                <DatePicker
                                    style={{width: '100%',marginBottom: 10, marginTop: 10}}
                                    mode="date"
                                    date={this.state.dob}
                                    placeholder="Date of Birth"
                                    format="YYYY-MM-DD"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            right: 0,
                                            top: 4
                                        },
                                        dateInput: {
                                            position: 'absolute',
                                            left: 0, 
                                            right: 10,
                                            height: 50,
                                            paddingTop: 10,
                                            marginBottom: 0,
                                            marginLeft: 10,
                                            paddingLeft: 10,
                                            backgroundColor: '#FFFFFF',
                                            borderBottomColor: '#BEBEBE',
                                            color: '#121111',
                                            borderBottomWidth: 0.5,
                                            borderLeftWidth: 0,
                                            borderRightWidth: 0,
                                            borderTopWidth: 0,
                                            textAlign: 'left',
                                            alignItems: 'flex-start'
                                        }
                                        // ... You can check the source to find the other keys.
                                    }}
                                    onDateChange={(date) => {this.setState({dob: date})}}
                                />
                                {this.state.dobError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                
                                <Button icon="account-plus" mode="contained" labelStyle={styles.labelStyle} style={styles.btn} onPress={() => this._editMember()}>
                                    Update Member Account
                                </Button>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <BottomTab navigation={this.props.navigation} />
            </View>
        );
    }

}

export default EditMember;