import React from 'react';
import { Image, View, TextInput, Text, Button, Linking, StatusBar } from 'react-native';
import styles from './styles';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

import Icon from 'react-native-vector-icons/FontAwesome';
import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';

class FAQs extends React.Component {

    faqs = [
        { question: 'What about Justbooks', answer: 'Justbooks Under 19 Open Cricket tournament. 3CS Cricket Ground , 11/05/2020. Register Now. Noida Open Badminton Event. Crowdfit Badminton' },
        { question: 'Reading Platform', answer: '"Justbooks"​ as name suggests a sports firm which organises tournaments for corporates,kids,colleges students,etc in Delhi/Gurgaon regions. We strive to' },
        { question: 'How we can use', answer: '"Justbooks"​ as name suggests a sports firm which organises tournaments for corporates,kids,colleges students,etc in Delhi/Gurgaon regions. We strive to' }
    ];

    _connectScreen(action) {
        if (action === 'gmail') {
            Linking.openURL('mailto:sanjeevkumar@justbooksclc.com');
        }

        if (action === 'call') {
            let phoneNumber = '';

            if (Platform.OS === 'android') {
                phoneNumber = 'tel:${7004798516}';
            } else {
                phoneNumber = 'telprompt:${7004798516}';
            }
            Linking.openURL(phoneNumber);
        }

        if (action === 'whatsapp') {
            Linking.openURL("https://wa.me/+91 7004798516?text=I'm%20interested%20to%20buy%20book! %20please%20connect%20with%20me.")
        }
    }

    renderFAQ(faq, index) {
        return <View style={styles.cardContainer} key={index}>
            <View style={styles.headingContainer}>
                <View style={styles.calendarConainer}>
                    <Icon name="question-circle-o" size={16} color="#2E2E2E" style={{ marginLeft: 5, marginRight: 3 }} />
                    <Text style={[styles.dateText, { marginLeft: 10, color: '#2E2E2E'}]}>{faq.question}</Text>
                </View>
            </View>
            <View style={styles.view } />
            <View style={styles.mainContainer}>
                <View style={styles.headingContainer}>
                    <TouchableOpacity onPress={this.handleClick}>
                        <View style={{ padding: 16 }}>
                            <Text style={styles.fontStyle}>
                                {faq.answer}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    }

    renderFAQs() {
        return this.faqs.map((faq, index) => {
            return this.renderFAQ(faq, index);
        })
    }

    _bookDetailsNavigate = async() => {
        this.props.navigation.navigate("BookDetails");
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <ScrollView>
                    <View style={styles.containBody}>
                        <View class={styles.shareContainer}>
                            <View style={styles.viewIssue}>
                                <Text
                                    style={{ fontSize: 17, textAlign: 'center', color: '#2E2E2E' }}>
                                    Found an issue? Let us know!
                                </Text>
                            </View>

                            <View style={styles.subtitleView}>
                                <View style={styles.containerr}>

                                    <View style={styles.buttonContainer}>
                                        <TouchableOpacity onPress={() => this._connectScreen('gmail')}>
                                            <View style={{ alignSelf: "flex-end" }}>
                                                <View style={styles.profileImage}>
                                                    <Image source={require("../../assets/gmail.png")} style={styles.image} ></Image>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={styles.buttonContainer}>
                                        <TouchableOpacity onPress={() => this._connectScreen('call')}>
                                            <View style={{ alignSelf: "center" }}>
                                                <View style={styles.profileImage}>
                                                    <Image source={require("../../assets/call.png")} style={styles.image} ></Image>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={styles.buttonContainer}>
                                        <TouchableOpacity onPress={() => this._connectScreen('whatsapp')}>
                                            <View style={{ alignSelf: "flex-start" }}>
                                                <View style={styles.profileImage}>
                                                    <Image source={require("../../assets/whatsapp.png")} style={styles.image} ></Image>
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>

                        {this.renderFAQs()}


                    </View>
                </ScrollView>
                <SearchTab navigation={this.props.navigation} />
                <BottomTab navigation={this.props.navigation} />
            </View>
        );
    }

}

export default FAQs;