import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 55
    },
    cardHomeView: {
        backgroundColor: '#101211', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 15, 
        marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },
    divider : {
        margin: 10,
    },
    cardStyle : {
        backgroundColor: '#39403c', 
        borderRadius: 6
    },
    titleStyle: {
        color : '#fff'
    },
    subtitleStyle: {
        color : '#fff'
    },
    rightStyle : {
        backgroundColor: '#fff', 
        borderBottomRightRadius: 6, 
        borderTopRightRadius: 6, 
        height: 70, 
        paddingTop: 8
    },
    seperator: {
      width: '100%',
    },
    shareContainer: {
      marginBottom: 20
    },
    subtitleView: {
      marginTop: 5,
      marginBottom: 35
    },
    containerr: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonContainer: {
      flex: 1,
      paddingRight: 10,
    },
    profileImage: {
      marginTop: 20,
      width: 50,
      height: 50,
      borderRadius: 100,
      overflow: "hidden"
    },
    image: {
      flex: 1,
      height: 50,
      width: undefined
    },
    cardContainer: {
      marginLeft: 20,
      marginRight: 20,
      marginTop: 10,
      marginBottom: 10,
      padding: 15,
      backgroundColor: '#F1F1F1',
      shadowColor: "#F1F1F1",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,
      elevation: 0,
      borderRadius: 4,
      borderWidth: 0.2,
      borderColor: '#fff'
    },
    headingContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    calendarConainer: {
      paddingVertical: 6,
      display: 'flex',
      flexDirection: 'row',
      width: '80%'
    },
    dateText: {
      fontSize: 18,
      fontWeight: '600',
      marginTop: -5
    },
    button : {
      alignItems: 'flex-end'
    },
    wrapper: {
      flex: 1,
      width: '100%',
      paddingHorizontal: 35,
    },
    buttonWrapper: {
      marginTop: 50,
      paddingHorizontal: 35,
      justifyContent: 'flex-end',
    },
    view: {
        marginTop: 10,
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
    },
    fontStyle: { 
        fontSize: 15, 
        color: "#2E2E2E" 
    },
    viewIssue: { 
        marginTop: 20, 
        marginBottom: 0, 
        textAlign: 'center' 
    }

});
