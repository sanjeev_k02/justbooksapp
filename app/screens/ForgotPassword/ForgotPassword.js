import React from 'react';
import { View, StatusBar, Image } from 'react-native';
import styles from './styles';
import { TextInput, Appbar, Divider, RadioButton, Button, Card, Title, Paragraph, Headline, Subheading, Text } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { showMessage, hideMessage } from "react-native-flash-message";

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import API from '../../env';
import * as axios from 'axios';

class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            otp: '',
            emailError: false,
            errorMsg: ''
        }
    }

    
    _openResendPasswordRequest = () => {
        if (this.state.email === '') {
            this.setState({ 
                emailError : true,
                errorMsg: 'email Id Required field!'
            })
            return;
        }
    
        this._SendForgotPasswordRequest();
      };
    
      async _SendForgotPasswordRequest() {
    
        const generateForgotPassword = API.API_JUSTBOOK_ENDPOINT + "/requestPasswordLink?queryParam="+this.state.email;
        try {
          let response = await axios.get(generateForgotPassword, { headers: { 'X-SECRET-TOKEN': 'qwerty' } });
          console.log("response email", response.data.successObject);
          if (response.data.status) {
                showMessage({
                    message: "Reset Password",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
            //   this._navigateSignInScreen(response.data.successObject);
          } else {
            showMessage({
                message: "Reset Password",
                description: response.data.errorDescription,
                icon: "danger",
                type: "danger",
            });
          }
        } catch(err) {
          console.log(err);
          showMessage({
              message: "Reset Password",
              description: 'Oops Something went wrong! try again later',
              icon: "danger",
              type: "danger",
          });
        }   
      }
    
    _navigateSignInScreen (successMsg) {
        this.props.navigation.navigate('GetForgotPassword', {
          email: this.state.email,
          successMsg: successMsg
        });
    }

    _GetOtpSigInNavigate = async() => {
        this.props.navigation.navigate("GetForgotPassword");
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <ScrollView>
                    <View style={{ marginBottom: 5 ,marginTop: 10}}>
                        <View style={styles.containerLogo}>
                            <Image
                                style={styles.logoImage}
                                source={require('../../../assets/images/logo.jpg')}
                            />
                        </View>
                        <View style={styles.containerImage}>
                            <View style={styles.wrapper}>
                                <Text style={styles.nameDesc}>
                                    <Headline style={styles.colorSignIn}> Forgot Password </Headline>
                                    {'\n'}{'\n'}
                                </Text>
                            </View>
                            <Image
                                style={styles.signInImage}
                                source={require('../../assets/login.png')}
                            />
                        </View>
                        <View style={styles.cardHomeView}>
                            <View style={styles.form}>
                                <TextInput
                                    placeholder="Enter Registred Email Id"
                                    value={this.state.email}
                                    style={styles.textInput}
                                    underlineColor={'#BEBEBE'}
                                    selectionColor={'#BEBEBE'}
                                    labelStyle={{color: '#BEBEBE'}}
                                    placeholderTextColor={'#BEBEBE'}
                                    theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                    onChangeText={text => this.setState({ emailError: false, email : text})}
                                />
                                {this.state.emailError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                               
                                <View style={styles.wrapper}>
                                    <Text style={styles.buttonDesc}>{'\n'}
                                        <Button icon="send" mode="outline" labelStyle={{fontSize: 18, color: '#FFFFFF', textTransform: "none"}} style={styles.btn} onPress={()=>{this._openResendPasswordRequest()}}>
                                           Reset Password
                                        </Button>{'\n'}{'\n'}{'\n'}
                                    </Text>
                                </View>
                            </View>

                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

}

export default ForgotPassword;