import React from 'react';
import { View, Text, StatusBar, Image } from 'react-native';
import styles from './styles';
import { TextInput, Appbar, Divider, RadioButton, Button, Card, Title, Paragraph, Headline, ActivityIndicator, Colors   } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

import OTPTextView from 'react-native-otp-textinput';
import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import API from '../../env';
import * as axios from 'axios';

class GetOtpSignIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loginActivity: false,
            mobile: this.props.route.params.mobile,
            // getOtp: this.props.route.params.otp,
            successMsg: this.props.route.params.successMsg,
            otp: '',
            inputText: '',
            otpError: false,
            resendOtpError: false,
            errorMsg: ''
        }
    }

    _resendOtp = async () => {
        const generateOTP = API.API_JUSTBOOK_ENDPOINT + "/requestOtp?queryParam="+this.state.mobile;
        try {
          let response = await axios.post(generateOTP, { headers: { 'X-SECRET-TOKEN': 'qwerty' } });
          console.log("response mobile", response.data.successObject);
          if (response.data.status) {
              this.setState({
                    resendOtpError : false,
                })
              this._navigateSignInScreen(response.data.successObject);
          } else {
            this.setState({ 
                resendOtpError : true,
                errorMsg: response.data.errorDescription
            })
            return;
          }
        } catch(err) {
          console.log(err);
          this.setState({ 
              resendOtpError : true,
              errorMsg: 'Oops Something went wrong!'
          })
        } 
    }
    
    _navigateSignInScreen (successMsg) {
        this.props.navigation.push('GetOtpSignIn', {
          mobile: this.state.mobile,
          successMsg: successMsg
        });
    }

    _verifiedOtp = () => {
        if (this.state.otp === '') {
            this.setState({ 
                otpError : true,
                errorMsg: 'Otp Required field!'
            })
          return;
        }

        // if( this.state.getOtp != this.state.otp ) {
        //     this.setState({ 
        //         otpError : true,
        //         errorMsg: 'Otp should not match. please enter valid Otp!'
        //     })
        //     return;
        // }
        this.setState({ 
            loginActivity: true
        })

        this._signInOtpRequest();
    };
    
    _signInOtpRequest() {

        const validateOTP = API.API_JUSTBOOK_ENDPOINT + "/validateOTP?queryParam="+this.state.mobile+"&otp="+this.state.otp;
        console.log(validateOTP);
        axios.post(validateOTP , { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response otp", response.data);
            if (response.data.status) {
                let loggedIn = {
                    "active": response.data.successObject.user.subscriptions[0].active,
                    "branchId": response.data.successObject.user.subscriptions[0].branchId,
                    "cityId": response.data.successObject.user.subscriptions[0].cityId,
                    "memberCard": response.data.successObject.user.subscriptions[0].memberCard,
                    "primus": response.data.successObject.user.subscriptions[0].primus,
                    "primusMappedBranches": response.data.successObject.user.subscriptions[0].primusMappedBranches,
                    "subscriptionId": response.data.successObject.user.subscriptions[0].subscriptionId
                }
                this._getSubscriptionDetails(response.data.successObject.user.subscriptions[0].subscriptionId, loggedIn)
            } else {
                this.setState({ 
                    otpError : true,
                    errorMsg: response.data.errorDescription
                })
            }
        })
        .catch(function (error) {
            console.log(error);
            this.setState({ 
                otpError : true,
                errorMsg: 'Something went wrong, Please try again later!'
            })
        });
    }

    _getSubscriptionDetails = (subscriptionId, loggedIn) => {
        const getSubscription = API.API_JUSTBOOK_ENDPOINT+"/getSubscriptionForId?subscription_id="+subscriptionId;
        console.log(getSubscription);
        axios.get(getSubscription, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
          .then((response) => {
            // console.log(response.data.successObject);
              if (response.data.status) {
                  let successObject = Object.assign(response.data.successObject, {"login" : loggedIn});
                  this._storeData(successObject)
              } else {
                  console.log("subscription Id not found");
              }
          })
          .catch((error) => {
            console.log(error);
          });
    }
  
    _storeData = async (response) => {
        try {
            AsyncStorage.setItem('userData', JSON.stringify(response), () => {
            setTimeout(() => {
                this.props.navigation.push("Home");
            }, 0);
            });
        } catch (error) {
            // Error saving data
            console.log('error msg', error);
        }
    };

    _OtpNavigate = async() => {
        this.props.navigation.navigate("OtpSignIn");
    }

    alertText = () => {
        const {otp = ''} = this.state;
        if (otp) {
            Alert.alert(otp);
        }
    };

    clear = () => {
        this.input1.clear();
    };

    updateOtpText = () => {
        // will automatically trigger handleOnTextChange callback passed
        this.input1.setValue(this.state.inputText);
    };
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <ScrollView>
                    <View style={{ marginBottom: 5 ,marginTop: 10}}>
                        <View style={styles.containerLogo}>
                            <Image
                                style={styles.logoImage}
                                source={require('../../../assets/images/logo.jpg')}
                            />
                        </View>
                        <View style={styles.containerImage}>
                            <View style={styles.wrapper}>
                                <Text style={styles.nameDesc}>
                                    {/* <Headline style={styles.colorSignIn}> Sign In </Headline> */}
                                    {'\n'}{'\n'}
                                </Text>
                            </View>
                            <Image
                                style={styles.signInImage}
                                source={require('../../assets/login.png')}
                            />
                        </View>
                        <View style={styles.cardHomeView}>
                            <View style={styles.form}>
                               <Text style={styles.nameDesc}>
                                    <Text style={styles.chargescolor}> {this.state.successMsg} </Text>{'\n'}
                                </Text>

                                <OTPTextView
                                    ref={(e) => (this.input1 = e)}
                                    containerStyle={styles.textInputContainer}
                                    textInputStyle={styles.roundedTextInput}
                                    handleTextChange={(text) => this.setState({otp: text})}
                                    inputCount={6}
                                    keyboardType="numeric"
                                /> 
                                {this.state.otpError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}

                                <View style={styles.wrapper}>
                                    <Text style={styles.buttonDesc}>{'\n'}
                                        { this.state.loginActivity ? <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                                        : <Button icon="send" mode="outline" labelStyle={{fontSize: 18, color: '#FFFFFF', textTransform: "none"}} style={styles.btn} onPress={() => this._verifiedOtp()}> Sign In </Button>}
                                        {'\n'}{'\n'}{'\n'}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.wrapper}>
                                <TouchableOpacity  onPress={() => this._resendOtp()}>
                                    <Text style={styles.nameDesc}>
                                        <Headline style={styles.color}>Resend OTP </Headline>
                                        <Divider style = {styles.divider}/>{'\n'}{'\n'}
                                    </Text>
                                </TouchableOpacity>
                                {this.state.resendOtpError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                            </View>

                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

}

export default GetOtpSignIn;