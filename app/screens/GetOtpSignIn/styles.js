import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    cardHomeView: {
        alignSelf: 'center',
        backgroundColor: '#FFFFFF',
        margin: 10, 
        padding: 15,
    },
    divider : {
        margin: 0,
        color: '#fff',
        backgroundColor: '#fff'
    },
    cardStyle : {
        backgroundColor: '#39403c', 
        borderRadius: 6
    },
    titleStyle: {
        color : '#fff'
    },
    subtitleStyle: {
        color : '#fff'
    },
    rightStyle : {
        backgroundColor: '#fff', 
        borderBottomRightRadius: 6, 
        borderTopRightRadius: 6, 
        height: 70, 
        paddingTop: 8
    },
    textInput: {
        height: 55,
        marginTop: 8,
        marginBottom: 8,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 5
    },
    wrapper: {
        alignItems: "center",
        marginTop: 10,
        marginBottom: 0
    },
    buttonDesc : {
        alignItems: "center",
        textAlign: 'center'
    },
    form: {
        // backgroundColor: '#fff',
        alignItems: "center",
        textAlign: 'center',
        marginTop: 25
    },
    btn: {
        height: 42, 
        textAlign: 'center', 
        margin: 20,
        fontSize: 18,
        borderColor :'#2E2E2E',
        borderWidth : 1,
        width: '100%',
        color: '#FFFFFF',
        backgroundColor: '#2E2E2E',
        borderRadius: 20
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start' // if you want to fill rows left to right
    },
    item: {
        width: '50%' // is 50% of container width
    },
    color: {
        color: '#2E2E2E',
        fontSize: 20,
        textAlign: 'center',
        borderBottomColor: '#2E2E2E',
        borderWidth: 2
    },
    colorParagraph: {
        color: 'pink',
    },
    nameDesc: {
        margin: 10
    },
  instructions: {
    fontSize: 22,
    fontWeight: '500',
    textAlign: 'center',
    color: '#333333',
    marginBottom: 20,
  },
  textInputContainer: {
    marginBottom: 20,
    color: '#2E2E2E'
  },
  roundedTextInput: {
    borderRadius: 6,
    borderWidth: 2,
    color: '#2E2E2E',
    width: 42
  },
  buttonWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 20,
    width: '60%',
  },
  chargescolor: {
      color: '#2E2E2E',
      fontSize: 14,
      textAlign: 'center'
  },
  nameDesc: {
      marginTop: 0,
      marginBottom: 0
  },
  colorError: {
      color: 'red',
      fontSize: 20,
      textAlign: 'center'
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40
  },
  containerLogo: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 0
  },
  logoImage: {
    width: 250,
    height: 60,
    resizeMode: 'cover',
    marginTop: 0 //Dimensions.get('window').height / 4,
  },
  signInImage: {
    width: 280,
    height: 125,
    resizeMode: 'stretch',
    marginTop: 0 //Dimensions.get('window').height / 4,
  },

});
