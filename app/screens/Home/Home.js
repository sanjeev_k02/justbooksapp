import React from 'react';
import { View,  ScrollView, RefreshControl, BackHandler, Image } from 'react-native';
import styles from './styles';
import { Divider, Text, Card, Title, Paragraph, ActivityIndicator, Colors  } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import {  } from 'react-native-gesture-handler';
import { Appbar } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import SearchTab from '../../components/SearchTab/SearchTab';
import BottomTab from '../../components/BottomTab/BottomTab';
import CategoryTab from '../../components/CategoryTab/CategoryTab';
import API from '../../env';
import * as axios from 'axios';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          subscription_id: 0,
          memberCard: 0,
          branchId: 0,
          cityId: 0,
          primusMappedBranches: 0,
          primusMember: false,
          page: 1,
          pageSize: 5,
          userDetails: {},
          refreshing: true,
          authorFlag: false,
          authorData: {},
          newArrivlesFlag: false,
          newArrivlesData: {},
          shelfRecommendedBooksFlag: false,
          shelfRecommendedBooks: {},
          comingSoonBooksFlag: false,
          comingSoonBooks: {},
          topMagazineBooksFlag: false,
          topMagazineBooks: {},
          topTitleFlag: false,
          topTitleData: {},
          widgetBooksFlag: false,
          widgetBook: {}
        };
    }

    _toTitleCase(str) { 
        if(str.length > 16) {
            var res = str.substring(0, 16);
            return res.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            )+"";
        } else {
            return str.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            );
        }
    }

    componentDidMount = async() => {
      BackHandler.addEventListener("hardwareBackPress", this.backPressed);
        try {
            const user = await AsyncStorage.getItem('userData');
            // console.log("user",user);
            if (user !== null) {
                const userData = JSON.parse(user);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard,
                    branchId: userData.login.branchId,
                    primusMappedBranches: userData.login.primusMappedBranches,
                    cityId: userData.login.cityId,
                    primusMember: userData.login.primus
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        if(this.state.subscription_id > 0) {
            await this.getAllNewArrivlesAfterLogin(this.state.subscription_id);
        } else {
            await this.getAllNewArrivles();
        }

        // getShelfRecommendedBooks
        if(this.state.subscription_id > 0) {
            await this.getAllShelfRecommendedBooksAfterLogin();
        }

        // getShelfRecommendedBooks
        if(this.state.subscription_id > 0) {
            await this.getAllTopMagazineBooksAfterLogin();
        }

        // comingSoon
        if(this.state.subscription_id > 0) {
            await this.getComingSoonAfterLogin();
        }
        await this.getAllAuthorData();
        await this.getAllTopTitle(this.state.subscription_id);

        //widget function
        if(this.state.subscription_id > 0) {
            this.getUserWidgetData(this.state.subscription_id);
        }
        setTimeout(() => {
            this.setState({ refreshing: false });
        }, 1000);
    };

    componentWillUnmount() {
      BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    }

    backPressed = () => {
        let routeName = this.props.route.name;
        if (routeName == "Home" && this.props.navigation.isFocused()) {
        //   BackHandler.exitApp();
        }
    };

    getUserWidgetData = (subscriptionId) => {
        let getUserWidget = API.API_JUSTBOOK_ENDPOINT+"/getUserWidgetData?subscriptionId="+subscriptionId+"&pageNumber="+this.state.page+"&pageSize="+this.state.pageSize;
        console.log(getUserWidget);
        axios.get(getUserWidget, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
          .then((response) => {
            // console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    widgetBook : response.data.successObject,
                    widgetBookFlag: true
                })
            } else {
                this.setState({
                    widgetBookFlag: false,
                });
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }

    getAllNewArrivlesAfterLogin = (subscriptionId) => {
        let getNewArrivles;
        if(this.state.primusMember) {
            getNewArrivles = API.API_ESAPI_POINT+"/getNewArrivalsForPrimus?subscriptionId="+subscriptionId+"&branchIds="+this.state.primusMappedBranches+"&cityId="+this.state.cityId;
        } else { 
            getNewArrivles = API.API_ESAPI_POINT+"/getNewArrivalsMobile?subscriptionId="+subscriptionId+"&branchId="+this.state.branchId;
        } 
      console.log(getNewArrivles);
      axios.get(getNewArrivles)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.content.length > 0) {
                this.setState({
                    newArrivlesData : response.data.successObject.content,
                    newArrivlesFlag: true
                })
            } else {
                this.setState({
                    newArrivlesFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllShelfRecommendedBooksAfterLogin = () => {
      const getShelfRecommendedBooks = API.API_ESAPI_POINT+"/getRecommendedTitles?membershipNo="+this.state.memberCard;
      console.log(getShelfRecommendedBooks);
      axios.get(getShelfRecommendedBooks)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    shelfRecommendedBooks : response.data.successObject,
                    shelfRecommendedBooksFlag: true
                })
            } else {
                this.setState({
                    shelfRecommendedBooksFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllTopMagazineBooksAfterLogin = () => {
      const getTopMagazineBooks = API.API_ESAPI_POINT+"/getTopMagazinesAvailableForBranch?branchId="+this.state.branchId+"&page="+this.state.page;
      console.log(getTopMagazineBooks);
      axios.get(getTopMagazineBooks)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    topMagazineBooks : response.data.successObject,
                    topMagazineBooksFlag: true
                })
            } else {
                this.setState({
                    topMagazinesBooksFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getComingSoonAfterLogin = () => {
      const getComingSoonBooks = API.API_ESAPI_POINT+"/getComingSoonBooks?page="+this.state.page;
      console.log(getComingSoonBooks);
      axios.get(getComingSoonBooks)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    comingSoonBooks : response.data.successObject,
                    comingSoonBooksFlag: true
                })
            } else {
                this.setState({
                    comingSoonBooksFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllNewArrivles = () => {
      const getNewArrivles = API.API_ESAPI_POINT+"/getNewArrivalsMobile";
      console.log(getNewArrivles);
      axios.get(getNewArrivles)
        .then((response) => {
        //   console.log(response.data.successObject.content);
            if (response.data.status && response.data.successObject.content.length > 0) {
                this.setState({
                    newArrivlesData : response.data.successObject.content,
                    newArrivlesFlag: true
                })
            } else {
                this.setState({
                    newArrivlesFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllAuthorData = () => {
      const getAuthor = API.API_ESAPI_POINT+"/getPopularAuthors";
      console.log(getAuthor);
      axios.get(getAuthor)
        .then((response) => {
        //   console.log(response.data.successObject.content);
            if (response.data.status && response.data.successObject.content.length > 0) {
                this.setState({
                    authorData : response.data.successObject.content,
                    authorFlag: true
                })
            } else {
                this.setState({
                    authorFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllTopTitle = (subscriptionId) => {
      let getTopTitle = API.API_ESAPI_POINT+"/getTopTitles";
      if(subscriptionId > 0 && this.state.primusMember) {
        getTopTitle = API.API_ESAPI_POINT+"/getTopTitlesForPrimus?subscriptionId="+subscriptionId+"&page="+this.state.page+"&branchIds="+this.state.primusMappedBranches+"&cityId="+this.state.cityId;
      } else {
        getTopTitle = API.API_ESAPI_POINT+"/getTopTitles?subscriptionId="+subscriptionId+"&page="+this.state.page;
      }
      console.log(getTopTitle);
      axios.get(getTopTitle)
        .then((response) => {
        //   console.log(response.data.successObject.content);
            if (response.data.status && response.data.successObject.content.length > 0) {
                this.setState({
                    topTitleData : response.data.successObject.content,
                    topTitleFlag: true
                })
            } else {
                this.setState({
                    topTitleFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _onRefresh = async () => {
        this.setState({refreshing: true});

        if(this.state.subscription_id > 0) {
            await this.getAllNewArrivlesAfterLogin(this.state.subscription_id);
        } else {
            await this.getAllNewArrivles();
        }

        // getShelfRecommendedBooks
        if(this.state.subscription_id > 0) {
            await this.getAllShelfRecommendedBooksAfterLogin();
        }

        // getShelfRecommendedBooks
        if(this.state.subscription_id > 0 && this.state.primusMember) {
            await this.getAllTopMagazineBooksAfterLogin();
        }

        // comingSoon
        if(this.state.subscription_id > 0) {
            await this.getComingSoonAfterLogin();
        }
        await this.getAllAuthorData();
        await this.getAllTopTitle(this.state.subscription_id);

        //widget function
        if(this.state.subscription_id > 0) {
            this.getUserWidgetData(this.state.subscription_id);
        }

        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
    }

    _editUserWidget = (widgetId, widgetName, authorCsv, seriesCsv, categoryCsv) => {
        this.props.navigation.push("EditWidget",{
            widgetId: widgetId,
            widgetName: widgetName,
            authorCsv: authorCsv,
            seriesCsv: seriesCsv,
            categoryCsv: categoryCsv
        })
    }

    _bookDetailsNavigate = async(title_id) => {
        this.props.navigation.push("BookDetails",{ title_id: title_id });
    }

    _allAuthorsNavigate = async() => {
        this.props.navigation.navigate("AllAuthors");
    }

    _authorDetailsNavigate = async(author_id) => {
        this.props.navigation.navigate("AuthorDetails", { author_id });
    }

    _allBooksNavigate = async(action) => {
        this.props.navigation.push("AllBooks", {
            action
        });
    }

    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { (this.state.refreshing) ?
                <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView 
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            <CategoryTab navigation={this.props.navigation} />
                            <View style={styles.sliderView}>
                                <View style={styles.cardSliderView}>
                                    <ScrollView 
                                    horizontal={true}>
                                    <Card style={styles.cardSliderSection} onPress={()=>{this._bookDetailsNavigate(281743)}}>
                                        <Card.Cover style={styles.coverSliderImage} source={{ uri: 'https://vcbookcovers.com/wp-content/uploads/2019/02/vc-book-cover-design-slider-3.jpg' }} />
                                    </Card>
                                    <Card style={styles.cardSliderSection} onPress={()=>{this._bookDetailsNavigate(187520)}}>
                                        <Card.Cover style={styles.coverSliderImage} source={{ uri: 'https://buzbooks.com/wp-content/uploads/2018/03/Untitled-2-min-669x272.jpg' }} />
                                    </Card>
                                    <Card style={styles.cardSliderSection} onPress={()=>{this._bookDetailsNavigate(1392)}}>
                                        <Card.Cover style={styles.coverSliderImage} source={{ uri: 'https://www.fionajaydemedia.com/sandbox_new/wp-content/uploads/2015/04/Slider-1186x5231.png' }} />
                                    </Card>
                                    </ScrollView>
                                </View>
                            </View>

                            {this.state.widgetBookFlag && <View style={styles.newArrivles}>
                                {this.state.widgetBook.map((parentWidget) => {
                                    return (<View key={parentWidget.widgetId}>
                                            {parentWidget.titles.length > 0 ? (<View style={styles.parentWidget}>
                                                <View style={styles.headingView}>
                                                    <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                                        <Appbar.Content title={parentWidget.widgetName} titleStyle={[styles.headingStyle,{width: 250}]} style={styles.heading} />
                                                        <Appbar.Action icon="playlist-edit" title="Edit" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._editUserWidget(parentWidget.widgetId, parentWidget.widgetName, parentWidget.authorCsv, parentWidget.seriesCsv, parentWidget.categoryCsv)}} />
                                                    </Appbar.Header>
                                                </View>
                                                <View style={styles.cardView}>
                                                    { parentWidget.titles.length > 0 &&  <View style={styles.childWidget}>
                                                        <ScrollView 
                                                        horizontal={true}>
                                                                {parentWidget.titles.map((childWidget)=> {
                                                                    return (
                                                                        <Card key={childWidget.titleId} style={styles.cardSection} onPress={()=>{this._bookDetailsNavigate(childWidget.titleId)}}>
                                                                            {(childWidget.titleImage != null && childWidget.titleImage != '' && childWidget.titleImage != 'null' && childWidget.titleImage != 'images/noimage.jpg') ? <Card.Cover style={styles.coverImage} source={{ uri: childWidget.titleImage }} />
                                                                            : <View style={styles.NonImage}><Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                                                <Card.Content style={styles.NonImageCardText}>
                                                                                    <Title style={styles.NonImageTitle}>{this._toTitleCase(childWidget.titleName)}</Title>
                                                                                </Card.Content>
                                                                            </View>}
                                                                            {this.state.primusMember && <View style={styles.primusTag}>
                                                                                <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                                            </View>}
                                                                        </Card>
                                                                    )
                                                                })}
                                                        </ScrollView>
                                                    </View>}
                                                </View>
                                            </View>)
                                            :
                                            <View></View>}
                                        </View>
                                    )
                                })}
                                <Divider style={styles.divider} />
                            </View>}


                            {this.state.shelfRecommendedBooksFlag && <View style={styles.newArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                        <Appbar.Content title="You Might Like" titleStyle={styles.headingStyle} style={styles.heading} />
                                        <Appbar.Content title="More" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._allBooksNavigate('shelfRecommendedBook')}} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.shelfRecommendedBooks.map((item)=> {
                                            return (
                                                    <Card key={item.id} style={styles.cardSection} onPress={()=>{this._bookDetailsNavigate(item.title_id)}}>
                                                        {(item.image_url != null && item.image_url != '') ? <Card.Cover style={styles.coverImage} source={{ uri: item.image_url }} />
                                                        : <View style={styles.NonImage}><Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                            <Card.Content style={styles.NonImageCardText}>
                                                                <Title style={styles.NonImageTitle}>{this._toTitleCase(item.title)}</Title>
                                                                {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>by: {item.author ? this._toTitleCase(item.author) : ''}</Paragraph>}
                                                            </Card.Content>
                                                        </View>}
                                                    {this.state.primusMember && <View style={styles.primusTag}>
                                                        <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                    </View>}
                                                </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}

                            {this.state.topMagazineBooksFlag && <View style={styles.newArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                        <Appbar.Content title="Top Magazines" titleStyle={styles.headingStyle} style={styles.heading} />
                                        <Appbar.Content title="More" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._allBooksNavigate('topMagazineBooks')}} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.topMagazineBooks.map((item)=> {
                                            return (
                                                <Card key={item.titleId} style={styles.cardSection} onPress={()=>{console.log("magnize")}}>
                                                    {(item.imageUrl != null && item.imageUrl != '' && item.imageUrl != 'null') ? <Card.Cover style={styles.coverImage} source={{ uri: item.imageUrl }} />
                                                    : <View style={styles.NonImage}><Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                        <Card.Content style={styles.NonImageCardText}>
                                                            <Title style={styles.NonImageTitle}>{this._toTitleCase(item.title)}</Title>
                                                            {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.author ? this._toTitleCase(item.author) : ''}</Paragraph>}
                                                        </Card.Content>
                                                    </View>}
                                                    {this.state.primusMember && <View style={styles.primusTag}>
                                                        <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                    </View>}
                                                </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}


                            {this.state.topMagazineBooksFlag && <View style={styles.newArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                        <Appbar.Content title="Top Magazines" titleStyle={styles.headingStyle} style={styles.heading} />
                                        <Appbar.Content title="More" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._allBooksNavigate('topMagazineBooks')}} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.topMagazineBooks.map((item)=> {
                                            return (
                                                <Card key={item.titleId} style={styles.cardSection} onPress={()=>{console.log("magnize")}}>
                                                    {(item.imageUrl != null && item.imageUrl != '' && item.imageUrl != 'null') ? <Card.Cover style={styles.coverImage} source={{ uri: item.imageUrl }} />
                                                    : <View style={styles.NonImage}><Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                        <Card.Content style={styles.NonImageCardText}>
                                                            <Title style={styles.NonImageTitle}>{this._toTitleCase(item.title)}</Title>
                                                            {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.author ? this._toTitleCase(item.author) : ''}</Paragraph>}
                                                        </Card.Content>
                                                    </View>}
                                                    {this.state.primusMember && <View style={styles.primusTag}>
                                                        <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                    </View>}
                                                </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}

                            {this.state.comingSoonBooksFlag && <View style={styles.newArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                        <Appbar.Content title="Coming Soon" titleStyle={styles.headingStyle} style={styles.heading} />
                                        <Appbar.Content title="More" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._allBooksNavigate('comingSoonBooks')}} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.comingSoonBooks.map((item)=> {
                                            return (
                                                <Card key={item.titleId} style={styles.cardSection} onPress={()=>{this._bookDetailsNavigate(item.titleId)}}>
                                                    {(item.imageUrl != null && item.imageUrl != '' && item.imageUrl != 'null') ? <Card.Cover style={styles.coverImage} source={{ uri: item.imageUrl }} />
                                                    : <View style={styles.NonImage}><Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                        <Card.Content style={styles.NonImageCardText}>
                                                            <Title style={styles.NonImageTitle}>{this._toTitleCase(item.title)}</Title>
                                                            {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.author ? this._toTitleCase(item.author) : ''}</Paragraph>}
                                                        </Card.Content>
                                                    </View>}
                                                    {this.state.primusMember && <View style={styles.primusTag}>
                                                        <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                    </View>}
                                                </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}

                            {this.state.newArrivlesFlag && <View style={styles.newArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                        <Appbar.Content title="New Arrivles" titleStyle={styles.headingStyle} style={styles.heading} />
                                        <Appbar.Content title="More" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._allBooksNavigate('newArrivlesData')}} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.newArrivlesData.map((item)=> {
                                            return (
                                                <Card key={item.id} style={styles.cardSection} onPress={()=>{this._bookDetailsNavigate(item.title_id)}}>
                                                    {(item.image_url != null && item.image_url != '' && item.image_url != 'null') ? <Card.Cover style={styles.coverImage} source={{ uri: item.image_url }} />
                                                    : <View style={styles.NonImage}><Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                        <Card.Content style={styles.NonImageCardText}>
                                                            <Title style={styles.NonImageTitle}>{this._toTitleCase(item.title)}</Title>
                                                            {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.author ? this._toTitleCase(item.author) : ''}</Paragraph>}
                                                        </Card.Content>
                                                    </View>}
                                                    {this.state.primusMember && <View style={styles.primusTag}>
                                                        <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                    </View>}
                                                </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}
                            
                            {this.state.topTitleFlag && <View style={styles.bindDiv}>
                                <View style={styles.latestBook}>
                                    <View style={styles.headingView}>
                                        <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                            <Appbar.Content title="Most Reads" titleStyle={styles.headingStyle} style={styles.heading} />
                                            <Appbar.Content title="More" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._allBooksNavigate('topTitleData')}} />
                                        </Appbar.Header>
                                    </View>
                                    <View style={styles.cardView}>
                                        <ScrollView 
                                        horizontal={true}>
                                            {this.state.topTitleData.map((item)=> {
                                                return (
                                                    <Card key={item.id} style={styles.cardSection} onPress={()=>{this._bookDetailsNavigate(item.title_id)}}>
                                                        {(item.image_url != null && item.image_url != "null" && item.image_url != '') ? <Card.Cover style={styles.coverImage} source={{ uri: item.image_url }} />
                                                        : <View style={styles.NonImage}><Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                            <Card.Content style={styles.NonImageCardText}>
                                                                <Title style={styles.NonImageTitle}>{this._toTitleCase(item.title)}</Title>
                                                                {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.author ? this._toTitleCase(item.author) : ''}</Paragraph>}
                                                            </Card.Content>
                                                        </View>}
                                                        {this.state.primusMember && <View style={styles.primusTag}>
                                                            <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                        </View>}
                                                    </Card>
                                                )
                                            })}
                                        </ScrollView>
                                    </View>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}
                            
                            {this.state.authorFlag && <View style={styles.author}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                        <Appbar.Content title="Popular Authors" titleStyle={styles.headingStyle} style={styles.heading} />
                                        <Appbar.Content title="More" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._allAuthorsNavigate()}} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {/* {(Object.keys(this.state.authorData).length > 0 ) && (
                                            <View>
                                                <FlatList 
                                                    data={this.state.authorData}
                                                    ref={"flatlist"}
                                                    removeClippedSubviews={true}
                                                    initialNumToRender={10}
                                                    bounces={false}
                                                    renderItem={({ item, index }) => _renderRideItem(item, index)}
                                                    keyExtractor={(item, index) => item.id}
                                                    bounces={false}
                                                    onEndReachedThreshold={0.5}
                                                />
                                            </View>
                                        )} */}
                                        {this.state.authorData.map((item)=> {
                                            return (
                                                <Card key={item.id} style={styles.cardAuthorSection} onPress={()=>{this._authorDetailsNavigate(item.author_id)}}>
                                                    <Card.Cover style={styles.coverAuthorImage} source={{ uri: item.image_url }} />
                                                    <Card.Content style={styles.cardText}>
                                                        <Title style={styles.title}>{this._toTitleCase(item.author_name)}</Title>
                                                        <Paragraph style={styles.Paragraph}>{item.languages ? this._toTitleCase(item.languages) : ''}</Paragraph>
                                                    </Card.Content>
                                                </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                            </View>}
                        </ScrollView>
                        {/* <ChatBotIcon navigation={this.props.navigation} /> */}
                        <SearchTab navigation={this.props.navigation} />
                        <BottomTab navigation={this.props.navigation} routeName="home"/>
                    </View>
                }
            </View>
        );
    }

}

export default Home;