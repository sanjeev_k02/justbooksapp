import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        marginTop: 10
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 60
    },
    headingView: {
        marginTop: 0,
        marginBottom: 0,
        marginRight: 0,
        marginLeft: 0
    },
    heading: {
        color: '#fff',
        fontSize: 20,
        fontWeight: '400'
    },
    SeeAllHeading: {
        color: '#fff',
        fontSize: 14,
        fontWeight: '600',
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end'
    },
    titleStyle : {
        fontSize: 14, 
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end'
    },
    headingStyle : {
        fontSize: 20, 
        fontWeight: '600'
    },
    cardSliderView: { 
        display: "flex", 
        flexDirection: "row", 
        flexWrap: "wrap", 
        alignContent: "center", 
        alignItems: "center", 
        justifyContent: "center",
        marginLeft: 10,
        marginTop: 20
    },
    cardSliderSection: {
        width: 250,
        height: 140,
        margin: 10,
        // backgroundColor: '#101211'
    },
    coverSliderImage: {
        height: 140,
    },
    cardView: { 
        display: "flex", 
        flexDirection: "row", 
        flexWrap: "wrap", 
        alignContent: "center", 
        alignItems: "center", 
        justifyContent: "center",
        marginLeft: 10
    },
    cardSection: {
        width: 125,
        height: 190,
        margin: 10,
        backgroundColor: '#FFFFFF'
    },
    sliderView: {
        marginTop: 35
    },
    coverImage: {
        height: 190,
    },
    cardAuthorSection: {
        width: 125,
        height: 190,
        margin: 10,
        backgroundColor: '#FFFFFF'
    },
    coverAuthorImage: {
        height: 150,
    },
    title: {
        width: '100%',
        fontSize: 15,
        lineHeight: 15,
        paddingTop: 5,
        marginBottom: 0,
        fontWeight: '600',
        color: '#273C96'
    },
    Paragraph: {
        marginTop: 0,
        width: '100%',
        fontSize: 12,
        color: '#273C96'
    },
    // newArrivles: {
    //     marginBottom: 5,
    // },
    // latestBook: {
    //     marginBottom: 15,
    // },
    divider: {
        backgroundColor: '#6f6c6c',
        marginTop: 15
    },
    cardText: {
        marginLeft: -10
    },
    NonImage: {
        position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        top: 0,
        alignItems:'center',
    },
    NonImageCoverImage: {
        position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        top: 0,
        height: 190,
    },
    NonImageCardText: {
        textAlign: 'center'
    },
    NonImageTitle: {
        width: 100,
        fontSize: 17,
        lineHeight: 17,
        marginTop: 15,
        fontWeight: '600',
        color: '#CB8223',
        textAlign: 'center'
    },
    NonImageParagraph: {
        marginTop: 5,
        width: 100,
        fontSize: 14,
        color: '#CB8223',
        textAlign: 'center',
        lineHeight: 14
    },
    primusImage: {
      width: 110, 
      height: 40,
   },
   primusTag: {
      width: 100, 
      height: 40, 
      position: 'absolute', 
      bottom: 5,
      left: 0
   }

});
