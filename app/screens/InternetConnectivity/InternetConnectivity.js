import React from 'react';
import { View, Text, StatusBar, RefreshControl, Image, FlatList } from 'react-native';
import styles from './styles';
import { Avatar, Appbar, Divider, Button, Card, Title, Paragraph, ActivityIndicator, Colors } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

import Icons from 'react-native-vector-icons/FontAwesome5';
import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';

class InternetConnectivity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
        }
    }

    componentDidMount = async() => {
        setTimeout(() => {
            this.setState({ refreshing: false });
        }, 1000);
    };

    _onRefresh = () => {
        this.setState({refreshing: true});
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
    }

    _enableInternet = () => {
        console.log("enable Internet");
        this.setState({refreshing: true});
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { (this.state.refreshing) ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView 
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>    
                            <View class={styles.shareContainer}>
                                <View style={{ alignSelf: "center" }}>
                                    <View style={{
                                        marginTop: 60,
                                        width: 220,
                                        height: 220,
                                        borderRadius: 100,
                                        overflow: "hidden"}}>
                                    <Image source={require('../../assets/illustration.jpg')}style={{
                                        flex: 1,
                                        height: 350,
                                        width: undefined}} ></Image>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.wrapper}>
                                <Button mode="outline" labelStyle={styles.btnText} style={styles.btn} onPress={() => this._enableInternet()}>
                                        Retry
                                </Button>
                            </View>
                    </ScrollView>
                </View>
            }
            </View>
        );
    }

}

export default InternetConnectivity;