import React from 'react';
import { View, Text, Image, Alert } from 'react-native';
import styles from './styles';
import { Avatar, DataTable, Appbar, List, Divider, Button,  ActivityIndicator, Colors } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import AwesomeAlert from 'react-native-awesome-alerts';
import { showMessage } from "react-native-flash-message";
import API from '../../env';
import * as axios from 'axios';

class Member extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subscription_id: 0,
            memberCard: 0,
            branchId: 0,
            primusMappedBranches: 0,
            cityId: 0,
            primusMember: false,
            page: 1, 
            refreshing: true,
            memberAccounts: {},
            memberAccountFlag: false,
            showAlert: false,
            actionPerform: '',
            familyMemberId: 0,
        }
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user);
                // console.log("us",userData);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard,
                    branchId: userData.login.branchId,
                    primusMappedBranches: userData.login.primusMappedBranches,
                    cityId: userData.login.cityId,
                    primusMember: userData.login.primus
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }
        this.getAllMemberAccount(this.state.subscription_id);
    };



    getAllMemberAccount = (subscriptionId) => {
        let getMember = API.API_JUSTBOOK_ENDPOINT+"/getAllFamilyMembersForSubscription?subscriptionId="+subscriptionId;
        console.log(getMember);
        axios.get(getMember, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
          .then((response) => {
            // console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                memberAccounts : response.data.successObject,
                refreshing: false,
                memberAccountFlag: true
                })
            } else {
                this.setState({
                    refreshing: false,
                    memberAccountFlag: true,
                    errorMsg: response.data.errorDescription
                });
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }

    _switchMemberAction = async(familyMemberId) => {
        if(this.state.subscription_id == 0) {
            showMessage({
                message: "Member Account",
                description: 'Become a member to add member account!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            this.setState({ 
                showAlert: true, 
                actionPerform: 'switch',
                familyMemberId: familyMemberId 
            });
            
        }
    }

    _switchMemberActionPreform = (familyMemberId) => {
        this.setState({ showAlert: false  });
        let switchMember = API.API_JUSTBOOK_ENDPOINT+"/getAllFamilyMembersForSubscription?familyMemberId="+familyMemberId;
        console.log(switchMember);
        axios.get(switchMember, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
          .then((response) => {
            // console.log(response.data.successObject);
            if (response.data.status) {
                showMessage({
                    message: "Member Account",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                setTimeout(() => {
                    this.getAllMemberAccount(this.state.subscription_id);
                },2000);
            } else {
                showMessage({
                    message: "Member Account",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch((error) => {
            console.log(error);
            showMessage({
                message: "Member Account",
                description: 'Something went wrong, please try again later!!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _editMember = (familyMemberId, name, mobile, dateOfBirth) => {
        this.props.navigation.push("EditMember",{
            familyMemberId: familyMemberId,
            name: name,
            mobile: mobile,
            dateOfBirth: dateOfBirth
        })
    }

    _deleteMemberAction = async(familyMemberId) => {
        if(this.state.subscription_id == 0) {
            showMessage({
                message: "Member Account",
                description: 'Become a member to delete account!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            this.setState({ 
                showAlert: true, 
                actionPerform: 'delete',
                familyMemberId: familyMemberId 
            });
            
        }
    }

    _deleteMemberActionPreform = (familyMemberId) => {
        this.setState({ showAlert: false  });
        let deleteMember = API.API_JUSTBOOK_ENDPOINT+"/deleteFamilyMember";
        console.log(deleteMember);
        axios.post(deleteMember,{
            "familyMemberId" : familyMemberId,
             "createdAt" : 810,
             "createdBy" : 1000
          }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
          .then((response) => {
            // console.log(response);
            if (response.data.status) {
                showMessage({
                    message: "Member Account",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                setTimeout(() => {
                    this.getAllMemberAccount(this.state.subscription_id);
                },2000);
            } else {
                showMessage({
                    message: "Member Account",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch((error) => {
            console.log(error);
            showMessage({
                message: "Member Account",
                description: 'Something went wrong, please try again later!!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _alertActionPreform =(action, familyMemberId) =>{
        if(action == 'delete') {
            this._deleteMemberActionPreform(familyMemberId);
        } else if( action == 'switch') {
            this._switchMemberActionPreform(familyMemberId);
        }

    }
     
    hideAlert = () => {
        this.setState({
          showAlert: false
        });
    };
    
    render() {
        return (
            <View style={styles.contain}>
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    icon={'alert'}
                    iconColor={'#AB3247'}
                    title="JustBooks"
                    message={this.state.actionPerform == 'delete' ? "Do you want to delete member account!" : "Do you want to switch member account!"}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText="No, cancel"
                    confirmText={this.state.actionPerform == 'delete' ? "Yes, delete it" : "Yes, switch it"}
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this.hideAlert();
                    }}
                    onConfirmPressed={() => {
                        this._alertActionPreform(this.state.actionPerform, this.state.familyMemberId);
                    }}
                />
                <StatusTopBar />
                { (this.state.refreshing) ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <View style={styles.mainContainer}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <View style={styles.primusCard}>
                                    {this.state.memberAccountFlag && <View style={styles.newArrivles}>
                                        <View style={styles.cardView}>
                                            <View style={{ marginBottom: 5 ,marginTop: 0}}>
                                                <View style={styles.cardHomeView}>
                                                    {(Object.keys(this.state.memberAccounts).length > 0 ) && (
                                                        <View style={styles.wrapper}>
                                                            <Appbar.Header style={{backgroundColor: '#FFFFFF', elevation: 0}}>
                                                                <Button icon="account-multiple-plus-outline" mode="outline" labelStyle={styles.btnText} style={styles.btn} onPress={() => this.props.navigation.navigate('AddMember')}>
                                                                    Add Member Account
                                                                </Button>
                                                            </Appbar.Header>
                                                        </View>
                                                    )}
                                                    {(Object.keys(this.state.memberAccounts).length > 0 ) && (
                                                        <View style={styles.flatList}>
                                                            {this.state.memberAccounts.map((item) => {
                                                                return (
                                                                    <View key={item.id} style={{backgroundColor : '#F1F1F1', marginTop: 10}}>
                                                                        <List.Item
                                                                            title={item.mobile ? item.mobile : 'N/A'}
                                                                            description={item.name}
                                                                            titleStyle={{color:'#2E2E2E'}}
                                                                            descriptionStyle={{color:'#2E2E2E'}}
                                                                            right={props => (
                                                                            <View style={styles.memberCard}>
                                                                                <View style={styles.actionStyle}>
                                                                                    <View style={styles.footer}>
                                                                                        <TouchableOpacity style={styles.bottomButtons} onPress={()=>{this._switchMemberAction(item.id)}} >
                                                                                            <Icons name="account-switch" size={28} color={'#000000'} />
                                                                                            <Text style={styles.footerText}>Switch</Text>
                                                                                        </TouchableOpacity>
                                                                                        
                                                                                        <TouchableOpacity style={styles.bottomButtons} onPress={()=>{this._editMember(item.id, item.name, item.mobile ? item.mobile : 'N/A',  item.dateOfBirth)}} >
                                                                                            <Icons name="account-edit" size={28} color={'#000000'} />
                                                                                            <Text style={styles.footerText}>Edit</Text>
                                                                                        </TouchableOpacity>
                                            
                                                                                        <TouchableOpacity style={styles.bottomButtons} onPress={()=>{this._deleteMemberAction(item.id)}} >
                                                                                            <Icons name="delete" size={24} color={'#AB3247'} />
                                                                                            <Text style={[styles.footerText,{color: '#AB3247'}]}>Delete</Text>
                                                                                        </TouchableOpacity>
                                                                                    </View>
                                                                                </View>
                                                                            </View>)
                                                                            }
                                                                        />
                                                                    </View> 
                                                                )
                                                            })}
                                                        </View>
                                                    )}

                                                    {((Object.keys(this.state.memberAccounts).length == 0 )) && (
                                                    <View class={styles.shareContainer}>
                                                        <View style={{ alignSelf: "center" }}>
                                                            <View style={{
                                                                marginTop: 60,
                                                                width: 220,
                                                                height: 220,
                                                                borderRadius: 100,
                                                                overflow: "hidden"}}>
                                                            <Image source={require("../../assets/no-data.png")} style={{
                                                                flex: 1,
                                                                height: 250,
                                                                width: undefined}} ></Image>
                                                            </View>
                                                            <View style={styles.wrapper}>
                                                                <Text style={styles.buttonDesc}>
                                                                    <Button icon="account-multiple-plus-outline" mode="outline" labelStyle={{fontSize: 18, color: '#FFFFFF', textTransform: 'none'}} style={styles.btn} onPress={()=>{this.props.navigation.navigate('AddMember')}}>
                                                                    Add New Member
                                                                    </Button>{'\n'}
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                    )}
                                                </View>
                                            </View>
                                        </View>
                                    </View>}
                                    
                                </View>
                            </ScrollView>
                        </View>
                        <BottomTab navigation={this.props.navigation} routeName="profile" />
                    </View>
                }
            </View>
        );
    }

}

export default Member;