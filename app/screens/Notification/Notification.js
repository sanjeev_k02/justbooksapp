import React from 'react';
import { View, Image } from 'react-native';
import styles from './styles';
import { Avatar, Divider, Card, IconButton, ActivityIndicator, Colors } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';

class Notification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
            notificationDetails: {}
        }
    }

    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
              const userData = JSON.parse(user);
              this.setState({
                  refreshing: false,
                  subscriptionDetails: userData
                })
            } else {
                this.setState({
                    refreshing: false,
                })
                console.log("subscription Not Found");
            }
          } catch (error) {
            console.log("error", error)
        }
    };
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    :
                    <View style={styles.activityClass}>
                        <ScrollView>
                            <View style={{ marginBottom: 5 ,marginTop: 10}}>
                                <View style={styles.cardHomeView}>
                                    {/* <Card.Title
                                        style={styles.cardStyle}
                                        titleStyle={styles.titleStyle}
                                        subtitleStyle = {styles.subtitleStyle}
                                        rightStyle = {styles.rightStyle}
                                        title="Divergnet (FILM TIE IN)"
                                        subtitle="renting books formjustbooks"
                                        left={(props) => <Avatar.Icon {...props} size={42} color={'#FFFFFF'}  style={styles.iconColor} icon="bell" />}
                                        right={(props) => <IconButton {...props} size={30} color={'#8F8E94'} icon="delete" onPress={() => {}} />}
                                    />
                                    <Divider style={{margin:8, backgroundColor: '#FFFFFF'}} /> */}
        
                                    <View class={styles.shareContainer}>
                                        <View style={{ alignSelf: "center" }}>
                                            <View style={{
                                                marginTop: 100,
                                                width: 220,
                                                height: 220,
                                                borderRadius: 100,
                                                overflow: "hidden"}}>
                                            <Image source={require("../../assets/no-data.png")} style={{
                                                flex: 1,
                                                height: 250,
                                                width: undefined}} ></Image>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
        
                        <SearchTab navigation={this.props.navigation} />
                        <BottomTab navigation={this.props.navigation} routeName="notification"/>
                    </View>
                    }
            </View>
        );
    }

}

export default Notification;