import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },
    divider : {
        margin: 10,
    },
    cardStyle : {
        backgroundColor: '#F1F1F1', 
        borderRadius: 6
    },
    titleStyle: {
        color : '#2E2E2E'
    },
    subtitleStyle: {
        color : '#2E2E2E'
    },
    rightStyle : {
        backgroundColor: '#F1F1F1', 
        borderBottomRightRadius: 6, 
        borderTopRightRadius: 6, 
        height: 70, 
        paddingTop: 8
    },
    iconColor : {
        backgroundColor: '#8F8E94',
    }

});
