import React from 'react';
import { View, StatusBar, Image } from 'react-native';
import styles from './styles';
import { TextInput, Appbar, Divider, RadioButton, Button, Card, Title, Paragraph, Headline, Subheading, Text } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import API from '../../env';
import * as axios from 'axios';

class OtpSignIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mobile: '',
            otp: '',
            mobileError: false,
            errorMsg: ''
        }
    }

    
    _openOtpScreen = () => {
        if (this.state.mobile === '') {
            this.setState({ 
                mobileError : true,
                errorMsg: 'Mobile No Required field!'
            })
            return;
        }
    
        this._SendOtpRequest();
      };
    
      async _SendOtpRequest() {
    
        const generateOTP = API.API_JUSTBOOK_ENDPOINT + "/requestOtp?queryParam="+this.state.mobile;
        try {
          let response = await axios.post(generateOTP, { headers: { 'X-SECRET-TOKEN': 'qwerty' } });
          console.log("response mobile", response.data.successObject);
          if (response.data.status) {
              this.setState({
                    mobileError : false,
                })
              this._navigateSignInScreen(response.data.successObject);
          } else {
            this.setState({ 
                mobileError : true,
                errorMsg: response.data.errorDescription
            })
            return;
          }
        } catch(err) {
          console.log(err);
          this.setState({ 
              mobileError : true,
              errorMsg: 'Oops Something went wrong!'
          })
        }   
      }
    
    _navigateSignInScreen (successMsg) {
        this.props.navigation.navigate('GetOtpSignIn', {
          mobile: this.state.mobile,
          successMsg: successMsg
        });
    }

    _GetOtpSigInNavigate = async() => {
        this.props.navigation.navigate("GetOtpSignIn");
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <ScrollView>
                    <View style={{ marginBottom: 5 ,marginTop: 10}}>
                        <View style={styles.containerLogo}>
                            <Image
                                style={styles.logoImage}
                                source={require('../../../assets/images/logo.jpg')}
                            />
                        </View>
                        <View style={styles.containerImage}>
                            <View style={styles.wrapper}>
                                <Text style={styles.nameDesc}>
                                    <Headline style={styles.colorSignIn}> Sign In </Headline>
                                    {'\n'}{'\n'}
                                </Text>
                            </View>
                            <Image
                                style={styles.signInImage}
                                source={require('../../assets/login.png')}
                            />
                        </View>
                        <View style={styles.cardHomeView}>
                            <View style={styles.form}>
                                <TextInput
                                    placeholder="Enter Registred Mobile Number"
                                    value={this.state.email}
                                    style={styles.textInput}
                                    underlineColor={'#BEBEBE'}
                                    selectionColor={'#BEBEBE'}
                                    labelStyle={{color: '#BEBEBE'}}
                                    placeholderTextColor={'#BEBEBE'}
                                    theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                    onChangeText={text => this.setState({ mobileError: false, mobile : text})}
                                />
                                {this.state.mobileError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                               
                                <View style={styles.wrapper}>
                                    <Text style={styles.buttonDesc}>{'\n'}
                                        <Button icon="send" mode="outline" labelStyle={{fontSize: 18, color: '#FFFFFF', textTransform: "none"}} style={styles.btn} onPress={()=>{this._openOtpScreen()}}>
                                           Request OTP
                                        </Button>{'\n'}{'\n'}{'\n'}
                                    </Text>
                                </View>
                            </View>

                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

}

export default OtpSignIn;