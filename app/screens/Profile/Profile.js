import React from 'react';
import { View, Text, StatusBar, Image,Alert } from 'react-native';
import styles from './styles';
import { Avatar, Appbar, Divider, Button, Card, Title, Paragraph, Headline, ActivityIndicator, Colors } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
            subscriptionDetails: {},
            fields : [
                {fieldName: 'Personal Information', key: 'personal_info', icon: 'account-edit', navigateTo: 'ProfileInfo'},
                // {fieldName: 'Member Account', key: 'member_info', icon: 'account-group', navigateTo: 'Member'},
                {fieldName: 'My Subscription', key: 'subscription_info', icon: 'star', navigateTo: 'Subscription'},
                {fieldName: 'My Recommendations', key: 'recommendations', icon: 'help-rhombus', navigateTo: 'Recommendations'},
                {fieldName: 'Logout', key: 'logout_info', icon: 'exit-to-app', navigateTo: ''}
            ]
        }
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }

    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
              const userData = JSON.parse(user)
              this.setState({
                  refreshing: false,
                  subscriptionDetails: userData
                })
            } else {
                console.log("subscription Not Found");
            }
          } catch (error) {
            console.log("error", error)
        }
    };

    _allNavigate = (fieldName, navigateTo) => {
        if(fieldName == 'Logout'){
            Alert.alert(
                'Hold On',
                'Are you want to log out?',
                [
                    { text: 'Nope',style: 'cancel' },
                    {
                        text: 'Yeah', onPress: () => {
                            AsyncStorage.clear().then(() => {
                                this.props.navigation.push("Home")
                            })
                        }
                    },
                ]
            );
        } else {
            this.props.navigation.navigate(navigateTo);
        }
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    :
                    <View style={styles.activityClass}>
                        <View style={styles.mainContainer}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <View style={styles.wrapper}>
                                    <View style={styles.ProfileCard}>
                                        <Image
                                            style={{
                                                width: 100,
                                                height: 100,
                                                borderRadius: 100}}
                                            source={{ uri: 'https://writechoice.files.wordpress.com/2020/05/small_handwriting_girl.jpg' }}
                                        />
                                    </View>
                                    <Text style={styles.nameDesc}>
                                        <Headline style={styles.color}>{this._toTitleCase(this.state.subscriptionDetails.user.userInfo.firstname)} {this._toTitleCase(this.state.subscriptionDetails.user.userInfo.secName)} </Headline>{'\n'}  
                                        <Text style={{color: 'green', fontSize: 16 }}>
                                            <Paragraph style={styles.colorParagraph}>Status : Active</Paragraph>
                                        </Text>{'\n'}
                                    </Text>
                                </View>
                                {this.state.subscriptionDetails.primusMember && <View style={styles.primusCard}>
                                    <Card  style={styles.cardProfileSection}>
                                        <Card.Cover source={{ uri: 'https://techstory.in/wp-content/uploads/2016/12/justbooks-funding.jpg' }} style={styles.primusImage} />
                                        <Text style={styles.titleDesciption}>
                                            <Headline style={styles.headline}>Primus Member</Headline>{'\n'}
                                            <Headline style={styles.headline}>{this.state.subscriptionDetails.memberCard}</Headline>{'\n'}
                                        </Text>
                                    </Card>
                                </View>}
                                <View>
                                    <View style={styles.containers}>
                                        {this.state.fields.map((field, index) => {
                                            const TouchableOpacityAttributes = {
                                                style: styles.container,
                                                key: index
                                            }
                                            return <View key={index}>
                                                <TouchableOpacity {...TouchableOpacityAttributes} onPress={()=>{this._allNavigate(field.fieldName, field.navigateTo)}}>
                                                    <Text style={{ color: "#2E2E2E", fontSize: 18 }}>{field.fieldName}</Text>
                                                    <Icon name={field.icon} size={30} color="#2E2E2E" />
                                                </TouchableOpacity>
                                                <Divider style={styles.divider} />
                                            </View>;
                                        })}
                                        </View>
                                </View>
                            </ScrollView>
                        </View>
                        <BottomTab navigation={this.props.navigation} routeName="profile" />
                    </View>
                }
            </View>
        );
    }

}

export default Profile;