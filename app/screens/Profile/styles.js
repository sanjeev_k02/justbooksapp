import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    divider : {
        margin: 0,
        color:'#8F8E94',
        backgroundColor: '#8F8E94'
    },
    primusCard: {
        marginBottom: 20,
    },
    cardProfileSection: {
        backgroundColor: '#FFFFFF',
        elevation: 0
    },
    primusImage : {
        width: '100%', 
        height:90, 
        padding: 10, 
        backgroundColor: '#FFFFFF'
    },
    dividerPrimus : {
        margin: 15,
        color:'#FFFFFF',
        backgroundColor: '#FFFFFF'
    },
    mainContainer: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
        marginBottom: 40,
        flexDirection: "column"
    },
    image: {
        width: 160, 
        height: 160, 
        borderRadius: 180/ 2,
    },
    welcomeBlock:{
        alignItems: "center",
        marginTop: 20
    },
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        borderBottomWidth: 1,
        paddingBottom: 15,
        paddingTop: 15
    },
    wrapper: {
        alignItems: "center",
        marginTop: 10,
        marginBottom: 0
    },
    mainviewStyle: {
        flex: 1,
        flexDirection: 'column',
    },
    profileCard: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100
    },
    headline: {
        color: '#2E2E2E',
        fontSize: 25,
        textAlign: 'center',
        fontWeight: '600'
    },
    titleDesciption : {
        textAlign: 'center',
        margin: 8
    },
    color: {
        color: '#2E2E2E',
        fontSize: 25,
        textAlign: 'center'
    },
    colorParagraph: {
        color: 'pink',
    },
    nameDesc: {
        color: '#2E2E2E', 
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30, 
        textAlign: 'center'
    }
});
