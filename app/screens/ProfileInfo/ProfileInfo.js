import React from 'react';
import { View, Text, StatusBar } from 'react-native';
import styles from './styles';
import { TextInput, Appbar, Divider, RadioButton, Button, Card, Title, Paragraph, Headline, ActivityIndicator, Colors  } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import AsyncStorage from '@react-native-community/async-storage';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import API from '../../env';
import * as axios from 'axios';

class ProfileInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
            userId: 0,
            email: '',
            mobile: '',
            dob: '',
            address: '',
            pincode: '',
            locality: '',
            emailError: false,
            mobileError: false,
            dobError: false,
            addressError: false,
            errorMsg: '',
            successError: false
        }
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
  
    componentDidMount = async() => {
        this._gettingUserInfo();
    };

    _gettingUserInfo = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
              const userData = JSON.parse(user);
            //   console.log(userData)
              this.setState({
                  refreshing: false,
                  email: userData.user.email ? userData.user.email : '',
                  mobile: userData.user.mobile ? userData.user.mobile : '',
                  dob: userData.user.userInfo.dob ? userData.user.userInfo.dob : '',
                  address: userData.user.userInfo.address ? userData.user.userInfo.address : '',
                  pincode: userData.user.userInfo.pincode ? userData.user.userInfo.pincode : '',
                  locality: userData.user.locality ? userData.user.locality : '',
                  userId: userData.user.id ? userData.user.id : '',
                })
            } else {
                console.log("subscription Not Found");
            }
          } catch (error) {
            console.log("error", error)
        }
    }

    _updateProfile = async() => {
        if (this.state.email === '') {
            this.setState({ 
                emailError : true,
                errorMsg: 'Mobile/Email Required field!'
            })
            return;
        }

        if (this.state.mobile === '') {
            this.setState({ 
                mobileError : true,
                errorMsg: 'Mobile Required field!'
            })
            return;
        }

        if (this.state.dob === '') {
            this.setState({ 
                dobError : true,
                errorMsg: 'Date of birth Required field!'
            })
            return;
        }

        if (this.state.address === '') {
            this.setState({ 
                addressError : true,
                errorMsg: 'Address Required field!'
            })
            return;
        }
    
        this._updateProfileRequest();
    }

    _updateProfileRequest() {

        const updateProfile = API.API_JUSTBOOK_ENDPOINT + "/updateUserProfile";
        axios.post(updateProfile ,{
            "createdAt" : 810,
            "createdBy" : 1000,
            "locality" :this.state.locality,
            "pincode" :this.state.pincode,
            "userId" :this.state.userId,
            "mobile" : this.state.mobile,
            "email" : this.state.email,
            "address" : this.state.address,
            "dob" : this.state.dob
            }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response update profile", response.data);
            if (response.data.status) {
                this.setState({ 
                    refreshing: true,
                    successError : true,
                    errorMsg: response.data.successObject
                });
                this._gettingUserInfo();
                setTimeout(() => {
                    this.setState({ refreshing: false });
                },2000);
            } else {
                this.setState({ 
                    successError : true,
                    errorMsg: response.data.errorDescription
                })
            }
        })
        .catch(function (error) {
            console.log(error);
            this.setState({ 
                successError : true,
                errorMsg: 'Something went wrong, Please try again later!'
            })
            return;
        });
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    :
                    <View style={styles.activityClass}>
                        <View style={styles.mainContainer}>
                            <ScrollView>
                                <View style={{ marginBottom: 5 ,marginTop: 10}}>
                                    <View style={styles.cardHomeView}>
                                        <Text style={styles.titleDesciption}>
                                            <Headline style={styles.titleStyle}>User Details</Headline>{'\n'}
                                            <Text style={{color: 'green', fontSize: 16 }}>
                                                <Paragraph style={styles.subtitleStyle}>Enter your personal details to ensure account</Paragraph>
                                            </Text>{'\n'}
                                        </Text>
                                        <View style={styles.form}>
                                            <TextInput
                                                placeholder="Email"
                                                value={this.state.email}
                                                style={styles.textInput}
                                                underlineColor={'#BEBEBE'}
                                                selectionColor={'#BEBEBE'}
                                                labelStyle={{color: '#BEBEBE'}}
                                                placeholderTextColor={'#BEBEBE'}
                                                theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                onChangeText={text => this.setState({email : text})}
                                            />
                                            {this.state.emailError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}

                                            <TextInput
                                                placeholder="Enter Mobile"
                                                value={this.state.mobile}
                                                style={styles.textInput}
                                                underlineColor={'#BEBEBE'}
                                                selectionColor={'#BEBEBE'}
                                                labelStyle={{color: '#BEBEBE'}}
                                                placeholderTextColor={'#BEBEBE'}
                                                theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                onChangeText={text => this.setState({mobile : text})}
                                            />
                                            {this.state.mobileError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                            
                                            {/* <View style={styles.container}>
                                                <View style={styles.item}>
                                                    <RadioButton
                                                        label="Male"
                                                        value="male"
                                                        status={ this.state.gender === 'male' ? 'checked' : 'unchecked' }
                                                />
                                                </View>
                                                <View style={styles.item}>
                                                    <RadioButton
                                                        value="female"
                                                        status={ this.state.gender === 'female' ? 'checked' : 'unchecked' }
                                                /> 
                                                </View>
                                            </View> */}

                                            {/* <TextInput
                                                label="Date of Birth"
                                                value={this.state.dob}
                                                style={styles.textInput}
                                                onChangeText={text => this.setState({name : text})}
                                            /> */}

                                            <DatePicker
                                                style={{width: '100%',marginBottom: 10}}
                                                mode="date"
                                                date={this.state.dob}
                                                placeholder="Date of Birth"
                                                format="YYYY-MM-DD"
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                customStyles={{
                                                    dateIcon: {
                                                        position: 'absolute',
                                                        right: 0,
                                                        top: 4
                                                    },
                                                    dateInput: {
                                                        position: 'absolute',
                                                        left: 0, 
                                                        right: 10,
                                                        height: 50,
                                                        paddingTop: 10,
                                                        marginBottom: 0,
                                                        marginLeft: 10,
                                                        paddingLeft: 10,
                                                        backgroundColor: '#FFFFFF',
                                                        borderBottomColor: '#121111',
                                                        color: '#121111',
                                                        borderBottomWidth: 0.5,
                                                        borderLeftWidth: 0,
                                                        borderRightWidth: 0,
                                                        borderTopWidth: 0,
                                                        textAlign: 'left',
                                                        alignItems: 'flex-start'
                                                    }
                                                    // ... You can check the source to find the other keys.
                                                }}
                                                onDateChange={(date) => {this.setState({dob: date})}}
                                            />
                                            {this.state.dobError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                            
                                            <TextInput
                                                placeholder="Address"
                                                multiline={true}
                                                value={this.state.address}
                                                style={styles.addressInput}
                                                underlineColor={'#BEBEBE'}
                                                selectionColor={'#BEBEBE'}
                                                labelStyle={{color: '#BEBEBE'}}
                                                placeholderTextColor={'#BEBEBE'}
                                                theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                onChangeText={text => this.setState({address : text})}
                                            />
                                            {this.state.addressError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                            
                                            <View style={styles.container}>
                                                <View style={styles.item}>
                                                    <TextInput
                                                        placeholder="State"
                                                        value={this.state.pincode}
                                                        style={styles.textInput}
                                                        underlineColor={'#BEBEBE'}
                                                        selectionColor={'#BEBEBE'}
                                                        labelStyle={{color: '#BEBEBE'}}
                                                        placeholderTextColor={'#BEBEBE'}
                                                        theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                        onChangeText={text => this.setState({pincode : text})}
                                                    />
                                                </View>
                                                <View style={styles.item}>
                                                    <TextInput
                                                        placeholder="City"
                                                        value={this.state.locality}
                                                        style={styles.textInput}
                                                        underlineColor={'#BEBEBE'}
                                                        selectionColor={'#BEBEBE'}
                                                        labelStyle={{color: '#BEBEBE'}}
                                                        placeholderTextColor={'#BEBEBE'}
                                                        theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                        onChangeText={text => this.setState({locality : text})}
                                                    />
                                                </View>
                                            </View>
                                        

                                            <Button icon="check" mode="contained" labelStyle={{fontSize: 18, color:'#FFFFFF'}} style={styles.btn} onPress={()=>{this._updateProfile()}}>
                                                Update Now
                                            </Button>
                                            {this.state.successError && <Paragraph style={styles.successError}> {this.state.errorMsg} </Paragraph>}
                                        </View>
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                        <BottomTab navigation={this.props.navigation} />
                    </View>
                }
            </View>
        );
    }

}

export default ProfileInfo;