import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    mainContainer: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 0,
        marginBottom: 0,
        flexDirection: "column"
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },
    divider : {
        margin: 10,
    },
    cardStyle : {
        backgroundColor: '#39403c', 
        borderRadius: 6
    },
    titleStyle: {
        color : '#2E2E2E'
    },
    subtitleStyle: {
        color : '#2E2E2E'
    },
    rightStyle : {
        backgroundColor: '#2E2E2E', 
        borderBottomRightRadius: 6, 
        borderTopRightRadius: 6, 
        height: 70, 
        paddingTop: 8
    },
    textInput: {
        height: 50,
        marginTop: 8,
        marginBottom: 8,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: '#FFFFFF',
        color: '#000000'
    },
    addressInput: {
        height: 50,
        marginTop: 8,
        marginBottom: 8,
        marginLeft: 10,
        marginRight: 10,
        backgroundColor: '#FFFFFF',
        color: '#000000'
    },
    // form: {
    //     backgroundColor: '#fff'
    // },
    btn: {
        height: 42, 
        textAlign: 'center', 
        margin: 15,
        fontSize: 20,
        backgroundColor: '#273C96',
        color: '#FFFFFF'
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start' // if you want to fill rows left to right
    },
    item: {
        width: '50%' // is 50% of container width
    },
    colorError: {
        color: 'red',
        fontSize: 12,
        marginTop: -10,
        textAlign: 'center'
    },
    successError: {
        color: 'blue',
        fontSize: 14,
        marginTop: -10,
        textAlign: 'center'
    },

});
