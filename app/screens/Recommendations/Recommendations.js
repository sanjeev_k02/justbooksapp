import React from 'react';
import { View, Share, Alert, RefreshControl, Text, FlatList } from 'react-native';
import { Title, Appbar, Divider,  Checkbox, ActivityIndicator, Colors, Headline } from 'react-native-paper';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import BottomTab from '../../components/BottomTab/BottomTab';
import { showMessage, hideMessage } from "react-native-flash-message";
import API from '../../env';
import * as axios from 'axios';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
class Recommendations extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            subscription_id: 0,
            ageGroups: {},
            genres: {},
            otherRecommedationTypes: {},
            refreshing: true,
            ageGroupsCheckedId: [],
            genresCheckedId: [],
            otherRecommedationTypesCheckedId: [],
            checked:[],
        }
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user)
                this.setState({
                    refreshing: false,
                    subscription_id: userData.login.subscriptionId,
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        await this.getMetadataForCustomizeUserRecommendations(this.state.subscription_id);
        setTimeout(
            () => {
                this.setState({ refreshing: false });
            },
            2000
        );
    };

    getMetadataForCustomizeUserRecommendations = (subscriptionId) => {
      const getMetadata = API.API_JUSTBOOK_ENDPOINT+"/getMetadataForCustomizeUserRecommendations?subscriptionId="+subscriptionId;
      console.log(getMetadata);
      axios.get(getMetadata, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
          console.log(response.data.successObject.ageGroups);
            if (response.data.status) {
                // ageGroups Category
                var ageGroupsArr =[];
                var ageGroups = response.data.successObject.ageGroups.map(function(el) {
                    if(el.mapped){
                        ageGroupsArr.push(el.ageGroupId)
                    }
                    var o = Object.assign({}, el);
                    o.checked = false;
                    return o;
                })
                // genres Category
                var genresArr = [];
                var genres = response.data.successObject.genres.map(function(el) {
                    if(el.mapped){
                        genresArr.push(el.id)
                    }
                    var o = Object.assign({}, el);
                    o.checked = false;
                    return o;
                })
                // otherRecommedationTypes Category
                var otherRecommedationTypesArr = [];
                var otherRecommedationTypes = response.data.successObject.otherRecommedationTypes.map(function(el) {
                    if(el.mapped){
                        otherRecommedationTypesArr.push(el.id)
                    }
                    var o = Object.assign({}, el);
                    o.checked = false;
                    return o;
                })
                // console.log(successData);
                this.setState({
                    ageGroups : ageGroups,
                    genres : genres,
                    otherRecommedationTypes : otherRecommedationTypes,
                    ageGroupsCheckedId: ageGroupsArr,
                    genresCheckedId: genresArr,
                    otherRecommedationTypesCheckedId: otherRecommedationTypesArr,
                    refreshing: false
                })
            } else {
                this.setState({
                    refreshing: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _checkedVal = (id, action) => {
        
        let actionCategory;
            if(action == 'genres') {
                const index = this.state.genresCheckedId.indexOf(id);
            if (index > -1) {
                this.state.genresCheckedId.splice(index, 1);
            } else {
                this.state.genresCheckedId.push(id);
            }
            actionCategory=this.state.genres;
            actionCategory[id].checked=!actionCategory[id].checked
            this.setState({actionCategory:actionCategory})
        } else if(action == 'otherRecommedation') {
                const index = this.state.otherRecommedationTypesCheckedId.indexOf(id);
            if (index > -1) {
                this.state.otherRecommedationTypesCheckedId.splice(index, 1);
            } else {
                this.state.otherRecommedationTypesCheckedId.push(id);
            }
            actionCategory=this.state.otherRecommedationTypes;
            actionCategory[id].checked=!actionCategory[id].checked
            this.setState({actionCategory:actionCategory})
        } else if(action == 'ageGroups') {

            const index = this.state.ageGroupsCheckedId.indexOf(id);
            if (index > -1) {
                this.state.ageGroupsCheckedId.splice(index, 1);
            } else {
                this.state.ageGroupsCheckedId.push(id);
            }
            actionCategory=this.state.ageGroups;
            actionCategory[id].checked=!actionCategory[id].checked
            this.setState({actionCategory:actionCategory})
        }
    }

    _renderGenersItem = ({ item }) => (
        <View style={styles.item} key={item.id}>
            <TouchableOpacity>
                <View style={styles.catContainer}>
                    <View style={styles.catLeftItem}>
                        <Text style={styles.title}>{this._toTitleCase(item.name)}</Text>
                    </View>
                    <View style={styles.catRightItem}>
                        <Checkbox
                            status={this.state.genresCheckedId.includes(item.id) ? 'checked' : 'unchecked'}
                            // status={item.mapped ? 'checked' : 'unchecked'}
                            uncheckedColor={'#273C96'}
                            color={'#273C96'}
                            onPress={() => {this._checkedVal(item.id, 'genres')}}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );

    _renderOtherRecommedationItem = ({ item, index }) => (
        <View style={styles.item} key={item.id}>
            <TouchableOpacity>
                <View style={styles.catContainer}>
                    <View style={styles.catLeftItem}>
                        <Text style={styles.title}>{this._toTitleCase(item.name)}</Text>
                    </View>
                    <View style={styles.catRightItem}>
                        <Checkbox
                            status={this.state.otherRecommedationTypesCheckedId.includes(item.id) ? 'checked' : 'unchecked'}
                            // status={item.mapped ? 'checked' : 'unchecked'}
                            uncheckedColor={'#273C96'}
                            color={'#273C96'}
                            onPress={() => {this._checkedVal(item.id, 'otherRecommedation')}}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );

    _renderAgeGroupsItem = ({ item }) => (
        <View style={styles.item} key={item.ageGroupId}>
            <TouchableOpacity>
                <View style={styles.catContainer}>
                    <View style={styles.catLeftItem}>
                        <Text style={styles.title}>{item.ageGroup}</Text>
                    </View>
                    <View style={styles.catRightItem}>
                        <Checkbox
                            status={this.state.ageGroupsCheckedId.includes(item.ageGroupId) ? 'checked' : 'unchecked'}
                            // status={item.mapped ? 'checked' : 'unchecked'}
                            uncheckedColor={'#273C96'}
                            color={'#273C96'}
                            onPress={() => {this._checkedVal(item.ageGroupId, 'ageGroups')}}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );

    _navigateCategoryFilter = () => {
        // this.props.navigation.push('CategoryResults',{
        //     categories: this.state.genresCheckedId+""
        // });
    };

    _navigateCategoryFilter = () => {
        console.log("this.state.genresCheckedId",this.state.genresCheckedId+"");

        const insertOrUpdateUser = API.API_JUSTBOOK_ENDPOINT + "/insertOrUpdateUserRecommendationFilter";
        axios.post(insertOrUpdateUser ,{
            "createdAt":810,
            "createdBy":1000,
            "subscriptionId":this.state.subscription_id,
            "genreIdCsv":this.state.genresCheckedId+"",
            "otherTypeIdCsv":this.state.otherRecommedationTypesCheckedId+"",
            "ageGroupIdCsv":this.state.ageGroupsCheckedId+""
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty', createdAt: 810, createdBy: 1000 } })
        .then((response) => {
            console.log("response Recommendation", response.data);
            if (response.data.status) {
                showMessage({
                    message: 'My Recommendations',
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
            } else {
                showMessage({
                    message: 'My Recommendations',
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: 'My Recommendations',
                description: 'Something went wrong! Please try again later',
                icon: "danger",
                type: "danger",
            });
        });
    }

    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView 
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            <View style={styles.containBody}>
                                {this.state.genres.length > 0 && <View style={styles.branchView}>
                                    <View style={styles.headingSearchView}>
                                        <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                            <Appbar.Content title="Select Your Category" titleStyle={styles.headingSearchStyle} style={styles.searchHeading} />
                                            <Appbar.Content title="OK" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._navigateCategoryFilter()}} />
                                        </Appbar.Header>
                                    </View>
                                    <FlatList
                                        data={this.state.genres}
                                        renderItem={this._renderGenersItem}
                                        keyExtractor={item => item.id}
                                    />
                                </View>}

                                {this.state.otherRecommedationTypes.length > 0 && <View style={styles.branchView}>
                                    <View style={styles.headingSearchView}>
                                        <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                            <Appbar.Content title="Other Categories" titleStyle={styles.headingSearchStyle} style={styles.searchHeading} />
                                            <Appbar.Content title="OK" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._navigateCategoryFilter()}} />
                                        </Appbar.Header>
                                    </View>
                                    <FlatList
                                        data={this.state.otherRecommedationTypes}
                                        renderItem={this._renderOtherRecommedationItem}
                                        keyExtractor={item => item.id}
                                    />
                                </View>}

                                {this.state.ageGroups.length > 0 && <View style={styles.branchView}>
                                    <View style={styles.headingSearchView}>
                                        <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                            <Appbar.Content title="Age Groups" titleStyle={styles.headingSearchStyle} style={styles.searchHeading} />
                                            <Appbar.Content title="OK" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._navigateCategoryFilter()}} />
                                        </Appbar.Header>
                                    </View>
                                    <FlatList
                                        data={this.state.ageGroups}
                                        renderItem={this._renderAgeGroupsItem}
                                        keyExtractor={item => item.ageGroupId}
                                    />
                                </View>}
                            </View>
                        </ScrollView>
                        <BottomTab navigation={this.props.navigation} routeName="profile" />
                    </View>
                }
            </View>
        );
    }

}

export default Recommendations;