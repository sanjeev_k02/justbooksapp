import React from 'react';
import { View, Text, TextInput, ToastAndroid, SafeAreaView, FlatList, Image} from 'react-native';
import {  Headline, Title , Divider, Paragraph, Chip} from 'react-native-paper';
import Icons from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import CategoryTab from '../../components/CategoryTab/CategoryTab';
import API from '../../env';
import * as axios from 'axios';

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            subscription_id: 0,
            memberCard: 0,
            branchId: 0,
            primusMappedBranches: 0,
            cityId: 0,
            primusMember: false,
            page: 1,
            getSearchData: [],
            getSearchDataFlag: false,
            searchFilter: [],
            searchSelectedValue: {},

        }
    }

    _toTitleCase(str) { 
        if(str.length > 45) {
            var res = str.substring(0, 45);
            return res.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            )+"...";
        } else {
            return str.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            );
        }
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user);
                // console.log("us",userData);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard,
                    branchId: userData.login.branchId,
                    primusMappedBranches: userData.login.primusMappedBranches,
                    cityId: userData.login.cityId,
                    primusMember: userData.login.primus
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }
    };

    searchBooks = () => {
        if (this.state.searchText === '') {
        ToastAndroid.show("Search query cannot be empty", ToastAndroid.LONG)
          return;
        }
    
        this.props.navigation.navigate('SearchResults', {
          searchQuery: this.state.searchText,
        });
    };

    _typeHead = (searchText) => {
        this.setState({
            searchText: searchText
        })
        let typeHeadResult;
        if(this.state.primusMember){
            typeHeadResult = API.API_ESAPI_POINT+"/getSuggestBooksForPrimus?search="+this.state.searchText+"&page="+this.state.page+"&branchIds="+this.state.primusMappedBranches+"&cityId="+this.state.cityId;
        } else {
            typeHeadResult = API.API_ESAPI_POINT+"/getSuggestBooks?search="+searchText+"&page="+this.state.page;
        }
        console.log(typeHeadResult);
        axios.get(typeHeadResult, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
          .then((response) => {
            // console.log(response.data.successObject);
              if (response.data.status && response.data.successObject.length > 0) {
                  this.setState({
                        getSearchDataFlag: true,
                        getSearchData : response.data.successObject,
                  })
              } else {
                this.setState({
                    getSearchDataFlag: false,
                    getSearchData : [],
                })
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }

    _bookDetailsNavigate = async(title_id) => {
        this.props.navigation.push("BookDetails",{ title_id });
    }

    _categoryAction = (mainCategoryId) => {
        this.props.navigation.push('CategoryResults',{
          categories: mainCategoryId,
          isMainCategory: true
        });
    }
    
    render() {
        var _renderRideItem =(item,index) => {
            return (
                <View key={index}>
                        <View style={styles.card} >
                            <View style={styles.cardDescription}>
                                <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.id)}}>
                                    <Text style={styles.titleDesciption}>
                                        <Paragraph style={styles.searchColor}>{this._toTitleCase(item.name)}</Paragraph>
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    <Divider style={styles.divider} />
                </View>
            )
        }
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={styles.contain}>
                    <StatusTopBar />
                    {/* <ScrollView> */}
                        <View style={styles.containBody}>
                            <View style={styles.textStyle}>
                                <Text style={styles.titleDesciption}>
                                    <Headline style={styles.color}>Find For</Headline>{'\n'}
                                    <Text style={{color: 'green', fontSize: 16 }}>
                                        <Headline style={styles.colorParagraph}>Books and Authors</Headline>{'\n'}
                                    </Text>{'\n'}
                                </Text>
                            </View>
                            <View style={styles.SectionStyle}>
                                <TextInput 
                                    label= "searchText"
                                    returnKeyType= "next"
                                    style={styles.searchTextInput}
                                    underlineColor= "#2E2E2E"
                                    placeholderTextColor = "#2E2E2E"
                                    placeholder= "What book are looking for..."
                                    mode= "outlined" 
                                    onChangeText={changedText => this._typeHead(changedText)}
                                    returnKeyType='search'
                                    autoFocus={true}
                                    onSubmitEditing={()=>this.searchBooks()} 
                                    clearButtonMode="while-editing"
                                />
                                <Icons name="search1" size={24} color="#2E2E2E" style={{ marginRight: 20 }} />
                            </View>
                        </View>
                        <View style={styles.listData}>
                            <View style={styles.cardHomeView}>
                            {(Object.keys(this.state.getSearchData).length > 0 && this.state.getSearchDataFlag) && (
                                <View style={{marginBottom: 100}}>
                                    <FlatList 
                                        data={this.state.getSearchData}
                                        ref={"flatlist"}
                                        removeClippedSubviews={true}
                                        initialNumToRender={10}
                                        bounces={false}
                                        renderItem={({ item, index }) => _renderRideItem(item, index)}
                                        keyExtractor={(item, index) => item.id}
                                        bounces={false}
                                        onEndReachedThreshold={0.5}
                                    />
                                </View>
                            )}
                            </View>
                            <View style={styles.category}>
                                <ScrollView >
                                    <View style={styles.flexContainer}>
                                        <View style={styles.item}>
                                            <Chip avatar={<Image source={{ uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/main_category_icons/nf.png' }} />} style={styles.bottomButtons}  onPress={()=>this._categoryAction(2)}>
                                                <Title style={styles.footerText}>Non-Ficition</Title>
                                            </Chip>
                                        </View>
                                        <View style={styles.item}>
                                            <Chip avatar={<Image source={{ uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/main_category_icons/Fiction.png' }} />} style={styles.bottomButtons}  onPress={()=>this._categoryAction(1)}>
                                                <Title style={styles.footerText}>Fiction</Title>
                                            </Chip>
                                        </View>
                                    </View>


                                    <View style={styles.flexContainer}>
                                        <View style={styles.item}>
                                            <Chip avatar={<Image source={{ uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/main_category_icons/Young+readers.png' }} />} style={styles.bottomButtons} onPress={()=>this._categoryAction(4)}>
                                                <Title style={styles.footerText}>Young Readers</Title>
                                            </Chip>
                                        </View>
                                        <View style={styles.item}>
                                            <Chip avatar={<Image source={{ uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/main_category_icons/Indian+fiction.png' }} />} style={styles.bottomButtons}  onPress={()=>this._categoryAction(5)}>
                                                <Title style={styles.footerText}>Indian Writing</Title>
                                            </Chip> 
                                        </View>
                                    </View>


                                    <View style={[styles.flexContainer,{textAlign: 'center', alignContent: 'center', alignItems: 'center', alignSelf: 'center'}]}>
                                        <View style={[styles.item,{width: undefined}]}>
                                            <Chip avatar={<Image source={{ uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/main_category_icons/regional.png' }} />} style={[styles.bottomButtons,{width: 200}]}  onPress={()=>this._categoryAction(3)}>
                                                <Title style={styles.footerText}>Regional Languages</Title>
                                            </Chip>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    {/* </ScrollView> */}
                    {/* <BottomTab navigation={this.props.navigation} /> */}
                </View>
            </SafeAreaView>
        );
    }

}

export default Search;