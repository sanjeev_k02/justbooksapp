import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 40
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 15, 
        marginTop: 0, 
        paddingLeft: 15,
        paddingBottom: 25,
    },
    divider : {
        margin: 10,
    },
    containBody: { 
        // flex: 1,
        display: 'flex', 
        alignContent: 'center', 
        justifyContent: 'center' ,
        padding: 20,
        backgroundColor: 'green',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        // height: 200,
    },
    SectionStyle: {
        // flex: 1,
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#FFFFFF',
        borderRadius: 4,
        // padding: 20,
        paddingRight: 20,
        shadowColor: 'grey',
        shadowOffset: {
            width: 0,
            height: 1
        },
        elevation: 0,
        marginBottom: 10,
        shadowOpacity: 0.5,
        shadowRadius: 1,
        backgroundColor: '#F1F1F1',
    },
    searchTextInput: {
        width: 250, 
        color: '#2E2E2E',
        backgroundColor: '#F1F1F1',
        borderColor: '#FFFFFF'
    },
    titleDesciption: {
        marginTop: 5,
        marginRight: 30
    },
    color: {
        color: '#FFFFFF',
        fontWeight: '600',
        fontFamily: 'Montserrat-Bold'
    },
    colorParagraph: {
        color: '#FFFFFF',
        fontWeight: '600',
        fontFamily: 'Montserrat-Bold'
    },
    container: {
        backgroundColor: '#F1F1F1',
        flex: 1,
        padding: 16,
        marginTop: 40,
    },
    autocompleteContainer: {
        backgroundColor: '#F1F1F1',
        borderWidth: 0,
        borderColor: '#FFFFFF'
    },
    descriptionContainer: {
        flex: 1,
        justifyContent: 'center',
        borderWidth: 0,
        borderColor: '#FFFFFF'
    },
    itemText: {
        fontSize: 15,
        paddingTop: 5,
        paddingBottom: 5,
        // margin: 2,
    },
    infoText: {
        textAlign: 'center',
        fontSize: 16,
    },
    cardDescription : {
        width: '100%',
        flexDirection: 'column',
        marginLeft: 10, 
        marginRight: 10, 
        justifyContent: 'space-between'
    },
    searchColor: {
        color: '#2E2E2E',
    },
    category: {
        // position: 'absolute',
        // flex:1,
        // left: 0,
        // right: 0,
        // top: 60,
        // height:200,
        // alignItems:'center',
        // elevation:25,
        // display: "flex", 
        // flexDirection: "row", 
        // flexWrap: "wrap", 
        alignContent: "center", 
        alignItems: "center", 
        justifyContent: "center",
    },
    bottomButtons: {
      width: 160,
      height: 40,
      margin: 10,
      alignItems:'center',
      justifyContent: 'space-around',
      flex:1,
      marginTop:9,
      backgroundColor: '#F1F1F1'
    },
    footerText: {
      color:'#101211',
      fontWeight:'600',
      alignItems:'center',
      fontSize:18,
      lineHeight:20,
      margin: 10
    },
    flexContainer: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start' // if you want to fill rows left to right
    },
    item: {
        width: '50%' // is 50% of container width
    }

});
