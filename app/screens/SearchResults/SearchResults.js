import React from 'react';
import { View, Text, StatusBar, RefreshControl, Image, FlatList } from 'react-native';
import styles from './styles';
import { Avatar, Appbar, Divider, Button, Card, Title, Paragraph, ActivityIndicator, Colors } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

import Icons from 'react-native-vector-icons/Ionicons';
import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';
import Filter from '../../components/Filter/Filter';
import API from '../../env';
import * as axios from 'axios';

class SearchResults extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subscription_id: 0,
            searchBookDetails: {},
            refreshing: true,
            searchQuery : this.props.route.params.searchQuery,
            page: 1,
            cityId: 26
        }
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user)
                this.setState({
                    subscription_id: userData.subscription_id,
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        await this.getSearchBookDetails(this.state.searchQuery);
        // setTimeout(
        //     () => {
        //     this.setState({ refreshing: false });
        //     },
        //     2000
        // );
    };

    _onRefresh = () => {
        this.setState({refreshing: true});
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
      }

    _bookDetailsNavigate = async(title_id) => {
        this.props.navigation.push("BookDetails",{ title_id });
    }

    getSearchBookDetails = (searchQuery) => {
      const getSubscription = API.API_ESAPI_POINT+"/getSuggestBookDetails?search="+searchQuery+"&page="+this.state.page+"&cityId="+this.state.cityId;
      console.log(getSubscription);
      axios.get(getSubscription, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status) {
                this.setState({
                    refreshing: false,
                    searchBookDetails : response.data.successObject,
                })
            } else {
                this.setState({
                    refreshing: false
                })
                console.log("No books found for this Query " + this.state.searchQuery);
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _avgRating = (rating) => {
        var avg_reading_time = rating ? rating : 0;
        while(avg_reading_time > 5){
            avg_reading_time = avg_reading_time / 5;
        }
        return avg_reading_time;
    }
    
    render() {
        var _renderRideItem =(item,index) => {
            return (
                <View key={index}>
                        <View style={styles.card} >
                            <TouchableOpacity  onPress={()=>{this._bookDetailsNavigate(item.jb_info.titleid)}}>
                                {(item.image_url != null && item.image_url != '') ? 
                                    <Image style={styles.imageCard} source={{ uri: item.image_url }} ></Image>
                                    : 
                                    <Image style={styles.imageCard} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} ></Image>
                                }
                            </TouchableOpacity>
                            <View style={styles.cardDescription}>
                                <TouchableOpacity  onPress={()=>{this._bookDetailsNavigate(item.jb_info.titleid)}}>
                                    <Text style={styles.titleDesciption}>
                                        <Title style={styles.color}>{this._toTitleCase(item.jb_info.title)}</Title>{'\n'}
                                        {(item.jb_info.author != null && item.jb_info.author.name != "" && item.jb_info.author.name != null) && <Text style={{color: 'green', fontSize: 16 }}>By : 
                                            <Paragraph style={styles.colorParagraph}> {this._toTitleCase(item.jb_info.author.name)}</Paragraph>
                                        </Text>}{'\n'}
                                        {(item.jb_info.times_rented != "" && item.jb_info.times_rented != null) && <Text style={{color: 'green', fontSize: 16 }}>
                                            <Paragraph style={styles.colorParagraph}>No of times read : {item.jb_info.times_rented}</Paragraph>{'\n'}
                                        </Text>}
                                    </Text>
                                </TouchableOpacity>
                                <Text style={styles.titleDesciption}>
                                    <Rating
                                        startingValue={ item.jb_info.times_rented ? this._avgRating(item.jb_info.times_rented): 0 }
                                        type='custom'
                                        ratingColor='#FFD54B'
                                        ratingBackgroundColor='#F1F1F1'
                                        ratingCount={5}
                                        imageSize={20}
                                        onFinishRating={this.ratingCompleted}
                                        style={{ paddingVertical: 10 }}
                                    />
                                </Text>
                            </View>
                        </View>
                    <Divider style={styles.divider} />
                </View>
            )
        }
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView 
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            <View style={{ marginBottom: 5 ,marginTop: 15}}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={styles.rowHeader}>
                                        <Text title="Latest Books" titleStyle={styles.headingStyle} style={styles.heading}>
                                            Search for "<Text style={{color: 'blue'}}>{this.state.searchQuery}</Text>"
                                        </Text>
                                        {/* <Text title="filter" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} ></Text>
                                        <Filter navigation={this.props.navigation} /> */}
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardHomeView}>
                                    {(Object.keys(this.state.searchBookDetails).length > 0 ) && (
                                        <View>
                                            <FlatList 
                                                data={this.state.searchBookDetails}
                                                ref={"flatlist"}
                                                removeClippedSubviews={true}
                                                initialNumToRender={10}
                                                bounces={false}
                                                renderItem={({ item, index }) => _renderRideItem(item, index)}
                                                keyExtractor={(item, index) => item.id}
                                                bounces={false}
                                                onEndReachedThreshold={0.5}
                                            />
                                        </View>
                                    )}
                                    
                                    {((Object.keys(this.state.searchBookDetails).length == 0 ) && this.state.refreshing) && (
                                    <View class={styles.shareContainer}>
                                        <View style={{ alignSelf: "center" }}>
                                            <View style={{
                                                marginTop: 60,
                                                width: 220,
                                                height: 220,
                                                borderRadius: 100,
                                                overflow: "hidden"}}>
                                            <Image source={require("../../assets/no-data.png")} style={{
                                                flex: 1,
                                                height: 250,
                                                width: undefined}} ></Image>
                                            </View>
                                        </View>
                                    </View>
                                    )}
                                </View>
                            </View>
                        </ScrollView>
                        <SearchTab navigation={this.props.navigation} />
                        <BottomTab navigation={this.props.navigation} />
                    </View>
                }
            </View>
        );
    }

}

export default SearchResults;