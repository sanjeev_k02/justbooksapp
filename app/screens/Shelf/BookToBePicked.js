import React from 'react';
import { View, Share, RefreshControl, Text, Image } from 'react-native';
import { Title, Paragraph, Divider,  ActivityIndicator, Colors } from 'react-native-paper';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import API from '../../env';
import * as axios from 'axios';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';

class BookToBePicked extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            subscription_id: 0,
            book_req_type: 'PICKUP',
            bookToBePicked: {},
            refreshing: true,
        }
    }

    _toTitleCase(str) { 
        if(str.length > 45) {
            var res = str.substring(0, 45);
            return res.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            )+"...";
        } else {
            return str.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            );
        }
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user)
                this.setState({
                    refreshing: false,
                    subscription_id: userData.login.subscriptionId,
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        await this.getBookToBePickedDetails(this.state.subscription_id);
        setTimeout(
            () => {
                this.setState({ refreshing: false });
            },
            2000
        );
    };

    getBookToBePickedDetails = (subscriptionId) => {
      const getBookToBePicked = API.API_JUSTBOOK_ENDPOINT+"/getPendingOrdersForSubscription?subscription_id="+subscriptionId+"&book_req_type="+this.state.book_req_type;
      console.log(getBookToBePicked);
      axios.get(getBookToBePicked, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.data.length > 0) {
                this.setState({
                    bookToBePicked : response.data.successObject.data,
                    refreshing: false
                })
            } else {
                this.setState({
                    refreshing: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _bookDetailsNavigate = async(title_id) => {
        this.props.navigation.navigation.navigate("BookDetails",{ title_id });
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
    }

    _shareBookDetails = async(title) => {
        Share.share({
            message: "I found A " + title + " on JustBooks and thought you might find it interesting! Sign up today to read ",
          })
        //after successful share return result
        .then((result) => console.log(result))
        //If any thing goes wrong it comes here
        .catch((errorMsg) => console.log(errorMsg));
    }


    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView 
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            <View style={styles.containBody}>
                                <View class={styles.shareContainer}>
                                    <View style={styles.titleHeader}>
                                        <Title style={{textAlign: 'center'}}>
                                            Book To Be Picked
                                        </Title>
                                        <Divider style={styles.divider} />
                                    </View>
                                </View>
                                {this.state.bookToBePicked.length > 0 ? <View style={styles.cardHomeView}>
                                    {this.state.bookToBePicked.map((item) => {
                                        return (
                                            <View key={item.titleId} >
                                                <View style={styles.card} >
                                                    <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}}>
                                                    {(item.titleImage != null && item.titleImage != 'null' && item.titleImage != '' && item.titleImage != 'images/noimage.jpg') ? 
                                                        <Image style={styles.imageCard} source={{ uri: item.titleImage }} ></Image>
                                                        : 
                                                        <Image style={styles.imageCard} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} ></Image>
                                                    }
                                                    </TouchableOpacity>
                                                    <View style={styles.cardDescription}>
                                                        <Text style={styles.titleDesciption}>
                                                            <Title style={styles.color}>{this._toTitleCase(item.titleName)}</Title>{'\n'}
                                                            <Text style={{color: 'green', fontSize: 16 }}>By : 
                                                                <Paragraph style={styles.colorParagraph}> {this._toTitleCase(item.authors[0].authorName)}</Paragraph>
                                                            </Text>{'\n'}
                                                        </Text>
                                                        <View style={styles.actionStyle}>
                                                            <View style={styles.footer}>
                                                                <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(this._toTitleCase(item.titleName))} >
                                                                    <Icons name="share-variant" size={26} color={'#000000'} />
                                                                    <Text style={styles.footerText}>Share</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <Divider style={styles.divider} />
                                            </View>
                                        )
                                    })}
                                </View>
                                :
                                <View class={styles.shareContainer}>
                                    {/* <View style={styles.titleHeader}>
                                        <Text style={{color: '#000'}}>You don't have currently Reading Book</Text>
                                    </View> */}
                                    <View style={{ alignSelf: "center" }}>
                                        <View style={{
                                            marginTop: 60,
                                            width: 220,
                                            height: 220,
                                            borderRadius: 100,
                                            overflow: "hidden"}}>
                                        <Image source={require("../../assets/no-data.png")} style={{
                                            flex: 1,
                                            height: 250,
                                            width: undefined}} ></Image>
                                        </View>
                                    </View>
                                </View>}
                            </View>
                        </ScrollView>
                    </View>
                }
            </View>
        );
    }

}

export default BookToBePicked;