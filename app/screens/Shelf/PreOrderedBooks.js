import React from 'react';
import { View, Share, Alert, RefreshControl, Text, Image } from 'react-native';
import { Title, Paragraph, Divider,  ActivityIndicator, Colors } from 'react-native-paper';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { showMessage, hideMessage } from "react-native-flash-message";
import API from '../../env';
import * as axios from 'axios';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';

class PreOrderedBooks extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            subscription_id: 0,
            preOrderedBook: {},
            refreshing: true,
            successError: false,
            errorMsg: ''
        }
    }

    _toTitleCase(str) { 
        if(str.length > 45) {
            var res = str.substring(0, 45);
            return res.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            )+"...";
        } else {
            return str.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            );
        }
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user)
                this.setState({
                    refreshing: false,
                    subscription_id: userData.login.subscriptionId,
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        await this.getPreOrderedBookDetails(this.state.subscription_id);
        setTimeout(
            () => {
                this.setState({ refreshing: false });
            },
            2000
        );
    };

    getPreOrderedBookDetails = (subscriptionId) => {
      const getPreOrderedBook = API.API_JUSTBOOK_ENDPOINT+"/getReservedBookForSubscription?subscriptionId="+subscriptionId;
      console.log(getPreOrderedBook);
      axios.get(getPreOrderedBook, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    preOrderedBook : response.data.successObject,
                    refreshing: false
                })
            } else {
                this.setState({
                    refreshing: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _bookDetailsNavigate = async(title_id) => {
        this.props.navigation.navigation.navigate("BookDetails",{ title_id });
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        setTimeout(() => {
           this.getPreOrderedBookDetails(this.state.subscription_id);
          this.setState({refreshing: false});
        }, 2000);
    }

    _shareBookDetails = async(title) => {
        Share.share({
            message: "I found A " + title + " on JustBooks and thought you might find it interesting! Sign up today to read ",
          })
        //after successful share return result
        .then((result) => console.log(result))
        //If any thing goes wrong it comes here
        .catch((errorMsg) => console.log(errorMsg));
    }

    _deleteBookDetails = async(titleId) => {
        console.log("cancel")
        if(this.state.subscription_id == 0) {
            showMessage({
                message: "",
                description: 'Become a member to wishlist books!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            Alert.alert(
                'Hold On',
                'do you want to cancel Order?',
                [
                    { text: 'Nope',style: 'cancel' },
                    {
                        text: 'Yeah', onPress: () => {
                            this._cancelActionPerform(titleId)
                        }
                    },
                ]
            );
            
        }
    }

    _cancelActionPerform = (titleId) => {
        let rentNow  = API.API_JUSTBOOK_ENDPOINT + "/cancelReservedOrder";
        axios.post(rentNow ,{
            "titleId":titleId, 
            "subscriptionId": this.state.subscription_id,
            "createdBy": 1000,
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("rent now", response.data);
            if (response.data.status) {
                showMessage({
                    message: "",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                setTimeout(() => {
                    this.setState({ refreshing: true, successError : false });
                    this.getPreOrderedBookDetails(this.state.subscription_id);
                },2000);
            } else {
                showMessage({
                    message: "",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }


    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView 
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            <View style={styles.containBody}>
                                <View class={styles.shareContainer}>
                                    <View style={styles.titleHeader}>
                                        <Title style={{textAlign: 'center'}}>
                                            Past Ordered Books
                                        </Title>
                                        <Divider style={styles.divider} />
                                    </View>
                                </View>
                                {this.state.preOrderedBook.length > 0 ? <View style={styles.cardHomeView}>
                                    {this.state.preOrderedBook.map((item) => {
                                        return (
                                            <View key={item.titleId}>
                                                <View style={styles.card} >
                                                    <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}}>
                                                    {(item.titleImage != null && item.titleImage != 'null' && item.titleImage != '' && item.titleImage != 'images/noimage.jpg') ? 
                                                        <Image style={styles.imageCard} source={{ uri: item.image }} ></Image>
                                                        : 
                                                        <Image style={styles.imageCard} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} ></Image>
                                                    }
                                                    </TouchableOpacity>
                                                    <View style={styles.cardDescription}>
                                                        <Text style={styles.titleDesciption}>
                                                            <Title style={styles.color}>{this._toTitleCase(item.titleName)}</Title>{'\n'}
                                                            {item.author != "" && <Text style={{color: 'green', fontSize: 16 }}>By : 
                                                                <Paragraph style={styles.colorParagraph}> {this._toTitleCase(item.author)}</Paragraph>
                                                            </Text>}
                                                        </Text>
                                                        <View style={styles.actionStyle}>
                                                            <View style={styles.footer}>
                                                                <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(this._toTitleCase(item.titleName))} >
                                                                    <Icons name="share-variant" size={26} color={'#000000'} />
                                                                    <Text style={styles.footerText}>Share</Text>
                                                                </TouchableOpacity>

                                                                <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._deleteBookDetails(item.titleId)} >
                                                                    <Icons name="delete" size={26} color={'#000000'} />
                                                                    <Text style={styles.footerText}>Cancel</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <Divider style={styles.divider} />
                                            </View>
                                        )
                                    })}
                                </View>
                                :
                                <View class={styles.shareContainer}>
                                    {/* <View style={styles.titleHeader}>
                                        <Text style={{color: '#000'}}>You don't have currently Reading Book</Text>
                                    </View> */}
                                    <View style={{ alignSelf: "center" }}>
                                        <View style={{
                                            marginTop: 60,
                                            width: 220,
                                            height: 220,
                                            borderRadius: 100,
                                            overflow: "hidden"}}>
                                        <Image source={require("../../assets/no-data.png")} style={{
                                            flex: 1,
                                            height: 250,
                                            width: undefined}} ></Image>
                                        </View>
                                    </View>
                                </View>}
                            </View>
                        </ScrollView>
                        {this.state.successError && <View style={styles.footerMessage}>
                            <View style={styles.subscriptionViewMessage}>
                                <TouchableOpacity style={styles.subscriptionBottomButtons} onPress={()=>{console.log("error Message")}} >
                                    <Text style={[
                                        styles.subscriptionFooterText,
                                        { color: "#ffffff" }
                                    ]}> {this.state.errorMsg} </Text>
                                </TouchableOpacity>
                            </View>
                        </View>}
                    </View>
                }
            </View>
        );
    }

}

export default PreOrderedBooks;