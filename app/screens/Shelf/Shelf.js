import React from 'react';
import { View, Share, Alert, ScrollView, RefreshControl, BackHandler, Image, TouchableHighlight } from 'react-native';
import styles from './shelfStyle';
import { Divider, Text, Card, Title, Paragraph, ActivityIndicator, Colors  } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import {  } from 'react-native-gesture-handler';
import { Appbar } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { showMessage } from "react-native-flash-message";
import Modal from 'react-native-modal';
import { WebView } from 'react-native-webview';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import CategoryTab from '../../components/CategoryTab/CategoryTab';
import API from '../../env';
import * as axios from 'axios';

class Shelf extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          subscription_id: 0,
          memberCard: 0,
          branchId: 0,
          cityId: 0,
          primusMappedBranches: 0,
          primusMember: false,
          page: 1,
          pageSize: 10,
          userDetails: {},
          refreshing: true,
          authorFlag: false,
          authorData: {},
          currentlyReadingFlag: false,
          currentlyReading: {},
          bookToBeDeliveredFlag: false,
          bookToBeDelivered: {},
          bookToBePickedFlag: false,
          bookToBePicked: {},
          wishlistBookFlag: false,
          wishlistBook: {},
          pastReadFlag: false,
          pastRead: {},
          preOrderedBookFlag: false,
          preOrderedBook: {},
          videoReviewBookFlag: false,
          videoReviewBook: {},
          isModalVisible: false,
          videoReviewUrl: '',
        };
    }

    _toTitleCase(str) { 
        if(str.length > 16) {
            var res = str.substring(0, 16);
            return res.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            )+"";
        } else {
            return str.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            );
        }
    }

    _toAuthorTitleCase(str) { 
        if(str.length > 12) {
            var res = str.substring(0, 12);
            return res.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            )+"";
        } else {
            return str.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            );
        }
    }

    componentDidMount = async() => {
      BackHandler.addEventListener("hardwareBackPress", this.backPressed);
        try {
            const user = await AsyncStorage.getItem('userData');
            // console.log("user",user);
            if (user !== null) {
                const userData = JSON.parse(user);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard,
                    branchId: userData.login.branchId,
                    primusMappedBranches: userData.login.primusMappedBranches,
                    cityId: userData.login.cityId,
                    primusMember: userData.login.primus
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }
        // getcurrentlyReading
        await this.getAllCurrentlyReadingBooks(this.state.subscription_id);

        // bookToBeDelivered
        await this.getAllBookToBeDelivered(this.state.subscription_id);

        // bookToBeDelivered
        await this.getAllBookToBePicked(this.state.subscription_id);

        // wishlistBook
        await this.getAllWishlistBook(this.state.subscription_id);

        // PastRead
        await this.getAllPastRead(this.state.subscription_id);

        // preOrderedBook
        await this.getAllPreOrderedBook(this.state.subscription_id);

        // videoReview
        await this.getAllVideoReviewBook(this.state.subscription_id);

        setTimeout(() => {
            this.setState({ refreshing: false });
        }, 1000);
    };

    componentWillUnmount() {
      BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    }

    backPressed = () => {
        let { routeName } = this.props.route.name;
        if (routeName == "Home" && this.props.navigation.isFocused()) {
          BackHandler.exitApp();
        }
    };

    getAllCurrentlyReadingBooks = async(subscriptionId) => {
        const getCurrentlyReading = API.API_JUSTBOOK_ENDPOINT+"/getCurrentlyReadings?subscription_id="+subscriptionId;
        console.log(getCurrentlyReading);
        await axios.get(getCurrentlyReading, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject.data);
            if (response.data.status && response.data.successObject.data.length > 0) {
                this.setState({
                    currentlyReading : response.data.successObject.data,
                    currentlyReadingFlag: true
                })
            } else {
                this.setState({
                    currentlyReadingFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllBookToBeDelivered = async (subscriptionId) => {
        const book_req_type = 'DELIVERY';
        const getBookToBeDelivered = API.API_JUSTBOOK_ENDPOINT+"/getPendingOrdersForSubscription?subscription_id="+subscriptionId+"&book_req_type="+book_req_type;
        console.log(getBookToBeDelivered);
        axios.get(getBookToBeDelivered, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject.data);
            if (response.data.status && response.data.successObject.data.length > 0) {
                this.setState({
                    bookToBeDelivered : response.data.successObject.data,
                    bookToBeDeliveredFlag: true
                })
            } else {
                this.setState({
                    bookToBeDeliveredFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllBookToBePicked = async(subscriptionId) => {
        const book_req_type = 'PICKUP';
        const getBookToBePicked = API.API_JUSTBOOK_ENDPOINT+"/getPendingOrdersForSubscription?subscription_id="+subscriptionId+"&book_req_type="+book_req_type;
        console.log(getBookToBePicked);
        await axios.get(getBookToBePicked, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject.data);
            if (response.data.status && response.data.successObject.data.length > 0) {
                this.setState({
                    bookToBePicked : response.data.successObject.data,
                    bookToBePickedFlag: true
                })
            } else {
                this.setState({
                    bookToBePickedFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllWishlistBook = async (subscriptionId) => {
        const getWishlistBook = API.API_JUSTBOOK_ENDPOINT+"/getWishlistForSubscription?subscription_id="+subscriptionId;
        console.log(getWishlistBook);
        await axios.get(getWishlistBook, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    wishlistBook : response.data.successObject,
                    wishlistBookFlag: true
                })
            } else {
                this.setState({
                    wishlistBookFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllPastRead = async (subscriptionId) => {
        const getPastReadBook = API.API_JUSTBOOK_ENDPOINT+"/getPastReads?subscription_id="+subscriptionId+"&pageNumber=1&pageSize=1000";
        console.log(getPastReadBook);
        axios.get(getPastReadBook, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject.data.data);
            if (response.data.status && response.data.successObject.data.data.length > 0) {
                this.setState({
                    pastRead : response.data.successObject.data.data,
                    pastReadFlag: true
                })
            } else {
                this.setState({
                    pastReadFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllPreOrderedBook = async (subscriptionId) => {
        const getPreOrderedBook = API.API_JUSTBOOK_ENDPOINT+"/getReservedBookForSubscription?subscriptionId="+subscriptionId;
        console.log(getPreOrderedBook);
        axios.get(getPreOrderedBook, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    preOrderedBook : response.data.successObject,
                    preOrderedBookFlag: true
                })
            } else {
                this.setState({
                    preOrderedBookFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    getAllVideoReviewBook = async (subscriptionId) => {
        const getVideoReviewBook = API.API_JUSTBOOK_ENDPOINT+"/titleReview/getTitleVideoReviewsForSubscriptionId?subscriptionId="+subscriptionId+"&pageNumber="+this.state.page+"&pageSize="+this.state.pageSize;
        console.log(getVideoReviewBook);
        axios.get(getVideoReviewBook, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.data != null && response.data.successObject.data.length > 0) {
                this.setState({
                    videoReviewBook : response.data.successObject.data,
                    videoReviewBookFlag: true
                })
            } else {
                this.setState({
                    videoReviewBookFlag: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _shareBookDetails = async(title) => {
        Share.share({
            message: "I found A " + title + " on JustBooks and thought you might find it interesting! Sign up today to read ",
          })
        //after successful share return result
        .then((result) => console.log(result))
        //If any thing goes wrong it comes here
        .catch((errorMsg) => console.log(errorMsg));
    }


    _wishlistAction = async(action, titleId) => {
        console.log("cancel")
        if(this.state.subscription_id == 0) {
            console.log("Become a member to rent books!!")
            showMessage({
                message: "Wishlist",
                description: 'Become a member to wishlist books!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            Alert.alert(
                'Hold On',
                'do you want to remove from Wishlist?',
                [
                    { text: 'Nope',style: 'cancel' },
                    {
                        text: 'Yeah', onPress: () => {
                            this._wishlistActionPreform(action, titleId)
                        }
                    },
                ]
            );
            
        }
    }

    _wishlistActionPreform = async(action, titleId) => {
        console.log("wishlist")
        let wishlistAddRemove;
        if(action == 'add') {
            wishlistAddRemove = API.API_JUSTBOOK_ENDPOINT + "/addToWishlist";
        } else {
            wishlistAddRemove = API.API_JUSTBOOK_ENDPOINT + "/removeFromWishlist";
        }
        axios.post(wishlistAddRemove ,{
            "titleId":titleId, 
            "createdAt":810, 
            "createdBy": 1000, 
            "subscriptionId": this.state.subscription_id
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response update profile", response.data);
            if (response.data.status) {
                this.setState({ 
                    // refreshing: true,
                    wishlistId: (action == 'add') ? response.data.successObject.wishlistId : 0,
                    errorMsg: (action == 'add') ? response.data.successObject.message : response.data.successObject,
                });
                showMessage({
                    message: "Wishlist",
                    description: (action == 'add') ? response.data.successObject.message : response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                this.getAllWishlistBook(this.state.subscription_id);
                setTimeout(() => {
                    this.setState({ refreshing: false});
                },2000);
            } else {
                showMessage({
                    message: "Wishlist",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Wishlist",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _deleteBookDetails = async(titleId) => {
        console.log("cancel")
        if(this.state.subscription_id == 0) {
            showMessage({
                message: "",
                description: 'Become a member to wishlist books!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            Alert.alert(
                'Hold On',
                'do you want to cancel Order?',
                [
                    { text: 'Nope',style: 'cancel' },
                    {
                        text: 'Yeah', onPress: () => {
                            this._cancelActionPerform(titleId)
                        }
                    },
                ]
            );
            
        }
    }

    _cancelActionPerform = (titleId) => {
        let rentNow  = API.API_JUSTBOOK_ENDPOINT + "/cancelReservedOrder";
        axios.post(rentNow ,{
            "titleId":titleId, 
            "subscriptionId": this.state.subscription_id,
            "createdBy": 1000,
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("rent now", response.data);
            if (response.data.status) {
                showMessage({
                    message: "",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                setTimeout(() => {
                    this.setState({ refreshing: true, successError : false });
                    this.getAllPreOrderedBook(this.state.subscription_id);
                },2000);
            } else {
                showMessage({
                    message: "",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _currentlyReadingReturnAction = async(bookId, titleId) => {
        console.log("rent")
        if(this.state.subscription_id == 0) {
            showMessage({
                message: "Rent Now",
                description: 'Become a member to wishlist books!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            Alert.alert(
                'Justbooks',
                'do you want to return book',
                [
                    { text: 'Nope',style: 'cancel' },
                    { text: 'Yeah', onPress: () => {this._currentlyReadingReturnActionPerform('PICKUP', bookId, titleId)} },
                ],
                { cancelable: true }
            );
            
        }
    }

    _currentlyReadingReturnActionPerform = async(orderType, bookId, titleId) => {
        let rentNow  = API.API_JUSTBOOK_ENDPOINT + "/insertOrUpdateBookRequest";
        axios.post(rentNow ,{
            "createdAt": 810,
            "createdBy": 1000,
            "subscriptionId": this.state.subscription_id,
            "titleId": titleId,
            "bookNumber":bookId,
            "bookReqType":orderType,
            "reqSource":"WEBSITE",
            "status":"NEW",
            "requestedBranchId":this.state.branchId ? this.state.branchId : 1008,
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("rent now", response.data);
            if (response.data.status) {
                this.setState({ 
                    // refreshing: true,
                    errorMsg: response.data.successObject,
                });
                showMessage({
                    message: "Rent",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                // getcurrentlyReading
                this.getAllCurrentlyReadingBooks(this.state.subscription_id);
            } else {
                showMessage({
                    message: "Rent",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Rent",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _bookToBeDeliveredCancelAction = async(ibtId) => {
        console.log("rent")
        if(this.state.subscription_id == 0) {
            showMessage({
                message: "Rent",
                description: 'Become a member to wishlist books!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            Alert.alert(
                'Justbooks',
                'do you want to cancel order Request!',
                [
                    { text: 'Nope',style: 'cancel' },
                    { text: 'Yeah', onPress: () => {this._bookToBeDeliveredCancelActionActionPerform(ibtId)} },
                ],
                { cancelable: true }
            );
            
        }
    }

    _bookToBeDeliveredCancelActionActionPerform = async(ibtId) => {
        let rentNow  = API.API_JUSTBOOK_ENDPOINT + "/insertOrUpdateBookRequest";
        axios.post(rentNow ,{
            "createdAt": 810,
            "createdBy": 1000,
            "ibtId":ibtId,
            "status":"CLOSE",
            "requestedBranchId":this.state.branchId ? this.state.branchId : 1008,
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("rent now", response.data);
            if (response.data.status) {
                this.setState({ 
                    // refreshing: true,
                    errorMsg: response.data.successObject,
                });
                showMessage({
                    message: "Rent",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                // bookToBeDelivered
                this.getAllBookToBeDelivered(this.state.subscription_id);

                // bookToBePicked
                this.getAllBookToBePicked(this.state.subscription_id);
            } else {
                showMessage({
                    message: "Rent",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Rent",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _onRefresh = async () => {
        this.setState({refreshing: true});

        // getcurrentlyReading
        await this.getAllCurrentlyReadingBooks(this.state.subscription_id);

        // bookToBeDelivered
        await this.getAllBookToBeDelivered(this.state.subscription_id);

        // bookToBePicked
        await this.getAllBookToBePicked(this.state.subscription_id);

        // wishlistBook
        await this.getAllWishlistBook(this.state.subscription_id);

        // PastRead
        await this.getAllPastRead(this.state.subscription_id);

        // preOrderedBook
        await this.getAllPreOrderedBook(this.state.subscription_id);

        // videoReviewBook
        await this.getAllVideoReviewBook(this.state.subscription_id);

        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
    }

    _bookDetailsNavigate = async(title_id) => {
        this.props.navigation.push("BookDetails",{ title_id: title_id });
    }

    _allAuthorsNavigate = async() => {
        this.props.navigation.navigate("AllAuthors");
    }

    _authorDetailsNavigate = async(author_id) => {
        this.props.navigation.navigate("AuthorDetails", { author_id });
    }

    _allBooksNavigate = async(action) => {
        this.props.navigation.push("AllBooks", {
            action
        });
    }

    _toggleOpenModal = (videoReviewUrl) => {
        this.setState({
            videoReviewUrl: videoReviewUrl,
            // videoReviewUrl: "https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/video_reviews/title_review_499063_1598946259912.mp4",
            isModalVisible: !this.state.isModalVisible
        });
    };

    _toggleModal = () => {
        this.setState({
            isModalVisible: !this.state.isModalVisible
        });
    };

    _editBookReview = (titleId, reviewId, titleName, videoReviewUrl) => {
        this.props.navigation.navigate("EditVideoReview",{
            titleId: titleId,
            reviewId: reviewId,
            titleName: titleName,
            videoReviewUrl: videoReviewUrl
        })
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <View style={styles.playModel}>
                    <Modal isVisible={this.state.isModalVisible}>
                        <View style={{flex: 1}}>
                            <WebView
                                style={{ flex: 1 }} 
                                scalesPageToFit={true}
                                javaScriptEnabled={true}
                                domStorageEnabled={true}
                                source={{uri: this.state.videoReviewUrl }}
                            />
                            <View
                            style={{ flexDirection: 'row', alignItems: 'space-between', position: 'absolute', bottom: 0}}>
                                <TouchableHighlight
                                     onPress={()=>{this._toggleModal()}}
                                    style={{ flex: 1, backgroundColor: '#F1F1F1' }}>
                                    <Text style={styles.actionClose}>Close</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </Modal>
                </View>
                { (this.state.refreshing) ?
                <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView 
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>

                            {this.state.currentlyReadingFlag && <View style={styles.firstArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={styles.headingStyleCover}>
                                        <Appbar.Content title="Currently Reading" titleStyle={styles.headingStyle} style={styles.heading} />
                                        {/* <Appbar.Content title="More..." titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._allBooksNavigate()}} /> */}
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.currentlyReading.map((item)=> {
                                            return (
                                                <Card key={item.titleId} style={styles.cardSection}>
                                                    {(item.titleImage != null && item.titleImage != ''  && item.titleImage != 'null' && item.titleImage != 'images/noimage.jpg') ? (<View style={styles.UnNonImage}>
                                                        <TouchableOpacity  onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.coverImage} source={{ uri: item.titleImage }} />
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(this._toTitleCase(item.titleName))} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._currentlyReadingReturnAction(item.bookId,item.titleId)} >
                                                                        <Icons name="cart-arrow-right" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Return</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)
                                                    : (<View style={styles.NonImage}>
                                                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                            <Card.Content style={styles.NonImageCardText}>
                                                                <Title style={styles.NonImageTitle}>{this._toTitleCase(item.titleName)}</Title>
                                                                {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.authors[0].authorName ? this._toAuthorTitleCase(item.authors[0].authorName) : ''}</Paragraph>}
                                                            </Card.Content>
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageNonContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._currentlyReadingReturnAction(item.bookId,item.titleId)} >
                                                                        <Icons name="cart-arrow-right" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Return</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)}
                                                {this.state.primusMember && <View style={styles.primusTag}>
                                                    <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                </View>}
                                            </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}

                            {this.state.bookToBeDeliveredFlag && <View style={styles.newArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={styles.headingStyleCover}>
                                        <Appbar.Content title="Book To Be Delivered" titleStyle={styles.headingStyle} style={styles.heading} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.bookToBeDelivered.map((item)=> {
                                            return (
                                                <Card key={item.titleId} style={styles.cardSection}>
                                                    {(item.titleImage != null && item.titleImage != ''  && item.titleImage != 'null' && item.titleImage != 'images/noimage.jpg') ? (<View style={styles.UnNonImage}>
                                                        <TouchableOpacity  onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.coverImage} source={{ uri: item.titleImage }} />
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._bookToBeDeliveredCancelAction(item.ibtId)} >
                                                                        <Icons name="cancel" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Cancel</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)
                                                    : (<View style={styles.NonImage}>
                                                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                            <Card.Content style={styles.NonImageCardText}>
                                                                <Title style={styles.NonImageTitle}>{this._toTitleCase(item.titleName)}</Title>
                                                                {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.authors[0].authorName ? this._toAuthorTitleCase(item.authors[0].authorName) : ''}</Paragraph>}
                                                            </Card.Content>
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageNonContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._bookToBeDeliveredCancelAction(item.ibtId)} >
                                                                        <Icons name="cancel" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Cancel</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)}
                                                {this.state.primusMember && <View style={styles.primusTag}>
                                                    <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                </View>}
                                            </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}

                            {this.state.bookToBePickedFlag && <View style={styles.newArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={styles.headingStyleCover}>
                                        <Appbar.Content title="Book To Be Picked" titleStyle={styles.headingStyle} style={styles.heading} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.bookToBePicked.map((item)=> {
                                            return (
                                                <Card key={item.titleId} style={styles.cardSection}>
                                                    {(item.titleImage != null && item.titleImage != ''  && item.titleImage != 'null' && item.titleImage != 'images/noimage.jpg') ? (<View style={styles.UnNonImage}>
                                                        <TouchableOpacity  onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.coverImage} source={{ uri: item.titleImage }} />
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._bookToBeDeliveredCancelAction(item.ibtId)} >
                                                                        <Icons name="cancel" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Cancel</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)
                                                    : (<View style={styles.NonImage}>
                                                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                            <Card.Content style={styles.NonImageCardText}>
                                                                <Title style={styles.NonImageTitle}>{this._toTitleCase(item.titleName)}</Title>
                                                                {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.authors[0].authorName ? this._toAuthorTitleCase(item.authors[0].authorName) : ''}</Paragraph>}
                                                            </Card.Content>
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageNonContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._bookToBeDeliveredCancelAction(item.ibtId)} >
                                                                        <Icons name="cancel" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Cancel</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)}
                                                {this.state.primusMember && <View style={styles.primusTag}>
                                                    <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                </View>}
                                            </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}

                            {this.state.wishlistBookFlag && <View style={styles.newArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={styles.headingStyleCover}>
                                        <Appbar.Content title="Wishlist " titleStyle={styles.headingStyle} style={styles.heading} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.wishlistBook.map((item)=> {
                                            return (
                                                <Card key={item.titleId} style={styles.cardSection}>
                                                    {(item.titleImage != null && item.titleImage != ''  && item.titleImage != 'null' && item.titleImage != 'images/noimage.jpg') ? (<View style={styles.UnNonImage}>
                                                        <TouchableOpacity  onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.coverImage} source={{ uri: item.titleImage }} />
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._wishlistAction('remove',item.titleId)} >
                                                                        <Icons name="delete" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Remove</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)
                                                    : (<View style={styles.NonImage}>
                                                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                            <Card.Content style={styles.NonImageCardText}>
                                                                <Title style={styles.NonImageTitle}>{this._toTitleCase(item.titleName)}</Title>
                                                                {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.authors[0].authorName ? this._toAuthorTitleCase(item.authors[0].authorName) : ''}</Paragraph>}
                                                            </Card.Content>
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageNonContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._wishlistAction('remove',item.titleId)} >
                                                                        <Icons name="delete" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Remove</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)}
                                                {this.state.primusMember && <View style={styles.primusTag}>
                                                    <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                </View>}
                                            </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}

                            {this.state.pastReadFlag && <View style={styles.newArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={styles.headingStyleCover}>
                                        <Appbar.Content title="Past Read " titleStyle={styles.headingStyle} style={styles.heading} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.pastRead.map((item)=> {
                                            return (
                                                <Card key={item.titleId} style={styles.cardSection}>
                                                    {(item.titleImage != null && item.titleImage != ''  && item.titleImage != 'null' && item.titleImage != 'images/noimage.jpg') ? (<View style={styles.UnNonImage}>
                                                        <TouchableOpacity  onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.coverImage} source={{ uri: item.titleImage }} />
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    {/* <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._wishlistAction('remove',item.titleId)} >
                                                                        <Icons name="delete" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Remove</Text>
                                                                    </TouchableOpacity> */}
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)
                                                    : (<View style={styles.NonImage}>
                                                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                            <Card.Content style={styles.NonImageCardText}>
                                                                <Title style={styles.NonImageTitle}>{this._toTitleCase(item.titleName)}</Title>
                                                                {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.authors[0].authorName ? this._toAuthorTitleCase(item.authors[0].authorName) : ''}</Paragraph>}
                                                            </Card.Content>
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageNonContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    {/* <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._wishlistAction('remove',item.titleId)} >
                                                                        <Icons name="delete" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Remove</Text>
                                                                    </TouchableOpacity> */}
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)}
                                                {this.state.primusMember && <View style={styles.primusTag}>
                                                    <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                </View>}
                                            </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}
                            
                            {this.state.preOrderedBookFlag && <View style={styles.newArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={styles.headingStyleCover}>
                                        <Appbar.Content title="Pre Ordered Book " titleStyle={styles.headingStyle} style={styles.heading} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.preOrderedBook.map((item)=> {
                                            return (
                                                <Card key={item.titleId} style={styles.cardSection}>
                                                    {(item.titleImage != null && item.titleImage != ''  && item.titleImage != 'null' && item.titleImage != 'images/noimage.jpg') ? (<View style={styles.UnNonImage}>
                                                        <TouchableOpacity  onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.coverImage} source={{ uri: item.titleImage }} />
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._deleteBookDetails(item.titleId)} >
                                                                        <Icons name="cancel" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Cancel</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)
                                                    : (<View style={styles.NonImage}>
                                                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                            <Card.Content style={styles.NonImageCardText}>
                                                                <Title style={styles.NonImageTitle}>{this._toTitleCase(item.titleName)}</Title>
                                                                {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.author ? this._toAuthorTitleCase(item.author) : ''}</Paragraph>}
                                                            </Card.Content>
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageNonContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._deleteBookDetails(item.titleId)} >
                                                                        <Icons name="cancel" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Cancel</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)}
                                                {this.state.primusMember && <View style={styles.primusTag}>
                                                    <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                </View>}
                                            </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}

                            {this.state.videoReviewBookFlag && <View style={styles.lastNewArrivles}>
                                <View style={styles.headingView}>
                                    <Appbar.Header style={styles.headingStyleCover}>
                                        <Appbar.Content title="My Video Reviews " titleStyle={styles.headingStyle} style={styles.heading} />
                                    </Appbar.Header>
                                </View>
                                <View style={styles.cardView}>
                                    <ScrollView 
                                    horizontal={true}>
                                        {this.state.videoReviewBook.map((item)=> {
                                            return (
                                                <Card key={item.titleId} style={styles.cardSection}>
                                                    {(item.titleImage != null && item.titleImage != ''  && item.titleImage != 'null' && item.titleImage != 'images/noimage.jpg') ? (<View style={styles.UnNonImage}>
                                                        <TouchableOpacity  onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.coverImage} source={{ uri: item.titleImage }} />
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons}  onPress={()=>{this._toggleOpenModal(item.videoReviewUrl)}} >
                                                                        <Icons name="play-circle" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>View</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._editBookReview(item.titleId, item.reviewId, item.titleName, item.videoReviewUrl)} >
                                                                        <Icons name="content-save-edit" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Edit</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)
                                                    : (<View style={styles.NonImage}>
                                                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}} >
                                                            <Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                            <Card.Content style={styles.NonImageCardText}>
                                                                <Title style={styles.NonImageTitle}>{this._toTitleCase(item.titleName)}</Title>
                                                                {!this.state.primusMember && <Paragraph style={styles.NonImageParagraph}>{item.author ? this._toAuthorTitleCase(item.author) : ''}</Paragraph>}
                                                            </Card.Content>
                                                        </TouchableOpacity>
                                                        <Card.Content style={styles.imageNonContent}>
                                                            <View style={styles.actionStyle}>
                                                                <View style={styles.footer}>
                                                                    <TouchableOpacity style={styles.bottomButtons}  onPress={()=>{this._toggleOpenModal(item.videoReviewUrl)}} >
                                                                        <Icons name="play-circle" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(item.titleName)} >
                                                                        <Icons name="share-variant" size={20} color={'#000000'} />
                                                                        <Text style={styles.footerText}>Share</Text>
                                                                    </TouchableOpacity>

                                                                    <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._editBookReview(item.titleId, item.reviewId, item.titleName, item.videoReviewUrl)} >
                                                                        <Icons name="content-save-edit" size={20} color={'#AB3247'} />
                                                                        <Text style={[styles.footerText,{color: '#AB3247'}]}>Edit</Text>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </Card.Content>
                                                    </View>)}
                                                {this.state.primusMember && <View style={styles.primusTag}>
                                                    <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                                                </View>}
                                            </Card>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                                <Divider style={styles.divider} />
                            </View>}
                            
                        </ScrollView>
                        <BottomTab navigation={this.props.navigation} routeName="shelf"/>
                    </View>
                }
            </View>
        );
    }

}

export default Shelf;