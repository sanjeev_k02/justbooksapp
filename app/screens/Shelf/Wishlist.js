import React from 'react';
import { View, Share, Alert, RefreshControl, Text, Image } from 'react-native';
import { Title, Paragraph, Divider,  ActivityIndicator, Colors } from 'react-native-paper';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { showMessage, hideMessage } from "react-native-flash-message";
import API from '../../env';
import * as axios from 'axios';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';

class Wishlist extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            subscription_id: 2107300,
            wishlistBook: {},
            refreshing: true,
        }
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user)
                this.setState({
                    refreshing: false,
                    subscription_id: userData.login.subscriptionId,
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        await this.getWishlistBookDetails(this.state.subscription_id);
        setTimeout(
            () => {
                this.setState({ refreshing: false });
            },
            2000
        );
    };

    getWishlistBookDetails = (subscriptionId) => {
      const getWishlistBook = API.API_JUSTBOOK_ENDPOINT+"/getWishlistForSubscription?subscription_id="+subscriptionId;
      console.log(getWishlistBook);
      axios.get(getWishlistBook, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    wishlistBook : response.data.successObject,
                    refreshing: false
                })
            } else {
                this.setState({
                    refreshing: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _bookDetailsNavigate = async(title_id) => {
        this.props.navigation.navigation.navigate("BookDetails",{ title_id });
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
    }

    _shareBookDetails = async(title) => {
        Share.share({
            message: "I found A " + title + " on JustBooks and thought you might find it interesting! Sign up today to read ",
          })
        //after successful share return result
        .then((result) => console.log(result))
        //If any thing goes wrong it comes here
        .catch((errorMsg) => console.log(errorMsg));
    }


    _wishlistAction = async(action, titleId) => {
        console.log("cancel")
        if(this.state.subscription_id == 0) {
            console.log("Become a member to rent books!!")
            showMessage({
                message: "Wishlist",
                description: 'Become a member to wishlist books!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            Alert.alert(
                'Hold On',
                'do you want to remove from Wishlist?',
                [
                    { text: 'Nope',style: 'cancel' },
                    {
                        text: 'Yeah', onPress: () => {
                            this._wishlistActionPreform(action, titleId)
                        }
                    },
                ]
            );
            
        }
    }

    _wishlistActionPreform = async(action, titleId) => {
        console.log("wishlist")
        let wishlistAddRemove;
        if(action == 'add') {
            wishlistAddRemove = API.API_JUSTBOOK_ENDPOINT + "/addToWishlist";
        } else {
            wishlistAddRemove = API.API_JUSTBOOK_ENDPOINT + "/removeFromWishlist";
        }
        axios.post(wishlistAddRemove ,{
            "titleId":titleId, 
            "createdAt":810, 
            "createdBy": 1000, 
            "subscriptionId": this.state.subscription_id
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response update profile", response.data);
            if (response.data.status) {
                this.setState({ 
                    // refreshing: true,
                    wishlistId: (action == 'add') ? response.data.successObject.wishlistId : 0,
                    errorMsg: (action == 'add') ? response.data.successObject.message : response.data.successObject,
                });
                showMessage({
                    message: "Wishlist",
                    description: (action == 'add') ? response.data.successObject.message : response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                this.getWishlistBookDetails(this.state.subscription_id);
                setTimeout(() => {
                    this.setState({ refreshing: false});
                },2000);
            } else {
                showMessage({
                    message: "Wishlist",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Wishlist",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }


    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView 
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            <View style={styles.containBody}>
                                <View class={styles.shareContainer}>
                                    <View style={styles.titleHeader}>
                                        <Title style={{textAlign: 'center'}}>
                                            Wishlist
                                        </Title>
                                        <Divider style={styles.divider} />
                                    </View>
                                </View>
                                {this.state.wishlistBook.length > 0 ? <View style={styles.cardHomeView}>
                                    {this.state.wishlistBook.map((item) => {
                                        return (
                                            <View key={item.titleId}>
                                                <View style={styles.card} >
                                                    <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}}>
                                                    {item.titleImage != "images/noimage.jpg" ? 
                                                        <Image style={styles.imageCard} source={{ uri: item.titleImage }} ></Image>
                                                        : 
                                                        <Image style={styles.imageCard} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} ></Image>
                                                    }
                                                    </TouchableOpacity>
                                                    <View style={styles.cardDescription}>
                                                        <Text style={styles.titleDesciption}>
                                                            <Title style={styles.color}>{this._toTitleCase(item.titleName)}</Title>{'\n'}
                                                            {(item.authors[0].authorName != "" && item.authors[0].authorName != null) && <Text style={{color: 'green', fontSize: 16 }}
                                                                >By : 
                                                                <Paragraph style={styles.colorParagraph}> {this._toTitleCase(item.authors[0].authorName)}</Paragraph>
                                                            </Text>}{'\n'}
                                                            {/* {item.totalRented != "" && <Text style={{color: 'green', fontSize: 16 }}>
                                                                <Paragraph style={styles.colorParagraph}>No of times read : {item.totalRented}</Paragraph>
                                                            </Text>} */}
                                                        </Text>
                                                        <View style={styles.actionStyle}>
                                                            <View style={styles.footer}>
                                                                <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(this._toTitleCase(item.titleName))} >
                                                                    <Icons name="share-variant" size={26} color={'#000000'} />
                                                                    <Text style={styles.footerText}>Share</Text>
                                                                </TouchableOpacity>

                                                                <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._wishlistAction('remove',item.titleId)} >
                                                                    <Icons name="delete" size={26} color={'#000000'} />
                                                                    <Text style={styles.footerText}>Remove</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <Divider style={styles.divider} />
                                            </View>
                                        )
                                    })}
                                </View>
                                :
                                <View class={styles.shareContainer}>
                                    {/* <View style={styles.titleHeader}>
                                        <Text style={{color: '#000'}}>You don't have currently Reading Book</Text>
                                    </View> */}
                                    <View style={{ alignSelf: "center" }}>
                                        <View style={{
                                            marginTop: 60,
                                            width: 220,
                                            height: 220,
                                            borderRadius: 100,
                                            overflow: "hidden"}}>
                                        <Image source={require("../../assets/no-data.png")} style={{
                                            flex: 1,
                                            height: 250,
                                            width: undefined}} ></Image>
                                        </View>
                                    </View>
                                </View>}
                            </View>
                        </ScrollView>
                        {this.state.successError && <View style={styles.footerMessage}>
                            <View style={styles.subscriptionViewMessage}>
                                <TouchableOpacity style={styles.subscriptionBottomButtons} onPress={()=>{console.log("error Message")}} >
                                    <Text style={[
                                        styles.subscriptionFooterText,
                                        { color: "#ffffff" }
                                    ]}> {this.state.errorMsg} </Text>
                                </TouchableOpacity>
                            </View>
                        </View>}
                    </View>
                }
            </View>
        );
    }

}

export default Wishlist;