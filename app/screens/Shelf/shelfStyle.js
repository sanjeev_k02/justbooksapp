import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        // marginTop: 10
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 60,
        // paddingTop: 20
    },
    headingView: {
        marginTop: 0,
        marginBottom: 0,
        marginRight: 10,
        marginLeft: -6,
        borderRadius: 10
    },
    heading: {
        color: '#fff',
        fontSize: 20,
        fontWeight: '400',
    },
    SeeAllHeading: {
        color: '#fff',
        fontSize: 14,
        fontWeight: '600',
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end'
    },
    titleStyle : {
        fontSize: 14, 
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end'
    },
    cardSliderView: { 
        display: "flex", 
        flexDirection: "row", 
        flexWrap: "wrap", 
        alignContent: "center", 
        alignItems: "center", 
        justifyContent: "center",
        marginLeft: 10,
        marginTop: 20
    },
    cardSliderSection: {
        width: 250,
        height: 140,
        margin: 10,
        // backgroundColor: '#101211'
    },
    coverSliderImage: {
        height: 140,
    },
    cardView: { 
        display: "flex", 
        flexDirection: "row", 
        flexWrap: "wrap", 
        alignContent: "center", 
        alignItems: "center", 
        justifyContent: "center",
        marginLeft: 10
    },
    cardSection: {
        width: 115,
        height: 180,
        marginLeft: 0,
        marginRight: 10,
        marginTop: 5,
        marginTop: 10,
        backgroundColor: '#FFFFFF'
    },
    sliderView: {
        marginTop: 35
    },
    coverImage: {
        width: 115,
        height: 140,
        resizeMode: 'stretch',
        borderRadius: 2
    },
    cardAuthorSection: {
        width: 115,
        height: 180,
        margin: 10,
        // backgroundColor: '#101211'
    },
    coverAuthorImage: {
        height: 140,
    },
    title: {
        width: '100%',
        fontSize: 15,
        lineHeight: 15,
        paddingTop: 5,
        marginBottom: 0,
        fontWeight: '600',
        color: '#273C96'
    },
    Paragraph: {
        marginTop: 0,
        width: '100%',
        fontSize: 12,
        color: '#273C96'
    },
    // newArrivles: {
    //     marginBottom: 5,
    // },
    // latestBook: {
    //     marginBottom: 15,
    // },
    divider: {
        backgroundColor: '#6f6c6c',
        marginTop: 10
    },
    NonImage: {
        position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        top: 0,
        alignItems:'center',
    },
    UnNonImage: {
        alignItems:'center',
    },
    NonImageCoverImage: {
        // position: 'absolute',
        // left: 0,
        // right: 0,
        // top: 0,
        // height: '100%',
        // width: 115,
        width: 115,
        height: 140,
        resizeMode: 'stretch',
        borderRadius: 2,
        marginLeft: 16
    },
    NonImageCardText: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 10,
        textAlign: 'center'
    },
    NonImageTitle: {
        width: 115,
        fontSize: 14,
        lineHeight: 14,
        marginTop: 10,
        paddingLeft: 5,
        paddingRight: 5,
        fontWeight: '600',
        color: '#CB8223',
        textAlign: 'center'
    },
    NonImageParagraph: {
        paddingTop: 2,
        height: 95,
        width: 115,
        fontSize: 14,
        color: '#CB8223',
        textAlign: 'center',
        lineHeight: 14
    },
    primusImage: {
      width: 110, 
      height: 40,
   },
   primusTag: {
      width: 100, 
      height: 40, 
      position: 'absolute', 
      bottom: 40,
      left: 0
   },
   author: {
       marginBottom: 80
   },
   newArrivles: {
        backgroundColor: '#FFFFFF',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 15
    },
    firstArrivles: {
         backgroundColor: '#FFFFFF',
         marginLeft: 10,
         marginRight: 10,
         marginBottom: 15,
         marginTop: 10,
     },
    lastNewArrivles : {
        backgroundColor: '#FFFFFF',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 80
    },
    headingStyle : {
        fontSize: 18, 
        fontWeight: '600',
        backgroundColor: '#FFFFFF',
        borderRadius: 10
    },
    headingStyleCover : {
        backgroundColor: '#FFFFFF',
        elevation:0, 
        margin: 0, 
        height: 32,
        borderRadius: 10
    },
    footer: {
        // position: 'absolute',
        // flex:1,
        left: 0,
        right: 0,
        bottom: 0,
        flexDirection:'row',
        alignItems:'center',
    },
    bottomButtons: {
      alignItems:'center',
    //   justifyContent: 'space-around',
      marginTop: 5,
      marginRight: 10
    },
    footerText: {
      color:'#2E2E2E',
      fontWeight:'600',
      alignItems:'center',
      fontSize:10,
    }, 
    imageNonContent : {
        marginLeft: 0,
        textAlign: 'left',
        backgroundColor: '#FFFFFF'
    },
    actionClose : {
      color:'#2E2E2E',
      margin: 15,
      textAlign: 'center'
    },

});
