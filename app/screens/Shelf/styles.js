import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 30
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },
    cardView: {
        backgroundColor: '#101211', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 15, 
        marginBottom: 0, 
        marginTop: 0,
    },
    cardSection: {
        width: 120,
        height: 225,
        margin: 10,
        // backgroundColor: '#101211'
    },
    coverImage: {
        height: 160,
    },
    card: {
        width: '90%',
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    imageCard: {
        width: 90, 
        height: 140,
        borderRadius: 4
    },
    cardDescription : {
        width: '80%',
        flexDirection: 'column',
        marginLeft: '6%', 
        marginTop: 5, 
        justifyContent: 'space-between'
    },
    titleDesciption: {
        marginTop: 5,
        marginRight: 30
    },
    color: {
        color: '#2E2E2E',
        lineHeight: 22
    },
    colorParagraph: {
        color: 'pink',
    },
    title: {
        width: '100%',
        color: '#273C96',
        fontSize: 16,
        lineHeight: 15,
        marginTop: 8,
        marginBottom: 0,
        fontWeight: '600'
    },
    Paragraph: {
        marginTop: 0,
        width: '100%',
        fontSize: 12,
        color: '#273C96'
    },
    divider : {
        backgroundColor: '#8F8E94', 
        marginTop: 15, 
        marginBottom: 15
    },
    actionStyle: {
        marginTop: 20,
    },
    footer: {
        // position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        bottom: 0,
        flexDirection:'row',
        height:60,
        alignItems:'center',
        elevation:25,
    },
    bottomButtons: {
      alignItems:'center',
      justifyContent: 'space-around',
      flex:1,
      marginTop:9,
      marginRight: 20
    },
    footerText: {
      color:'#2E2E2E',
      fontWeight:'600',
      alignItems:'center',
      fontSize:11,
      lineHeight:20,
      marginBottom:12
    },
    footerMessage: {
        position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        bottom: 5,
        flexDirection:'row',
        height:80,
        alignItems:'center',
        elevation:25,
    },
    subscriptionViewMessage: {
        position: "absolute",
        flex: 1,
        // top: (Dimensions.get('screen').height - 130),
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: "green",
        height: 80,
        alignItems: "center",
        elevation: 25,
    },
    subscriptionBottomButtons: {
        alignItems: "center",
        justifyContent: "space-around",
        flex: 1,
        width: "100%",
        marginTop: 9,
    },
    subscriptionFooterText: {
      // color:'#8F8E94',
      fontWeight: "600",
      alignItems: "center",
      textAlign: 'center',
      fontSize: 18,
      marginBottom: 12,
      marginLeft: 10,
      marginRight: 10,
      fontWeight: "600",
    },
    item: {
      paddingBottom: 5,
      marginVertical: 2,
      marginHorizontal: 6,
      marginLeft: 15
    },
    catContainer: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginRight: 20,
        alignItems: 'flex-start' // if you want to fill rows left to right
    },
    catLeftItem: {
        width: '90%' // is 50% of container width
    },
    catRightItem: {
        width: '10%', // is 50% of container width
    },
    headingSearchView: {
      marginTop: 0,
      marginBottom: 0,
      marginRight: 10,
      marginLeft: -10 ,
      padding: 15,
    },
    searchHeading: {
        color: '#fff',
        fontSize: 20,
        width: 250,
        fontWeight: '600',
        fontFamily: 'Montserrat-Bold'
    },
    headingSearchStyle : {
        fontSize: 20, 
        width: 250,
        fontWeight: '600',
        fontFamily: 'Montserrat-Bold'
    },
    titleStyle : {
        fontSize: 16, 
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end',
        color : '#CB8223'
    },
});
