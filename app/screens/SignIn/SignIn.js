import React from 'react';
import { View, Text, Image } from 'react-native';
import styles from './styles';
import { TextInput, Button, Paragraph, Headline, ActivityIndicator, Colors  } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

import { showMessage } from "react-native-flash-message";
import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import API from '../../env';
import * as axios from 'axios';

class SignIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            email: '',
            password: '',
            emailError: false,
            passwordError: false,
            successError: false,
            errorMsg: '',
            loginActivity: false
        }
    }

    _signInNavigate = async() => {
        if (this.state.email === '') {
            this.setState({ 
                emailError : true,
                errorMsg: 'Mobile/Email Required field!'
            })
            return;
        }

        if (this.state.password === '') {
            this.setState({ 
                passwordError : true,
                errorMsg: 'Password Required field!'
            })
            return;
        }
        this.setState({ 
            loginActivity : true,
        })
    
        this._verifyLoginRequest();
    }

    _verifyLoginRequest() {

        const validateOTP = API.API_JUSTBOOK_ENDPOINT + "/userSignin";
        axios.post(validateOTP ,{
            "username" : this.state.email,
            "password": this.state.password
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            // console.log("response login", response.data.successObject);
            if (response.data.status) {
                let loggedIn = {
                    "active": response.data.successObject.user.subscriptions[0].active,
                    "branchId": response.data.successObject.user.subscriptions[0].branchId,
                    "cityId": response.data.successObject.user.subscriptions[0].cityId,
                    "memberCard": response.data.successObject.user.subscriptions[0].memberCard,
                    "primus": response.data.successObject.user.subscriptions[0].primus,
                    "primusMappedBranches": response.data.successObject.user.subscriptions[0].primusMappedBranches,
                    "subscriptionId": response.data.successObject.user.subscriptions[0].subscriptionId
                }
                this._getSubscriptionDetails(response.data.successObject.user.subscriptions[0].subscriptionId, loggedIn)
            } else {
                this.setState({ 
                    loginActivity : false,
                    // successError : true,
                    // errorMsg: response.data.errorDescription
                })
                showMessage({
                    message: 'User Authentication',
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            this.setState({ 
                successError : true,
                errorMsg: 'Something went wrong, Please try again later!'
            })
            return;
        });
    }

    _getSubscriptionDetails = (subscriptionId, loggedIn) => {
        const getSubscription = API.API_JUSTBOOK_ENDPOINT+"/getSubscriptionForId?subscription_id="+subscriptionId;
        console.log(getSubscription);
        axios.get(getSubscription, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
          .then((response) => {
            // console.log(response.data.successObject);
              if (response.data.status) {
                  let successObject = Object.assign(response.data.successObject, {"login" : loggedIn});
                  this._storeData(successObject)
              } else {
                  console.log("subscription Id not found");
              }
          })
          .catch((error) => {
            console.log(error);
          });
      }
  
      _storeData = async (response) => {
          try {
              AsyncStorage.setItem('userData', JSON.stringify(response), () => {
                setTimeout(() => {
                    this.props.navigation.push("Home");
                }, 0);
              });
          } catch (error) {
              // Error saving data
              console.log('error msg', error);
          }
      };

    _OtpNavigate = async() => {
        this.props.navigation.navigate("OtpSignIn");
    }

    _signUpNavigate = async() => {
        this.props.navigation.navigate("SignUp");
    }

    _forgotPasswordNavigate = async() => {
        this.props.navigation.navigate("ForgotPassword");
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView>
                            <View style={{ marginBottom: 5 ,marginTop: 10}}>
                                <View style={styles.containerLogo}>
                                    <Image
                                        style={styles.logoImage}
                                        source={require('../../../assets/images/logo.jpg')}
                                    />
                                </View>
                                <View style={styles.containerImage}>
                                    <View style={styles.wrapper}>
                                        <Text style={styles.nameDesc}>
                                            <Headline style={styles.colorSignIn}> Sign In </Headline>{'\n'}
                                        </Text>
                                    </View>
                                    <Image
                                        style={styles.signInImage}
                                        source={require('../../assets/login.png')}
                                    />
                                </View>
                                <View style={styles.cardHomeView}>
                                    <View style={styles.form}>
                                        <TextInput
                                            value={this.state.email}
                                            style={styles.textInput}
                                            underlineColor={'#BEBEBE'}
                                            selectionColor={'#BEBEBE'}
                                            labelStyle={{color: '#BEBEBE'}}
                                            placeholderTextColor={'#BEBEBE'}
                                            placeholder={"Email or Mobile Number"}
                                            // label="Email or Mobile Number"
                                            theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                            onChangeText={text => this.setState({ emailError: false, email : text})}
                                        />
                                        {this.state.emailError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}

                                        <TextInput
                                            placeholder="Password"
                                            value={this.state.password}
                                            secureTextEntry={true}
                                            style={styles.textInput}
                                            underlineColor={'#BEBEBE'}
                                            selectionColor={'#BEBEBE'}
                                            labelStyle={{color: '#BEBEBE'}}
                                            placeholderTextColor={'#BEBEBE'}
                                            theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                            onChangeText={text => this.setState({ passwordError: false, password : text})}
                                        />
                                        {this.state.passwordError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                        <Text style={styles.forgot}>
                                            <TouchableOpacity onPress={()=>{this._forgotPasswordNavigate()}}>
                                                <Headline style={styles.forgotColor}> forgot password! </Headline>
                                            </TouchableOpacity>
                                        </Text>
                                    

                                        <View style={styles.wrapper}>
                                            {this.state.successError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                            <Text style={styles.buttonDesc}>{'\n'}
                                                { this.state.loginActivity ? <ActivityIndicator style={{flex : 1}} size={20} animating={true} color={Colors.blue700} />
                                                : <Button icon="login" mode="outline" labelStyle={{fontSize: 18, color: '#FFFFFF', textTransform: 'none'}} style={styles.btn} onPress={()=>{this._signInNavigate()}}>
                                                    Sign In
                                                </Button>}
                                                {'\n'}
                                                <Text style={styles.nameDesc}>
                                                    <Headline style={[styles.color,{fontSize: 14}]}>OR </Headline>{'\n'}
                                                </Text>
                                                <Button icon="phone" mode="outline" labelStyle={{fontSize: 18, color: '#FFFFFF', textTransform: 'none'}} style={styles.btn} onPress={()=>{this._OtpNavigate()}}>
                                                Login with OTP
                                                </Button>{'\n'}{'\n'}
                                            </Text>
                                        </View>
                                    </View>

                                    <View style={styles.wrapper}>
                                        <TouchableOpacity onPress={()=>{this._signUpNavigate()}}>
                                            <Text style={styles.nameDesc}>
                                                <Headline style={styles.color}>Not a member yet?  <Text style={{color: '#273C96', textDecorationLine : 'underline'}}>Sign up.</Text> </Headline>{'\n'}{'\n'}
                                            </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                }
            </View>
        );
    }

}

export default SignIn;