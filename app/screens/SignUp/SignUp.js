import React from 'react';
import { View, Text, StatusBar,Animated } from 'react-native';
import styles from './styles';
import { TextInput, Checkbox, Divider, RadioButton, Button, Card, ActivityIndicator, Colors  } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { Dropdown } from 'react-native-material-dropdown';
import AsyncStorage from '@react-native-community/async-storage';
import API from '../../env';
import * as axios from 'axios';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';

class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
            primus: false,
            PackageId: 0, 
            magazinesInpackageCount: 0,
            getSignupMetadataObj: {},
            booksInPackage: [], 
            termsInpackage: [],
            booksPackageCount: '',
            monthPacakgeCount: '',
            magazinesInpackage: [],
            payableAmount: 0,
            checked: true,
            successError: false,
            errorMsg: ''
        }
    }

    componentDidMount = async() => {
        await this._getSignupMetadataForWebAndMobile(this.state.primus);
        // setTimeout(() => {
        //     this.setState({ refreshing: false });
        // }, 2000);
    };

    _getSignupMetadataForWebAndMobile = async(primusValue) => {

       let getSignupMetadata = API.API_JUSTBOOK_ENDPOINT+"/getSignupMetadataForWebAndMobile?primus="+primusValue;
      console.log(getSignupMetadata);
      await axios.get(getSignupMetadata)
        .then((response) => {
          console.log(response.data.successObject);
            if (response.data.status) {
                const _BIP = response.data.successObject.webPackage.booksInPackage; // Added 'const' here
                var stringBooksInPackagArray = new Array();
                var booksInPackageArray = new Array();
                stringBooksInPackagArray = _BIP.replace(/ /g, "").split(","); // Added the 'replace' method
                for (var x = 0; x < stringBooksInPackagArray.length; x++) {
                    var obj = {};
                    obj['value'] = stringBooksInPackagArray[x]+" book(s) at a time";
                    obj['id'] = stringBooksInPackagArray[x];
                    booksInPackageArray.push(obj);
                };

                const _TIP = response.data.successObject.webPackage.termsInpackage; // Added 'const' here
                var stringTermsInpackageArray = new Array();
                var termsInpackageArray = new Array();
                stringTermsInpackageArray = _TIP.replace(/ /g, "").split(","); // Added the 'replace' method
                for (var x = 0; x < stringTermsInpackageArray.length; x++) {
                    var obj = {};
                    obj['value'] = stringTermsInpackageArray[x]+" months(s)";
                    obj['id'] = stringTermsInpackageArray[x];
                    termsInpackageArray.push(obj);
                };

                this.setState({
                    getSignupMetadataObj : response.data.successObject,
                    booksInPackage: booksInPackageArray,
                    termsInpackage: termsInpackageArray,
                    booksPackageCount: booksInPackageArray[0].id,
                    monthPacakgeCount: termsInpackageArray[0].id,
                    refreshing: false,
                })
                this._getSubscriptionCost(booksInPackageArray[0].id, termsInpackageArray[0].id);
            } else {
                console.log("No getSignupMetadataObj found");
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _getSubscriptionCost = async(booksCount, termsCount) => {

        const getSubscriptionCost = API.API_JUSTBOOK_ENDPOINT + "/getSubscriptionCost";
        await axios.post(getSubscriptionCost ,{
            "packageId":this.state.getSignupMetadataObj.webPackage.id,
            "noOfBooks":booksCount,
            "noOfTerms":termsCount,
            "noOfMagzines":this.state.magazinesInpackageCount,
            "branchId":this.state.getSignupMetadataObj.webBranchId,
            "primusMember":this.state.primus,
            "noOfDeliveries":0
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("booksCount", booksCount);
            console.log("termsCount", termsCount);
            if (response.data.status) {
                this.setState({
                    payableAmount: response.data.successObject.payableAmount
                })
            } else {
                this.setState({ 
                    successError : true,
                    errorMsg: response.data.errorDescription
                })
            }
        })
        .catch(function (error) {
            console.log(error);
            this.setState({ 
                successError : true,
                errorMsg: 'Something went wrong, Please try again later!'
            })
            return;
        });
    }


    _packagesChanges = async (packages) => {
        let packagesCount = packages.split(" ");
        this.setState({ booksPackageCount: packagesCount[0] });
        // await this._getSignupMetadataForWebAndMobile(this.state.primus);
        await this._getSubscriptionCost(packagesCount[0], this.state.monthPacakgeCount);
        console.log("payableAmount", this.state.payableAmount);                                   
    } 

    
    _monthsChanges = async (months) => {
        let monthsCount = months.split(" ");
        console.log(monthsCount);
        this.setState({ monthPacakgeCount: monthsCount[0]});
        // await this._getSignupMetadataForWebAndMobile(this.state.primus)
        await this._getSubscriptionCost(this.state.booksPackageCount, monthsCount[0]);;                                       
    } 


    _signUpNavigate = async() => {
        this.props.navigation.navigate("SignUp");
    }

    _signUpInfo = () => {
        let response = {
            "packageId":this.state.getSignupMetadataObj.webPackage.id,
            "noOfBooks":this.state.booksPackageCount,
            "noOfTerms":this.state.monthPacakgeCount,
            "noOfMagzines":this.state.magazinesInpackageCount,
            "branchId":this.state.getSignupMetadataObj.webBranchId,
            "primusMember":this.state.primus,
            "noOfDeliveries":0,
            "payableAmount": this.state.payableAmount
        };
        try {
            AsyncStorage.setItem('userSignupStorage', JSON.stringify(response), () => {
              setTimeout(() => {
                  this.props.navigation.navigate("SignUpInfo");
              }, 0);
            });
        } catch (error) {
            // Error saving data
            console.log('error msg', error);
        }
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { (this.state.refreshing) ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView>
                            <View style={{ marginBottom: 5 ,marginTop: 0}}>
                                <View style={styles.cardHomeView}>
                                    <View style={styles.form}>
                                        <View style={styles.container}>
                                            <View style={styles.item}>
                                                <Card style={styles.cardDesign}>
                                                    <Text style={styles.titleDesciption}>
                                                        <Text style={styles.headlineTop}>Delivery</Text>{'\n'}
                                                        <Text style={styles.headline}>within 3 Hours</Text>
                                                    </Text>
                                                </Card>
                                            </View>
                                            <View style={styles.item}>
                                                <Card style={styles.cardDesign}>
                                                    <Text style={styles.titleDesciption}>
                                                        <Text style={styles.headlineTop}>Magnize</Text>{'\n'}
                                                        <Text style={styles.headline}>delivery and pickup</Text>
                                                    </Text>
                                                </Card>
                                            </View>
                                            <View style={styles.item}>
                                                <Card style={styles.cardDesign}>
                                                    <Text style={styles.titleDesciption}>
                                                        <Text style={styles.headlineTop}>Reverse New</Text>{'\n'}
                                                        <Text style={styles.headline}>order before launch</Text>
                                                    </Text>
                                                </Card>
                                            </View>
                                            <View style={styles.item}>
                                                <Card style={styles.cardDesign}>
                                                    <Text style={styles.titleDesciption}>
                                                        <Text style={styles.headlineTop}>3 days of access</Text>{'\n'}
                                                        <Text style={styles.headline}>and all new books</Text>
                                                    </Text>
                                                </Card>
                                            </View>
                                        </View>
                                        <View style={styles.wrapper}>
                                            <Text style={styles.nameDesc}>
                                                <Text style={styles.color}> - More you read the less you paid - </Text>
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.form}>
                                        <Dropdown
                                            label='Packages'
                                            containerStyle={{width: 200}}
                                            data={this.state.booksInPackage}
                                            onChangeText={(text) =>this._packagesChanges(text)}
                                            value={this.state.booksInPackage[0].value}
                                        />

                                        <Dropdown
                                            label='Duration'
                                            containerStyle={{width: 200}}
                                            data={this.state.termsInpackage}
                                            onChangeText={(text) =>this._monthsChanges(text)}
                                            value={this.state.termsInpackage[0].value}
                                        />

                                        <Divider style={styles.divider} />

                                        <Checkbox
                                            status={this.state.primus ? 'checked' : 'unchecked'}
                                            uncheckedColor={'#2E2E2E'}
                                            onPress={() => {
                                                this.setState({primus :  !this.state.primus, refreshing: true});
                                                this._getSignupMetadataForWebAndMobile(!this.state.primus);
                                            }}
                                        /><Text style={styles.primuscolor}> I want Primus </Text>


                                        <View style={styles.wrapper}>
                                            <Text style={styles.nameDesc}>
                                                <Text style={styles.color}> Rs. {this.state.payableAmount} /- </Text>
                                            </Text>
                                        </View>
                                    
                                        <View style={styles.wrapper}>
                                            <Text style={styles.buttonDesc}>
                                                <Button icon="login" mode="outline" labelStyle={{fontSize: 18, color: '#2E2E2E', textTransform: 'none'}} style={styles.btn} onPress={() => this._signUpInfo()}>
                                                    Sign Up
                                                </Button>
                                            </Text>
                                        </View>
                                            <Text style={styles.nameDesc}>
                                                <Text style={styles.chargescolor}> +Includes One Time Security Deposit of Rs.598/- </Text>{'\n'}
                                            </Text>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </View>
                }
            </View>
        );
    }

}

export default SignUp;