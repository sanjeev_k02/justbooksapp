import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF',
        marginLeft: 10, 
        marginRight: 10, 
        paddingLeft: 15, 
        paddingRight: 15,
    },
    divider : {
        margin: 10,
    },
    cardStyle : {
        backgroundColor: '#39403c', 
        borderRadius: 6
    },
    titleStyle: {
        color : '#2E2E2E'
    },
    subtitleStyle: {
        color : '#2E2E2E'
    },
    rightStyle : {
        backgroundColor: '#2E2E2E', 
        borderBottomRightRadius: 6, 
        borderTopRightRadius: 6, 
        height: 70, 
        paddingTop: 8
    },
    textInput: {
        height: 40,
        marginTop: 8,
        marginBottom: 8,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 5,
        width: 200,
        backgroundColor: '#fff'
    },
    wrapper: {
        alignItems: "center",
        marginTop: 0,
        marginBottom: 0
    },
    buttonDesc : {
        alignItems: "center",
        textAlign: 'center'
    },
    form: {
        // backgroundColor: '#fff',
        marginTop: 0,
        alignItems: 'center'
    },
    btn: {
        height: 48, 
        textAlign: 'center', 
        margin: 20,
        fontSize: 18,
        borderColor :'#2E2E2E',
        borderWidth : 1,
        width: '100%',
        color: '#2E2E2E',
        borderRadius: 50
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start' // if you want to fill rows left to right
    },
    item: {
        width: '48%', // is 50% of container width
        margin: 2,
        height: undefined
    },
    color: {
        color: '#2E2E2E',
        fontSize: 20,
        textAlign: 'center'
    },
    chargescolor: {
        color: '#2E2E2E',
        fontSize: 14,
        textAlign: 'center'
    },
    primuscolor: {
        color: '#2E2E2E',
        fontSize: 18,
        textAlign: 'center'
    },
    colorParagraph: {
        color: 'pink',
    },
    nameDesc: {
        marginTop: 20,
        marginBottom: 20
    },
    titleDesciption : {
        textAlign: 'center',
        margin: 10,
        height: 60
    },
    headline: {
        color: '#000',
        fontSize: 16,
        textAlign: 'center'
    },
    headlineTop: {
        color: 'red',
        fontSize: 16,
        textAlign: 'center',
    },
    cardDesign: {
        backgroundColor: '#FFFFFF'
    }

});
