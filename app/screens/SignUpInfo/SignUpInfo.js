import React from 'react';
import { View, Text, StatusBar } from 'react-native';
import styles from './styles';
import { TextInput, Appbar, Divider, RadioButton, Button, Card, Title, Paragraph, Headline, ActivityIndicator, Colors  } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import AsyncStorage from '@react-native-community/async-storage';
import { Dropdown } from 'react-native-material-dropdown';
import { showMessage, hideMessage } from "react-native-flash-message";
import RazorpayCheckout from 'react-native-razorpay';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import API from '../../env';
import * as axios from 'axios';

class SignUpInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
            genderData: [
                { id:'M', value: 'Male' }, 
                { id:'F', value: 'Female'}
             ],
            userId: 0,
            firstName: '',
            lastName: '',
            email: '',
            mobile: '',
            dob: '',
            address: '',
            gender: '',
            pincode: '',
            stateId: '',
            cityId: '',
            firstNameError: false,
            lastNameError: false,
            emailError: false,
            mobileError: false,
            dobError: false,
            addressError: false,
            pincodeError: false,
            stateIdError: false,
            cityIdError: false,
            errorMsg: '',
            successError: false,
            packageId:0,
            noOfBooks:'',
            noOfTerms:'',
            noOfMagzines:'',
            branchId:'',
            isPrimusAvailed:'',
            noOfDeliveries:'',
            payableAmount: 'userSignupStorage.payableAmount'
        }
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }

    componentDidMount = async() => {
        try {
            const signup = await AsyncStorage.getItem('userSignupStorage');
            console.log("signup",signup);
            if (signup !== null) {
                const userSignupStorage = JSON.parse(signup);
                this.setState({
                    packageId:userSignupStorage.packageId,
                    noOfBooks:userSignupStorage.noOfBooks,
                    noOfTerms:userSignupStorage.noOfTerms,
                    noOfMagzines:userSignupStorage.noOfMagzines,
                    branchId:userSignupStorage.branchId,
                    isPrimusAvailed:userSignupStorage.primus,
                    noOfDeliveries:userSignupStorage.noOfDeliveries,
                    payableAmount: userSignupStorage.payableAmount
                })
            } else {
                console.log("userSignupStorage Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        setTimeout(() => {
            this.setState({ refreshing: false });
        }, 1000);
    };

    _updateProfile = async() => {
        if (this.state.firstName === '') {
            this.setState({ 
                firstNameError : true,
                errorMsg: 'First Name Required field!'
            })
            return;
        }

        if (this.state.lastName === '') {
            this.setState({ 
                lastNameError : true,
                errorMsg: 'Last Name Required field!'
            })
            return;
        }

        if (this.state.mobile === '') {
            this.setState({ 
                mobileError : true,
                errorMsg: 'Mobile Required field!'
            })
            return;
        }

        if (this.state.email === '') {
            this.setState({ 
                emailError : true,
                errorMsg: 'Email Required field!'
            })
            return;
        }

        if (this.state.dob === '') {
            this.setState({ 
                dobError : true,
                errorMsg: 'Date of birth Required field!'
            })
            return;
        }

        if (this.state.address === '') {
            this.setState({ 
                addressError : true,
                errorMsg: 'Address Required field!'
            })
            return;
        }

        if (this.state.pincode === '') {
            this.setState({ 
                pincodeError : true,
                errorMsg: 'Pincode Required field!'
            })
            return;
        }

        if (this.state.stateId === '') {
            this.setState({ 
                stateIdError : true,
                errorMsg: 'State Required field!'
            })
            return;
        }

        if (this.state.cityId === '') {
            this.setState({ 
                cityIdError : true,
                errorMsg: 'City Required field!'
            })
            return;
        }
    
        this._registerNewRequest();
    }

    _registerNewRequest() {

        const registerRequest = API.API_JUSTBOOK_ENDPOINT + "/insertNewSignup";
        axios.post(registerRequest ,{
            "name" : this.state.firstName,
            "pincode" : this.state.pincode,
            "email" : this.state.email,
            "mobile" : this.state.mobile,
            "urlRefarral":'app',
            "primus":this.state.isPrimusAvailed
            }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response insert Register", response.data);
            if (response.data.status) {
                this.setState({ 
                    refreshing: false,
                    successError : true
                });
                this._initPayNowMethod();
            } else {
                showMessage({
                    message: "Signup",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
                // this.setState({ 
                //     successError : true,
                //     errorMsg: response.data.errorDescription
                // })
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Signup",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
            // this.setState({ 
            //     successError : true,
            //     errorMsg: 'Something went wrong, Please try again later!'
            // })
            // return;
        });
    }

    _initPayNowMethod = () => {
        console.log("pay Now");

        var transactionTypeId = 1;
        var vendorId = 2;
        var urlReferrer = 'app';

        const payNowRequest = API.API_JUSTBOOK_ENDPOINT + "/processPayment";
        axios.post(payNowRequest ,{
            "createdBy": "1000",
            "createdAt": "810",
            "vendorId": vendorId,
            "transactionTypeId": transactionTypeId,
            "amount": this.state.payableAmount,
            "urlReferrer": urlReferrer,
            "subscriptionDTO":{
                "user": {
                    "email": this.state.email,
                    "mobile": this.state.mobile,
                    "userInfo": {
                        "firstname": this.state.firstName,
                        "secName" : this.state.lastName,
                        "sourceId": 4,
                        "pincode": this.state.pincode,
                        "address" : this.state.address,
                        "dob" : this.state.dob,
                        "gender" : this.state.gender
                    }
                },
                "numBooks": this.state.noOfBooks,
                "numMagazine": this.state.noOfMagzines,
                "noOfTerms": this.state.noOfTerms,
                "branchId": "810",
                "packageId": this.state.packageId,
                "couponsCsv": '',
                "noOfDoorDeliveries": this.state.noOfDeliveries,
                "primusMember" : this.state.isPrimusAvailed
            },
            "payments":[]
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response paynow", response.data);
            if (response.data.status) {
                if(response.data.successObject.isTransactionCompleted === false){
                    var vendorTransactionId = response.data.successObject.vendorTransactionId;
                    this._initPaymentMethod(vendorTransactionId);
                }else if(response.data.successObject.isTransactionCompleted === true){
                    showMessage({
                        message: "Signup",
                        description: 'Transaction completed!',
                        icon: "danger",
                        type: "danger",
                    });
                }
            } else {
                showMessage({
                    message: "Signup",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Signup",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _convertToPaise(amount) {
        return amount * 100;
    }

    _initPaymentMethod = (orderId) => {
        console.log("payment");
        const initiatePayment = API.API_JUSTBOOK_ENDPOINT + "/updateOnlineTransaction";
        var options = {
            description: 'JustBooks',
            image: 'https://justbooks.in/assets/img/logo.svg',
            currency: 'INR',
            key: API.RZP_API_KEY, // Your api key
            amount: this._convertToPaise(this.state.payableAmount),
            name: 'Justbook Subscription',
            description: 'connect with to read,sell and buy book',
            order_id: orderId,
            prefill: {
                email: this.state.email,
                contact: this.state.mobile,
                name: this.state.firstName
            },
            theme: {color: '#273C96'}
        }
        RazorpayCheckout.open(options).then((data) => {
            axios.post(initiatePayment ,{
                'currentTransactionPaymentId':'3',
                'vendorId': '2',//for razorpay
                'vendorTransactionId': orderId,
                "razorpay_payment_id": data.razorpay_payment_id,
                "razorpay_signature": data.razorpay_signature,
                "razorpay_order_id": data.razorpay_order_id
            }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
            .then(async (responsePayment) => {
                consolelog(responsePayment);
                if (responsePayment.data.status) {
                    showMessage({
                        message: "Signup",
                        description: 'Payment successfully Completed!',
                        icon: "success",
                        type: "success",
                    });
                      setTimeout(() => {
                        this.props.navigation.navigate('SignIn');
                    },2000);
                } else {
                    showMessage({
                        message: "Signup",
                        description: responsePayment.data.errorDescription,
                        icon: "danger",
                        type: "danger",
                    });
                }
            })
            .catch(function (error) {
                console.log(error);
                showMessage({
                    message: "Signup",
                    description: 'Something went wrong, Please try again later!',
                    icon: "danger",
                    type: "danger",
                });
            });

        }).catch((error) => {
            // handle failure
                console.log(error);
            showMessage({
                message: "",
                description: 'Your payment was cancelled, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    :
                    <View style={styles.activityClass}>
                        <View style={styles.mainContainer}>
                            <ScrollView>
                                <View style={{ marginBottom: 5 ,marginTop: 10}}>
                                    <View style={styles.cardHomeView}>
                                        {/* <Text style={styles.titleDesciption}>
                                            <Headline style={styles.titleStyle}>User Details</Headline>{'\n'}
                                            <Text style={{color: 'green', fontSize: 16 }}>
                                                <Paragraph style={styles.subtitleStyle}>Enter your personal details to ensure account</Paragraph>
                                            </Text>{'\n'}
                                        </Text> */}
                                        <View style={styles.form}>

                                            <View style={styles.container}>
                                                <View style={styles.item}>
                                                    <TextInput
                                                        placeholder="First Name"
                                                        value={this.state.firstName}
                                                        style={styles.textInput}
                                                        underlineColor={'#BEBEBE'}
                                                        selectionColor={'#BEBEBE'}
                                                        labelStyle={{color: '#BEBEBE'}}
                                                        placeholderTextColor={'#BEBEBE'}
                                                        theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                        onChangeText={text => this.setState({firstNameError: false, firstName : text})}
                                                    />
                                                    {this.state.firstNameError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                                </View>
                                                <View style={styles.item}>
                                                    <TextInput
                                                        placeholder="Last Name"
                                                        value={this.state.lastName}
                                                        style={styles.textInput}
                                                        underlineColor={'#BEBEBE'}
                                                        selectionColor={'#BEBEBE'}
                                                        labelStyle={{color: '#BEBEBE'}}
                                                        placeholderTextColor={'#BEBEBE'}
                                                        theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                        onChangeText={text => this.setState({lastNameError: false, lastName : text})}
                                                    />
                                                    {this.state.lastNameError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                                </View>
                                            </View>

                                            <TextInput
                                                placeholder="Enter Mobile"
                                                keyboardType={"numeric"}
                                                value={this.state.mobile}
                                                style={styles.textInput}
                                                underlineColor={'#BEBEBE'}
                                                selectionColor={'#BEBEBE'}
                                                labelStyle={{color: '#BEBEBE'}}
                                                placeholderTextColor={'#BEBEBE'}
                                                theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                onChangeText={text => this.setState({mobileError: false, mobile : text})}
                                            />
                                            {this.state.mobileError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}

                                            <TextInput
                                                placeholder="Email"
                                                value={this.state.email}
                                                style={styles.textInput}
                                                underlineColor={'#BEBEBE'}
                                                selectionColor={'#BEBEBE'}
                                                labelStyle={{color: '#BEBEBE'}}
                                                placeholderTextColor={'#BEBEBE'}
                                                theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                onChangeText={text => this.setState({mobileError: false, email : text})}
                                            />
                                            {this.state.emailError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                            
                                            {/* <View style={styles.container}>
                                                <View style={styles.item}>
                                                    <RadioButton
                                                        label="Male"
                                                        value="male"
                                                        status={ this.state.gender === 'male' ? 'checked' : 'unchecked' }
                                                />
                                                </View>
                                                <View style={styles.item}>
                                                    <RadioButton
                                                        value="female"
                                                        status={ this.state.gender === 'female' ? 'checked' : 'unchecked' }
                                                /> 
                                                </View>
                                            </View> */}

                                            {/* <TextInput
                                                label="Date of Birth"
                                                value={this.state.dob}
                                                style={styles.textInput}
                                                onChangeText={text => this.setState({name : text})}
                                            /> */}

                                            <DatePicker
                                                style={{width: '100%',marginBottom: 10}}
                                                mode="date"
                                                date={this.state.dob}
                                                placeholder="Date of Birth"
                                                format="YYYY-MM-DD"
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                customStyles={{
                                                    dateIcon: {
                                                        position: 'absolute',
                                                        right: 0,
                                                        top: 4
                                                    },
                                                    dateInput: {
                                                        position: 'absolute',
                                                        left: 0, 
                                                        right: 10,
                                                        height: 50,
                                                        paddingTop: 10,
                                                        marginBottom: 0,
                                                        marginLeft: 10,
                                                        paddingLeft: 10,
                                                        backgroundColor: '#FFFFFF',
                                                        borderBottomColor: '#BEBEBE',
                                                        borderBottomWidth: 0.5,
                                                        borderLeftWidth: 0,
                                                        borderRightWidth: 0,
                                                        borderTopWidth: 0,
                                                        textAlign: 'left',
                                                        alignItems: 'flex-start'
                                                    }
                                                    // ... You can check the source to find the other keys.
                                                }}
                                                onDateChange={(date) => {this.setState({dobError: false, dob: date})}}
                                            />
                                            {this.state.dobError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                            
                                            <TextInput
                                                placeholder="Address"
                                                multiline={true}
                                                value={this.state.address}
                                                style={styles.addressInput}
                                                underlineColor={'#BEBEBE'}
                                                selectionColor={'#BEBEBE'}
                                                labelStyle={{color: '#BEBEBE'}}
                                                placeholderTextColor={'#BEBEBE'}
                                                theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                onChangeText={text => this.setState({addressError: false, address : text})}
                                            />
                                            {this.state.addressError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}

                                            <View style={styles.container}>
                                                <View style={styles.item}>
                                                    <Dropdown
                                                        label='Gender'
                                                        containerStyle={styles.textGenderInput}
                                                        data={this.state.genderData}
                                                        onChangeText={text =>
                                                            this.setState({ genderError: false, gender: text })
                                                        }
                                                        value={this.state.genderData[0].value}
                                                    />
                                                    {this.state.genderError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                                </View>
                                                <View style={styles.item}>
                                                    <TextInput
                                                        placeholder="Pincode"
                                                        value={this.state.pincode}
                                                        style={styles.textInput}
                                                        underlineColor={'#BEBEBE'}
                                                        selectionColor={'#BEBEBE'}
                                                        labelStyle={{color: '#BEBEBE'}}
                                                        placeholderTextColor={'#BEBEBE'}
                                                        theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                        onChangeText={text => this.setState({pincodeError: false, pincode : text})}
                                                    />
                                                    {this.state.pincodeError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                                </View>
                                            </View>
                                            
                                            <View style={styles.container}>
                                                <View style={styles.item}>
                                                    <TextInput
                                                        placeholder="State"
                                                        value={this.state.stateId}
                                                        style={styles.textInput}
                                                        underlineColor={'#BEBEBE'}
                                                        selectionColor={'#BEBEBE'}
                                                        labelStyle={{color: '#BEBEBE'}}
                                                        placeholderTextColor={'#BEBEBE'}
                                                        theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                        onChangeText={text => this.setState({stateIdError: false, stateId : text})}
                                                    />
                                                    {this.state.stateIdError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                                </View>
                                                <View style={styles.item}>
                                                    <TextInput
                                                        placeholder="City"
                                                        value={this.state.cityId}
                                                        style={styles.textInput}
                                                        underlineColor={'#BEBEBE'}
                                                        selectionColor={'#BEBEBE'}
                                                        labelStyle={{color: '#BEBEBE'}}
                                                        placeholderTextColor={'#BEBEBE'}
                                                        theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                                        onChangeText={text => this.setState({cityIdError: false, cityId : text})}
                                                    />
                                                    {this.state.cityIdError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}
                                                </View>
                                            </View>
                                        

                                            <Button icon="check" mode="contained" labelStyle={{fontSize: 18,color: '#FFFFFF'}} style={styles.btn} onPress={()=>{this._updateProfile()}}>
                                                Next
                                            </Button>
                                            {this.state.successError && <Paragraph style={styles.successError}> {this.state.errorMsg} </Paragraph>}
                                        </View>
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                }
            </View>
        );
    }

}

export default SignUpInfo;