import React from 'react';
import { View, Text, Image, StatusBar, BackHandler, TouchableHighlight,  TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from './styles';
import SwiperFlatList from 'react-native-swiper-flatlist';

class Splash1 extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount = async() => {
      BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    }

    componentWillUnmount() {
      BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    }

    backPressed = () => {
        let routeName = this.props.route.name;
        console.log(this.props.navigation.isFocused())
        if (routeName == "IntroSlider" && this.props.navigation.isFocused()) {
            this.props.navigation.navigate("Splash");
        }
    };

    _splash1NavigateNext = async() => {
        this.props.navigation.push("Splash2");
    }

    _splash1NavigatePrev = async() => {
        console.log("Splash1");
        this.props.navigation.push("Splash1");
    }

    _splashSkipNavigate = async() => {
        console.log("Home");
        this.props.navigation.push("Home");
    }

    render() {
        return (
        <View style={styles.container}>
            <StatusBar   
                backgroundColor = "#FFFFFF" 
            />
            <SwiperFlatList
                ref='swiper'
                autoplayDelay={10}
                autoplayLoop
                index={0}
                showPagination
                paginationDefaultColor={'#EFEFEF'}
                paginationActiveColor={'#5A5A5A'}
                paginationStyleItem={{ width:8, height:8, borderRadius:5, borderColor: '#707070', borderWidth: 1 }}
            >
                <View style={[styles.child, { backgroundColor: '#FFFFFF' }]}>
                    <View style={{ width: "100%", height: "100%", backgroundColor: '#FFFFFF'}}>
                        <View style={{ flexDirection: 'row', alignItems: 'space-between', position: 'absolute', top: 0}}>
                            <TouchableOpacity
                                onPress={() => {console.log("first Splash")}}
                                style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                                <Text style={styles.actionClose}></Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {this._splashSkipNavigate()}}
                                style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                                <Text style={styles.actionUpload}>SKIP <Icon name="chevron-right" style={styles.icon} size={14} color="#000000" onPress={()=>{this._splashSkipNavigate()}} />
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.containerImage}>
                            <Image
                                style={styles.splashImage}
                                source={require('../../assets/Splash1.png')}
                            />
                        </View>
                    </View>
                </View>
                <View style={[styles.child, { backgroundColor: '#FFFFFF' }]}>
                    <View style={{ width: "100%", height: "100%", backgroundColor: '#FFFFFF'}}>
                        <View style={{ flexDirection: 'row', alignItems: 'space-between', position: 'absolute', top: 0}}>
                            <TouchableOpacity
                                onPress={() => {this.refs.swiper.goToFirstIndex()}}
                                style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                                <Text style={styles.actionClose}><Icon name="chevron-left" style={styles.icon} size={14} color="#000000" onPress={()=>{this.refs.swiper.goToFirstIndex()}} /> PREVIOUS</Text>
                            </TouchableOpacity>
                            <TouchableHighlight
                                onPress={() => {this._splashSkipNavigate()}}
                                style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                                <Text style={styles.actionUpload}>SKIP <Icon name="chevron-right" style={styles.icon} size={14} color="#000000" onPress={()=>{this._splashSkipNavigate()}} /></Text>
                            </TouchableHighlight>
                        </View>
                        <View style={styles.containerImage}>
                            <Image
                                style={styles.splashImage}
                                source={require('../../assets/Splash2.png')}
                            />
                        </View>
                    </View>
                </View>
            </SwiperFlatList>
        </View>
        );
    }

}

export default Splash1;