import React from 'react';
import { View, Text, Image, StatusBar, ImageBackground, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from './styles';

class Splash extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount (){
        setTimeout(() => {
            this.props.navigation.navigate("IntroSlider");
        }, 1500)
    }

    _splashNavigate = async() => {
        this.props.navigation.navigate("IntroSlider");
    }

    render() {
        return (
            <View style={{ display: 'flex', flex: 1 }}>
            <StatusBar backgroundColor = "#FFFFFF" />
            <View style={styles.logoAlign}>
                   <View style={styles.containerImage}>
                        <Image
                            style={styles.logoImage}
                            source={require('../../../assets/images/logo.jpg')}
                        />
                    </View>
                    {/* <Icon name="chevron-circle-right" style={styles.icon} size={18} color="#000000" onPress={()=>{this._splashNavigate()}} />
                    <TouchableOpacity style={styles.iconText} onPress={()=>{this._splashNavigate()}} >
                        <Text style={{textAlign: 'center',color: "#000000"}}>next</Text>
                    </TouchableOpacity> */}
            </View>
        </View>
        );
    }

}

export default Splash;