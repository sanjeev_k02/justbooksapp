import React from 'react';
import { View, Text, Image, StatusBar, ImageBackground, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from './styles';

class Splash1 extends React.Component {
    constructor(props) {
        super(props);
    }

    _splash1NavigatePrev = async() => {
        this.props.navigation.push("Home");
    }

    _splash1NavigateNext = async() => {
        this.props.navigation.push("Splash2");
    }

    render() {
        return (
            <View style={{ display: 'flex', flex: 1 }}>
            <StatusBar   
                backgroundColor = "#FFFFFF" 
            />
            <View style={{ width: "100%", height: "100%", backgroundColor: '#FFFFFF'}}>
                <View style={{ flexDirection: 'row', alignItems: 'space-between', position: 'absolute', top: 0}}>
                    <TouchableOpacity
                        onPress={() => {this._splash1NavigatePrev()}}
                        style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                        <Text style={styles.actionClose}></Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {this._splash1NavigatePrev()}}
                        style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                        <Text style={styles.actionUpload}>SKIP <Icon name="chevron-right" style={styles.icon} size={14} color="#000000" onPress={()=>{this._splashNavigate()}} />
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.containerImage}>
                    <Image
                        style={styles.splashImage}
                        source={require('../../assets/Splash1.png')}
                    />
                </View>
            </View>
        </View>
        );
    }

}

export default Splash1;