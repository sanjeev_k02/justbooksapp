import React from 'react';
import { View, Text, Image, StatusBar, ImageBackground, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from './styles';

class Splash2 extends React.Component {
    constructor(props) {
        super(props);
    }

    _splash1NavigatePrev = async() => {
        console.log("Splash1");
        this.props.navigation.push("Splash1");
    }

    _splashSkipNavigate = async() => {
        console.log("Home");
        this.props.navigation.push("Home");
    }

    render() {
        return (
            <View style={{ display: 'flex', flex: 1 }}>
            <StatusBar   
                backgroundColor = "#FFFFFF" 
            />
            <View style={{ width: "100%", height: "100%", backgroundColor: '#FFFFFF'}}>
                <View style={{ flexDirection: 'row', alignItems: 'space-between', position: 'absolute', top: 0}}>
                    <TouchableHighlight
                        onPress={() => {this._splash1NavigatePrev()}}
                        style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                        <Text style={styles.actionClose}><Icon name="chevron-left" style={styles.icon} size={14} color="#000000" onPress={()=>{this._splash1NavigatePrev()}} /> PREVIOUS</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        onPress={() => {this._splashSkipNavigate()}}
                        style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                        <Text style={styles.actionUpload}>SKIP <Icon name="chevron-right" style={styles.icon} size={14} color="#000000" onPress={()=>{this._splashSkipNavigate()}} /></Text>
                    </TouchableHighlight>
                </View>
                <View style={styles.containerImage}>
                    <Image
                        style={styles.splashImage}
                        source={require('../../assets/Splash2.png')}
                    />
                </View>
            </View>
        </View>
        );
    }

}

export default Splash2;