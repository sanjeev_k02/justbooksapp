import { StyleSheet, Dimensions } from "react-native";
export const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
	contain: {
    display: 'flex',
    flex: 1
  },
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  image: {
    flexGrow:1,
    height:null,
    width:null,
    alignItems: 'center',
    justifyContent:'center',
  },
  paragraph: {
    textAlign: 'center',
  },
  text: {
    textAlign: 'center',
    margin: 50,
    fontSize: 15
  },
  icon: {
    textAlign: 'center',
    marginTop: Dimensions.get('window').height / 2 - 80,
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
    // marginTop: 15
  },
  logoAlign: { 
    width: "100%", 
    height: "100%", 
    backgroundColor: '#FFFFFF',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoImage: {
    width: 250,
    height: 250,
    marginTop: 0 //Dimensions.get('window').height / 4,
  },
  splashImage: {
    width: '100%',
    height: '98%',
    marginTop: 50
  },
  actionClose : {
    color:'#000000',
    marginTop: 10,
    marginLeft: 15,
    textAlign: 'left'
  },
  actionUpload : {
    color:'#000000',
    marginTop: 10,
    marginRight: 15,
    textAlign: 'right'
  },
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  child: {
    height: '100%',
    width,
    justifyContent: 'center'
  },
  text: {
    fontSize: width * 0.5,
    textAlign: 'center'
  }
});
