import React from 'react';
import { View, Text, StatusBar, Image,Alert } from 'react-native';
import styles from './styles';
import { Avatar, DataTable, Checkbox, Divider, Button, Card, Title, Paragraph, Headline, ActivityIndicator, Colors } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Moment from 'moment';
import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import { Dropdown } from 'react-native-material-dropdown';
import { showMessage } from "react-native-flash-message";
import API from '../../env';
import * as axios from 'axios';
import RazorpayCheckout from 'react-native-razorpay';

class Subscription extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subscription_id: 0,
            memberCard: 0,
            branchId: 0,
            cityId: 0,
            primusMappedBranches: 0,
            primusMember: false,
            page: 1,
            refreshing: true,
            renewBtn: true,
            subscriptionDetails: {},
            bookscount: '',
            monthcount: '', 
            booksData: [
               { value: '2 book(s) at a time' }, 
               { value: '4 book(s) at a time'},
               { value: '6 book(s) at a time' }, 
               { value: '8 book(s) at a time'}
            ], 
            monthsData: [
               { value: '3 month(s) ' }, 
               { value: '6 month(s) '},
               { value: '9 month(s) ' }, 
               { value: '12 month(s) '}
            ],
            isPrimus: true,
            nonPrimus: false,
            primus: false,
            PackageId: 0, 
            magazinesInpackageCount: 0,
            getSignupMetadataObj: {},
            booksInPackage: [], 
            termsInpackage: [],
            booksPackageCount: '',
            monthPacakgeCount: '',
            magazinesInpackage: [],
            payableAmount: 0,
            checked: true,
        }
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
              const userData = JSON.parse(user);
              this.setState({
                  refreshing: false,
                  subscriptionDetails: userData,
                  subscription_id: userData.login.subscriptionId,
                  memberCard: userData.login.memberCard,
                  branchId: userData.login.branchId,
                  primusMappedBranches: userData.login.primusMappedBranches,
                  cityId: userData.login.cityId,
                  primusMember: userData.login.primus
                })
            } else {
                this.setState({
                    refreshing: false,
                })
                console.log("subscription Not Found");
            }
          } catch (error) {
            console.log("error", error)
        }
    };

    _getRenewalMetadataForWebAndMobile = async(subscriptionId, primusValue) => {

       let getSignupMetadata = API.API_JUSTBOOK_ENDPOINT+"/getRenewalMetaData?subscriptionid="+subscriptionId+"&transactionBranchid=810&primus="+primusValue;
      console.log(getSignupMetadata);
      await axios.get(getSignupMetadata,{ headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status) {
                const _BIP = response.data.successObject.currentPackage.specificBookNumbersCSV; // Added
                var stringBooksInPackagArray = new Array();
                var booksInPackageArray = new Array();
                stringBooksInPackagArray = _BIP.replace(/ /g, "").split(","); // Added the 'replace' method
                for (var x = 0; x < stringBooksInPackagArray.length; x++) {
                    var obj = {};
                    obj['value'] = stringBooksInPackagArray[x]+" book(s) at a time";
                    obj['id'] = stringBooksInPackagArray[x];
                    booksInPackageArray.push(obj);
                };

                const _TIP = response.data.successObject.currentPackage.specificMonthsCSV; // Added
                var stringTermsInpackageArray = new Array();
                var termsInpackageArray = new Array();
                stringTermsInpackageArray = _TIP.replace(/ /g, "").split(","); // Added the 'replace' method
                for (var x = 0; x < stringTermsInpackageArray.length; x++) {
                    var obj = {};
                    obj['value'] = stringTermsInpackageArray[x]+" month(s)";
                    obj['id'] = stringTermsInpackageArray[x];
                    termsInpackageArray.push(obj);
                };

                const _MIP = response.data.successObject.currentPackage.specificMagazineCSV; // Added
                var stringMagazineInPackagArray = new Array();
                var magazineInPackageArray = new Array();
                stringMagazineInPackagArray = _MIP.replace(/ /g, "").split(","); // Added the 'replace' method
                for (var x = 0; x < stringMagazineInPackagArray.length; x++) {
                    var obj = {};
                    obj['value'] = stringMagazineInPackagArray[x]+" book(s) at a time";
                    obj['id'] = stringMagazineInPackagArray[x];
                    magazineInPackageArray.push(obj);
                };

                this.setState({
                    getSignupMetadataObj : response.data.successObject,
                    booksInPackage: booksInPackageArray,
                    termsInpackage: termsInpackageArray,
                    magazinesInpackage: magazineInPackageArray,
                    booksPackageCount: booksInPackageArray[0].id,
                    monthPacakgeCount: termsInpackageArray[0].id,
                    packageId: response.data.successObject.currentPackage.id,
                    refreshing: false,
                })
                this._getRenewalCalculationsCost(booksInPackageArray[0].id, termsInpackageArray[0].id);
            } else {
                console.log("No getSignupMetadataObj found");
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _getRenewalCalculationsCost = async(booksCount, termsCount) => {
        console.log("subscription_id", this.state.primus);
        const getRenewalCost = API.API_JUSTBOOK_ENDPOINT + "/getRenewalCalculations";
        console.log("getRenewalCost", getRenewalCost);
        await axios.post(getRenewalCost ,{
            "branchId": 1008,
            "id": this.state.subscription_id,
            "noOfDoorDeliveries": 0,
            "numBooks":booksCount,
            "noOfTerms":termsCount,
            "numMagazine":this.state.magazinesInpackageCount,
            "primusMember":this.state.primus,
            "renewalImmediateEffect": true
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty', createdAt: 810, createdBy: 1000 } })
        .then((response) => {
            // console.log("response", response);
            if (response.data.status) {
                this.setState({
                    payableAmount: response.data.successObject.payableAmount
                })
            } else {
                showMessage({
                    message: "",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }


    _packagesChanges = async (packages) => {
        let packagesCount = packages.split(" ");
        this.setState({ booksPackageCount: packagesCount[0] });
        // await this._getRenewalMetadataForWebAndMobile(this.state.primus);
        await this._getRenewalCalculationsCost(packagesCount[0], this.state.monthPacakgeCount);
        console.log("payableAmount", this.state.payableAmount);                                   
    } 

    
    _monthsChanges = async (months) => {
        let monthsCount = months.split(" ");
        console.log(monthsCount);
        this.setState({ monthPacakgeCount: monthsCount[0]});
        // await this._getRenewalMetadataForWebAndMobile(this.state.primus)
        await this._getRenewalCalculationsCost(this.state.booksPackageCount, monthsCount[0]);;                                       
    }

    _renewOption = async () => {
        console.log("renewOption");
        await this._getRenewalMetadataForWebAndMobile(this.state.subscription_id, this.state.primus);
        this.setState({ 
            renewBtn : false,
            refreshing: true
        })
        setTimeout(() => {
            this.setState({ refreshing: false });
        },2000 );
    } 

    _initRenewPayNowMethod = () => {
        console.log("renew Now");

        var transactionTypeId = 2;
        var vendorId = 2;
        var urlReferrer = 'web';

        const payNowRequest = API.API_JUSTBOOK_ENDPOINT + "/processPayment";
        axios.post(payNowRequest ,{
            "createdBy": "1000",
            "createdAt": "810",
            "vendorId": vendorId,
            "transactionTypeId": transactionTypeId,
            "amount": this.state.payableAmount,
            "urlReferrer": urlReferrer,
            "source": "web",
            "subscriptionDTO":{
                "id":this.state.subscription_id,
                "noOfTerms":this.state.monthPacakgeCount,
                "numBooks":this.state.booksPackageCount,
                "numMagazine":this.state.magazinesInpackageCount,
                "packageId":this.state.packageId,
                "branchId":"",
                "couponsCsv":"",
                "noOfDoorDeliveries":0,
                "renewalImmediateEffect":true,
                "readingFeeAdjustmentAmount":0,
                "readingFeeAdjustmentNarration":0,
                "overDueAdjustmentAmount":0,
                "payableAmount":this.state.payableAmount,
                "amountPaid":this.state.payableAmount,
                "lastCardNumber":"0000",
                "createdBy":"1000",
                "createdAt":"810",
                "primusMember":this.state.primus,
            },
            "payments":[]
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response paynow", response.data);
            if (response.data.status) {
                if (response.data.successObject.isTransactionCompleted === false) {
                    // window.location.href = "/RazorPayKit/index.php?txn_num=" + vendorTransactionId + '&amount=' + amount + '&planName=' + planName + '&mobile=' + mobile;
                    var orderId = response.data.successObject.orderId;
                    var amount = response.data.successObject.amount;
                    var onlinePaymentId = response.data.successObject.onlinePaymentId;
                    var planName = 'RENEWAL';
                    var vendorTransactionId = response.data.successObject.vendorTransactionId;
                    
                    this._renewPaymentMethod(orderId, vendorTransactionId);
                } else if (response.data.successObject.isTransactionCompleted === true) {
                    showMessage({
                        message: "Renew Subscription",
                        description: response.data.statusMsg,
                        icon: "success",
                        type: "success",
                    });
                }
            } else {
                showMessage({
                    message: "Renew Subscription",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Renew Subscription",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _convertToPaise(amount) {
        return amount * 100;
    }

    _renewPaymentMethod = (orderId, vendorTransactionId) => {
        console.log("payment");
        const initiatePayment = API.API_JUSTBOOK_ENDPOINT + "/updateOnlineTransaction";
        var options = {
            description: 'JustBooks',
            image: 'https://justbooks.in/assets/img/logo.svg',
            currency: 'INR',
            key: API.RZP_API_KEY, // Your api key
            amount: this._convertToPaise(this.state.payableAmount),
            name: 'Justbook Subscription',
            description: 'connect with to read,sell and buy book',
            order_id: vendorTransactionId,
            prefill: {
                email: this.state.email,
                contact: this.state.mobile,
                name: this.state.firstName
            },
            theme: {color: '#273C96'}
        }
        RazorpayCheckout.open(options).then((data) => {
            axios.post(initiatePayment ,{
                'currentTransactionPaymentId':'3',
                'vendorId': '2',//for razorpay
                'vendorTransactionId': vendorTransactionId,
                "razorpay_payment_id": data.razorpay_payment_id,
                "razorpay_signature": data.razorpay_signature,
                "razorpay_order_id": data.razorpay_order_id
            }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
            .then(async (responsePayment) => {
                consolelog(responsePayment);
                if (responsePayment.data.status) {
                    showMessage({
                        message: "Renew Subscription",
                        description: 'Renew successfully Completed!',
                        icon: "success",
                        type: "success",
                    });
                      setTimeout(() => {
                        this.props.navigation.navigate('Subscription');
                    },2000);
                } else {
                    showMessage({
                        message: "Renew Subscription",
                        description: responsePayment.data.errorDescription,
                        icon: "danger",
                        type: "danger",
                    });
                }
            })
            .catch(function (error) {
                console.log(error);
                showMessage({
                    message: "Renew Subscription",
                    description: 'Something went wrong, Please try again later!',
                    icon: "danger",
                    type: "danger",
                });
            });

        }).catch((error) => {
            // handle failure
                console.log(error);
            showMessage({
                message: "Renew Subscription",
                description: 'Your payment was cancelled, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _diffYMDHMS(date1, date2, action) {

        let years = date1.diff(date2, 'year');
        date2.add(years, 'years');
    
        let months = date1.diff(date2, 'months');
        date2.add(months, 'months');
    
        let days = date1.diff(date2, 'days');
        date2.add(days, 'days');
    
        let hours = date1.diff(date2, 'hours');
        date2.add(hours, 'hours');
    
        let minutes = date1.diff(date2, 'minutes');
        date2.add(minutes, 'minutes');
    
        let seconds = date1.diff(date2, 'seconds');

        if(action == "remaining") {
            return ({
                "remaining_years": years,
                "remaining_months": months,
                "remaining_days": days,
                "remaining_hours": hours,
                "remaining_minutes": minutes,
                "remaining_seconds": seconds
            })
        } else {
            return ({
                "duration_years": years,
                "duration_months": months,
                "duration_days": days,
                "duration_hours": hours,
                "duration_minutes": minutes,
                "duration_seconds": seconds
            })
        } 
    
    }
    
    render() {
        Moment.locale('en');
        var startDate = (""+this.state.subscriptionDetails.startDate).trim()+"T00:00:00";
        var expiryDate = (""+this.state.subscriptionDetails.expiryDate).trim()+"T00:00:00";

        const todayDate = Moment(new Date(), "YYYY-MM-DD");
        const durationStartDate = Moment((""+this.state.subscriptionDetails.startDate).trim(), "YYYY-MM-DD");
        const durationEndDate = Moment((""+this.state.subscriptionDetails.expiryDate).trim(), "YYYY-MM-DD");
        // console.log(this._diffYMDHMS(durationEndDate, durationStartDate))
        const {duration_years, duration_months, duration_days} = this._diffYMDHMS(durationEndDate, durationStartDate, "duration");
        const {remaining_years, remaining_months, remaining_days} = this._diffYMDHMS(durationEndDate, todayDate, "remaining");
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    :
                    <View style={styles.activityClass}>
                        <View style={styles.mainContainer}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <View style={styles.wrapper}>
                                    {this.state.renewBtn && <View style={styles.ProfileCard}>
                                        <Avatar.Icon size={110} icon="star" />
                                    </View>}
                                    <Text style={styles.nameDesc}>
                                        <Headline style={styles.color}>{this._toTitleCase(this.state.subscriptionDetails.user.userInfo.firstname)} {this._toTitleCase(this.state.subscriptionDetails.user.userInfo.secName)} </Headline>{'\n'}
                                        <Text style={{color: 'green', fontSize: 16 }}>
                                            <Paragraph style={styles.colorParagraph}>Subscription</Paragraph>
                                        </Text>{'\n'}
                                    </Text>
                                </View>
                                <View style={styles.primusCard}>
                                    <Card style={styles.susbscriptionCard}>
                                        <DataTable style={{backgroundColor:'#FFFFFF'}}>
                                            <DataTable.Row>
                                                <DataTable.Cell>
                                                    <Paragraph style={{color:'#000000'}}>Online Package :</Paragraph>
                                                </DataTable.Cell>
                                                <DataTable.Cell numeric>
                                                    <Paragraph style={{color:'#000000'}}>{this.state.subscriptionDetails.numBooks} Books and {this.state.subscriptionDetails.numMagazine} Magnize</Paragraph>
                                                </DataTable.Cell>
                                            </DataTable.Row>

                                            <DataTable.Row>
                                                <DataTable.Cell>
                                                    <Paragraph style={{color:'#000000'}}>Duration :</Paragraph>
                                                </DataTable.Cell>
                                                <DataTable.Cell numeric>
                                                    <Paragraph style={{color:'#000000'}}>{duration_years} Years {duration_months} Months {duration_days} days</Paragraph>
                                                </DataTable.Cell>
                                            </DataTable.Row>

                                            <DataTable.Row>
                                                <DataTable.Cell>
                                                    <Paragraph style={{color:'#000000'}}>Start Date :</Paragraph>
                                                </DataTable.Cell>
                                                <DataTable.Cell numeric>
                                                    <Paragraph style={{color:'#000000'}}>{Moment(startDate).format('MMM d, YYYY')}</Paragraph>
                                                </DataTable.Cell>
                                            </DataTable.Row>

                                            <DataTable.Row>
                                                <DataTable.Cell>
                                                    <Paragraph style={{color:'#000000'}}>End Date :</Paragraph>
                                                </DataTable.Cell>
                                                <DataTable.Cell numeric>
                                                    <Paragraph style={{color:'#000000'}}>{Moment(expiryDate).format('MMM d, YYYY')}</Paragraph>
                                                </DataTable.Cell>
                                            </DataTable.Row>

                                            <DataTable.Row>
                                                <DataTable.Cell>
                                                    <Paragraph style={{color:'#000000'}}>Remaining Duration :</Paragraph>
                                                </DataTable.Cell>
                                                <DataTable.Cell numeric>
                                                    <Paragraph style={{color:'#000000'}}>{remaining_years} Years {remaining_months} Months {remaining_days} days</Paragraph>
                                                </DataTable.Cell>
                                            </DataTable.Row>

                                            {this.state.renewBtn && <DataTable.Row>
                                                <Button icon="currency-inr" mode="contained" labelStyle={{ color:'#FFFFFF'}} style={styles.btn} onPress={() => {this._renewOption()}}>
                                                    Renew Subscription
                                                </Button>
                                            </DataTable.Row>}
                                        </DataTable>
                                    </Card>
                                </View>


                                {!this.state.renewBtn && <View style={styles.primusSubscriptionCard}>
                                    <Text style={styles.nameSubscriptionDesc}>
                                        <Headline style={styles.renewColor}>Select Renew Option</Headline>{'\n'}
                                    </Text>
                                    <Card style={styles.susbscriptionCard}>
                                        <View style={styles.subscriptionContainer}>
                                            <View style={styles.item}>
                                                <Dropdown
                                                    label='Packages'
                                                    containerStyle={{width: '90%'}}
                                                    data={this.state.booksInPackage}
                                                    onChangeText={(text) =>this._packagesChanges(text)}
                                                    value={this.state.booksInPackage[0].value}
                                                />
                                            </View>
                                            <View style={styles.item}>
                                                <Dropdown
                                                    label='Duration'
                                                    containerStyle={{width: '90%'}}
                                                    data={this.state.termsInpackage}
                                                    onChangeText={(text) =>this._monthsChanges(text)}
                                                    value={this.state.termsInpackage[0].value}
                                                />
                                            </View>
                                        </View>
                                        <View>
                                            <View style={styles.subscriptionContainer}>
                                                <View style={styles.primusInsideItem}>
                                                    <Headline style={styles.renewTextColor}>Avail Primus Membership?</Headline>
                                                </View>
                                                <View style={styles.primusInsideItem}>
                                                    <Checkbox
                                                        status={this.state.primus ? 'checked' : 'unchecked'}
                                                        uncheckedColor={'#2E2E2E'}
                                                        onPress={() => {
                                                            this.setState({primus :  !this.state.primus, refreshing: true});
                                                            this._getRenewalMetadataForWebAndMobile(this.state.subscription_id, this.state.primus);
                                                        }}
                                                    />
                                                </View>
                                                <View style={styles.primusItem}></View>
                                            </View>
                                        </View>
                                        <View style={styles.subscriptionContainer}>
                                            <View style={styles.item}>
                                                <Headline style={styles.renewTextColor}>Renewal amount to pay</Headline>
                                            </View>
                                            <View style={styles.item}>
                                                <Headline style={styles.renewPriceColor}>Rs.  {this.state.payableAmount}</Headline>
                                            </View>
                                        </View>
                                        <View style={{marginTop: 20}}>
                                            <Button icon="currency-inr" mode="contained" labelStyle={{ color:'#FFFFFF'}}  style={styles.btn} onPress={() => {this._initRenewPayNowMethod()}}>
                                                Proceed to Pay
                                            </Button>
                                        </View>
                                    </Card>
                                </View>}
                            </ScrollView>
                        </View>
                        <BottomTab navigation={this.props.navigation} routeName="subscription" />
                </View>}
            </View>
        );
    }

}

export default Subscription;