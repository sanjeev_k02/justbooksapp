import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 0
    },
    divider : {
        margin: 0,
        color:'#2E2E2E',
        backgroundColor: '#2E2E2E'
    },
    primusCard: {
        marginBottom: 0,
        backgroundColor: '#FFFFFF',
        elevation: 0
    },
    primusSubscriptionCard: {
        marginBottom: 40,
        marginLeft: 10,
        backgroundColor: '#FFFFFF',
        elevation: 0
    },
    susbscriptionCard: {
        backgroundColor: '#FFFFFF',
        color: '#2E2E2E',
        elevation: 0
    },
    dividerPrimus : {
        margin: 15,
        color:'#2E2E2E',
        backgroundColor: '#2E2E2E'
    },
    mainContainer: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 20,
        marginBottom: 40,
        flexDirection: "column"
    },
    image: {
        width: 160, 
        height: 160, 
        borderRadius: 180/ 2,
    },
    welcomeBlock:{
        alignItems: "center",
        marginTop: 20
    },
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        borderBottomWidth: 1,
        paddingBottom: 15,
        paddingTop: 15
    },
    subscriptionContainer: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start' // if you want to fill rows left to right
    },
    item: {
        width: '50%' // is 50% of container width
    },
    primusItem: {
        width: '33%' // is 50% of container width
    },
    primusInsideItem: {
        marginTop: 10,
        marginBottom: 10
    },
    primuscolor: {
        color: '#2E2E2E',
        fontSize: 18,
        marginLeft: 5
    },
    wrapper: {
        alignItems: "center",
        marginTop: 10,
        marginBottom: 0
    },
    mainviewStyle: {
        flex: 1,
        flexDirection: 'column',
    },
    profileCard: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100
    },
    headline: {
        color: 'red',
        fontSize: 25,
        textAlign: 'center',
        fontWeight: '600'
    },
    titleDesciption : {
        textAlign: 'center',
        margin: 8
    },
    color: {
        color: '#2E2E2E',
        fontSize: 25,
        textAlign: 'center'
    },
    renewColor: {
        color: '#2E2E2E',
        fontSize: 20,
        textAlign: 'center'
    },
    renewTextColor: {
        color: '#2E2E2E',
        fontSize: 16,
        textAlign: 'left'
    },
    renewPriceColor: {
        color: '#2E2E2E',
        fontSize: 20,
        marginRight: 10,
        textAlign: 'right'
    },
    colorParagraph: {
        color: 'pink',
    },
    nameDesc: {
        color: '#2E2E2E', 
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30, 
        textAlign: 'center'
    },
    nameSubscriptionDesc: {
        color: '#2E2E2E', 
        marginTop: 10,
        marginBottom: 0,
        marginLeft: 30,
        marginRight: 30, 
        textAlign: 'center'
    },
    btn: {
        height: 40,
        textAlign: 'center', 
        // marginLeft: 40,
        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        alignSelf: 'center',
        marginTop: 5,
        elevation: 0,
        backgroundColor: '#273C96',
        textTransform: 'none'
    }
});
