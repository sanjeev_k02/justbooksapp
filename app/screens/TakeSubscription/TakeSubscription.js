import React from 'react';
import { View, Text, StatusBar, Image, Alert } from 'react-native';
import styles from './styles';
import { Avatar, DataTable, Appbar, Divider, Button, Card, Title, Paragraph, Headline, Subheading, IconButton } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

import AsyncStorage from '@react-native-community/async-storage';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import RazorpayCheckout from 'react-native-razorpay';

class TakeSubscription extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    _paymentMethod = () => {
        console.log("payment");
        var options = {
            description: 'JustBooks',
            image: 'https://justbooks.in/assets/img/logo.svg',
            currency: 'INR',
            key: 'rzp_test_A9kyfNmjRMRX6E', // Your api key
            amount: '1599',
            name: 'Justbook Subscription',
            description: 'connect with to read,sell and buy book',
            prefill: {
                email: '',
                contact: '',
                name: ''
            },
            theme: {color: '#273C96'}
        }
        RazorpayCheckout.open(options).then((data) => {
            // handle success
            // alert(`Success: ${data.razorpay_payment_id}`);
            var userSubscription = {
                'subscription': true,
            }
            this._storeData(userSubscription);
        }).catch((error) => {
            // handle failure
            // alert(`Error: ${error.code} | ${error.description}`);
            var userSubscription = {
                'subscription': false,
            }
            this._storeData(userSubscription);
        });
    }
  
    _storeData = async (response) => {
      try {
          AsyncStorage.setItem('userSubscription', JSON.stringify(response), () => {
              this.props.navigation.navigate("Home");
            });
      } catch (error) {
      // Error saving data
      console.log('error msg', error);
      }
    };
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <View style={styles.mainContainer}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.wrapper}>
                            <View style={styles.ProfileCard}>
                                <Avatar.Icon size={125} icon="star" />
                            </View>
                            <Text style={styles.nameDesc}>
                                <Headline style={styles.color}>Subscription </Headline>{'\n'}
                            </Text>
                        </View>
                        <View style={styles.primusCard}>
                            <Card style={styles.susbscriptionCard}>
                                <DataTable>
                                    <DataTable.Row>
                                        <DataTable.Cell>Online Package :</DataTable.Cell>
                                        <DataTable.Cell numeric>2Books and 0 Magnize</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Duration :</DataTable.Cell>
                                        <DataTable.Cell numeric>2 Years 2 Months 26 days</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Start Date :</DataTable.Cell>
                                        <DataTable.Cell numeric>Wed Oct 31, 2019</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>End Date :</DataTable.Cell>
                                        <DataTable.Cell numeric>Tue Jan 26,2020</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>
                                        <DataTable.Cell>Total Amount :</DataTable.Cell>
                                        <DataTable.Cell numeric>Rs. 1599</DataTable.Cell>
                                    </DataTable.Row>

                                    <DataTable.Row>

                                        <Button icon="currency-inr" mode="contained" style={styles.btn} onPress={() => this._paymentMethod()}>
                                            Subscription Now
                                        </Button>
                                    </DataTable.Row>
                                </DataTable>
                            </Card>
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }

}

export default TakeSubscription;