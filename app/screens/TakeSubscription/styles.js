import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
    },
    divider : {
        margin: 0,
        color:'#2E2E2E',
        backgroundColor: '#2E2E2E'
    },
    primusCard: {
        marginBottom: 40,
        backgroundColor: '#FFFFFF',
        elevation: 0
    },
    susbscriptionCard: {
        // backgroundColor: '#2b2a2a',
        color: '#2E2E2E',
        elevation: 0
    },
    dividerPrimus : {
        margin: 15,
        color:'#2E2E2E',
        backgroundColor: '#2E2E2E'
    },
    mainContainer: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingBottom: 20,
        flexDirection: "column"
    },
    image: {
        width: 160, 
        height: 160, 
        borderRadius: 180/ 2,
    },
    welcomeBlock:{
        alignItems: "center",
        marginTop: 20
    },
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        borderBottomWidth: 1,
        paddingBottom: 15,
        paddingTop: 15
    },
    wrapper: {
        alignItems: "center",
        marginTop: 10,
        marginBottom: 0
    },
    mainviewStyle: {
        flex: 1,
        flexDirection: 'column',
    },
    profileCard: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100
    },
    headline: {
        color: 'red',
        fontSize: 25,
        textAlign: 'center',
        fontWeight: '600'
    },
    titleDesciption : {
        textAlign: 'center',
        margin: 8
    },
    color: {
        color: '#2E2E2E',
        fontSize: 25,
        textAlign: 'center'
    },
    colorParagraph: {
        color: 'pink',
    },
    nameDesc: {
        color: '#2E2E2E', 
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30, 
        textAlign: 'center'
    },
    btn: {
        height: 40,
        textAlign: 'center', 
        // marginLeft: 40,
        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        alignSelf: 'center',
        marginTop: 5,
        elevation: 0,
        backgroundColor: '#273C96',
        textTransform: 'none'
    }
});
