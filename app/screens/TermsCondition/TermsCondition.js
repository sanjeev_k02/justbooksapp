import React from 'react';
import { View, StatusBar, Text } from 'react-native';
import { Title, Paragraph, Divider } from 'react-native-paper';
import styles from './styles';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';

class TermsCondition extends React.Component {
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <ScrollView>
                    <View style={styles.containBody}>
                        <View class={styles.shareContainer}>
                            <View style={styles.viewIssue}>
                                {/* <Title style={{textAlign: 'center', color: '#fff' }}>
                                    Terms & Conditions 
                                </Title> */}

                                <Text style={styles.titleDesciption}>
                                    <Paragraph  style={styles.para} >

                                        We recommend that you carefully go through our Terms of Use, to better understand the different features of our service. By subscribing to the registration process at Just Books, Members accept all items in the Terms of Use and agree to abide by and be legally bound by the Terms of Use.{'\n'}{'\n'}

                                        <Text style={styles.fontHead}>1. Eligibility</Text>{'\n'}
                                        Any individual who submits a fully complete application form for membership with a) address proof of residence / work b) mobile phone number & proof of billing address and c) communicable email address is welcome to sign up with JustBooks Solutions Private Limited for the Company’s Library named JustBooks. However, JustBooks reserves the right to accept or reject membership at its sole discretion. Membership is for personal use only. However, it can be transferred to any other individual’s name with explicit communication to JustBooks. No member may re-lend books borrowed from JustBooks at any time. Doing so will entail automatic cancellation of membership.{'\n'}{'\n'}

                                        <Text style={styles.fontHead}> 2. Plans & Payment terms</Text>{'\n'}
                                        By subscribing to a specific membership plan of JustBooks, the member is explicitly agreeing to pay, in advance, the reading fee attached to the plan plus and any other applicable fee or tax at the frequency specified by the plan. Any non compliance with the payment terms in spite of 2 reminders will attract a penalty of 10% per payment period of the amount due and may lead to cancellation of membership.{'\n'}{'\n'}

                                        <Text style={styles.fontHead}>3. Reading period and reading fee</Text>{'\n'}
                                        JustBooks proposes a reasonable reading period with a restriction on the number of books that a member can hold at any point in time. No penalty will be levied for delayed return of books. However, under specific circumstances where other library members have requested for and are waiting for a book/s for more than 10 days, JustBooks reserves the right to contact and request members holding book/s to be returned. Members are requested to then return the requested book/s within five (5) days of such request. Reading fees should be paid as per the plan. In the event a member does not renew his/her subscription the company reserves the right to adjust the deposit amount towards the reading fees. If there is no balance in the Deposit Amount, then the company will terminate the membership of the member. All correspondence in this regard will be communicated to the email address of the member as submitted in the application form. Members are requested to update their contact details periodically when changed. Reading fee paid in advance will not be refunded.{'\n'}{'\n'}

                                        <Text style={styles.fontHead}>4. Lost or Damaged books</Text>{'\n'}
                                        At JustBooks, we take utmost care to maintain books in good condition. Any anomaly (unprinted pages) of damage if found should be reported either at the front desk while getting the book issued or to the delivery executive if the book is delivered at the door. Failure to check and/or report the same will be constituted as if the member has received a book in good condition. While returning the book, The front desk executive or the deliver executive (if the book is collected at the door) will check for any damage of book(s) and report to the member accordingly. The determination of a book being “damaged” will be at the sole discretion of JustBooks. Members can choose to replace a lost or damaged book within 10 days or pay the penalty amount determined by Just Books. The damage policy is detailed below.{'\n'}{'\n'}

                                        To discourage ad-hoc and short term membership, members are required to comply with a minimum membership term of three months. For members who subscribe to plans with a refundable deposit, any premature closure of membership in less than three months from the date of sign-up, will result in the reading fee for the remaining period of the mandatory term getting adjusted against the deposit. The rest of the deposit, however will be refunded. For members who subscribe to annual plans without deposit there is no refund applicable at the time of closure. Under extreme situations caused by non-compliance to the Terms of Use by member, JustBooks clc reserves the right to terminate a membership for any or no reason without any refund.{'\n'}{'\n'}

                                        <Text style={styles.fontHead}>5. Unclaimed Refundable Deposit</Text>{'\n'}
                                        Upon expiry of membership, JustBooks sends reminders over emails/sms to its members for renewal. Three months after expiry, JustBooks reserves the right to classify the account as ‘Dormant’ and initiate steps to recover cost of books and any unpaid reading fee from the refundable deposit, if there is no action on the reminders, which would be communicated to the member through email/sms. Post this recovery, any excess deposit left in the account will be retained as “Unclaimed” refundable deposit for a period of 3 months and periodic reminders would be sent to the member to claim the same. After 12 months of expiry of membership, JustBooks reserves the right to treat the member as “not reachable” and take appropriate action on the unclaimed refundable deposit if there has been no response from the member on the reminder communications.{'\n'}{'\n'}

                                        The ‘Terms of Use’ of JustBooks is critical in its continuous efforts to offer a unique reading experience to members. Please read the conditions & stipulations set forth in this document, We look forward to your co-operation in adhering to the same. Thank you for your membership. We will strive to provide you best-in-class reading experience at JustBooks.{'\n'}{'\n'}{'\n'}
                                    </Paragraph>
                                </Text>
                            </View>
                        </View>

                    </View>
                </ScrollView>
                <SearchTab navigation={this.props.navigation} />
                <BottomTab navigation={this.props.navigation} />
            </View>
        );
    }

}

export default TermsCondition;