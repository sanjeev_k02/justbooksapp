import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 55
    },
    cardHomeView: {
        backgroundColor: '#101211', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 15, 
        marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },
    divider : {
        margin: 0,
    },
    view: {
        marginTop: 10,
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
    },
    fontStyle: { 
        fontSize: 15, 
        color: "#fff" 
    },
    viewIssue: { 
        marginTop: 20, 
        marginBottom: 0, 
        textAlign: 'center' 
    },
    para : {
      color: "#2E2E2E",
      textAlign: 'justify'
    },
    titleDesciption : {
      margin: 25
    },
    fontHead: {
      fontSize: 18, 
      fontWeight: '600'
    }

});
