import React from 'react';
import { View, Text, StatusBar } from 'react-native';
import styles from './styles';
import { Avatar, Divider, Card, IconButton } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { WebView } from 'react-native-webview';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';

class VideoReview extends React.Component {
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                    <WebView
                        style={ styles.WebViewContainer }
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        source={{uri: 'https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/video_reviews/title_review_194_1599041865883.mp4' }}
                    />

                <SearchTab navigation={this.props.navigation} />
                <BottomTab navigation={this.props.navigation} />
            </View>
        );
    }

}

export default VideoReview;