import { StyleSheet ,Dimensions, Platform } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 60
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF',
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },
    divider : {
        margin: 10,
    },
    cardStyle : {
        backgroundColor: '#39403c', 
        borderRadius: 6
    },
    titleStyle: {
        color : '#fff'
    },
    subtitleStyle: {
        color : '#fff'
    },
    rightStyle : {
        backgroundColor: '#39403c', 
        borderBottomRightRadius: 6, 
        borderTopRightRadius: 6, 
        height: 70, 
        paddingTop: 8
    },
    iconColor : {
        backgroundColor: '#273C96'
    },
    WebViewContainer: {
        marginTop: (Platform.OS == 'ios') ? 20 : 0,
    }

});
