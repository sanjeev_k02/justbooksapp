import React from 'react';
import { View, RefreshControl, FlatList, Image, TouchableHighlight } from 'react-native';
import styles from './styles';
import { Divider, Text, Button, Title, Paragraph, ActivityIndicator, Colors  } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { Appbar } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Modal from 'react-native-modal';
import { WebView } from 'react-native-webview';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import CategoryTab from '../../components/CategoryTab/CategoryTab';
import API from '../../env';
import * as axios from 'axios';

class ViewVideoReview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subscription_id: 0,
            memberCard: 0,
            branchId: 0,
            cityId: 0,
            primusMappedBranches: 0,
            primusMember: false,
            page: 1,
            isModalVisible: false,
            userDetails: {},
            refreshing: true,
            videoReviewFlag: false,
            videoReview: [
                    // {
                    //     "titleImage": "images/noimage.jpg",
                    //     "titleName": " Thea Stilton #27: Thea Stilton And Niagara Splash",
                    //     "titleId": 499063,
                    //     "videoReviewUrl": "https://s3.ap-south-1.amazonaws.com/assets.jbclc.com/video_reviews/title_review_499063_1598946259912.mp4",
                    //     "reviewId": 74
                    // }
                ],
            videoReviewUrl: '',
            refreshing: true,
            title_id: this.props.route.params.title_id,
            bookTitle: this.props.route.params.bookTitle,
        };
    }

    _toTitleCase(str) { 
        if(str.length > 60) {
            var res = str.substring(0, 60);
            return res.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            )+"...";
        } else {
            return str.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            );
        }
    }

    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            // console.log("user",user);
            if (user !== null) {
                const userData = JSON.parse(user);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard,
                    branchId: userData.login.branchId,
                    primusMappedBranches: userData.login.primusMappedBranches,
                    cityId: userData.login.cityId,
                    primusMember: userData.login.primus
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        await this.getAllVideoReviewBooks( this.state.title_id);

        setTimeout(() => {
            this.setState({ refreshing: false });
        }, 1000);
    };

    getAllVideoReviewBooks = (titleId) => {
      let getVideoReview = API.API_JUSTBOOK_ENDPOINT+"/titleReview/getTitleVideoReviewsForTitleId?titleId="+titleId+"&pageNumber="+this.state.page+"&pageSize=10";
      
      console.log(getVideoReview);
      axios.get(getVideoReview, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
          console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.data.length > 0) {
                this.setState({
                    videoReview : response.data.successObject.data,
                    videoReviewFlag: true
                })
            } else {
                this.setState({
                    videoReviewFlag: true
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _onRefresh = async () => {
        this.setState({refreshing: true});
        await this.getAllVideoReviewBooks();
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
    }

    _bookDetailsNavigate = async(titleid) => {
        console.log(titleid);
        this.props.navigation.push("BookDetails",{ title_id: titleid });
    }

    _backNavigation = () => {
        if(this.state.isMainCategory) {
            this.props.navigation.navigate("Home");
        } else {
            this.props.navigation.navigate("CategoryFilter");
        }
    }

    _toggleOpenModal = (videoReviewUrl) => {
        this.setState({
            videoReviewUrl: videoReviewUrl,
            isModalVisible: !this.state.isModalVisible
        });
    };

    _toggleModal = () => {
        this.setState({
            isModalVisible: !this.state.isModalVisible
        });
    };

    render() {
        var _renderRideItem =(item,index) => {
            return (
                <View key={item.reviewId}>
                    <View style={styles.card} >
                        <TouchableOpacity onPress={()=>{console.log("play Icon click")}}>
                            {(item.titleImage != 'null' && item.titleImage != null && item.titleImage != 'images/noimage.jpg' && item.titleImage != '') ? <Image 
                                source={{ uri: item.titleImage }} style={styles.imageCard}>
                            </Image>
                            : <Image 
                                source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} style={styles.imageCard}>
                            </Image>}
                            <View style={styles.playTag}>
                                <TouchableOpacity onPress={()=>{this._toggleOpenModal(item.videoReviewUrl)}}>
                                    <Image source={require("../../assets/play.png")}  style={styles.playImage}></Image>
                                </TouchableOpacity>
                            </View>
                            {this.state.primusMember && <View style={styles.primusTag}>
                                <Image source={{ uri: 'https://justbooks.in/assets/img/primus_tag.png' }} style={styles.primusImage}></Image>
                            </View>}
                        </TouchableOpacity>
                        <View style={styles.cardDescription}>
                            <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}}>
                                <Text style={styles.titleDesciption}>
                                    <Title style={styles.color}>{this._toTitleCase(item.titleName)}</Title>{'\n'}
                                    {item.postedTime && <Paragraph style={styles.color}>{item.postedTime}</Paragraph>}
                                </Text>
                            </TouchableOpacity>
                            <Text style={styles.titleDesciption}>
                                <Rating
                                    type='custom'
                                    ratingColor='#FFD54B'
                                    ratingBackgroundColor='#F1F1F1'
                                    ratingCount={5}
                                    imageSize={20}
                                    onFinishRating={this.ratingCompleted}
                                    style={{ paddingVertical: 10 }}
                                />
                            </Text>
                        </View>
                    </View>
                    <Divider style={styles.divider} />
                </View>
            )
        }
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <View>
                    <Modal isVisible={this.state.isModalVisible}>
                        <View style={{flex: 1}}>
                            <WebView
                                style={{ flex: 1 }} 
                                scalesPageToFit={true}
                                javaScriptEnabled={true}
                                domStorageEnabled={true}
                                source={{uri: this.state.videoReviewUrl }}
                            />
                            <View
                            style={{ flexDirection: 'row', alignItems: 'space-between', position: 'absolute', bottom: 0}}>
                                <TouchableHighlight
                                     onPress={()=>{this._toggleModal()}}
                                    style={{ flex: 1, backgroundColor: '#F1F1F1' }}>
                                    <Text style={styles.actionClose}>Close</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </Modal>
                </View>
                { (this.state.refreshing) ?
                <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        {this.state.videoReviewFlag && <View style={styles.newArrivles}>
                            <View style={styles.cardView}>
                                <View style={{ marginBottom: 5 ,marginTop: 15}}>
                                    <View style={styles.cardHomeView}>
                                        {(Object.keys(this.state.videoReview).length > 0 ) && (
                                            <View>
                                                <View style={styles.headingSearchView}>
                                                    <Text style={styles.buttonDesc}>{'\n'}
                                                        <Button icon="upload" mode="outline" labelStyle={{fontSize: 18, color: '#FFFFFF', textTransform: 'none'}} style={styles.btn} onPress={()=>{this.props.navigation.navigate("AddVideoReview", {
                                                                title_id: this.state.title_id,
                                                                bookTitle : this.state.bookTitle
                                                            })}}>
                                                        Upload New Review
                                                        </Button>{'\n'}{'\n'}
                                                    </Text>
                                                </View>
                                                <FlatList 
                                                    data={this.state.videoReview}
                                                    ref={"flatlist"}
                                                    removeClippedSubviews={true}
                                                    initialNumToRender={10}
                                                    bounces={false}
                                                    renderItem={({ item, index }) => _renderRideItem(item, index)}
                                                    keyExtractor={(item, index) => item.titleId}
                                                    bounces={false}
                                                    onEndReachedThreshold={0.5}
                                                />
                                            </View>
                                        )}

                                        {((Object.keys(this.state.videoReview).length == 0 )) && (
                                        <View class={styles.shareContainer}>
                                            <View style={{ alignSelf: "center" }}>
                                                <View style={{
                                                    marginTop: 60,
                                                    width: 220,
                                                    height: 220,
                                                    borderRadius: 100,
                                                    overflow: "hidden"}}>
                                                <Image source={require("../../assets/no-data.png")} style={{
                                                    flex: 1,
                                                    height: 250,
                                                    width: undefined}} ></Image>
                                                </View>
                                                <View style={styles.wrapper}>
                                                    <Text style={styles.buttonDesc}>
                                                        <Button icon="upload" mode="outline" labelStyle={{fontSize: 18, color: '#FFFFFF', textTransform: 'none'}} style={styles.btn} onPress={()=>{this.props.navigation.navigate("AddVideoReview", {
                                                                title_id: this.state.title_id,
                                                                bookTitle : this.state.bookTitle
                                                            })}}>
                                                        Upload New Review
                                                        </Button>{'\n'}
                                                    </Text>
                                                </View>
                                            </View>
                                        </View>
                                        )}
                                    </View>
                                </View>
                            </View>
                            {/* <Divider style={styles.divider} /> */}
                        </View>}
                        {/* <ChatBotIcon navigation={this.props.navigation} /> */}
                        <BottomTab navigation={this.props.navigation} routeName="home"/>
                    </View>
                }
            </View>
        );
    }

}

export default ViewVideoReview;