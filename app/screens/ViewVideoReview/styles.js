import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 60
    },
    headingView: {
        marginTop: 0,
        marginBottom: 0,
        marginRight: 10,
        marginLeft: 10
    },
    rowHeader : {
        backgroundColor: '#FFFFFF', 
        margin: 0, 
        flex: 1, 
        flexDirection: 'row',
        elevation: 0
    },
    heading: {
        color: '#2E2E2E',
        fontSize: 16,
        fontWeight: '600'
    },
    SeeAllHeading: {
        flex: 1,
        color: '#2E2E2E',
        fontSize: 16,
        fontWeight: '600',
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end'
    },
    headingStyle : {
        fontSize: 16, 
        fontWeight: '600'
    },
    titleStyle : {
        fontSize: 14, 
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end'
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 15, 
        marginTop: 0, 
        paddingLeft: 15,
    },
    cardView: {
        backgroundColor: '#FFFFFF', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 0, 
        marginTop: 0,
    },
    cardSection: {
        width: 120,
        height: 225,
        margin: 10,
        // backgroundColor: '#101211'
    },
    coverImage: {
        height: 160,
    },
    card: {
        width: '90%',
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    imageCard: {
        width: 90, 
        height: 125,
        borderRadius: 4
    },
    primusImage: {
      width: 75, 
      height: 35,
   },
   primusTag: {
      width: 80, 
      height: 35, 
      position: 'absolute', 
      bottom: 5,
      left: 0
   },
    cardDescription : {
        width: '80%',
        flexDirection: 'column',
        marginLeft: '6%', 
        marginTop: -5, 
        justifyContent: 'space-between'
    },
    titleDesciption: {
        marginTop: 5,
        marginRight: 30
    },
    color: {
        color: '#2E2E2E',
    },
    colorParagraph: {
        color: 'pink',
    },
    title: {
        width: '100%',
        color: '#273C96',
        fontSize: 16,
        lineHeight: 15,
        marginTop: 8,
        marginBottom: 0,
        fontWeight: '600'
    },
    Paragraph: {
        marginTop: 0,
        width: '100%',
        fontSize: 12,
        color: '#273C96'
    },
    divider : {
        backgroundColor: '#8F8E94', 
        marginTop: 15, 
        marginBottom: 15
    },
   headingSearchView: {
     marginTop: 0,
     marginBottom: 0,
     marginRight: 10,
     marginLeft: -10 ,
     paddingLeft: 15,
   },
   headingSearchStyle : {
       fontSize: 20, 
       width: 250,
       fontWeight: '600',
       fontFamily: 'Montserrat-Bold'
   },
   wrapper: {
       alignItems: "center",
       marginTop: 10,
       marginBottom: 0
   },
   buttonDesc : {
       alignItems: "center",
       textAlign: 'center'
   },
   btn: {
       height: 42, 
       textAlign: 'center', 
       margin: 20,
       fontSize: 18,
       borderColor :'#2E2E2E',
       borderWidth : 1,
       width: '100%',
       color: '#FFFFFF',
       backgroundColor: '#2E2E2E',
       borderRadius: 20
   },
   WebViewContainer: {
       marginTop: (Platform.OS == 'ios') ? 20 : 0,
   },
   actionClose : {
     color:'#2E2E2E',
     margin: 15,
     textAlign: 'center'
   },
   actionUpload : {
     color:'#FFFFFF',
     margin: 15,
     textAlign: 'center'
   },
   playImage: {
     width: 30, 
     height: 30,
     borderRadius: 30
  },
  playTag: {
     width: 30, 
     height: 30, 
     position: 'absolute', 
     top: 5,
     right: 5,
     borderRadius: 30
  },

});
