import React from 'react';
import { View, Text, StatusBar, StyleSheet, Image } from 'react-native';
import styles from './styles';
import { Avatar, Appbar, Divider, Button, Card, Title, Paragraph, Headline, Subheading, IconButton,  ActivityIndicator, Colors   } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { showMessage } from "react-native-flash-message";

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';
import API from '../../env';
import * as axios from 'axios';

class BranchResults extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
            branchId : this.props.route.params.branchId,
            getCategories: {}
        }
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
  
    componentDidMount = async() => {
        await this.getCategoriesAndSubCategoriesForBranch(this.state.branchId);
        setTimeout( 
            () => { this.setState({ refreshing: false });
        },2000);
    };

    getCategoriesAndSubCategoriesForBranch = (branchId) => {
      const getCategories = API.API_ESAPI_POINT+"/getCategoriesAndSubCategoriesForBranch?branchId="+branchId;
      console.log(getCategories);
      axios.get(getCategories)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    getCategories : response.data.successObject,
                    refreshing: false
                })
            } else {
                this.setState({ refreshing: false })
                showMessage({
                    message: "Virtual Library",
                    description: response.data.errorDescription,
                    icon: "info",
                    type: "info",
                });
                setTimeout( () => { 
                    this.props.navigation.push("BranchSearch");
                },2000);
            }
        })
        .catch((error) => {
          console.log(error);
          showMessage({
              message: "Virtual Library",
              description: 'something went wrong! please try again later!',
              icon: "info",
              type: "info",
          });
          setTimeout( () => { 
              this.props.navigation.push("BranchSearch");
          },2000);
        });
    }

    _bookDetailsNavigate = async() => {
        this.props.navigation.navigate("BookDetails");
    }

    _allBooksNavigate = async() => {
        this.props.navigation.navigate("AllBooks");
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityBranchClass}>
                        <View style={styles.activityHomeClass}>
                            <View style={{ marginBottom: 5}}>
                                <View style={styles.headingBranchSearchView}>
                                    <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                        <Appbar.Content title="Virtual Library" titleStyle={styles.headingBranchSearchStyle} style={styles.searchHeading} />
                                        <Appbar.Content title="Back" titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this.props.navigation.goBack()}} />
                                    </Appbar.Header>
                                </View>
                                <ScrollView>
                                    {this.state.getCategories.map((category)=>{
                                        return (
                                            <View key={category.id} style={styles.newArrivles}>
                                                <Appbar.Header style={{backgroundColor: '#E8E8E8',elevation:0, height: 36}}>
                                                <Image source={{ uri: category.icon }} style={styles.imageCard}>
                                                </Image>
                                                <Appbar.Content title={category.name} titleStyle={styles.headingStyle} style={styles.heading} />
                                                </Appbar.Header>
                                                <View style={styles.cardView}>
                                                    {category.subCategories.length > 0 && <View style={styles.category}>
                                                        {category.subCategories.map((subCategory)=>{
                                                            return (
                                                                <View key={subCategory.id} style={styles.subItem}>
                                                                    <TouchableOpacity onPress={
                                                                        ()=>this.props.navigation.navigate('VirtualLibraryResults', {
                                                                            mainCategory: category.id,
                                                                            branchId: this.state.branchId,
                                                                            subCategories: subCategory.id

                                                                        })}>
                                                                        <Card style={styles.cardDesign}>
                                                                            <Text style={styles.titleDesciption}>
                                                                                <Text style={styles.headlineTop}>{subCategory.name}</Text>
                                                                            </Text>
                                                                        </Card>
                                                                    </TouchableOpacity>
                                                                </View>
                                                            )
                                                        })}
                                                    </View>}
                                                </View>
                                            </View>
                                        )
                                    })}
                                </ScrollView>
                            </View>
                        </View>
                        <SearchTab navigation={this.props.navigation} />
                        <BottomTab navigation={this.props.navigation} />
                    </View>
                }
            </View>
        );
    }

}

export default BranchResults;