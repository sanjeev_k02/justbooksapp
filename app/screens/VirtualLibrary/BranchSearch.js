import React from 'react';
import { View, Text, TextInput, FlatList, Image } from 'react-native';

import Icons from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Appbar, ActivityIndicator, Colors   } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { showMessage } from "react-native-flash-message";

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import API from '../../env';
import * as axios from 'axios';

class BranchSearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
            searchText: '',
            branch:{}
        };
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
  
    componentDidMount = async() => {
        await this.getAllBranchList();
        setTimeout(
            () => {
            this.setState({ refreshing: false });
            },
            2000
        );
    };

    _typeHead = (searchText) => {
        if(searchText == '' || searchText == null) {
            this.setState({
                searchText: ''
            })
            this.getAllBranchList(); 
        } else {
            this.setState({
                searchText: searchText
            })
    
            let keyword = searchText.toLowerCase();
            let filtered_branch = this.state.branch.filter(function(item){
                    item = item.branchName.toLowerCase();
                return item.indexOf(keyword) > -1; 
            });
            
            this.setState({
                branch: filtered_branch
            })
        }
    }

    getAllBranchList = () => {
      const getBranch = API.API_ESAPI_POINT+"/getBranchesListForVirtualLibrary";
      console.log(getBranch);
      axios.get(getBranch)
        .then((response) => {
        //   console.log(response.data.successObject.content);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    branch : response.data.successObject,
                    refreshing: false
                })
            } else {
                this.setState({
                    refreshing: true
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _renderItem = ({ item }) => (
        <View key={item.branchId} style={styles.item} key={item.branchId}>
            <TouchableOpacity onPress={
                ()=>this.props.navigation.navigate('BranchResults', {
                    branchId: item.branchId,
                })}>
                <Text style={styles.title}>{item.branchName}</Text>
            </TouchableOpacity>
        </View>
    );

    searchBooks = () => {
        if (this.state.searchText === '') {
            showMessage({
                message: "Virtual Library",
                description: 'Search query cannot be empty',
                icon: "info",
                type: "info",
            });
            return;
        }
        showMessage({
            message: "Virtual Library",
            description: 'Please tap on branch Name!',
            icon: "info",
            type: "info",
        });
    };
      
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activitySearchClass}>
                        {/* <ScrollView> */}
                            <View style={{ marginBottom: 5 ,marginTop: 10}}>
                                <View style={styles.cardHomeView}>
                                    <View style={styles.headingSearchView}>
                                        <Appbar.Header style={{backgroundColor: '#FFFFFF',elevation:0, margin: 0}}>
                                            <Appbar.Content title="Choose Your Branch" titleStyle={styles.headingSearchStyle} style={styles.searchHeading} />
                                        </Appbar.Header>
                                    </View>
                                    <View style={styles.SectionStyle}>
                                        <TextInput 
                                            label= "search"
                                            style={styles.searchTextInput}
                                            underlineColor= "#2E2E2E"
                                            placeholderTextColor = "#2E2E2E"
                                            placeholder= "Search Branch Name..."
                                            mode= "outlined"
                                            value={this.state.searchText}
                                            mode= "outlined" 
                                            onChangeText={changedText => this._typeHead(changedText)}
                                            returnKeyType='search'
                                            onSubmitEditing={()=>this.searchBooks()} 
                                            clearButtonMode="while-editing" />
                                        <Icons name="search" size={24} color="#2E2E2E" style={{ marginRight: 10 }} />
                                    </View>
                                </View>
                                <View style={styles.listHomeView}>
                                    <View style={styles.branchView}>
                                        <FlatList
                                            data={this.state.branch}
                                            renderItem={this._renderItem}
                                            keyExtractor={item => item.branchId}
                                        />
                                    </View>
                                </View>
                                {((Object.keys(this.state.branch).length == 0 )) && (
                                    <View class={styles.shareContainer}>
                                        <View style={{ alignSelf: "center" }}>
                                            <View style={{
                                                marginTop: 0,
                                                width: 220,
                                                height: 220,
                                                borderRadius: 100,
                                                overflow: "hidden"}}>
                                            <Image source={require("../../assets/no-data.png")} style={{
                                                flex: 1,
                                                height: 250,
                                                width: undefined}} ></Image>
                                            </View>
                                        </View>
                                    </View>
                                    )}
                            </View>
                        {/* </ScrollView> */}
                </View>}
                {/* <BottomTab navigation={this.props.navigation} /> */}
            </View>
        );
    }

}

export default BranchSearch;