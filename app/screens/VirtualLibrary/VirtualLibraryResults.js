import React from 'react';
import { View, Text, StatusBar, StyleSheet, Image } from 'react-native';
import styles from './styles';
import { Avatar, Appbar, Divider, Button, Card, Title, Paragraph, Headline, Subheading, IconButton, ActivityIndicator, Colors } from 'react-native-paper';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { showMessage } from "react-native-flash-message";

import Icons from 'react-native-vector-icons/Ionicons';
import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';
import API from '../../env';
import * as axios from 'axios';

class VirtualLibraryResults extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: true,
            page: 1,
            mainCategory: this.props.route.params.mainCategory,
            branchId: this.props.route.params.branchId,
            subCategories: this.props.route.params.subCategories,
            getVirtualBook: {}
        }
        console.log("mainCategory",this.state.mainCategory)
        console.log("branchId",this.state.branchId)
        console.log("subCategories",this.state.subCategories)
    }

    _toTitleCase(str) { 
        if(str.length > 16) {
            var res = str.substring(0, 16);
            return res.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            )+"";
        } else {
            return str.replace(
              /\w\S*/g,
              function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
              }
            );
        }
    }
  
    componentDidMount = async() => {
        await this.getCategorywiseForVirtualLibrary(this.state.mainCategory, this.state.branchId, this.state.subCategories);
        setTimeout( 
            () => { this.setState({ refreshing: false });
        },2000);
    };

    getCategorywiseForVirtualLibrary = (mainCategory, branchId, subCategories) => {
      const getVirtualBook = API.API_ESAPI_POINT+"/getCategorywiseForVirtualLibrary?subCategories="+subCategories+"&branchId="+branchId+"&page="+this.state.page+"&mainCategory="+mainCategory;
      console.log(getVirtualBook);
      axios.get(getVirtualBook)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    getVirtualBook : response.data.successObject,
                    refreshing: false
                })
            } else {
                this.setState({ refreshing: false })
            }
        })
        .catch((error) => {
          console.log(error);
          showMessage({
              message: "Virtual Library",
              description: 'something went wrong! please try again later!',
              icon: "info",
              type: "info",
          });
          setTimeout( () => { 
              this.props.navigation.navigate("BranchSearch");
          },2000);
        });
    }

    _bookDetailsNavigate = async(title_id) => {
        this.props.navigation.push("BookDetails",{ title_id: title_id });
    }

    _allBooksNavigate = async() => {
        this.props.navigation.navigate("AllBooks");
    }
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <View style={styles.activityHomeClass}>
                            <ScrollView>
                                <View style={{ marginBottom: 5}}>
                                    <View style={styles.virtualResult}>
                                        {/* <Appbar.Header style={{backgroundColor: '#E8E8E8',elevation:0, margin: 0, height: 36}}>
                                            <Appbar.Content title="Non-Ficition" titleStyle={styles.headingStyle} style={styles.heading} />
                                            <Appbar.Content title="More..." titleStyle={styles.titleStyle} style={styles.SeeAllHeading} onPress={()=>{this._allBooksNavigate()}} />
                                        </Appbar.Header> */}
                                        {this.state.getVirtualBook.length > 0 && <View style={styles.cardView}>
                                            {this.state.getVirtualBook.map((item)=>{
                                                return (
                                                    <Card key={item.jb_info.titleid} style={styles.cardSection}  onPress={()=>{this._bookDetailsNavigate(item.jb_info.titleid)}}>
                                                        {(item.image_url != null && item.image_url != '') ? <Card.Cover style={styles.coverImage} source={{ uri: item.image_url }} />
                                                        : <View style={styles.NonImage}><Card.Cover style={styles.NonImageCoverImage} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} />
                                                            <Card.Content style={styles.NonImageCardText}>
                                                                <Title style={styles.NonImageTitle}>{this._toTitleCase(item.jb_info.title)}</Title>
                                                            </Card.Content>
                                                        </View>}
                                                    </Card>
                                                )
                                                })}
                                        </View>}
                                        {((Object.keys(this.state.getVirtualBook).length == 0 )) && (
                                        <View class={styles.shareContainer}>
                                            <View style={{ alignSelf: "center" }}>
                                                <View style={{
                                                    marginTop: 60,
                                                    width: 220,
                                                    height: 220,
                                                    borderRadius: 100,
                                                    overflow: "hidden"}}>
                                                <Image source={require("../../assets/no-data.png")} style={{
                                                    flex: 1,
                                                    height: 250,
                                                    width: undefined}} ></Image>
                                                </View>
                                            </View>
                                        </View>
                                        )}
                                    </View>
                                </View>
                            </ScrollView>
                        </View>
                        <SearchTab navigation={this.props.navigation} />
                        <BottomTab navigation={this.props.navigation} />
                    </View>
                }
            </View>
        );
    }

}

export default VirtualLibraryResults;