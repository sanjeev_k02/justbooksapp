import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        // marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },
    listHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 100, 
        marginTop: 0, 
        paddingLeft: 15,
    },

    SectionStyle: {
    //   flex: 1,
      flexDirection: 'row-reverse',
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1,
      borderColor: '#565555',
      height: 50,
      borderRadius: 100,
      // padding: 20,
      // paddingRight: 10,
      shadowColor: 'grey',
      shadowOffset: {
          width: 0,
          height: 1
      },
      marginBottom: 10,
      marginRight: 10,
      shadowOpacity: 0.5,
      shadowRadius: 1,
      backgroundColor: '#FFFFFF',
    },
    searchTextInput: {
      width: 250, 
      color: '#2E2E2E',
      backgroundColor: '#FFFFFF'
    },
    divider : {
        margin: 10,
    },
    cardStyle : {
        backgroundColor: '#39403c', 
        borderRadius: 6
    },
    titleStyle: {
        color : '#CB8223'
    },
    subtitleStyle: {
        color : '#fff'
    },
    rightStyle : {
        backgroundColor: '#39403c', 
        borderBottomRightRadius: 6, 
        borderTopRightRadius: 6, 
        height: 70, 
        paddingTop: 8
    },
    iconColor : {
        backgroundColor: '#273C96'
    }
    ,button: {
        width: 50,
        height: 50,
      },
      container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        marginTop:25
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      action: {
        width: '100%',
        textAlign: 'center',
        color: 'white',
        paddingVertical: 8,
        marginVertical: 5,
        fontWeight: '600',
      },
      instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
      },
      stat: {
        textAlign: 'center',
        color: '#B0171F',
        marginBottom: 1,
        marginTop: 30,
      },
      action : {
        color:'#2E2E2E',
        margin: 10,
        textAlign: 'center'
      },
    headingSearchView: {
      marginTop: 0,
      marginBottom: 0,
      marginRight: 10,
      marginLeft: -10
    },
    branchView: {
      marginTop: 0,
      marginBottom: 0,
      marginRight: 0,
      marginLeft: 5
    },
    searchHeading: {
        color: '#fff',
        fontSize: 24,
        fontWeight: '600',
        fontFamily: 'Montserrat-Bold'
    },
    headingSearchStyle : {
        fontSize: 24, 
        fontWeight: '600',
        fontFamily: 'Montserrat-Bold'
    },
    item: {
      paddingBottom: 5,
      marginVertical: 2,
      marginHorizontal: 6,
    },
    title: {
      fontSize: 20,
      fontWeight: '600',
      fontFamily: 'Montserrat-Regular'
    },
    contain: {
      display: 'flex',
      flex: 1,
      marginTop: 0,
      marginBottom: 0
  },
  activityClass: {
    display: 'flex',
    flex: 1,
    marginTop: 0,
    paddingBottom: 60
  },
  activitySearchClass: {
    display: 'flex',
    flex: 1,
    marginTop: 0,
    paddingBottom: 120
  },
  activityBranchClass: {
    display: 'flex',
    flex: 1,
    marginTop: 0,
    paddingBottom: 100
  }, 
  activityHomeClass: {
    display: 'flex',
    flex: 1,
    marginTop: 0,
    paddingBottom: 0
  },
  headingView: {
      marginTop: 0,
      marginBottom: 0,
      marginRight: 10,
      marginLeft: 10
  },
  heading: {
      color: '#fff',
      fontSize: 14,
      fontWeight: '600'
  },
  SeeAllHeading: {
      color: '#CB8223',
      fontSize: 14,
      fontWeight: '600',
      textAlign: 'right',
      alignContent: 'flex-end',
      alignContent: 'flex-end'
  },
  titleStyle : {
      fontSize: 14, 
      textAlign: 'right',
      alignContent: 'flex-end',
      alignContent: 'flex-end',
      color : '#CB8223'
  },
  headingStyle : {
      fontSize: 16, 
      fontWeight: '600'
  },
  cardSliderView: { 
      display: "flex", 
      flexDirection: "row", 
      flexWrap: "wrap", 
      alignContent: "center", 
      alignItems: "center", 
      justifyContent: "center",
      marginLeft: 10,
      marginTop: 20
  },
  cardSliderSection: {
      width: 250,
      height: 140,
      margin: 10,
      // backgroundColor: '#101211'
  },
  coverSliderImage: {
      height: 140,
  },
  cardView: { 
      flex: 1,
      display: "flex", 
      flexDirection: "row", 
      flexWrap: "wrap", 
      alignContent: "flex-start", 
      alignItems: "flex-start", 
      justifyContent: "flex-start",
      // marginLeft: 15
  },
  cardSection: {
      height: 150,
      width: '30%', // is 50% of container width
      padding: 0,
      margin: 5
  },
  sliderView: {
      marginTop: 35
  },
  coverImage: {
      height: 150, 
      resizeMode: 'stretch',
      borderRadius: 2
  },
  // title: {
  //     width: '100%',
  //     fontSize: 16,
  //     lineHeight: 15,
  //     paddingTop: 5,
  //     marginBottom: 0,
  //     fontWeight: '600',
  //     color: '#273C96'
  // },
  Paragraph: {
      marginTop: 0,
      width: '100%',
      fontSize: 12,
      color: '#273C96'
  },
  newArrivles: {
      marginBottom: 15,
      backgroundColor: '#E8E8E8',
      margin: 10
  },
  virtualResult: {
      marginBottom: 15,
      // backgroundColor: '#E8E8E8',
      margin: 10
  },
  latestBook: {
      marginBottom: 15,
  },
  headingBranchSearchView: {
    marginTop: 0,
    marginBottom: 0,
    marginRight: 10,
    marginLeft: -10 ,
    paddingLeft: 15,
  },
  headingBranchSearchStyle : {
      fontSize: 20, 
      width: 250,
      fontWeight: '600',
      fontFamily: 'Montserrat-Bold'
  },
  subItem: {
    width: '50%', // is 50% of container width
    // margin: 2,
    padding: 8,
    height: undefined
  },
  cardDesign: {
      backgroundColor: '#FFFFFF',
      height: 50
  },
  titleDesciption : {
      textAlign: 'center',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      textAlignVertical: "center"
  },
  category: {
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-start' // if you want to fill rows left to right
  },
  imageCard: {
    marginLeft: 10,
    width: 25, 
    height: 25,
    resizeMode: 'stretch',
    borderRadius: 2
  },
  NonImage: {
      position: 'absolute',
      flex:1,
      left: 0,
      right: 0,
      top: 0,
      alignItems:'center',
  },
  NonImageCoverImage: {
      position: 'absolute',
      flex:1,
      left: 0,
      right: 0,
      top: 0,
      height: 150,
      borderRadius: 2
  },
  NonImageCardText: {
      textAlign: 'center'
  },
  NonImageTitle: {
      width: 100,
      fontSize: 17,
      lineHeight: 17,
      marginTop: 15,
      fontWeight: '600',
      color: '#CB8223',
      textAlign: 'center'
  },
  NonImageParagraph: {
      marginTop: 5,
      width: 100,
      fontSize: 14,
      color: '#CB8223',
      textAlign: 'center',
      lineHeight: 14
  },
  primusImage: {
    width: 110, 
    height: 40,
 },
 primusTag: {
    width: 100, 
    height: 40, 
    position: 'absolute', 
    bottom: 5,
    left: 0
 }

});
