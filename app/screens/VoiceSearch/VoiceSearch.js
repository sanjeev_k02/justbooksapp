import React from 'react';
import { View, Text, StatusBar,
    StyleSheet,
    SafeAreaView,
    Image,
    TouchableHighlight,
    PermissionsAndroid,
    TextInput
  } from 'react-native';

import Icons from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import { Avatar, Divider, Card, IconButton } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Voice from 'react-native-voice';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';

class VoiceSearch extends React.Component {
    state = {
        pitch: '',
        error: '',
        end: '',
        started: '',
        results: [],
        partialResults: [],
        searchText: ''
    };
    
    constructor(props) {
        super(props);
        //Setting callbacks for the process status
        Voice.onSpeechStart = this.onSpeechStart;
        Voice.onSpeechEnd = this.onSpeechEnd;
        Voice.onSpeechError = this.onSpeechError;
        Voice.onSpeechResults = this.onSpeechResults;
        Voice.onSpeechPartialResults = this.onSpeechPartialResults;
        Voice.onSpeechVolumeChanged = this.onSpeechVolumeChanged;
    }

    componentDidMount = async () => {
        try {
          let granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: "Justbooks needs audio Permission",
              message:
                "Justbook access to your Voice " +
                "so you can take awesome Search.",
              buttonNeutral: "Ask Me Later",
              buttonNegative: "Cancel",
              buttonPositive: "OK"
            }
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the voice search");
          } else {
            console.log("voice permission denied");
          }
        } catch (err) {
          console.warn(err);
        }
    };
    
    onSpeechStart = e => {
        //Invoked when .start() is called without error
        console.log('onSpeechStart: ', e);
        this.setState({
            started: '√',
        });
    };

    onSpeechEnd = e => {
        //Invoked when SpeechRecognizer stops recognition
        console.log('onSpeechEnd: ', e);
        this.setState({
            end: '√',
            searchText: e.value
        });
    };

    onSpeechError = e => {
        //Invoked when an error occurs. 
        console.log('onSpeechError: ', e);
        this.setState({
            error: JSON.stringify(e.error),
        });
    };

    onSpeechResults = e => {
        //Invoked when SpeechRecognizer is finished recognizing
        console.log('onSpeechResults: ', e);
        this.setState({
            results: e.value,
            searchText: e.value[0]
        });
    };

    onSpeechPartialResults = e => {
        //Invoked when any results are computed
        console.log('onSpeechPartialResults: ', e);
        this.setState({
            partialResults: e.value,
        });
    };

    onSpeechVolumeChanged = e => {
        //Invoked when pitch that is recognized changed
        console.log('onSpeechVolumeChanged: ', e);
        this.setState({
            pitch: e.value,
        });
    };

    _startRecognizing = async () => {
        //Starts listening for speech for a specific locale
        this.setState({
            pitch: '',
            error: '',
            started: '',
            results: [],
            partialResults: [],
            end: '',
        });

        try {
            await Voice.start('en-US');
        } catch (e) {
            //eslint-disable-next-line
            console.error(e);
        }
    };

    _stopRecognizing = async () => {
        //Stops listening for speech
        try {
            await Voice.stop();
        } catch (e) {
            //eslint-disable-next-line
            console.error(e);
        }
    };

    _cancelRecognizing = async () => {
        //Cancels the speech recognition
        try {
            await Voice.cancel();
        } catch (e) {
            //eslint-disable-next-line
            console.error(e);
        }
    };

    _destroyRecognizer = async () => {
        //Destroys the current SpeechRecognizer instance
        try {
            await Voice.destroy();
        } catch (e) {
            //eslint-disable-next-line
            console.error(e);
        }
        this.setState({
            pitch: '',
            error: '',
            started: '',
            results: [],
            partialResults: [],
            end: '',
        });
    };
      
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                <ScrollView>
                    <View style={{ marginBottom: 5 ,marginTop: 10}}>
                        <View style={styles.cardHomeView}>

                            <View style={styles.SectionStyle}>
                                <Icons name="mic-sharp" size={24} 
                                    onPress={this._startRecognizing}
                                    color="#2E2E2E" 
                                    style={{ marginRight: 30 }} 
                                />
                                <TextInput 
                                label= "search"
                                style={styles.searchTextInput}
                                underlineColor= "#2E2E2E"
                                placeholderTextColor = "#2E2E2E"
                                placeholder= "looking for book..."
                                mode= "outlined"
                                value={this.state.searchText}
                                onChangeText = {(text) => {
                                    this.setState({searchText : text})
                                }} />
                                <Icons name="search" size={24} color="#2E2E2E" style={{ marginRight: 10, marginLeft: 20 }} />
                            </View>

                            <View style={styles.container}>
                                <Text style={styles.welcome}>
                                    Example of Speech to Text conversion / Voice Recognition
                                </Text>
                                <Text style={styles.instructions}>
                                    Press mike to start Recognition
                                </Text>
                                <View
                                    style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    paddingVertical: 10,
                                    }}>
                                    <Text
                                    style={{
                                        flex: 1,
                                        textAlign: 'center',
                                        color: '#B0171F',
                                    }}>{`Started: ${this.state.started}`}</Text>
                                    <Text
                                    style={{
                                        flex: 1,
                                        textAlign: 'center',
                                        color: '#B0171F',
                                    }}>{`End: ${this.state.end}`}</Text>
                                </View>
                                <View
                                    style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    paddingVertical: 10,
                                    }}>
                                    <Text
                                    style={{
                                        flex: 1,
                                        textAlign: 'center',
                                        color: '#B0171F',
                                    }}>{`Pitch \n ${this.state.pitch}`}</Text>
                                    <Text
                                    style={{
                                        flex: 1,
                                        textAlign: 'center',
                                        color: '#B0171F',
                                    }}>{`Error \n ${this.state.error}`}</Text>
                                </View>
                                <TouchableHighlight
                                    onPress={this._startRecognizing}
                                    style={{ marginVertical: 20 }}>
                                    <Image
                                    style={styles.button}
                                    source={{
                                        uri:
                                        'https://raw.githubusercontent.com/AboutReact/sampleresource/master/microphone.png',
                                    }}
                                    />
                                </TouchableHighlight>
                                <Text
                                    style={{
                                    textAlign: 'center',
                                    color: '#B0171F',
                                    marginBottom: 1,
                                    fontWeight: '700',
                                    }}>
                                    Partial Results
                                </Text>
                                <ScrollView>
                                    {this.state.partialResults.map((result, index) => {
                                    return (
                                        <Text
                                        key={`partial-result-${index}`}
                                        style={{
                                            textAlign: 'center',
                                            color: '#B0171F',
                                            marginBottom: 1,
                                            fontWeight: '700',
                                        }}>
                                        {result}
                                        </Text>
                                    );
                                    })}
                                </ScrollView>
                                <Text style={styles.stat}>Results</Text>
                                <ScrollView style={{ marginBottom: 42 }}>
                                    {this.state.results.map((result, index) => {
                                        return (
                                            <Text key={`result-${index}`} style={styles.stat}>
                                            {result}
                                            </Text>
                                        );
                                    })}
                                </ScrollView>
                                <View
                                    style={{
                                    flexDirection: 'row',
                                    alignItems: 'space-between',
                                    position: 'absolute',
                                    bottom: 0,
                                    }}>
                                    <TouchableHighlight
                                        onPress={this._stopRecognizing}
                                        style={{ flex: 1, backgroundColor: '#F1F1F1' }}>
                                        <Text style={styles.action}>Stop</Text>
                                    </TouchableHighlight>
                                    <TouchableHighlight
                                        onPress={this._cancelRecognizing}
                                        style={{ flex: 1, backgroundColor: '#F1F1F1' }}>
                                        <Text style={styles.action}>Cancel</Text>
                                    </TouchableHighlight>
                                    <TouchableHighlight
                                        onPress={this._destroyRecognizer}
                                        style={{ flex: 1, backgroundColor: '#F1F1F1' }}>
                                        <Text style={styles.action}>Destroy</Text>
                                    </TouchableHighlight>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <BottomTab navigation={this.props.navigation} />
            </View>
        );
    }

}

export default VoiceSearch;