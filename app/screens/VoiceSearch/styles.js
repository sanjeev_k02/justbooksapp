import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1,
        paddingBottom: 60
    },
    cardHomeView: {
        backgroundColor: '#FFFFFF', 
        shadowColor: 'grey', 
        shadowOffset: { width: 0, height: 2 }, 
        shadowOpacity: 0.5,
        shadowRadius: 2, 
        elevation: 0, 
        marginBottom: 15, 
        marginTop: 0, 
        padding: 15,
    },

    SectionStyle: {
      flex: 1,
      flexDirection: 'row-reverse',
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1,
      borderColor: '#565555',
      height: 50,
      borderRadius: 4,
      // padding: 20,
      paddingRight: 10,
      shadowColor: 'grey',
      shadowOffset: {
          width: 0,
          height: 1
      },
      marginBottom: 10,
      shadowOpacity: 0.5,
      shadowRadius: 1,
      backgroundColor: '#FFFFFF',
    },
    searchTextInput: {
      width: 250, 
      color: '#2E2E2E',
      backgroundColor: '#FFFFFF'
    },
    divider : {
        margin: 10,
    },
    cardStyle : {
        backgroundColor: '#39403c', 
        borderRadius: 6
    },
    titleStyle: {
        color : '#fff'
    },
    subtitleStyle: {
        color : '#fff'
    },
    rightStyle : {
        backgroundColor: '#39403c', 
        borderBottomRightRadius: 6, 
        borderTopRightRadius: 6, 
        height: 70, 
        paddingTop: 8
    },
    iconColor : {
        backgroundColor: '#273C96'
    }
    ,button: {
        width: 50,
        height: 50,
      },
      container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        marginTop:25
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      action: {
        width: '100%',
        textAlign: 'center',
        color: 'white',
        paddingVertical: 8,
        marginVertical: 5,
        fontWeight: '600',
      },
      instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
      },
      stat: {
        textAlign: 'center',
        color: '#B0171F',
        marginBottom: 1,
        marginTop: 30,
      },
      action : {
        color:'#2E2E2E',
        margin: 10,
        textAlign: 'center'
      }

});
