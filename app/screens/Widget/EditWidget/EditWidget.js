import React from 'react';
import { View, Text, StatusBar } from 'react-native';
import styles from './styles';
import { TextInput, Appbar, Divider, RadioButton, Button, Card, Title, Paragraph, Headline, ActivityIndicator, Colors  } from 'react-native-paper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import DatePicker from 'react-native-datepicker';
import MultiSelect from 'react-native-multiple-select';

import StatusTopBar from '../../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../../components/BottomTab/BottomTab';
import { showMessage } from "react-native-flash-message";
import AsyncStorage from '@react-native-community/async-storage';
import API from '../../../env';
import * as axios from 'axios';

class EditWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subscription_id: 0,
            memberCard: 0,
            widgetId: this.props.route.params.widgetId,
            widgetName: this.props.route.params.widgetName,
            widgetNameError: false,
            author : [],
            selectedAuthors: this.props.route.params.authorCsv,
            series : [],
            selectedSeries: this.props.route.params.seriesCsv,
            geners : [],
            selectedGeners: this.props.route.params.categoryCsv,
            refreshing: true,
        }
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        // Selected Value 
        if(this.state.selectedAuthors != undefined || this.state.selectedAuthors != '') {
            let result= this.state.selectedAuthors.split(",").map((e)=>parseInt(e));
            this.setState({
                selectedAuthors: result
            })
        }
        if(this.state.selectedSeries != undefined || this.state.selectedSeries != '') {
            let result=this.state.selectedSeries.split(",").map((e)=>parseInt(e));
            this.setState({
                selectedSeries: result
            })
        }
        if(this.state.selectedGeners != undefined || this.state.selectedGeners != '') {
            let result=this.state.selectedGeners.split(",").map((e)=>parseInt(e));
            this.setState({
                selectedGeners: result
            })
        }

        await this._getAllAuthorData();
        await this._getAllSeriesData();
        await this._getAllGenersData();
        setTimeout(() => {
            this.setState({ refreshing: false });
        }, 2000);
    };

    _toTitleCase(str) { 
        return str.replace(
            /\w\S*/g,
            function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }
    
    _getAllAuthorData = () => {
        const getAuthor = API.API_ESAPI_POINT+"/getPopularAuthors";
        console.log(getAuthor);
        axios.get(getAuthor)
          .then((response) => {
          //   console.log(response.data.successObject.content);
              if (response.data.status && response.data.successObject.content.length > 0) {
                  var authorData = [];
                  response.data.successObject.content.map((item)=>{
                    authorData.push({
                        "id": item.author_id, 
                        "name": this._toTitleCase(item.author_name)
                      });
                  })
                  this.setState({
                      author : authorData
                  })
              } else {
                  this.setState({
                    author: []
                  })
              }
          })
          .catch((error) => {
            console.log(error);
          });
    }
    
    _getAllSeriesData = () => {
        const getSeries = API.API_JUSTBOOK_ENDPOINT+"/getMetadataToCreateUserWidget";
        console.log(getSeries);
        axios.get(getSeries, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
          .then((response) => {
          //   console.log(response.data.successObject.Series);
              if (response.data.status && response.data.successObject.Series.length > 0) {
                  var seriesData = [];
                  response.data.successObject.Series.map((item)=>{
                    seriesData.push({
                        "id": item.id, 
                        "name": this._toTitleCase(item.name)
                      });
                  })
                  this.setState({
                      series : seriesData
                  })
              } else {
                  this.setState({
                    series: []
                  })
              }
          })
          .catch((error) => {
            console.log(error);
          });
    }

    _getAllGenersData = () => {
      const getGeners = API.API_JUSTBOOK_ENDPOINT+"/getAllGenres";
      console.log(getGeners);
      axios.get(getGeners)
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                var genersData = [];
                  response.data.successObject.map((item)=>{
                    genersData.push({
                        "id": item.id, 
                        "name": this._toTitleCase(item.name)
                      });
                  })
                  this.setState({
                      geners : genersData
                  })
            } else {
                this.setState({
                    geners: []
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _updateUserWidget = async() => {
        if (this.state.selectedAuthors.length < 1 && this.state.selectedSeries.length < 1 && this.state.selectedGeners.length < 1 ) {
            showMessage({
                message: "Update User Widget",
                description: 'Please select atleast one author, series or genres',
                icon: "danger",
                type: "danger",
            });
            return;
        }

        if (this.state.widgetName === '') {
            this.setState({ 
                widgetNameError : true,
                errorMsg: 'Wedgit Name is required Field!'
            })
            return;
        }
    
        this._updateUserWidgetRequest();
    }

    _updateUserWidgetRequest() {

        const addUserWedgit = API.API_JUSTBOOK_ENDPOINT + "/insertOrUpdateUserWidget";
        axios.post(addUserWedgit ,{
            "widgetId" : this.state.widgetId,
            "authorCsv" : this.state.selectedAuthors+"",
            "seriesCsv" : this.state.selectedSeries+"",
            "genreCsv" : this.state.selectedGeners+"",
            "widgetName" : this.state.widgetName
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response add Widget", response.data);
            if (response.data.status) {
                showMessage({
                    message: "Update User Widget",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                setTimeout(() => {
                    this.props.navigation.push("ShowWidget");
                },2000);
            } else {
                showMessage({
                    message: "Update User Widget",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Update User Widget",
                description: "Something went wrong, Please try again later!",
                icon: "danger",
                type: "danger",
            });
        });
    }

    onSelectedAuthorsChange = selectedAuthors => {
        this.setState({ selectedAuthors });
    };

    onSelectedSeriesChange = selectedSeries => {
        this.setState({ selectedSeries });
    };

    onSelectedGenersChange = selectedGeners => {
        this.setState({ selectedGeners });
    };
    
    render() {
        return (
            <View style={styles.contain}>
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    :
                    <View style={styles.activityClass}>
                        <ScrollView>
                            <View style={{ marginBottom: 5 ,marginTop: 10}}>
                                <View style={styles.cardHomeView}>
                                    <Text style={styles.titleDesciption}>
                                        <Headline style={styles.titleStyle}>Widget Details</Headline>{'\n'}
                                        <Text style={{color: 'green', fontSize: 16 }}>
                                            <Paragraph style={styles.subtitleStyle}>Choose Top Authors, Series and Category to ensure your Custom Widget to display on Homepage</Paragraph>
                                        </Text>{'\n'}
                                    </Text>
                                    <View style={styles.form}>
                                        <MultiSelect
                                            items={this.state.author}
                                            uniqueKey="id"
                                            ref={(component) => { this.multiSelect = component }}
                                            onSelectedItemsChange={(text)=>this.onSelectedAuthorsChange(text)}
                                            selectedItems={this.state.selectedAuthors}
                                            selectText="Select Best Authors"
                                            searchInputPlaceholderText="Search Authors..."
                                            altFontFamily="Montserrat-ExtraBold"
                                            tagRemoveIconColor="#273C96"
                                            tagBorderColor="#273C96"
                                            tagTextColor="#273C96"
                                            selectedItemTextColor="#273C96"
                                            selectedItemIconColor="#273C96"
                                            itemTextColor="#000000"
                                            displayKey="name"
                                            styleDropdownMenuSubsection={styles.dropdownStyle}
                                            styleListContainer={{height: 250}}
                                            styleDropdownMenu={styles.textMultiInput}
                                            searchInputStyle={{ color: '#273C96' }}
                                            style={styles.textMultiInput}
                                            underlineColor={'#BEBEBE'}
                                            selectionColor={'#BEBEBE'}
                                            labelStyle={{color: '#BEBEBE'}}
                                            placeholderTextColor={'#BEBEBE'}
                                            theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                            submitButtonColor="#273C96"
                                            submitButtonText="Submit"
                                            hideSubmitButton={true}
                                        />


                                        <MultiSelect
                                            items={this.state.series}
                                            uniqueKey="id"
                                            ref={(component) => { this.multiSelect = component }}
                                            onSelectedItemsChange={(text)=>this.onSelectedSeriesChange(text)}
                                            selectedItems={this.state.selectedSeries}
                                            selectText="Select Series"
                                            searchInputPlaceholderText="Search Series..."
                                            altFontFamily="Montserrat-ExtraBold"
                                            tagRemoveIconColor="#273C96"
                                            tagBorderColor="#273C96"
                                            tagTextColor="#273C96"
                                            selectedItemTextColor="#273C96"
                                            selectedItemIconColor="#273C96"
                                            itemTextColor="#000000"
                                            displayKey="name"
                                            styleDropdownMenuSubsection={styles.dropdownStyle}
                                            styleListContainer={{height: 250}}
                                            styleDropdownMenu={styles.textMultiInput}
                                            searchInputStyle={{ color: '#273C96' }}
                                            style={styles.textMultiInput}
                                            underlineColor={'#BEBEBE'}
                                            selectionColor={'#BEBEBE'}
                                            labelStyle={{color: '#BEBEBE'}}
                                            placeholderTextColor={'#BEBEBE'}
                                            theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                            submitButtonColor="#273C96"
                                            submitButtonText="Submit"
                                            hideSubmitButton={true}
                                        />


                                        <MultiSelect
                                            items={this.state.geners}
                                            uniqueKey="id"
                                            ref={(component) => { this.multiSelect = component }}
                                            onSelectedItemsChange={(text)=>this.onSelectedGenersChange(text)}
                                            selectedItems={this.state.selectedGeners}
                                            selectText="Select Geners"
                                            searchInputPlaceholderText="Search Geners..."
                                            altFontFamily="Montserrat-ExtraBold"
                                            tagRemoveIconColor="#273C96"
                                            tagBorderColor="#273C96"
                                            tagTextColor="#273C96"
                                            selectedItemTextColor="#273C96"
                                            selectedItemIconColor="#273C96"
                                            itemTextColor="#000000"
                                            displayKey="name"
                                            styleDropdownMenuSubsection={styles.dropdownStyle}
                                            styleListContainer={{height: 250}}
                                            styleDropdownMenu={styles.textMultiInput}
                                            searchInputStyle={{ color: '#273C96' }}
                                            style={styles.textMultiInput}
                                            underlineColor={'#BEBEBE'}
                                            selectionColor={'#BEBEBE'}
                                            labelStyle={{color: '#BEBEBE'}}
                                            placeholderTextColor={'#BEBEBE'}
                                            theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                            submitButtonColor="#273C96"
                                            submitButtonText="Submit"
                                            hideSubmitButton={true}
                                        />

                                        {/* <Paragraph style={styles.label}> Enter Name </Paragraph> */}
                                        <TextInput
                                            placeholder="Enter Widget Name"
                                            value={this.state.widgetName}
                                            style={styles.textInput}
                                            underlineColor={'#BEBEBE'}
                                            selectionColor={'#BEBEBE'}
                                            labelStyle={{color: '#BEBEBE'}}
                                            placeholderTextColor={'#BEBEBE'}
                                            theme={{ colors: { text: '#000000', label: '#BEBEBE' } }}
                                            onChangeText={text => this.setState({widgetName : text, widgetNameError : false})}
                                        />
                                        {this.state.widgetNameError && <Paragraph style={styles.colorError}> {this.state.errorMsg} </Paragraph>}

                                        <Text style={{color: 'green', fontSize: 16 }}>{'\n'}</Text>
                                        <Button icon="tooltip-edit" mode="contained" labelStyle={styles.labelStyle} style={styles.btn} onPress={() => this._updateUserWidget()}>
                                            Update Widget
                                        </Button>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                        <BottomTab navigation={this.props.navigation} />
                    </View>
                }
            </View>
        );
    }

}

export default EditWidget;