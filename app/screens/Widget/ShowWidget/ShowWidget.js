import React from 'react';
import { View, Text, Image, Alert } from 'react-native';
import styles from './styles';
import { Avatar, DataTable, Appbar, List, Divider, Button,  ActivityIndicator, Colors } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import StatusTopBar from '../../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../../components/BottomTab/BottomTab';
import AwesomeAlert from 'react-native-awesome-alerts';
import { showMessage } from "react-native-flash-message";
import API from '../../../env';
import * as axios from 'axios';

class ShowWidget extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            subscription_id: 0,
            memberCard: 0,
            branchId: 0,
            primusMappedBranches: 0,
            cityId: 0,
            primusMember: false,
            page: 1, 
            pageSize: 5, 
            refreshing: true,
            customWidget: {},
            customWidgetFlag: false,
            showAlert: false,
            actionPerform: '',
            widgetId: 0,
        }
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user);
                // console.log("us",userData);
                this.setState({
                    subscription_id: userData.login.subscriptionId,
                    memberCard: userData.login.memberCard,
                    branchId: userData.login.branchId,
                    primusMappedBranches: userData.login.primusMappedBranches,
                    cityId: userData.login.cityId,
                    primusMember: userData.login.primus
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }
        this.getUserWidgetData(this.state.subscription_id);
    };



    getUserWidgetData = (subscriptionId) => {
        let getMember = API.API_JUSTBOOK_ENDPOINT+"/getUserWidgetData?subscriptionId="+subscriptionId+"&pageNumber="+this.state.page+"&pageSize="+this.state.pageSize;
        console.log(getMember);
        axios.get(getMember, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
          .then((response) => {
            // console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    customWidget : response.data.successObject,
                    refreshing: false,
                    customWidgetFlag: true
                })
            } else {
                this.setState({
                    refreshing: false,
                    customWidgetFlag: true,
                    errorMsg: response.data.errorDescription
                });
            }
        })
        .catch((error) => {
            console.log(error);
            this.setState({
                refreshing: false,
                customWidgetFlag: true,
            });
        });
    }

    _switchMemberAction = async(widgetId) => {
        if(this.state.subscription_id == 0) {
            showMessage({
                message: "User Widget",
                description: 'Become a member to add User Widget!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            this.setState({ 
                showAlert: true, 
                actionPerform: 'switch',
                widgetId: widgetId 
            });
            
        }
    }

    _moveUserWidgetActionPreform = (widgetId) => {
        this.setState({ showAlert: false  });
        this.props.navigation.push("Home");
    }

    _editUserWidget = (widgetId, widgetName, authorCsv, seriesCsv, categoryCsv) => {
        this.props.navigation.push("EditWidget",{
            widgetId: widgetId,
            widgetName: widgetName,
            authorCsv: authorCsv,
            seriesCsv: seriesCsv,
            categoryCsv: categoryCsv
        })
    }

    _deleteUserWidgetAction = async(widgetId) => {
        if(this.state.subscription_id == 0) {
            showMessage({
                message: "User Widget",
                description: 'Become a member to preform Action on Widget!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            this.setState({ 
                showAlert: true, 
                actionPerform: 'delete',
                widgetId: widgetId 
            });
            
        }
    }

    _deleteUserWidgetActionPreform = (widgetId) => {
        this.setState({ showAlert: false  });
        let deleteWidget = API.API_JUSTBOOK_ENDPOINT+"/deleteUserWidget";
        console.log(deleteWidget);
        axios.get(deleteWidget,{
            "widgetId": widgetId
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
          .then((response) => {
            console.log(response.data);
            if (response.data.status) {
                showMessage({
                    message: "User Widget",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                setTimeout(() => {
                    this.getUserWidgetData(this.state.subscription_id);
                },2000);
            } else {
                showMessage({
                    message: "User Widget",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch((error) => {
            console.log(error);
            showMessage({
                message: "User Widget",
                description: 'Something went wrong, please try again later!!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _alertActionPreform =(action, widgetId) =>{
        if(action == 'delete') {
            this._deleteUserWidgetActionPreform(widgetId);
        } else if( action == 'switch') {
            this._moveUserWidgetActionPreform(widgetId);
        }

    }
     
    hideAlert = () => {
        this.setState({
          showAlert: false
        });
    };
    
    render() {
        return (
            <View style={styles.contain}>
                <AwesomeAlert
                    show={this.state.showAlert}
                    showProgress={false}
                    icon={'alert'}
                    iconColor={'#AB3247'}
                    title="JustBooks"
                    message={this.state.actionPerform == 'delete' ? "Do you want to delete user Widget!" : "Do you want to switch user Widget!"}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText="No, cancel"
                    confirmText={this.state.actionPerform == 'delete' ? "Yes, delete it" : "Yes, Move it"}
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this.hideAlert();
                    }}
                    onConfirmPressed={() => {
                        this._alertActionPreform(this.state.actionPerform, this.state.widgetId);
                    }}
                />
                <StatusTopBar />
                { (this.state.refreshing) ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <View style={styles.mainContainer}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <View style={styles.primusCard}>
                                    {this.state.customWidgetFlag && <View style={styles.newArrivles}>
                                        <View style={styles.cardView}>
                                            <View style={{ marginBottom: 5 ,marginTop: 0}}>
                                                <View style={styles.cardHomeView}>
                                                    {(Object.keys(this.state.customWidget).length > 0 ) && (
                                                        <View style={styles.wrapper}>
                                                            <Appbar.Header style={{backgroundColor: '#FFFFFF', elevation: 0}}>
                                                                <Button icon="tooltip-edit" mode="outline" labelStyle={styles.btnText} style={styles.btn} onPress={() => this.props.navigation.navigate('AddWidget')}>
                                                                    Add User Widget
                                                                </Button>
                                                            </Appbar.Header>
                                                        </View>
                                                    )}
                                                    {(Object.keys(this.state.customWidget).length > 0 ) && (
                                                        <View style={styles.flatList}>
                                                            {this.state.customWidget.map((item) => {
                                                                return (
                                                                    <View key={item.widgetId} style={{backgroundColor : '#F1F1F1', marginTop: 10}}>
                                                                        <List.Item
                                                                            title={item.widgetName ? item.widgetName : 'Unknown'}
                                                                            description={"Widget #"+item.widgetId}
                                                                            titleEllipsizeMode={'tail'}
                                                                            titleNumberOfLines={4}
                                                                            titleStyle={{color:'#2E2E2E', fontSize: 18, fontWeight: 'bold', marginRight: 20}}
                                                                            descriptionStyle={{color:'#2E2E2E'}}
                                                                            right={props => (
                                                                            <View style={styles.memberCard}>
                                                                                <View style={styles.actionStyle}>
                                                                                    <View style={styles.footer}>
                                                                                        <TouchableOpacity style={styles.bottomButtons} onPress={()=>{this._switchMemberAction(item.widgetId)}} >
                                                                                            <Icons name="view-grid" size={20} color={'#000000'} />
                                                                                            <Text style={styles.footerText}>Switch</Text>
                                                                                        </TouchableOpacity>
                                                                                        
                                                                                        <TouchableOpacity style={styles.bottomButtons} onPress={()=>{this._editUserWidget(item.widgetId, item.widgetName, item.authorCsv, item.seriesCsv, item.categoryCsv)}} >
                                                                                            <Icons name="shield-edit" size={20} color={'#000000'} />
                                                                                            <Text style={styles.footerText}>Edit</Text>
                                                                                        </TouchableOpacity>
                                            
                                                                                        <TouchableOpacity style={styles.bottomButtons} onPress={()=>{this._deleteUserWidgetAction(item.widgetId)}} >
                                                                                            <Icons name="delete" size={20} color={'#AB3247'} />
                                                                                            <Text style={[styles.footerText,{color: '#AB3247'}]}>Delete</Text>
                                                                                        </TouchableOpacity>
                                                                                    </View>
                                                                                </View>
                                                                            </View>)
                                                                            }
                                                                        />
                                                                    </View> 
                                                                )
                                                            })}
                                                        </View>
                                                    )}

                                                    {((Object.keys(this.state.customWidget).length == 0 )) && (
                                                    <View class={styles.shareContainer}>
                                                        <View style={{ alignSelf: "center" }}>
                                                            <View style={{
                                                                marginTop: 60,
                                                                width: 220,
                                                                height: 220,
                                                                borderRadius: 100,
                                                                overflow: "hidden"}}>
                                                            <Image source={require("../../../assets/no-data.png")} style={{
                                                                flex: 1,
                                                                height: 250,
                                                                width: undefined}} ></Image>
                                                            </View>
                                                            <View style={styles.wrapper}>
                                                                <Text style={styles.buttonDesc}>
                                                                    <Button icon="tooltip-edit" mode="outline" labelStyle={styles.btnText} style={styles.btn} onPress={() => this.props.navigation.navigate('AddWidget')}>
                                                                        Add User Widget
                                                                    </Button>{'\n'}
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                    )}
                                                </View>
                                            </View>
                                        </View>
                                    </View>}
                                    
                                </View>
                            </ScrollView>
                        </View>
                        <BottomTab navigation={this.props.navigation} routeName="home" />
                    </View>
                }
            </View>
        );
    }

}

export default ShowWidget;