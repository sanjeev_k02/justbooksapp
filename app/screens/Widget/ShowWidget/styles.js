import { StyleSheet ,Dimensions } from "react-native";


export default StyleSheet.create({
	contain: {
        display: 'flex',
        flex: 1
    },
    activityClass: {
        display: 'flex',
        flex: 1,
        paddingBottom: 60
    },
    divider : {
        margin: 0,
        color:'#fff',
        backgroundColor: '#fff'
    },
    primusCard: {
        marginTop: 0,
        marginBottom: 20,
        backgroundColor: '#FFFFFF',
        borderRadius: 4
    },
    mainContainer: {
        paddingLeft: 20,
        paddingRight: 20,
        // paddingBottom: 20,
        // marginBottom: 40,
        flexDirection: "column"
    },
    wrapper: {
        alignItems: "center",
        marginTop: 10,
        marginBottom: 0
    },
    btn: {
        height: 40, 
        textAlign: 'center',
        marginTop: 5,
        backgroundColor: '#273C96'
    },
    titleStyle : {
        fontSize: 16, 
        textAlign: 'right',
        alignContent: 'flex-end',
        alignContent: 'flex-end'
    },
    btnText : {
        fontSize: 16, 
        color: '#fff', 
        textTransform: 'none',
    },
    footer: {
        // position: 'absolute',
        flex:1,
        left: 0,
        right: 0,
        bottom: 0,
        flexDirection:'row',
        height:60,
        alignItems:'center',
        elevation:25,
      },
      bottomButtons: {
        alignItems:'center',
        justifyContent: 'space-around',
        flex:1,
        marginTop:9,
        marginRight: 10
      },
      footerText: {
        color:'#2E2E2E',
        fontWeight:'600',
        alignItems:'center',
        fontSize:10,
        lineHeight:20,
        marginBottom:12
      },
});
