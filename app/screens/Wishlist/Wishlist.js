import React from 'react';
import { View, Share, Alert, RefreshControl, Text, Image } from 'react-native';
import { Title, Paragraph, Divider,  ActivityIndicator, Colors } from 'react-native-paper';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { showMessage, hideMessage } from "react-native-flash-message";
import AwesomeAlert from 'react-native-awesome-alerts';
import API from '../../env';
import * as axios from 'axios';

import StatusTopBar from '../../components/StatusTopBar/StatusTopBar';
import BottomTab from '../../components/BottomTab/BottomTab';
import SearchTab from '../../components/SearchTab/SearchTab';

class Wishlist extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            subscription_id: 0,
            primusMember: false,
            webUser : false,
            wishlistBook: {},
            refreshing: true,
            isData: false,
            removeWishlistAlert: false,
            removeWishlistAction: '', 
            removeWishlistTitleId: 0,
            isWebUserAlert: false,
            rentNowAlert: false, 
            webUserAlert: false, 
            rentNowTitleId: 0 
        }
    }

    _toTitleCase(str) {
        return str.replace(
          /\w\S*/g,
          function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          }
        );
    }
  
    componentDidMount = async() => {
        try {
            const user = await AsyncStorage.getItem('userData');
            if (user !== null) {
                const userData = JSON.parse(user)
                this.setState({
                    // refreshing: false,
                    subscription_id: userData.login.subscriptionId,
                    primusMember: userData.login.primus,
                    webUser: userData.webUser
                })
            } else {
                console.log("subscription Not Found");
            }
        } catch (error) {
            console.log("error", error)
        }

        await this.getWishlistBookDetails(this.state.subscription_id);
        // setTimeout(() => {
        //     this.setState({ refreshing: false });
        // },2000);
    };

    getWishlistBookDetails = async(subscriptionId) => {
      const getWishlistBook = API.API_JUSTBOOK_ENDPOINT+"/getWishlistForSubscription?subscription_id="+subscriptionId;
      console.log(getWishlistBook);
      await axios.get(getWishlistBook, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
        //   console.log(response.data.successObject);
            if (response.data.status && response.data.successObject.length > 0) {
                this.setState({
                    wishlistBook : response.data.successObject,
                    refreshing: false,
                    isData: true
                })
            } else {
                this.setState({
                    refreshing: false,
                    isData: false
                })
            }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    _bookDetailsNavigate = async(titleId) => {
        this.props.navigation.push("BookDetails",{ title_id: titleId });
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        setTimeout(() => {
          this.setState({refreshing: false});
        }, 2000);
    }

    _shareBookDetails = async(title) => {
        Share.share({
            message: "I found A " + title + " on JustBooks and thought you might find it interesting! Sign up today to read ",
          })
        //after successful share return result
        .then((result) => console.log(result))
        //If any thing goes wrong it comes here
        .catch((errorMsg) => console.log(errorMsg));
    }


    _wishlistAction = async(action, titleId) => {
        console.log("cancel")
        if(this.state.subscription_id == 0) {
            console.log("Become a member to rent books!!")
            showMessage({
                message: "Wishlist",
                description: 'Become a member to wishlist books!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            this.setState({ 
                removeWishlistAlert: true, 
                rentNowAlert: false, 
                removeWishlistAction: action, 
                removeWishlistTitleId: titleId 
            });    
        }
    }

    _wishlistActionPreform = async(action, titleId) => {
        this.setState({ rentNowAlert: false, removeWishlistAlert: false   });
        let wishlistAddRemove;
        if(action == 'add') {
            wishlistAddRemove = API.API_JUSTBOOK_ENDPOINT + "/addToWishlist";
        } else {
            wishlistAddRemove = API.API_JUSTBOOK_ENDPOINT + "/removeFromWishlist";
        }
        axios.post(wishlistAddRemove ,{
            "titleId":titleId, 
            "createdAt":810, 
            "createdBy": 1000, 
            "subscriptionId": this.state.subscription_id
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("response update profile", response.data);
            if (response.data.status) {
                this.setState({ 
                    // refreshing: true,
                    wishlistId: (action == 'add') ? response.data.successObject.wishlistId : 0,
                    errorMsg: (action == 'add') ? response.data.successObject.message : response.data.successObject,
                });
                showMessage({
                    message: "Wishlist",
                    description: (action == 'add') ? response.data.successObject.message : response.data.successObject,
                    icon: "success",
                    type: "success",
                });
                this.getWishlistBookDetails(this.state.subscription_id);
                setTimeout(() => {
                    this.setState({ refreshing: false});
                },2000);
            } else {
                showMessage({
                    message: "Wishlist",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Wishlist",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }

    _rentNowAction = async(isPrimusBook, titleId) => {
        console.log("rent")
        if(this.state.subscription_id == 0) {
            showMessage({
                message: "Rent Now",
                description: 'Become a member to wishlist books!!',
                icon: "danger",
                type: "danger",
            });
        } else {
            if(this.state.webUser) {
                if(isPrimusBook || isPrimusBook == 'true') {
                    this.setState({
                        rentNowAlert: true, 
                        webUserAlert: false,
                        removeWishlistAlert: false,
                        rentNowTitleId: titleId
                    });
                } else {
                    this._rentActionPerform('DOOR_DELIVERY', false, this.state.rentNowTitleId);
                }
            }  else {
                if(isPrimusBook || isPrimusBook == 'true') {
                    this.setState({
                        isWebUserAlert: true,
                        rentNowAlert: true, 
                        webUserAlert: false,
                        removeWishlistAlert: false,
                        rentNowTitleId: titleId
                    });
                } else {
                    this.setState({
                        webUserAlert: true,
                        rentNowAlert: false,
                        removeWishlistAlert: false,
                        rentNowTitleId: titleId 
                    });
                }
            }
        }
    }

    _rentActionPerform = (orderType, isPrimusOrder, titleId) => {
        this.setState({ rentNowAlert: false, webUserAlert: false, removeWishlistAlert: false,});
        let rentNow  = API.API_JUSTBOOK_ENDPOINT + "/insertOrUpdateBookRequest";
        axios.post(rentNow ,{
            "createdAt": 810,
            "createdBy": 1000,
            "subscriptionId": this.state.subscription_id,
            "titleId": titleId,
            "bookReqType": 'DELIVERY',
            "reqSource": 'WEBSITE',
            "status": 'NEW',
            "deliveryType": orderType,
            "requestedBranchId":this.state.branchId ? this.state.branchId : 1008,
            "titleIsMagazine" : false,
            "primusOrder":isPrimusOrder
        }, { headers: { 'X-SECRET-TOKEN': 'qwerty' } })
        .then((response) => {
            console.log("rent now", response.data);
            if (response.data.status) {
                this.setState({ 
                    // refreshing: true,
                    errorMsg: response.data.successObject,
                });
                showMessage({
                    message: "Rent",
                    description: response.data.successObject,
                    icon: "success",
                    type: "success",
                });
            } else {
                showMessage({
                    message: "Rent",
                    description: response.data.errorDescription,
                    icon: "danger",
                    type: "danger",
                });
            }
        })
        .catch(function (error) {
            console.log(error);
            showMessage({
                message: "Rent",
                description: 'Something went wrong, Please try again later!',
                icon: "danger",
                type: "danger",
            });
        });
    }
     
    hideWishlistAlert = () => {
        this.setState({
            rentNowAlert: false, 
            removeWishlistAlert: false 
        });
    };
    
    render() {
        return (
            <View style={styles.contain}>
                {/* rent Now */}
                <AwesomeAlert
                    show={this.state.rentNowAlert}
                    showProgress={false}
                    icon={'cart-arrow-right'}
                    iconColor={'#AB3247'}
                    title="JustBooks"
                    message="Hey, Please select Order Type!"
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelButtonColor="green"
                    cancelText="Normal Order"
                    confirmText="Primus Order"
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        if(this.state.isWebUserAlert) {
                            this.setState({
                                rentNowAlert: false,
                                webUserAlert: true,
                            })
                        } else {
                            this._rentActionPerform('DOOR_DELIVERY', false, this.state.rentNowTitleId);
                        }
                    }}
                    onConfirmPressed={() => {
                        this._rentActionPerform('DOOR_DELIVERY', true, this.state.rentNowTitleId)
                    }}
                />

                <AwesomeAlert
                    show={this.state.webUserAlert}
                    showProgress={false}
                    icon={'cart-arrow-right'}
                    iconColor={'#AB3247'}
                    title="JustBooks"
                    message="Pickup from Branch or Home Deliver?!"
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelButtonColor="green"
                    cancelText="Home Delivery"
                    confirmText="Store Delivery"
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this._rentActionPerform('DOOR_DELIVERY', false, this.state.rentNowTitleId)
                    }}
                    onConfirmPressed={() => {
                        this._rentActionPerform('BRANCH_DELIVERY', false, this.state.rentNowTitleId)
                    }}
                />

                {/* wishlist */}
                <AwesomeAlert
                    show={this.state.removeWishlistAlert}
                    showProgress={false}
                    icon={'alert'}
                    iconColor={'#AB3247'}
                    title="JustBooks"
                    message="Do you want to remove book from wishlist!"
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText="No, cancel"
                    confirmText="Yes, remove it"
                    confirmButtonColor="#DD6B55"
                    onCancelPressed={() => {
                        this.hideWishlistAlert();
                    }}
                    onConfirmPressed={() => {
                        this._wishlistActionPreform(this.state.removeWishlistAction, this.state.removeWishlistTitleId);
                    }}
                />
                <StatusTopBar />
                { this.state.refreshing ?
                    <ActivityIndicator style={{flex : 1}} size={25} animating={true} color={Colors.blue700} />
                    : 
                    <View style={styles.activityClass}>
                        <ScrollView 
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                            }>
                            <View style={styles.containBody}>
                                {(this.state.wishlistBook.length > 1 && this.state.isData) ? <View style={styles.cardHomeView}>
                                    {this.state.wishlistBook.map((item) => {
                                        return (
                                            <View key={item.titleId}>
                                                <View style={styles.card} >
                                                    <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}}>
                                                    {item.titleImage != "images/noimage.jpg" ? 
                                                        <Image style={styles.imageCard} source={{ uri: item.titleImage }} ></Image>
                                                        : 
                                                        <Image style={styles.imageCard} source={{ uri: "https://justbooks.in/images/ThumbnailNoCover.png" }} ></Image>
                                                    }
                                                    </TouchableOpacity>
                                                    <View style={styles.cardDescription}>
                                                        <TouchableOpacity onPress={()=>{this._bookDetailsNavigate(item.titleId)}}>
                                                            <Text style={styles.titleDesciption}>
                                                                <Title style={styles.color}>{this._toTitleCase(item.titleName)}</Title>{'\n'}
                                                                {(item.authors[0].authorName != "" && item.authors[0].authorName != null) && <Text style={{color: 'green', fontSize: 16 }}
                                                                    >By : 
                                                                    <Paragraph style={styles.colorParagraph}> {this._toTitleCase(item.authors[0].authorName)}</Paragraph>
                                                                </Text>}{'\n'}
                                                                {/* {item.totalRented != "" && <Text style={{color: 'green', fontSize: 16 }}>
                                                                    <Paragraph style={styles.colorParagraph}>No of times read : {item.totalRented}</Paragraph>
                                                                </Text>} */}
                                                            </Text>
                                                        </TouchableOpacity>
                                                        <View style={styles.actionStyle}>
                                                            <View style={styles.footer}>
                                                                <TouchableOpacity style={styles.bottomButtons} onPress={()=>{this._rentNowAction(item.primus, this.state.title_id)}} >
                                                                    <Icons name="book-outline" size={26} color={'#000000'} />
                                                                    <Text style={styles.footerText}>Rent</Text>
                                                                </TouchableOpacity>

                                                                <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._shareBookDetails(this._toTitleCase(item.titleName))} >
                                                                    <Icons name="share-variant" size={26} color={'#000000'} />
                                                                    <Text style={styles.footerText}>Share</Text>
                                                                </TouchableOpacity>

                                                                <TouchableOpacity style={styles.bottomButtons} onPress={()=>this._wishlistAction('remove',item.titleId)} >
                                                                    <Icons name="delete" size={26} color={'#AB3247'} />
                                                                    <Text style={[styles.footerText,{color: '#AB3247'}]}>Remove</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                                <Divider style={styles.divider} />
                                            </View>
                                        )
                                    })}
                                </View>
                                :
                                <View class={styles.shareContainer}>
                                    {/* <View style={styles.titleHeader}>
                                        <Text style={{color: '#000'}}>You don't have currently Reading Book</Text>
                                    </View> */}
                                    <View style={{ alignSelf: "center" }}>
                                        <View style={{
                                            marginTop: 60,
                                            width: 220,
                                            height: 220,
                                            borderRadius: 100,
                                            overflow: "hidden"}}>
                                        <Image source={require("../../assets/no-data.png")} style={{
                                            flex: 1,
                                            height: 250,
                                            width: undefined}} ></Image>
                                        </View>
                                    </View>
                                </View>}
                            </View>
                        </ScrollView>
                        <SearchTab navigation={this.props.navigation} />
                        <BottomTab navigation={this.props.navigation} routeName="wishlist"/>
                    </View>
                }
            </View>
        );
    }

}

export default Wishlist;